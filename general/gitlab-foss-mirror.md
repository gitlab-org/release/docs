---
tags:
  - release-management
---
# GitLab FOSS mirror

GitLab CE is built using the project
[gitlab-foss](https://gitlab.com/gitlab-org/gitlab-foss), which is a read-only
mirror of [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab).

Changes from the main GitLab repository are periodically mirrored to the GitLab
FOSS project. For this we use a project called [Merge
Train](https://ops.gitlab.net/gitlab-org/merge-train). This project ensures that
when we mirror changes to GitLab FOSS, we don't include any proprietary code. At
a high level, this basically works as follows:

1. Clone gitlab-org/gitlab and gitlab-org/gitlab-foss
1. `rm -rf` everything in gitlab-foss, except for the `.git` directory
1. Copy everything from gitlab to gitlab-foss
1. `rm -rf` several EE specific files and folders, such as the `ee/` directory
1. Restore the gitlab-foss VERSION file, so it contains the correct CE version
1. Commit and push the changes to gitlab-foss

This allows us to mirror changes to GitLab FOSS/CE, without having to worry
about any merge conflicts. A limitation of this setup is that we're unable to
partially mirror files, or partially restore CE files. For example, we can't
mirror a CI config file to GitLab FOSS and only restore part of it. As such,
files that use different logic for CE and EE must themselves make sure the right
logic is used for the right GitLab distribution.
