---
tags:
  - release-management
---
# Fixing Branch Divergence

:warning: Keep in mind your local git config may change the behavior of the
commands noted below.  Use this as a guide as both your local configuration and
situation may differ.  :warning:

## Problem Statement

When checking the status of the mirrors we'll occasionally diverge one or more
branches when a release is nearing completing.  This document will cover the
details necessary to fix said divergence.

### Intro

All of this work happens locally, it is expected that the Release Manager has
all the remotes configured, example:

```
% git remote --verbose
dev     git@dev.gitlab.org:gitlab/gitlab-ee.git (fetch)
dev     git@dev.gitlab.org:gitlab/gitlab-ee.git (push)
origin  git@gitlab.com:gitlab-org/gitlab.git (fetch)
origin  git@gitlab.com:gitlab-org/gitlab.git (push)
security        git@gitlab.com:gitlab-org/security/gitlab.git (fetch)
security        git@gitlab.com:gitlab-org/security/gitlab.git (push)
```

Release Tools utilizes the terminology Canonical, Security, and Build.  The
above translates to:

* dev == Build
* origin == Canonical
* security == Security

### Overview

1. We'll first update our local repository with all remotes to ensure we are in
   sync
1. We then discover which branches need to be updated and formulate the plan
1. We'll perform the necessary updates
1. We then push to all remotes

## Procedure

1. Update our local repository

   ```
   git fetch --all
   ```

1. Determine which remotes are out of sync

   ```
   git log -n 1 origin/master
   git log -n 1 dev/master
   git log -n 1 security/master
   ```

  Sometimes it's helpful to look in the Commits Web UI for each remote as it's
  easier to parse the output and look into the history.

1. Normally we'll see that both Build and Security are ahead of and behind
   Canonical. This is because Build and Security are utilized during our
   processes of a security release which is where we grab our commits from and
   perform our tagging procedures, therefore, Build and Security will usually be at
   the same `ref`.  We'll use this as our example for the below.  To fix this
   situation, we'll first bring Build and Security up-to-date with Canonical.

   ```
   git checkout master # Assuming we are tracking `origin` here
   git pull origin master

   # Create a new branch as to not interfere with any existing branches
   git checkout -b security-master security/master

   git merge master
   # enter fun message about why we're merging
   # Example: Merging Canonical into Security due to branch divergence

   # Let's push our now sync'd remote Security's master
   git push security security-master:master

   # ---
   # Now let's catch up Canonical with where it lags behind our Build and Security
   git checkout master
   git merge security-master

   # If origin got updated in this short amount of time we'll need to pull
   # Example: git pull origin master

   git fetch origin
   git rebase origin/master
   git push origin master
   ```

1. Now either wait or force a mirror update to happen and run the chatops command
