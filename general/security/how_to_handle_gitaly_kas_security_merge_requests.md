---
tags:
  - auto-deploy
---
# How to handle security merge requests from Gitaly and KAS

Security issues from Gitaly and KAS are rare and not automatically embedded in the patch
release process. The process to include them is composed of two sections:

1. The security fix prepared on the Gitaly or KAS security repository and,
1. The merge request that bumps the version file on the [GitLab Security repository].
   * [GITALY_SERVER_VERSION]
   * [GITLAB_KAS_VERSION]

The first part is prepared by the stage team (Gitaly or KAS), the second part requires manual intervention
by Release Managers, this last part aims to be automated by
[performing the Gitaly and KAS update task during the Patch Release].

Once the Gitaly or KAS security merge requests are ready:

1. Merge the security merge request targeting `master`.
1. Disable the automatic Gitaly KAS update task.
   * In [release-tools' pipeline schedules page]
   * Take over the ownership and deactivate the schedule with the description `components:update_managed_versioning_projects`
   * Note that regardless of its name, this schedule is used for both Gitaly and KAS
1. Bump the `GITALY_VERSION` or the `GITLAB_KAS_VERSION` on the [GitLab Security repository] by using the merge sha
from the security merge request (see examples: [Gitaly example], [KAS example])
1. Prior to merging the backports, ensure the merge request that bumps the version has been
deployed to GitLab.com.

After the patch release is published, the Gitaly and KAS update task should be re-enabled automatically.

[GITALY_SERVER_VERSION]: https://gitlab.com/gitlab-org/security/gitlab/-/blob/master/GITALY_SERVER_VERSION?ref_type=heads
[GITLAB_KAS_VERSION]: https://gitlab.com/gitlab-org/security/gitlab/-/blob/master/GITLAB_KAS_VERSION?ref_type=heads
[GitLab Security repository]: https://gitlab.com/gitlab-org/security/gitlab
[performing the Gitaly and KAS update task during the Patch Release]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1236
[release-tools' pipeline schedules page]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[Gitaly example]: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2176
[KAS example]: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4063
