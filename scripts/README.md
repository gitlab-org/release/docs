# Auto-generate Table of Content Script

- [Auto-generate Table of Content Script](#auto-generate-table-of-content-script)
  - [Introduction](#introduction)
  - [Usage](#usage)
    - [Example](#example)

## Introduction

In some directories like [runbook](../runbooks/), there are many readme files, and we use a [`README.md`](../runbooks/README.md) file to define a Table of Content (ToC), so it is easier to search through.

The [`generate-toc-file.sh`](./generate-toc-file.sh) script makes it easier to keep the ToC up to date with the list of documents by automatically generating the ToC based on the title and the name of each file.

## Usage

The script accepts two input arguments:

- Relative/Absolute path to the document directory.
- Title of the `README.md` file (i.e. the first line in the file with the `#` prefix).

### Example

```bash
bash scripts/generate-toc-file.sh runbooks/ "Runbooks"
```

The above command will read through the [runbook](../runbooks/) directory, generate the ToC file and print it out to stdout.
Optionally, you can redirect the output to a [`README.md`](../runbooks/README.md) file:

```bash
bash scripts/generate-toc-file.sh runbooks/ "Runbooks" > runbooks/README.md
```
