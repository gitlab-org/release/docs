---
tags:
  - auto-deploy
---
# Deployment SLO dashboard

[Deployment SLO] is a subcomponent of [Mean Time to Production (MTTP)][MTTP]
that measures the deployment frequency and duration by tracking the percentage
of deployment pipelines that complete within a target duration. Deployment
duration is measured as the elapsed time between the start of a deploy to
Staging through to its deployment to Production.

A [Deployment SLO dashboard] is used to track this metric. The dashboard includes:

* The **Number of deployments** performed within the timeframe (30 days by default).
* The **Target SLO** that we're striving to achieve.
* The **Apdex score** representing the percentage of deployment pipelines completed within the target.
* The **Deployment duration** to visualize the historical deployment durations within the timeframe.
* The **Apdex score** to plot the percentage of deployments falling below the target threshold.

Data for the dashboard is populated using coordinated pipelines:

1. Just before a deployment to Staging starts, the [start time] is recorded.
2. After a deployment to Production completes, the [end time] is recorded, and two metrics are registered:
  * A gauge metric to record the total duration.
  * A histogram metric to categorize the duration in different time buckets, ranging from 3.5h to 10h.
3. These metrics are then pushed to the [release-tools pushgateway].

### Delivery metrics

This metric is referenced on the [Delivery metrics page], since Grafana dashboards can't be embedded
for security reasons, we use an automated process to generate screenshots of the Deployment SLO Apdex:

1. A [pipeline schedule] is executed every day at 7:00UTC.
1. This pipeline executes a [bash script] that requests a rendered image of the Deployment SLO and stores it as an artifact.
1. The handbook uses the URL artifact to render the image.

[Deployment SLO]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/461
[MTTP]: https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#mean-time-to-production-mttp
[Deployment SLO Dashboard]: https://dashboards.gitlab.net/d/delivery-deployment_slo/delivery-deployment-slo?orgId=1&refresh=5m
[start time]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/auto_deploy.rake#L108-116
[end time]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/auto_deploy.rake#L119-123
[release-tools pushgateway]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/512
[Delivery metrics page]: https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/metrics.html
[pipeline schedule]: https://gitlab.com/gitlab-org/release-tools/-/pipeline_schedules/159674/edit
[bash script]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/scripts/fetch-deployment-slo-apdex.sh
