---
tags:
  - release-management
---
# Pro tips

This is a collection of tips that previous [Release Managers](release-manager.md)
have found to be helpful during their reign. They are _suggestions_, not
requirements. Feel free to contribute your own!

## Use Git aliases

### Add a `git stab` and a `git stabee` aliases

As a release manager, you will have to checkout the latest stable branches quite
often. As you'll be doing this a lot, it's helpful to have
[Git aliases][git-alias] to cut down on typing.

```ini
# ~/.gitconfig
[alias]
  plff = pull --ff-only
  # Check out the latest `X-Y-stable` branch
  stab   = "!f() { br=`git branch --list [0-9]*-[0-9]*-stable | tail -1 | tr -d ' ' | sed 's/*//'`; git checkout $br && git plff; }; f"
  # Check out the latest `X-Y-stable-ee` branch
  stabee = "!f() { br=`git branch --list [0-9]*-[0-9]*-stable-ee | tail -1 | tr -d ' ' | sed 's/*//'`; git checkout $br && git plff; }; f"
```

```sh
$ git stabee
Switched to branch '8-6-stable-ee'
```

### Add a `git pdo` alias

As a release manager, you will have to push to multiple remotes quite often. As
you'll be doing this a lot, it's helpful to have [Git aliases][git-alias] to cut
down on typing. In the following alias we are using `hub` as explained in the
[Push to multiple remotes](push-to-multiple-remotes.md) doc and we assume that:

- the dev.gitlab.org is called `dev`
- the GitLab.com remote is called `origin`

```ini
# ~/.gitconfig
[alias]
  pdo  = push dev,origin
```

```sh
$ git pdo
Total 0 (delta 0), reused 0 (delta 0)
To git@dev.gitlab.org:gitlab/gitlabhq.git
   c87adc4..63c8a05  8-6-stable -> 8-6-stable
Everything up-to-date
```

### Add `git cherry-pick` aliases

As merge requests get accepted into the default branch, you're responsible for making sure
they make it into the appropriate `stable` branch. Currently the preferred way
of doing this is via [Git cherry picks] of the merge commit. As you'll be doing
this a lot, it's helpful to have a [Git alias][git-alias] to cut down on typing.

```ini
# ~/.gitconfig
[alias]
  cp  = cherry-pick
  cpm = cherry-pick -m 1
```

```sh
$ git cp <COMMIT SHA>
$ git cpm <MERGE COMMIT SHA>
```

[git-alias]: https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases
[Git cherry picks]: https://git-scm.com/docs/git-cherry-pick

## Use a clipboard history or text expander app

So now that you're constantly [adding notes to yourself], you might find
yourself typing the same things over and over. Unacceptable!

> I use the [Clipboard History and Snippets](https://www.alfredapp.com/help/features/clipboard/)
> feature from Alfred on OS X so that adding something like "Picked into
> `8-4-stable`" is as simple as hitting <kbd>⌥⌘C</kbd>, typing "picked", and
> hitting <kbd>Enter</kbd>.
>
> -- @rspeicher

> I use [TextExpander](https://smilesoftware.com/TextExpander) on OS X so that
> adding something like "Picked into `8-4-stable`" is as simple as typing `pi`
> (or `piee` for "Picked into `8-4-stable-ee`"), and hitting <kbd>Tab</kbd>.
>
> -- @rymai

[adding notes to yourself]: #leave-notes-to-yourself

## Keep `#f_release_x_x` in Slack updated

Use the `#f_release_x_x` channel for all release specific status updates and
ChatOps commands. The only exception to this is security releases, which should
use the `#releases` channel.

Check previous messages to see how other release managers are communicating the release status.

If something looks useful, copy it!
One example is the following Slack snippet for build status:

```
Current Status 9.3.3
Gitlab.com
CE :green: - `https://gitlab.com/gitlab-org/gitlab-ce/commits/9-3-stable`
EE :running: - `https://gitlab.com/gitlab-org/gitlab-ee/commits/9-3-stable-ee`
Omnibus CE :green: - `https://gitlab.com/gitlab-org/omnibus-gitlab/commits/9-3-stable`
Omnibus EE :red: :repeat: - `https://gitlab.com/gitlab-org/omnibus-gitlab/commits/9-3-stable-ee`

Dev
CE :hourglass: - Awaiting MRs to merge before sync
EE :hourglass: - Awaiting MRs to merge before sync
Omnibus CE :running: - `https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/9-3-stable`
Omnibus EE :running: - `https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/9-3-stable-ee`
```


Having a detailed write up helps to:

- Maintain a single source of truth about the current state of the release
- Keep your fellow Release Managers updated
- Give a nice summary about what's done and what remains

---

[Return to Guides](../README.md#guides)
