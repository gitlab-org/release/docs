---
tags:
  - auto-deploy
---

# FAQ

## No Green Build

In situations where we are waiting for a green build, we have two options,
[notifying people](#notify-others) as necessary and wait, or we can find a
recent green and perform a bit of manual work to get things in sync.  The latter
does not guarantee we'll still end up with a green build in the long run.  The
former may take the longest time to achieve but will result in greater
confidence that we have a good build to pick from.

## Notify Others

Communication is imperative in this role.  We must be able to overly communicate
and express direct intention as our responsibility to perform a release is our
top priority.  Utilize the following set of guidelines towards discovering where
issues lies and to whom we ask questions.

### General Assistance

When needing help for situations where you need an engineers assistance, such as
merge conflicts, try to identify the people that worked on that commit and ping
them directly in one of the following channels:

* `#development`
* `#backend`
* `#frontend`

### Failed Builds

In the `#development` slack channel we have alerts when builds fail.  This is a
great place to start and starting to ask questions.  It's helpful if you can
find a targeted commit that might have started the red build and ping persons
directly associated with that commit.

### Backports for prior versions

There can exist situations where a fix needs to be introduced to an older
version of GitLab, but we cannot include that fix for newer versions of the
product.  In situations like this, the cherry-picking process will not work.
Engineers should create an MR targeting that version's stable branch, and when a
release is ready to start for the next patch, the release manager should
validate that this fix is then merged into the stable branch.  At that point,
the release manager can follow our existing [patch process].

### Items that are backported

Normally only security items are backported up to three minor versions.
Sometimes if a regression is serious enough, we'll backport these to X versions
as we deem necessary.  Additional details can be found in our [maintenance
policy]

### Deploy has Failed

Dedicated documentation exists to one troubleshoot and provide a path forward
located [in our deploy docs](./deploy/failures.md)

[patch process]: ./patch/process.md
[maintenance policy]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/policy/maintenance.md
