---
tags:
  - auto-deploy
---
# How to deploy an old package

There is a guard in place to prevent an older package from being deployed
and replacing a newer package.

This is part of the `auto_deploy:check_package` job.

If for any reason this job needs to be overridden and an older package
needs to be deployed:

1. Disable the `stop_deployment_if_old_package` feature flag on <https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags>
1. Retry the failing `check_package` job.
1. Keep the feature flag disabled until the package has been deployed
all the way to production, otherwise it will fail again in the next environment.
1. Re-enable the `stop_deployment_if_old_package` feature flag.

## Reasons you may want to deploy an old package

If you have a pure revert picked into an auto-deploy branch, when the two
commits are compared, this job will see the deployment will result in no
changes, triggering a failure.

An example:

- Failing job: <https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/10721040>
- Revert that was picked: <https://gitlab.com/gitlab-org/gitlab/-/merge_requests/127781>
- Failure message:
   ```sh
   2023-07-27 20:52:56.693634 F Rake::Task -- Task failed -- Exception: ReleaseTools::Tasks::AutoDeploy::CheckPackage::PackageCommitOlderThanLatestError: This package (16.3.202307271920-00ef80423c2.4e669aa5e3e) uses commit 'fa92c356b543dcf1f40cc890e52d7b13c2c0f78a' for 'gitlab-org/security/gitlab-shell',
   and the latest commit deployed on gstg-cny is 'a3a5036bf2e8b18163eae6974ff1a31b0eaa8ed0'. The comparison between
   these two commits returns no commits, which could mean the commit being deployed is older
   than the one already on gstg-cny. Stopping deployment!
   ```
