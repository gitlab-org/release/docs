---
tags:
  - auto-deploy
---
# Overview

Post-deployment patches (commonly called Hot Patches) make it possible for SREs
to apply changes outside of the normal release cycles. These changes bypass the
normal packaging and release cycle for expediency, when there is community
impact on GitLab.com or a security vulnerability that needs immediate attention.

Note: **Every post-deployment patch must have an associated S1 production incident**

Patches do not live past a release on GitLab.com without an exception
granted by the Delivery group.

Patches are NOT preserved in the next omnibus upgrade.  It is required that what
was patched was incorporated into the next release so that they receive the same
level of testing and go through the normal release pipeline.

Patches can only be issued for the Rails codebase. It is not easy to issue a
patch for frontend code.

Patches are automatically applied to staging canary, and then manually gated
for the other environments: staging, production-canary, and production. They
can be applied in any order, but typically you should apply them in order:
staging-canary, production-canary, staging, production. Ensure to try and keep
staging and production in sync (either apply to both, or don't apply to both).

The following components do _not_ support post deployment patches:

* Container Registry
* Front-end code or assets
* GitLab Shell
* Gitaly
* NGINX
* Postgresql
* Praefect
* Redis
* Workhorse

## Submitting a patch

The [video](https://www.youtube.com/watch?v=mliUSFcParg) walks through creating a post-deployment patch using the hotpatch Chatops command

Patches are initiated by the proposing backend team. There are two projects that are configured as mirrors for post-deployment patches:

1. <https://ops.gitlab.net/gitlab-com/engineering/patcher>: This project is where patches are created for patching GitLab.com
1. <https://ops.gitlab.net/gitlab-com/gl-infra/patcher>: This is a push mirror of [engineering/patcher](https://ops.gitlab.net/gitlab-com/engineering/patcher) where pipelines run for applying patches.

### Backend Developer

1. Ensure that an [incident issue](https://gitlab.com/gitlab-com/gl-infra/production/issues/new?issuable_template=incident) exists, if not engage the oncall on Slack using `@sre-oncall` to create one.
1. Initiate a hot patch by issuing the following Chatops command where `incident number` is the production incident number, ex: 1234.

    ```text
    /chatops run hotpatch --incident <incident number>
    ```

      * The only required option is `--incident`, in the unlikely situation where GitLab.com is down, any number can be used here as a placeholder.
      * The `--package` option can be used to override the package version, if GitLab.com is in a state where the version is not being reported correctly to Chef.

1. Engage the SRE on call/RM that you are going to hotpatch production
1. Drain canary using: `/chatops run canary --disable --production`
1. Lock production canary to prevent deployments: `/chatops run deploy lock gprd-cny`

1. Some links are provided in the output of the command to help prep the patch. Use the MR link provided to prep the patch in the [engineering/patcher](https://ops.gitlab.net/gitlab-com/engineering/patcher) project
1. Obtain the desired SHA revision number by clicking the `patch directory` for the relative environment.
    ![patch-direcotory](https://gitlab.com/gitlab-com/gl-infra/infrastructure/uploads/19ce07c6dab30fdc22776f0cd8358185/hotpatch-directory.png)
1. Create a working branch in [gitlab](https://gitlab.com/gitlab-org/gitlab-ee/) from the current SHA running on production (run `/chatops run auto_deploy status` to find out what this is or see the version from the output of Chatops hotpatch command as describe in (4.).
    * For example, you can do `git checkout -b patch/my-fix e8d93625538` if you want
     to derive your branch from `e8d93625538` revision based on the sha.
1. Make your code changes
    * You can cherry-pick commits.
1. Run the command `git --no-pager diff --color=never e8d93625538..  -- . ':!spec' ':!ee/spec' > path/to/patch.patch`

    * **Note**: this is an example - if you have changed non-spec files in other directories, be sure to include those

1. Copy the patch file to corresponding placeholder directory for the environment that is being patched on the branch created by the Chatops hotpatch command earlier.
  Example, to patch release `16.2.202307142100-e8d93625538.2dcd7514ec3` copy the patch file(s) into <https://ops.gitlab.net/gitlab-com/engineering/patcher/-/tree/master/patches/16.2.202307142100-e8d93625538.2dcd7514ec3>
1. If there are already patch files for the release, simply add them to the existing directory, the patcher tool will make sure all patches are applied.
1. Patches are applied in sorted order. To specify the order prefix the patch with a sort key such as:
    * `001-patch-for-issue-1.patch`
    * `002-patch-for-issue-2.patch`
1. When the merge request is ready inform the `@sre-oncall` in Slack and in the merge request to review and merge the patch.

### OnCall SRE or Release Manager

* An SRE or Release Manager will merge the MR
* To monitor the process of the patch deployment see the [pipeline view of the patcher repository](https://ops.gitlab.net/gitlab-com/gl-infra/patcher/-/pipelines).
* When the patch is deployed to staging-canary and verified, deploy to production
  canary using the `gprd-cny-prepare` manual task. When deployed and verified
  in production canary, deploy to staging and production using the `gstg-prepare`
  and `gprd-prepare` jobs respectively.

## Rolling back a patch

* If a patch needs to be rolled back rename the patch file to have a `.rollback` extension.
  For example if the patch file is named `patches/11.5.1-ee.0/profiles_helper.patch` rename
  the file on a branch to `patches/11.5.1-ee.0/profiles_helper.patch.rollback` with the same
  content.
* After the rollback file is created, contact a member of the delivery team to
  apply the rollback through the patcher pipeline.

## Monthly Release Hot Patching Production Practice

We have introduced hot patching practice as part of the [monthly release template](https://gitlab.com/gitlab-org/release-tools/-/blob/e6be520a1f8add5e990affd0baa6d1fae7f24577/templates/monthly.md.erb#L38-46).

Similar to the staging rollback practice, this aims to:

* Make sure that the patcher repositories, pipeline, and process continue to work as intended, and
* Give opportunity for delivery members to familiarize with the patcher repository and the patching process

These steps differ from the real patch procedure in these areas:

* All the steps should be performed by a Delivery Group member, who is not currently a Release Manager.
* We must coordinate with Release Managers in advance to deploy to `gstg` and `gprd` as part of the practice.
* The patch changes should not add actual code to the currently deployed package. We should simply add a few lines of comments  instead of a code change. The lines of comments would not have any functional effect, but they will be enough to verify the Patched package is deployed.
* There won't be any changes to the [GitLab](https://gitlab.com/gitlab-org/gitlab/) repository
* Since the changes do not contain actual code, we do not have to wait for the QA jobs except for `gstg-gitlab-qa` to finish successfully before deploying the next set of patches.
* Not waiting for a successful completion of all the QA jobs diminish this procedure to ~70 minutes, instead of ~2 hours for the actual post-deploy patch process. This is to minimize blocking the deployment process.

### Steps for Hot Patching Production Practice

The work is to be done by any delivery team member, excluding the current release managers.

**Duration**: approximately 70 minutes.

> **Note**: If there are blocking failures at any point in the steps, the practice should be cancelled to prevent further blocking the deployment process.

1. Coordinate with the release manager to find an appropriate time to perform this practice. It should occur while there are no deployments to `gstg` or `gprd`.
1. Ensure that there is not currently [an incident of severity 1 or 2](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Incident%3A%3AActive&first_page_size=20).
1. Go to `#production` Slack channel, and engage with the current OnCall SRE (`@sre-oncall`) and Release Manager (`@release-managers`) to let them know that we're about to perform the hot patching production practice. Wait for the acknowledgement from both of them to proceed with the next steps.

    * Ask the OnCall SRE to silence the alerts: `ApiServiceWorkhorseApdexSLOViolation` and `ApiServiceWorkhorseApdexSLOViolationRegional`  for at least 2 hours as canary being disabled will fire those alerts.
1. Go to `#e2e-run-production` Slack channel, and notify the Test Platform On-call DRI (`/chatops run quality dri schedule`) that we are performing the hot patching practice and that the canary will be disabled during that time.
1. In the current monthly release issue, start a comment thread to capture the procedure and outcomes.
1. Create a test incident typing `/incident declare` in `#production` Slack channel.

    * Name: `YYYY-MM-DD: [TEST] Delivery Monthly Release Hot Patching Production Practice`
    * Severity: 4
    * Unselect the boxes to engage other people.

1. In `#f_upcoming_release`:
   1. Drain canary using: `/chatops run canary --disable --production`
   1. Lock staging canary to prevent deployments: `/chatops run deploy lock gstg-cny`
   1. Lock production canary to prevent deployments: `/chatops run deploy lock gprd-cny`
1. In `#production` Slack channel, initiate a hot patch by issuing the following Chatops command , where `<incident number>` is the test incident number, ex: 1234.

    ```text
    /chatops run hotpatch --incident <incident number>
    ```

1. Some links are provided in the output of the command to help prep the patch. Use the MR link provided to prep the patch in the [engineering/patcher](https://ops.gitlab.net/gitlab-com/engineering/patcher) project.
    * Notice in the MR changes that there are folders for currently-deployed auto-deploy package versions for all the environments.
    * We can double check by comparing the versions to the output of the chatops command `/chatops run auto_deploy status` in `#f_upcoming_release` channel.
1. Obtain the desired SHA revision number by clicking the `patch directory` for the relative environment.

    * Note that if `gstg` and `gprd` run the same version, there is no entry for `staging`

    ![patch-direcotory](https://gitlab.com/gitlab-com/gl-infra/infrastructure/uploads/19ce07c6dab30fdc22776f0cd8358185/hotpatch-directory.png)

1. Locally checkout the patcher MR branch.
1. Delete the folders for canary environments' versions. We're aiming to only make patch files for the auto-deploy package versions for `gstg` and `gprd`. (If the canary versions are the same as `gstg` and `gprd`, don't delete that folder.)
1. Copy a previous practice patch file into the corresponding placeholder directory(ies) for the environment(s). The `.patch` file should not contain actual code change. Just adding a few lines of comments should be sufficient.

    ```text
    cp patches/16.3.202308182105-aca74f86626.372f136813c/001-patch-for-testing.patch patches/<current gprd|gstg auto-deploy version>/
    ```

1. Commit the changes (Usually the commit looks like 1+ folder for canary environments has been deleted, and 1+ `.patch` file has been added. Here is an [example MR commit](https://ops.gitlab.net/gitlab-com/engineering/patcher/-/merge_requests/111/diffs?commit_id=33851327dea0c25fd7e5024c1ef28871c736c5ef) for a previous practice run.)
1. When the merge request is ready, inform the `@sre-oncall` in Slack and in the merge request to review.
1. Merge the patch.
1. To monitor the process of the patch deployment, see the [pipeline view of the patcher repository](https://ops.gitlab.net/gitlab-com/gl-infra/patcher/-/pipelines).
1. In this pipeline, `gstg-cny-prepare` is automatically kicked off, but other `-prepare` jobs need to be manually started. We are deploying the patch change in the same order as the auto-deploy pipeline: `gstg-cny`, `gprd-cny`, `gstg`, then `gprd`. Wait until each environment's deploy job is finished before starting the next environment's `-prepare` job. For `gstg`, we will wait until `gstg-gitlab-qa` is also finished successfully, before starting `gprd-k8s` job.
    1. (automatic) `gstg-cny-prepare` started. Wait until `gstg-cny-k8s` deploy job is finished successfully.
    1. Start the `gprd-cny-prepare` job. Wait until `gprd-cny-k8s` deploy job is finished successfully.
    1. Start the `gstg-prepare` job. Wait until `gstg-gitlab-qa` **QA** job is finished successfully.
    1. Start the `gprd-prepare` job. Wait until `gprd-k8s` deploy job is finished successfully.
1. Make sure to unlock canary: `/chatops run deploy unlock gstg-cny` and `/chatops run deploy unlock gprd-cny` in `#f_upcoming_release`
1. Make sure to enable canary: `/chatops run canary --enable --production` in `#f_upcoming_release`
    * Let sre-oncall know that they can remove the silences.
1. Let the release manager know that the practice is complete, and we can proceed to promote the next auto-deploy packages.
1. Close the incident issue.
   * The incident Slack channel will be archived automatically. Nothing to be done with it.
1. Make sure to include in the release issue comment the following (an example from [release 16.4](https://gitlab.com/gitlab-org/release/tasks/-/issues/6322#note_1559881003)):
   1. Test incident issue:
   1. Practice patcher MR:
   1. Patch deploy pipeline:
   1. Practice duration:
   1. Result: Success/Failed and details if failed
