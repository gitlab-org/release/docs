---
tags:
  - auto-deploy
---
# Troubleshooting the Delivery-metrics server

You can find information about Delivery-metrics at
<https://gitlab.com/gitlab-org/release-tools/-/blob/master/metrics/README.md>.

## Delivery metrics appear to be missing

This only applies to metrics that are gathered by release-tools, such as `delivery_release_pressure` or
`delivery_auto_deploy_pressure`.

If metrics that are gathered by release-tools are missing, this can mean that the Delivery-metrics server is down.

One way to check if the server is down is to check the logs of the latest ["Update metrics"] pipeline. They run every
5 minutes.

If you see log lines like the following, it is likely that Delivery-metrics is down.

```
2023-10-25 12:35:47.977052 E ReleaseTools::Metrics::Client -- Recording metric failed -- {:name=>:release_pressure, :action=>:set, :status=>"502", :message=>"\n<html><head>\n<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\">\n<title>502 Server Error</title>\n</head>\n<body text=#000000 bgcolor=#ffffff>\n<h1>Error: Server Error</h1>\n<h2>The server encountered a temporary error and could not complete your request.<p>Please try again in 30 seconds.</h2>\n<h2></h2>\n</body></html>\n"}
```

To fix it, you can run a Delivery-metrics deployment pipeline. Instructions for doing this can be found in
the [metrics README](https://gitlab.com/gitlab-org/release-tools/-/blob/master/metrics/README.md#deployment).

Then check the next run of the ["Update metrics"] pipeline, to see if the errors still show up.

["Update metrics"]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines?page=1&scope=all&source=schedule
