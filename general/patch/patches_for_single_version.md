---
tags:
  - release-management
---
# Patch release for a single version

Patches are based on the GitLab Maintenance Policy targeting the two previous monthly releases in addition to the
current stable release. In the unlikely event a patch for a single version is required (a high-severity vulnerability
requires to be backported to a specific version), proceed with the following steps:

1. Execute the following command on the `#f_upcoming_release` Slack  channel:

```
/chatops run release prepare 16.11.3
```

The command will generate a [release task issue] and a blog post with the unreleased merge requests
for the single version on the [www-gitlab-com] repository. ChatOps should respond with the
URLs on the Slack channel.

1. Follow the steps on the release task issue.

[release task issue]: https://gitlab.com/gitlab-org/release/tasks/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Monthly%20Release&first_page_size=50
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
