---
tags:
  - auto-deploy
---
# Instrumentor Sandbox Setup

We can test Instrumentor changes using the sandbox environment that's spun up when we create a merge request on Instrumentor with certain labels.

- [Initial Setup](#initial-sandbox-setup)
- [Troubleshooting sandbox deployment pipeline errors](#troubleshooting-sandbox-deploy)
- [Testing version upgrades](#testing-version-upgrades)
  - [Upgrading using GitLab release versions](#upgrading-using-gitlab-release-versions)
  - [Upgrading using Auto-Deploy release versions](#upgrading-using-auto-deploy-release-versions)
  - [Testing zero-downtime upgrades (ZDU)](#testing-zero-downtime-upgrades-zdu)

## Initial Sandbox Setup

Refer to the [Sandbox setup instructions](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/engineering/sandbox-setup.html) as initial guideline and steps to setting up an instrumentor sandbox instance.

Personal sandboxes are to be set up during the onboarding issues for Delivery members that are created via an [issue template on the GitLab Dedicated Issue Tracker](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/.gitlab/issue_templates/contractor_onboarding.md?ref_type=heads).

After the sandbox environment is set up and deployed to, follow the [instructions on working locally on the sandbox environment](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/engineering/working-locally.html) to access it for further testing.

## Troubleshooting Sandbox Deploy

The following are troubleshooting steps for some of the common errors we've encountered during various `deploy` stages of instrumentor MR pipelines.

### General guideline for failing jobs

1. Retry the failing job
1. Check if it's one of the errors listed below
1. If the job log exceeds the max size, and all logs are not displayed, you can download the log file from the artifacts of the job.
1. Try re-bootstrapping the environment (`amp` scripts) following [step 7 of the sandbox setup documentation](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/engineering/sandbox-setup.md#step-7-environment-bootstrapping-process)
    - The sandbox vars most likely do not need to be re-generated (step 4 and 5). Double-check that `amp.git/environments/sandbox/overrides.json` is set up accordingly.
    - Retry the failing job after the bootstrap steps
    - This fixes some errors related to missing/incorrect AWS resources
1. Reach out to the dedicated team on `#g_dedicated-team` on Slack

### Failing `bin/migrate-env.sh` during [step 7 in sandbox setup](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/engineering/sandbox-setup.md#step-7-aws-account-bootstrapping-process)

`bin/migrate-env.sh` transfers AWS domain to be managed by TF, but has been seen to exit with an error code after. Proceed to run `bin/apply-env.sh`, then re-run `bin/migrate-env.sh` to make sure that it's a no-op.

### `integration_test_onboard`: Missing pending object in plan

The error looks like below. Rerun the job fixes.

<details><summary>Error: Missing pending object in plan</summary>

```text
│ Error: Missing pending object in plan
│
│   on ../common-modules/policy-group/main.tf line 23:
│   23: data "aws_iam_policy_document" "item" {
│
│ Instance
│ module.provision_role.module.dedicated_policy_group.data.aws_iam_policy_document.item["provision_terraform_dedicated_group1@itestdattangreviewap"]
│ is marked as having a change pending but that change is not recorded in the
│ plan. This is a bug in Terraform; please report it.
```

</details>

### `integration_test_provision`: getting S3 Bucket Replication Configuration for bucket

The error looks like below. Rerun the job fixes. Looks like it is a race condition in terraform-provider-aws. One suggestion is to upgrade the provider to at least version `v5.1.0`

<details><summary>Error: getting S3 Bucket Replication Configuration for bucket</summary>

```text
│ Error: getting S3 Bucket Replication Configuration for bucket (itestdattangreviewap-registry): ReplicationConfigurationNotFoundError: The replication configuration was not found
│  status code: 404, request id: 05KBTVQ1D7EGM943, host id: rqkhij/GXAovNGHU0fyGWE8Onni81v/BO5om3zVCK2QsvIG5KY9R4347W44QiD9yurhv+DLTPcw=
│
│   with module.provisional_regional_r0.aws_s3_bucket_replication_configuration.gitlab_s3_backup_replication_configuration["registry"],
│   on modules/provision-regional/backups-objectstorage.tf line 187, in resource "aws_s3_bucket_replication_configuration" "gitlab_s3_backup_replication_configuration":
│  187: resource "aws_s3_bucket_replication_configuration" "gitlab_s3_backup_replication_configuration" {
```

</details>

### `integration_test_prepare`: Stuck on `aws_acm_certificate_validation.opensearch` creation

The behaviour looks like the terraform creation is timing out on cert validation:

```text
module.onboard_regional_r0.aws_acm_certificate_validation.opensearch[0]: Still creating... [20m20s elapsed]
```

There may have been a mismatch between the NS glue records of the registered domain and the NS records in the hosted zone managed by Terraform.

Navigate to AWS Hosted Zones, and find the one that's managed by Terraform (description should say "Managed by Terraform"). Note the the NS record's values.

Navigate to the registered domain for that record, and the values should match the Registered domain's Name servers. If there is a mismatch, navigate to Actions > "Edit name servers" for the registered domain, and copy over the NS record's values. The change should take place in a few minutes, and the job can be restarted.

### Failing `integration_test_deconfigure`

If the job fails, but log exceeds the max size, and it does not have any error message, restart the job. It's common for the first configure stage to overrun the CI runner log limits.

If the following error occurs (or similar), you can consider the job as unrecoverable, and proceed to the next step.

<details><summary>No such file or directory: b''gitlab-ctl'''</summary>

```text
fatal: [itestdattangreviewap-gitaly-2]: FAILED! => changed=false
  cmd: gitlab-ctl cleanse
  msg: '[Errno 2] No such file or directory: b''gitlab-ctl'''
  rc: 2
  stderr: ''
  stderr_lines: <omitted>
  stdout: ''
  stdout_lines: <omitted>
```

</details>

## Testing Version Upgrades

Please read the docs in
<https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/engineering/testing-version-upgrades.md>.
