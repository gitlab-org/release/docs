---
tags:
  - release-management
---
# How to determine if a security fix includes assets modifications?

Internal releases require security fixes to be prepared to address high-severity vulnerabilities.
Code changes for security fixes may contain assets, and in this case, the assets image must be built
before preparing an internal release package, to guarantee the packages reference the updated assets image.

To determine if a security fix modifies the assets:

1. Review the code diff of the merge request, e.g. `https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1234/diffs`
1. If the code diff include any change inside the [assets directories] or includes [files that impact assets], then
  it can be deemed the security fix contains assets
1. Before triggering the `internal_release_release:start` job on the internal release pipeline, enable the
  `internal_release_wait_for_build_assets_image` [feature flag]

If the `internal_release_wait_for_build_assets_image` feature flag is enabled, the internal release automation
will wait for the `build-assets-image` job on the GitLab dev project, as such, the execution of the `internal_release_release:build_package`
job will take a while (~50 minutes).

## Assets directories

The list was taken from the [assets rake file].

- `app/assets/`
- `ee/app/assets/`
- `vendor/assets/`
- `scripts/frontend/`
- `locale/**/gitlab.po`

## [Files that impact assets]

The list was taken from the [assets rake file].

-  Any javscript file `*.js`
- `package.json`
- `yarn.lock`
- `babel.config.js`
- `.nvmrc`
- `config/application.rb`
- `Gemfile`
- `Gemfile.lock`

[feature flag]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags
[assets rake file]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/tasks/gitlab/assets.rake?ref_type=heads#L6-29
