---
tags:
  - release-management
---
# Unplanned patch release considerations

From a release manager standpoint, the [process for unplanned patches] is the same as the planned ones, but requires
requires additional coordination with security stakeholders.

A few questions that you can discuss with your fellow release manager:

* Are the fixes ready for issue(s) that prompted this unplanned release?
  * If no, find out when the fixes will be ready for the issue and *all backport* versions.
  * If yes, are all backports ready? If yes, this means that release managers have everything they need to work on the release.
* Where are we currently in the release cycle?
  * If you are working on the latest monthly release:
    * Will working on patch release endanger the monthly release? Is it
      possible to work on RCs and all backports for the patch release without
      breaching the deadline?
    * If you prepare a patch release and it gets postponed for some reason,
      will creating all other RCs as patch releases cause more work?

When you get answers for the questions above, start working on the timeline for
the unplanned release.

An example of how a release timeline could look like:

```text
To meet the deadline of the release date and not block the regular release, proposing a schedule:

* All fixes and backports need to be ready for merge by the end of Monday.
* On Tuesday, @release-manager-1 will merge all security fixes into their respective branches,
  and deploy the security fixes to our different environments.
* On Tuesday, @release-manager-2 will ensure the deployment makes it to production and tag the release.
* On Wednesday, @release-manager-1 will publish the packages and wrap up the rest of the patch release tasks.
```

The plan can be more detailed to include more people with specific tasks.
Aim to `@`-mention people responsible for specific tasks, to avoid the
[bystander effect](https://en.wikipedia.org/wiki/Bystander_effect).

Once the plan is finalized it is important to stick with the plan. You should refuse any changes
with the items that need to be included in the release if they put the release deadlines at risk.
You can consider being flexible if there is enough time to recover from failure
(CI failing, deploy goes wrong, etc.), but in most cases you will need to stick strictly to the
plan to make sure that the deadlines given to the Security team are respected.

Once the plan is defined, Release Managers can start the unplanned patch release process.

[process for unplanned patches]: planned_patch_release.md#process
