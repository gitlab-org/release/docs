---
tags:
- auto-deploy
---
# [GitLab Release Process](../README.md)

## Components

This section of the documentation is about the components that are part of the GitLab Single Application.

A component is an application that is part of the GitLab package that does not have its source code in the [`gitlab-org/gitlab`](https://gitlab.com/gitlab-org/gitlab) repository.

Components may have different level of automations, we categorize them with the following definitions:

- **Centralized component versioning**: A single-source-of-truth for
  the "current" component version on GitLab.com and in releases. See
  [Requirements for a new
  component](#requirements-for-a-new-component).
- **Managed auto-deploy**: The Delivery group will automatically
  perform the rollout of merged changes via the "auto-deploy" pipeline
- **Managed versioning**: The Delivery group will create and manage
  all the stable branches and tags in a component's repository. See [Managed
  Versioning](managed-versioning/index.md) for details.
- **Automated version updates**: Allowing version files to be
  automatically updated for Centralized component versioning. With
  this capability, a successful merge will cause versions to be
  updated and allow changes to be automatically included in Managed
  auto-deploy.
- **Independent deploy**: Providing a fully-independent deployment
  process to allow a team to deploy only their changes through to
  Production. Independent deploy pipelines will be designed in
  collaboration with the Delivery group to guarantee reliability. More
  details in
  [independent-deploy documentation](independent-deploy/index.md)

### Requirements for a new component

The coordinator for all the releases is [`release-tools`](https://gitlab.com/gitlab-org/release-tools). Each component must be integrated into `release-tools` in order to have it correctly released as part of auto_deploy and regular releases.  If `release-tools` is unaware of such a component, it may not participate in Auto Deployments or not be released with the desired version when a new GitLab package is produced.

In order to be able to release a component, it must have the following properties:

1. It has [properly configured mirrors](general/security/mirrors.md).
1. A version file in [`gitlab-org/gitlab`](https://gitlab.com/gitlab-org/gitlab) repository. (i.e. [`GITALY_SERVER_VERSION`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/GITALY_SERVER_VERSION), [`GITLAB_PAGES_VERSION`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/GITLAB_PAGES_VERSION), or [`GITLAB_KAS_VERSION`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/GITLAB_KAS_VERSION))
1. It's built using [`gitlab-org/omnibus-gitlab`](https://gitlab.com/gitlab-org/omnibus-gitlab) according to the repository version file (i.e. [`GITALY_SERVER_VERSION`](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/GITALY_SERVER_VERSION), [`GITLAB_PAGES_VERSION`](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/GITLAB_PAGES_VERSION), or [`GITLAB_KAS_VERSION`](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/GITLAB_KAS_VERSION))
1. It's built as part of the [CNG images](https://gitlab.com/gitlab-org/build/CNG/) according to a variable defining its version inside [`ci_files/variable.yml`](https://gitlab.com/gitlab-org/build/CNG/-/blob/master/ci_files/variables.yml).
1. It has a [project class](https://gitlab.com/gitlab-org/release-tools/-/tree/master/lib/release_tools/project) in `release-tools`.
1. It is included by the following release-tools constants:
   1. `ReleaseTools::ComponentVersions::FILES`
   1. `ReleaseTools::ReleaseMetadata::AUTO_DEPLOY_COMPONENTS`
   1. `ReleaseTools::PublicRelease::OmnibusGitlabRelease::VERSION_FILES`
   1. `ReleaseTools::PublicRelease::CNGImageRelease::VERSION_FILES`
   1. `ReleaseTools::PublicRelease::HelmGitlabRelease::CHART_APP_VERSION_SOURCES`

The above list contains the minimum requirements for a new component. The development team will then tag its releases and bump the version file in `gitlab-org/gitlab`. From that point on `release-tools` will make sure that the version is part of the packages.

### First-time setup

When a team plans to develop a new component, other than working on the above requirements, they should also start a [Production Readiness Review](https://about.gitlab.com/handbook/engineering/infrastructure/production/readiness/).

### Should I build a new component?

Building a new feature in one of the existing components or building a new one is a big decision. The final choice will be on the development team, but we would like to give something to think about that may not be evident in the early stages of a new project.

Other than the above requirements, a new component introduces a new set of internal (or external) APIs that may cause backward and/or forward incompatibilities breaking the [zero-downtime upgrade](https://docs.gitlab.com/ee/update/zero_downtime.html) expectation causing outages on gitlab.com and for our customers on-premise.
A collection of methodologies to mitigate the multiple version incompatibility problem is available in the [coding at scale handbook page](https://about.gitlab.com/handbook/engineering/infrastructure/coding-at-scale/). Still, most of our libraries, examples, and company expertise are related to `gitlab-org/gitlab` codebase.

### Components and auto_deploy

Auto_deploy packages are based on `gitlab-org/gitlab` default branch, and each package will include the proper component version as described in the version file.

The next step is to embrace Continuous Delivery. The component will be deployed directly from the main branch, and release-tools will tag based on the last successfully deployed SHA.
So far, gitaly is the only component implementing the auto_deploy pattern, [gitlab-com/gl-infra&99](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/99) is the reference for auto_deploy on components.
