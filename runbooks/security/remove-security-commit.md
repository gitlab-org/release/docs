---
tags:
  - auto-deploy
  - release-management
---
# Remove Security Commit From A GitLab Component

This runbook aims to guide the Delivery team in reverting a security merge request and removing its related commits as
a means to mitigate a bug introduced by a security fix while avoiding leaking the security vulnerability.

**Reverting a security merge request is [not encouraged].
Consult the [documentation on various options to mitigate bugs introduced by security merge request] first before
continuing with the following steps.**

## Prerequisites

- **This is a last resort option and should only be considered if the patch release can't be postponed**
- The security issue must have `Severity::2` or higher.
- Target repository is a GitLab component (e.g. KAS, Gitaly), *not* the main GitLab Rails repository
  (`gitlab-org/gitlab`) and its mirrors.
- This activity requires approvals from:
  - Senior Engineering Manager of Delivery and/or VP of Engineering, Infrastructure
  - AppSec team
  - Stage group owners (Optional. If it impedes the release schedule, a notification on Slack is sufficient)

## Steps to remove git commits from GitLab components repositories

### Mitigate the production incident

A GitLab component refers its version in the main GitLab Rails repository via
the `<component>_VERSION` file, e.g. for GitLab KAS, it is
[`GITLAB_KAS_VERSION`] file.

An auto-deploy package is packaged with the matching component versions via these files. Therefore, to fix a
component running on `gitlab.com`, an auto-deploy package with the right version for the component needs to be
deployed to `gitlab.com`. Please refer to the [runbook on how to mitigate an incident] to choose
the best option for each incident.

### Remove git commits from Security and Dev repositories

Once a security merge request is merged into the default branch, its commits need to be removed in `security` and `dev` mirrors to avoid
them being leaked when publishing a new release.

- Use an issue to track the action. It must be CONFIDENTIAL.
  - Create a new issue from the [remove_git_commit template]
  - Perform the tasks following the template

### Unblock auto-deploy

1. Revert the component's SHA in  `<component>_VERSION` file (e.g. [`GITLAB_KAS_VERSION`] for GitLab KAS)
  to points to a commit before the buggy commits (that have been
  [removed](#remove-git-commits-from-security-and-dev-repositories)).
1. Create a new auto-deploy package with this updated `<component>_VERSION` file
1. [Skip production promotion checks](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/gitlab-com-deployer.md#skipping-production-promotion-checks)
  to deploy the fix to Staging and Production.
1. Update incident documentation with actions taken

[not encouraged]: https://handbook.gitlab.com/handbook/engineering/releases/patch-releases/#how-can-i-revert-a-security-merge-request
[documentation on various options to mitigate bugs introduced by security merge request]:
    ../../general/security/bugs_introduced_by_security_merge_request.md
[`GITLAB_KAS_VERSION`]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/GITLAB_KAS_VERSION?ref_type=heads
[runbook on how to mitigate an incident]: ../incident.md
[remove_git_commit template]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/blob/main/.gitlab/issue_templates/remove_git_commit.md
