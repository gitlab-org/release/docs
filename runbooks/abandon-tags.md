---
tags:
  - release-management
---
# Abandoning Tags

This runbook is to provide details on when failures are found after we tag our expected release version but prior to publishing.
If you encounter this situation, don't worry, this runbook will explain why we do this and how to proceed.

## Overview

For our monthly releases, we cut RCs.
This helps with identifying failures that may occur during the tagging phase of our process.
Occasionally, post tagging patch releases (for which we do not cut RC's), we identify that the package is not usable.
The reasons for the failure will vary widely, whether it be a late found bug, build process failure, or failing QA.
When this occurs, we cannot bring in a fix and re-tag.
The reasons for this is [explained below](#why-cant-we-re-tag).

**Note** This is not an optimal solution as it confuses customers, interrupts the release workflow, and prevents us from relying on our automation.

It is suggested to explore other alternatives.
If other alternatives have been explored and deemed non-viable this runbook will walk you through the process of abandoning a tag.
Example, if we begun the process and created version 17.1.1, but something causes that package to be un-releasable, we would skip that tag and move onto 17.1.2.

## Runbook

1. After identification that a tag must be thrown away, identify which packages are affected. Sometimes when working on a release with multiple versions, not every single target package is impacted.
1. Identify the next versions. This should simply be the next patch version.
1. Notify stakeholders that the tag is being skipped.
    - This includes AppSec, if necessary
    - Managers of Delivery
    - Active Release Managers
1. Update the release task issue - our automation currently is unable to handle this scenario, thus we may need to refer to our manual steps to complete various steps in the procedure
1. Update the blog post MR with the new tag versions
1. Update the blog post with a note indicating the skipping of versions
1. Update any linked Security Issue
1. Continue with the Release Procedure

## Historical Examples

* Skipping 2 of 3 tags in a Scheduled Patch Release: https://gitlab.com/gitlab-org/release/tasks/-/issues/12610#note_2067973354
* Another example of skipping all 3 tags: https://gitlab.com/gitlab-org/release/tasks/-/issues/7917
* Example update of blog target versions: https://gitlab.com/gitlab-org/security/www-gitlab-com/-/merge_requests/208/diffs?commit_id=55605ab52bb5a328859ba2ef7953c15e0ac447ce
* Example update of blog annotation of skipped versions: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/135926/diffs?commit_id=2b8e0e03d2953e35f0d8acca18d1409a50f631db

## Why Can't We Re-Tag?

Tagging is a rather complicated procedure that requires a lot of changes to many projects and repositories.
Currently, we'd need to manually un-protect tags across all projects that receive the tag.
Make note the tag slightly differs on some projects which increases the audit load required.
We'd need to then delete the old tag across all projects and all project mirrors.
Only then would we be in a position to re-tag.
We've never performed this action due to the complexity of the process.
There's also room for added failure since re-tagging is a form of rewriting history. For example, when looking at pipelines, we may see pipelines which do not behave, or look incorrect due to this work.
