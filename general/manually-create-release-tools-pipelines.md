---
tags:
  - release-management
---

# Manually create release-tools pipelines

In the [ops release-tools mirror repository](https://ops.gitlab.net/gitlab-org/release/tools), we can manually create
various release-related pipelines by passing in a set of environment variables when running a new pipeline.
It can be useful when we make a change in the pipeline configuration after a release pipeline was created already.

## Recreate monthly release finalize pipeline

We can recreate the pipeline to be run on the release day ([template](https://ops.gitlab.net/gitlab-org/release/tools/-/blob/27cd377d2009c04ca3fd36df0d4492b81e512f83/templates/monthly.md.erb#L225))
by setting `MONTHLY_RELEASE_PIPELINE` to `finalize` like so:

1. Go to <https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines>
1. Click 'Run pipeline' button
1. Set `MONTHLY_RELEASE_PIPELINE` to `finalize` as an environment variable
1. Once the pipeline gets created, _do not start any jobs_. Copy the link and edit the link on the issue.
