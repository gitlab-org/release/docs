---
tags:
  - auto-deploy
---
# Release Status Metrics

We emit prometheus metrics that indicate the current status of the monthly or patch release via
`delivery_release_monthly_status` and `delivery_release_patch_status` respectively.

Those metrics currently get used in the ["delivery: Release Information"] dashboard to communicate to folks outside the
Delivery group about the current status of the releases.

This runbook aims to help with troubleshooting and manually creating those metrics.

## Troubleshooting

### The panel(s) on the dashboard is presenting incorrect information

This could be due to multiple reasons, so we should ask the following questions to investigate:

1. Does the metric with the correct label and value show up on Grafana page?

    * Check these queries: [delivery_release_monthly_status], [delivery_release_patch_status]
    * If the latest metric appears correct, then the dashboard panel is not working as we intended. Investigate further
    into the panel setup. We can edit the Grafana panel temporarily, but the changes will not persist as the dashboard
    gets refreshed from the [runbooks repository].

1. Did the metric get created correctly?

    * The metrics get created during the release processes, either automatically via a release pipeline, or a pipeline
    manually created by a release manager. Investigate the logs produced by that job.
        * For example, when a patch release gets finalized, the
          [security_release_finalize:create_release_status_metric] job gets run as part of the [final steps] of the release.
          During the monthly release [initial preparation day], we ask the release managers to manually create a pipeline
          to update the monthly status metric to the "announced" state.
    * There should be a log line similar to:

      ```log
      ReleaseTools::Metrics::<Monthly|Patch>ReleaseStatus -- Setting release_<monthly|patch>_status metric -- {:status=>2, :labels=>"2024-06-20,17.1"}
      ```

      [example](https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/14323921#L77)
    * If there is an error, an incorrect status, or that log line does not exist, investigate further into the source
    code in release-tools.
    * If there seems to be an error with the metrics server, refer to [Troubleshooting the Delivery-metrics server].
    * Once the problem has been addressed via a MR, we need to manually create the correct metric, since the job that
    emitted the incorrect metric or errored out was for a commit that does not include the fix MR commit.

## Manual creation

Sometimes, we need to (re-)create the metrics manually creating a pipeline on the [ops release/tools repository].

Note that this is a more of a temporary solution if there is actually a bug within the dashboard or release-tools code.

1. Search for a rake task in the [ops release/tools repository] that creates the metric with the correct status.
Searching for ".new(status: :<open|announced|warning|tagged_rc|closed>)" is a good starting point to look for the rake task.
Usually it looks like:

    ```rake
    desc 'Create a new patch release status metric'
    task create_release_status_metric: :force_security do
      ReleaseTools::Metrics::PatchReleaseStatus
        .new(status: :open)
        .execute
    end
    ```

    [source](https://ops.gitlab.net/gitlab-org/release/tools/-/blob/165f611125c77a70c2e8fbfbe83bcaa529662587/lib/tasks/security.rake#L276-282)

1. Open [automation.gitlab-ci.yml]:
    * If there's already a job that calls that rake task, look at the `rules:` block, and use that variable to manually
    create the pipeline in ops. For example, if you see

    ```yml
    metrics:patch_release:open:
    extends: .with-bundle
    stage: automation
    rules:
      - if: $PATCH_RELEASE_METRIC == "open"
        when: always
      - when: never
    script:
      - bundle exec rake "metrics:patch_release:open"
    ```

    Set the variable key to `PATCH_RELEASE_METRIC` and variable value to `"open"`, then "Run pipeline".
    Watch for the logs of that job to make sure the expected metric got created.

    * If there is not already a job, we need to temporarily create a similar pipeline job for the rake task that we
    found in the previous step.

1. Inside of your local `release-tools` directory, add the `ops` remote and/or fetch from it.

    ```bash
    git remote add ops git@ops.gitlab.net:gitlab-org/release/tools.git
    git fetch ops
    ```

1. Create a new branch off of `ops/master`.

    ```bash
    git checkout -b <username>/manually-create-<monthly|patch>-metric ops/master
    ```

1. Make modifications to [automation.gitlab-ci.yml] like the following:

    ```yml
    metrics:<monthly|patch>_release:<open|announced|warning|tagged_rc|closed>:
    extends: .with-bundle
    stage: automation
    rules:
      - if: $<MONTHLY|PATCH>_RELEASE_METRIC == "<open|announced|warning|tagged_rc|closed>"
        when: always
      - when: never
    script:
      - bundle exec rake "<rake task>"
    ```

1. Make modifications to [workflow.gitlab.ci] accordingly:

    ```yml
    - if: $<MONTHLY|PATCH>_RELEASE_METRIC == "<open|announced|warning|tagged_rc|closed>"
      variables:
        PIPELINE_NAME: "<Monthly|Patch> release metric pipeline for <open|announced|warning|tagged_rc|closed> status"
    ```

1. Commit and push the change:

    ```bash
    git commit -m "Add temporary pipeline to manually create <monthly|patch> release metric for <open|announced|warning|tagged_rc|closed> status"
    git push ops <username>/manually-create-<monthly|patch>-metric
    ```

1. Go to the [ops release/tools repository] Repository settings, and protect the branch, so that we can run pipelines
from it.

1. Finally, [create the pipeline in ops] by setting the variable from the `rules:` block. Make sure to choose the new branch.
Watch for the logs of the new job to make sure the expected metric got created.

1. Make sure to unprotect and delete the branch afterwards on the [ops release/tools repository].

[automation.gitlab-ci.yml]: https://ops.gitlab.net/gitlab-org/release/tools/-/blob/165f611125c77a70c2e8fbfbe83bcaa529662587/.gitlab/ci/automation.gitlab-ci.yml
[create the pipeline in ops]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/new
[delivery_release_monthly_status]: https://dashboards.gitlab.net/goto/WeaaOEUIg?orgId=1
[delivery_release_patch_status]: https://dashboards.gitlab.net/goto/JEGadEUSR?orgId=1
[final steps]: https://ops.gitlab.net/gitlab-org/release/tools/-/blob/165f611125c77a70c2e8fbfbe83bcaa529662587/templates/patch.md.erb#L148
[initial preparation day]: https://ops.gitlab.net/gitlab-org/release/tools/-/blob/165f611125c77a70c2e8fbfbe83bcaa529662587/templates/monthly.md.erb#L71
[ops release/tools repository]: https://ops.gitlab.net/gitlab-org/release/tools
[runbooks repository]: https://gitlab.com/gitlab-com/runbooks/-/blob/master/dashboards/delivery/release_info.dashboard.jsonnet
[security_release_finalize:create_release_status_metric]: https://ops.gitlab.net/gitlab-org/release/tools/-/blob/165f611125c77a70c2e8fbfbe83bcaa529662587/.gitlab/ci/security/finalize-ci.yml#L95-103
[Troubleshooting the Delivery-metrics server]: troubleshoot-delivery-metrics.md
[workflow.gitlab.ci]: https://ops.gitlab.net/gitlab-org/release/tools/-/blob/165f611125c77a70c2e8fbfbe83bcaa529662587/.gitlab/ci/workflow.gitlab-ci.yml#L153-155
["delivery: Release Information"]: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1
