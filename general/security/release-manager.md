---
tags:
  - release-management
---
# Security runbooks for Release Manager

## Overview

As noted in the [patch release overview], security fixes are released on
patch releases. Release managers are responsible for handling patch releases
and ensuring packages are safely released to our customers.

Have a look at the [patch release manager runbook] for details.

## Runbooks

* [Security fixes introducing breaking changes]
* [Overview of the security pipeline]

---

[Return to Security Guide](readme.md)

[patch release manager runbook]: ../patch/release_managers.md
[patch release overview]: ../patch/readme.md
[Security fixes introducing breaking changes]: runbooks/security_fixes_introducing_breaking_changes.md
[Overview of the security pipeline]: utilities/security_pipeline.md
