---
tags:
  - auto-deploy
---
# Long-lived Sandbox for Delivery

- [Long-lived Sandbox for Delivery](#long-lived-sandbox-for-delivery)
  - [Introduction](#introduction)
  - [Get Access](#get-access)
  - [Run A Deployment](#run-a-deployment)
    - [Run A Deployment with QA](#run-a-deployment-with-qa)

## Introduction

In order to save cost and perform long-lived test on Dedicated setup, Delivery has our own Dedicated Sandbox. The [Tenant Model](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/blob/main/tenant_models/deliverysandbox831f36.json?ref_type=heads) can be found in `switchboard_uat` repository.

## Get Access

Unlike other tenants, Delivery team has access to the infrastructure. This section shows step-by-step how to gain access the to the Sandbox's infrastructure. This is a clone from [this comment](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1109#note_1568226849) on the Gitlab issue.

These are specifically ordered to succeed (the IAM policy is very precise and limits the available early actions); if you do these out of order, you're on your own (YMMV, no warranty, etc)

- Go to <https://140863853000.signin.aws.amazon.com/console> and login with the initial credentials provided in Slack
- At the top right, click `<username> @ 1408-6385-3000`, choose `Security credentials` from the drop down
- Click `Assign MFA device` and set up a multi-factor device of your choice
- Sign out
- Login again, this time with your newly enabled MFA
- Navigate to `Security credentials` again
- Click the `Update console password` button; enter current password and your new password and apply the change.
- If you used a yubikey or similar in your MFA above, you need to also create a TOTP key. If you used TOTP, skip this sub-step:
  - Click "Assign MFA device"
  - Enter your IAM username as the Device name (this is non-negotiable; the IAM policy will not permit anything else)
  - Leave `Authenticator app` selected
  - Follow the on-screen instructions to save/verify the TOTP in your choice of Authenticator app (e.g. Google Authenticator)
- Click `Create access key`.
  - Choose "Other" as the use-case (the use-case really doesn't matter; most of the others demand you click a checkbox acknowledging you understand their recommendations, but this choice has no permanent effect on anything)
  - Add a tag if you want, then click `Create access key`. Remain on the result page during the next step
  - On your laptop, ensure you're logged into 1password CLI or have the PMV/1password integration configured, then use `pmv capture aws -T Dedicated/Instrumentor/Env:Delivery`, copy/pasting the credentials from the result page in your browser.

Now for general AWS CLI access to the account you can use pmv to retrieve your access credentials and do the MFA TOTP login, e.g.:

```plaintext
$ eval $(pmv env Dedicated/Instrumentor/Env:Delivery)
pmv: 🔓 Fetching 1password items with tag Dedicated/Instrumentor/Env:Delivery...
pmv: 🧩 Loaded: `AWS Access: cmiskell: Acccount ID 140863853000`
pmv: 📲 Use your Authenticator app to provide a token code for `AWS Access: cmiskell: Acccount ID 140863853000`:
token: REDACTED
pmv: 🔓 Complete, loaded items...
$ aws sts get-caller-identity
{
    "UserId": "AIDASBTBD2HENZGOPOAK6",
    "Account": "140863853000",
    "Arn": "arn:aws:iam::140863853000:user/cmiskell"
}
```

For doing things specific to Dedicated, assuming that:

1. There is a version of PMV that has <https://gitlab.com/gitlab-com/gl-infra/pmv/-/merge_requests/327> (expecting: v3.10.0 or later, once merged).
2. <https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/merge_requests/659> has been merged and applied
3. The tenant model from <https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/blob/main/tenant_models/deliverysandbox831f36.json> is in the TENANT_MODEL env var in your shell (e.g. `export TENANT_MODEL=$(cat switchboard_uat/tenant_models/deliverysandbox831f36.json)` )

then:

```shell
$ eval $(pmv env Dedicated/Instrumentor/Env:Delivery --assume-role provision_terraform@deliverysandbox831f36)
$ eval $(tenctl provision -m "${TENANT_MODEL}" --generate-kubeconfig  env)
...
$ kubectl get certificates gitlab-gitlab-tls -o json|jq .spec.dnsNames
[
  "delivery.gitlab-private.org"
]
```

The kubectl command above is just an example, but is a handy way to confirm you're connected to the tenant you expect.

I prefer `provision` when all I need is kubectl, because the `tenctl provision env` invocation is several orders of magnitude faster than `tenctl configure env` for some annoying reasons that might be solved soon but which are out of scope here. If you need `configure` stage privileges for some reason (e.g. to use AWS SSM to get a shell on a Gitaly VM with <https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/runbooks/tenant-ssm.md>), start with this instead:

```shell
eval $(pmv env Dedicated/Instrumentor/Env:Delivery --assume-role configuration_ansible@deliverysandbox831f36)
eval $(tenctl configure -m "${TENANT_MODEL}" env)
```

## Run A Deployment

Same as other tenants in [`switchboard_uat`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat), a manual pipeline is triggered to perform a deployment to the sandbox (e.g. to update GitLab version after changing it in the Tenant Model)

- Required variable:

| Variable key |    Variable value     |
|:------------:|:---------------------:|
|  TENANT_ID   | deliverysandbox831f36 |

*Note*: `deliverysandbox831f36` matches the [Tenant Model file name](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/blob/main/tenant_models/deliverysandbox831f36.json?ref_type=heads).

[Run a deployment pipeline](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/pipelines/new?var%5BTENANT_ID%5D=deliverysandbox831f36)


### Run A Deployment with QA

By default, QA jobs don't run in the pipeline. To trigger them, you need to add another variable:

| Variable key |    Variable value     |
|:------------:|:---------------------:|
|  TENANT_ID   | deliverysandbox831f36 |
|    RUN_QA    |         true          |

[Run a deployment pipeline with QA](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/pipelines/new?var%5BTENANT_ID%5D=deliverysandbox831f36&var%5BRUN_QA%5D=true)
