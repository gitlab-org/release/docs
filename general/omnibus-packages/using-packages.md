---
tags:
  - auto-deploy
---
# Using packages

## Overview

This docs summarizes how we use Omnibus packages from packages.gitlab.com.

## Where Omnibus packages are used?

* [`deploy-tooling`](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling):
  * It is used via `deployer` in the [precheck job](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/blob/7ce65801d1dee43fb873858624fbc31dcf9a2baf/prechecks_pipeline.yml#L27) and [fetch assets](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/blob/c516ef77e558ecf7077eb43d81f9ac0dd027efb9/bin/fetch-and-upload-assets#L71).
  * The token is provided by the `PACKAGECLOUD_TOKEN` variable in the infra group.
* `gitlab-cookbooks`:
  * [Used to build the package URL.](https://gitlab.com/gitlab-cookbooks/cookbook-omnibus-gitlab/-/blob/6eb1241c962df385bb17e447bcc2f92cb286f4e2/recipes/default.rb#L34)
  * Token stored in Vault.
* `gitlab-runner`:
  * Used in both [canonical](https://gitlab.com/gitlab-org/gitlab-runner) and [security](https://gitlab.com/gitlab-org/security/gitlab-runner/-/settings/ci_cd) mirrors.
  * The token is provided by the `PACKAGECLOUD_TOKEN` variable.
  * Use a separated user for runners.
* `omnibus-gitlab`:
  * In [dev](https://dev.gitlab.org/gitlab/omnibus-gitlab/) mirror, packages are built and uploaded. Thus, a token with read/write permission is needed here. It uses a token from the main user.
  * In [canonical](https://gitlab.com/gitlab-org/omnibus-gitlab/) mirror, nightly packages are built and uploaded. It uses a token from another user.
  * The token is provided by the `PACKAGECLOUD_TOKEN` variable.
* Dedicated:
  * Pulls the packages to deploy to Dedicated instances.
  * For `pre-release` packages, a read-only token is used only for Dedicated.