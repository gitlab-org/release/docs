---
tags:
  - release-management
---
## Create a blog post for a patch release

Blog posts for patch releases are the responsibility of the Release Managers to create. The blog post should be created automatically:

* By the security pipeline during a [planned patch release].
* By the `/chatops run release prepare <VERSION>` command if a patch release for a single version is prepared.

If the automatic creation fails, follow this doc to manually create it. Make sure the blog is not merged until the packages are released.

## Required information

* Version that is being released. We'll refer to this as AA-B-C
* Date the release is happening. We'll refer to this as YYYY-MM-DD

## Create a MR for the blog post

1. Clone the release-tools repository and install dependencies

    ```shell
    $ git clone git@gitlab.com:gitlab-org/release-tools.git
    $ cd release-tools
    $ bundle install
    ```
1. Generate a new post.

For a planned patch release:

    ```shell
    $ bundle exec rake 'release:patch_blog_post[AA.B.C]'
    ```

For a single version patch release:

   ```shell
    $ bundle exec rake 'release:patch_blog_post'
   ```

1. If the blog post is for a planned patch release, the above command will have
    generated the blog post on the www-gitlab-com security repository. At this point
   you can continue with the patch release tasks. If the blog post is for a single
   patch release version, continue with the next steps.

1. Edit the template, ensure the following fields get completed:
    * title
    * author
    * author_gitlab
    * author_twitter *Please use gitlab if you don't want to use your own Twitter account*
    * description
    * Add links to merge requests for included fixes, along with a brief description
    * Update the **Important notes on upgrading** section as appropriate
1. Copy the file into www-gitlab-com repository, commit, and push the new blog post

    ```shell
    $ git clone git@gitlab.com:gitlab-com/www-gitlab-com.git
    $ cp YYYY-MM-DD-gitlab-AA-B-C-released.html.md www-gitlab-com/sites/uncategorized/source/releases/posts/
    $ cd www-gitlab-com
    $ git add source/releases/posts/YYYY-MM-DD-gitlab-AA-B-C-released.html.md
    $ git commit -m "Adding AA-B-C blog post"
    $ git push --set-upstream origin create-AA-B-C-post
    ```
1. Click the link in the `git push` output to bring up the new Merge Request page for your branch
1. Fill in the `Title`, `Description`, and `Milestone`, and assign to yourself.
1. Click the `Submit merge request` button
1. When the review environment is ready, read your blog post and ensure everything looks correct. Fix any errors.
1. Once packages are released, assign the merge request to someone with write access to [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/) to merge.

[planned patch release]: readme.md#planned-patch-release
