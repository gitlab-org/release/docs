---
tags:
  - auto-deploy
---
# Patch release runbook for GitLab engineers: Preparing security fixes for a patch release

## Overview

Patch releases are scheduled twice a month on the Wednesdays of the weeks before and after the monthly release.
For a general overview of the patch release process have a look at the [end-to-end explanation].

The purpose of this runbook is to guide GitLab engineers in preparing security fixes to be included in a patch release.
To ensure all the steps are done correctly and consistently, it is recommended that you:

* Understand the [Due Date] to include a security fix in a patch release.
* Have a look at the [Release Dashboard] for general information about the next patch release, including date
and targeted versions.
* Understand the [security terminology].
* Ensure [security fixes are worked on security repositories](#do-not-push-to-gitlab-orggitlab)
* Follow the steps listed on [process]
* Take a look at the patch release [frequently asked questions]

Feel free to ask any question about this process on the [`#releases`] Slack channel

The process is continuously improving, so be sure to check back frequently for updates.

## Due Date

> **:mega: NOTE:** TL;DR - Security merge requests should be approved, with green pipelines and ready to be merged 2 working days
> before the patch release due date, by the start of EMEA day. Check out the [Release Dashboard] for information about the upcoming
> patch release date and targeted versions.

To guarantee the security fix is included in the next patch release, **the ~"security-target" label should be applied to the security
implementation issue at least 48h, or 2 working days if occuring over a weekend or other non-working day, before the due date** established in the
[Security Tracking Issue]. For example, if a patch release is scheduled on a Wednesday, the patch release process will start on a Monday EMEA time,
security issues linked to the Tracking Issue before this are guaranteed to be included in the patch.

The release tooling will evaluate any security implementation issue containing the ~"security-target" label,
and link them to the [Security Tracking Issue] if they are ready. If they are not ready, comments will be posted on
the security implementation issue directing the assignee on what actions still need to be taken.

The [Security Tracking Issue] is the single source of truth (SSOT) for the security fixes included in the patch release.
**Only issues that are linked to the Security Tracking Issue are guaranteed to be in the release**.

Fixes not linked by 2 working days before the due date may not be considered for the patch release
and they'll have to go into the next one that should be scheduled in ~2 weeks.

Questions about the patch release process can be done in the Slack [`#releases`] channel (internal only).

## DO NOT PUSH TO `gitlab-org/gitlab`

As an engineer working on a fix for a security vulnerability, your main concern
is not disclosing the vulnerability or the fix before we're ready to publicly
disclose it. For the same reason all security fixes are deployed and released
as part of the patch release process and timing. We cannot apply a security fix to GitLab.com in another way.

To that end, you'll need to be sure that security vulnerabilities:

* For GitLab.com, are fixed in the [GitLab Security Repo].
* For Omnibus, are fixed in the [Omnibus Security Repo].
* For GitLab Pages, are fixed in the [GitLab Pages Security Repo].
* For Gitaly, are fixed in the [Gitaly Security Repo].

This is fundamental to our patch release process because Security repositories are not publicly-accessible.

## Preparation

### Making sure the issue needs to follow the security workflow

* Verify if the issue you're working on `gitlab-org/gitlab` is confidential, if it's public fix should be placed on GitLab canonical and no backports are required.
* If the issue you're fixing doesn't appear to be something that can be exploited by a malicious person and is instead simply a security enhancement do not hesitate to mention `@gitlab-com/gl-security/appsec` in the issue to discuss whether the fix can be done in a public MR, in the canonical repository.
* If you're updating a dependency that has a known vulnerability that isn't exploitable in GitLab or has very low severity feel free to engage `@gitlab-com/gl-security/appsec` in the related issue to see if the dependency can be updated in the canonical repository.

### Preparing the repository

Before starting, add the new `security` remote on your local GitLab repository:

```sh
git remote add security git@gitlab.com:gitlab-org/security/gitlab.git
```

Then run the `scripts/security-harness` script. This script will install a Git `pre-push` hook
that will prevent pushing to any remote besides `gitlab.com/gitlab-org/security` or `dev.gitlab.org`,
in order to prevent accidental disclosure.

Please make sure the output of running `scripts/security-harness` is:

```sh
Security harness installed -- you will only be able to push to gitlab.com/gitlab-org/security!
```

## Process

As with most GitLab development, a security fix starts with an issue identifying the vulnerability. For GitLab.com,
it should be a confidential issue in the `gitlab-org/gitlab` project or other appropriate canonical repository such
as `gitlab-org/gitaly`, `gitlab-org/gitlab-workhorse`, `gitlab-org/gitlab-runner`, etc.

Typically, only confidential issues that are security vulnerabilities or dependency updates are to follow the process detailed below.
If you are unsure if this process needs to be followed, please [ask the AppSec team]. Security vulnerability issues are typically
marked with the `security` label, have a `priority` and `severity` label/score tied to them and do **not** [have the feature label].
For more information, please see the [remediation SLAs].

Please also take note of the [upstream security patches process] if the vulnerability you're working on affects a third-party dependency.

Once an eligible confidential security issue is assigned to an engineer:

1. A [security implementation issue] must be created on the respective Security repository
   using the security issue template.
   * i.e. If an engineer is working on a security fix for GitLab, they will need to create an issue on [GitLab Security repo]
    using the [GitLab Security issue template]
   * The security implementation issue describes the required steps that need to be followed to remediate the security
     vulnerability. The same steps are summarized in this section.
   * If this fix for a security vulnerability has [far-reaching impact] or is a [breaking change] for a valid use-case, then additional solution evaluation must be performed. See [far-reaching impact or breaking fixes process page].
2. A security merge request targeting the default branch (e.g `master` or `main`) must be submitted with the security fix using
   the security merge request template.
   * i.e. If an engineer is working on a security fix for GitLab, they will need to submit the merge request using the
    [GitLab Security merge request template]
   * Merge requests on [GitLab Security] follow the same [code review and approval] process as any other change.
   * Additionally, the merge request targeting the default branch needs to be approved by an AppSec team member.
   * Note: The security repos have a setting enabled to remove existing approvals when a new commit is pushed. If you push changes during the approval process, you will need to ping anyone who previously approved the MR and ask for re-review and re-approval.
3. Once the merge request targeting the default branch is approved according to our Approval guidelines and by an AppSec team member, the
   engineer can proceed to prepare the [backports]
4. [Backports] need to be approved by the same maintainer that reviewed and approved the merge request targeting the default branch.
   * It's not required for the backports to have the AppSec approval.
   * You may need to wait for stable branch of the current version to be created in order to prepare the last backport. This happens close to the [monthly release date]. If the security implementation issue has the ~"security-notifications" label applied, a comment will be posted, pinging the assignee(s) when the stable branch has been created.
5. Once the merge request targeting the default branch and all backports are ready, they must be assigned to `@gitlab-release-tools-bot`.
   * It's OK for either the maintainer or the author to assign to the bot.
   * The bot will unassign itself and ping the MR author if any of the requirements are not met (all checkboxes checked, approvals present, pipeline green, etc).
6. **IMPORTANT:** When the issue is ready for release (a default MR and three backports with green pipelines and approvals are ready), apply the ~"security-target" label to the security implementation issue. The release tooling will evaluate if it is ready for release and link it to the next [Security Tracking issue].
   * If the issue is linked to the Security Tracking Issue, it will be included in the release.
   * If the issue is evaluated as not being ready, it will not be linked and a comment will be posted on the security implementation issue informing the assignee(s) what actions still need to be taken.
   * If the security fix is _required_ for the release, notify the release managers and ensure that the [security implementation issue] is linked as a blocker to the Security Tracking issue without applying the ~"security-target" label since that label may cause it to become unlinked. This prevents release-tools from unnecessarily unlinking issues that are added too close to the due date.
7. All done, no more action is needed.
   * Security merge requests are automatically merged by release managers during the preparation of the patch release.
   * You will be notified by a release manager if there is an issue with one or more of your MRs.

* **NOTE:**
  * When updating dependencies that could possibly be exploited in the wild (i.e. publicly acknowledged security issue by the vendor like
this [Mermaid release] mentioning the XSS), we can open the MR targeting the default branch in the canonical repository to protect GitLab.com
as soon as possible but we must create the backports regardless.
  * If a security issue is deemed severe and we need to immediately prevent exploitation by applying a firewall block, escalte the issue into an incident and engage the [Security Incident Response Team]. Be sure to involve a [CMOC] to [assist with communication].

### Security implementation issue

Create an issue on the respective [security repository],  using the [GitLab Security issue template].
The title should be the same as the original created on the Canonical repository, for example: `Prevent stored XSS in code blocks`.

It's not required to copy all the labels from the original issue on `gitlab-org/gitlab`, ~security and ~severity::x are enough.

This issue is now your "Security Implementation issue" and a single source of truth for
all related issues and merge requests. Once the issue is created, assign it
to yourself and start working on the tasks listed there.

#### Versions Affected

Within the description "Details" section, fill out the `Versions affected`
using one of the following formats:

* `All`
* `X.Y+`
* `X.Y - X.Y`
* `X.Y,X.Y`

### Security merge requests

The [security implementation issue] will ask you to create merge requests on the [security repository].
Ensure they are using the respective security template.

Your branch name must start with `security`, such as: `security-rs-milestone-xss-12-6`.

Branch names that start with `security` cannot be pushed to the canonical repositories on `gitlab.com/gitlab-org`.
This helps ensure security merge requests don't get leaked prematurely.

For the first merge request, make sure:

* The [GitLab Security merge request template] is used.
* Targets the default branch (`master` or `main`).
* Has correct ~security and ~severity::x labels.
* Has green pipelines.
* Has been approved by a maintainer and by an AppSec team member.

For every backport merge request created, make sure:

* The [GitLab Security merge request template] is used
* Targets the `X-Y-stable{,-ee}` branch that belongs to the target version.
  * For the MR targeting the current release, the stable branch might not yet exist.
    They are normally created two working days before the [monthly release date]. In the meantime, you can set the MR to target the default branch
    instead, and change it later once the stable branch has been created.
    If you're planning to take time off during the time when the stable branch is created,
    you can ask somebody from your team for help and reassign the security issue to them.
* Has correct labels ~security and ~severity::x labels..
* Has green pipelines.
* Has been approved by a maintainer.

**IMPORTANT:**

* In case one of these items is missing or incorrect, Release Managers will re-assign
all related merge requests to the original author and remove the issue from the current patch release.
This is necessary due to the number of merge requests that need to be handled for each patch release.
* Security merge requests are to be merged by the release-tooling or by Release Managers only.
* [Feature flags are not allowed] on security merge requests.
* Migrations that modify or delete data are discouraged from security merge requests. Best practice would instead be to hide the offending data in the application layer and follow up with a migration using the [regular development process](https://docs.gitlab.com/ee/development/database/).
* Be careful about information included in documentation updates, as issues are not made public until 30 days after the patch is released. It is OK to link to the issue, even though the link will be broken until the issue is made public ([example](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2441/diffs)).
* When implementing a security fix, it's best to go with the smallest change possible.
This is helpful to avoid problems/conflicts when creating backports for older
versions. It also helps to reduce the possibility of having unwanted side effects
as the fix will be focused on the issue. Improvements can be done publicly after
the patch release is done.

### Backports

Per the [GitLab Maintenance Policy], all security fixes are backported to the current released version and the two
previous GitLab versions. To determine the versions to create backports have a look next patch release information on the [Release Dashboard]. The `Release Versions` section contains the list of backport versions.

In total, for each security issue there will be four merge requests:

* One targeting the default branch
* Three backports

If there is a situation where not all backports are needed, apply the ~"reduced backports" label to the security implementation
issue and ping `@gitlab-org/release/managers` for their awareness.

**TIP:**
When creating your merge requests backports there's a handy script, [`secpick`],
that will allow you to cherry-pick commits across multiple releases and optionally
create the merge request backports for you. If changes can't be cleanly picked (e.g.
file changed doesn't exist or file was moved in the previous version), you will need to fix it manually.
As an alternative there is a [gitlab-dev-cli] tool which leverages secpick.

#### Missing a patch release

If your security fix has to be excluded from a release, have a look at the
[Release Dashboard] for information about the upcoming patch release.

### Code reviews and Approvals

Security merge requests follow the same review process stated in our [Code Review Guidelines]:

* Merge requests targeting the default branch must be approved:
  * According to our [Approval Guidelines]
  * By a member of the AppSec team: [ask the AppSec team] for further help.
* Backport merge requests must be approved by at least one maintainer. The Maintainer must be the same as the one assigned to the merge request targeting the default branch. AppSec approval is not needed for backport merge requests.

### Final steps

* Ensure all items have been completed:
  * Including [Summary section] from the security implementation issue.
  * Items listed on the [GitLab Security merge request template] for each merge request.
* Ensure all merge requests associated to the security implementation issue are assigned to `@gitlab-release-tools-bot`,
ping the corresponding maintainer if that's not the case.
* Be sure to run `scripts/security-harness` again to enable pushing to remotes other than [GitLab Security].

## Contact the AppSec team

To contact the AppSec team, please `@-mention` the [AppSec stable counterpart] for the [group relevant to the vulnerability being fixed]. Alternatively, ping the AppSec engineer associated to the issue in the [GitLab Canonical repository] or ask in the `#sec-appsec` Slack channel.

## Questions?

If you have any doubts or questions, feel free to ask for help in the [`#releases`] Slack channel

---

[`#releases`]: https://gitlab.enterprise.slack.com/archives/C0XM5UU6B
[`secpick`]: utilities/secpick_script.md
[Approval Guidelines]: https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines
[AppSec stable counterpart]: https://handbook.gitlab.com/handbook/security/product-security/application-security/stable-counterparts/
[ask the AppSec team]: #contact-the-appsec-team
[assist with communication]: https://about.gitlab.com/handbook/security/security-operations/sirt/security-incident-communication-plan.html#communicating-internally
[backports]: #backports
[Code Review Guidelines]: https://docs.gitlab.com/ee/development/code_review.html
[CMOC]: https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#when-to-engage-an-incident-manager
[code review and approval]: #code-reviews-and-approvals
[Due Date]: #due-date
[end-to-end explanation]: ../patch/readme.md#planned-patch-release
[far-reaching impact]: https://about.gitlab.com/handbook/engineering/development/#reducing-the-impact-of-far-reaching-work
[far-reaching impact or breaking fixes process page]: far_reaching_impact_fixes_or_breaking_change_fixes.md
[Feature flags are not allowed]: ./utilities/feature_flags.md
[frequently asked questions]: https://handbook.gitlab.com/handbook/engineering/releases/patch-releases/#patch-release-faqs
[gitlab-dev-cli]: https://gitlab.com/gitlab-org/gitlab-dev-cli#working-on-security-issues-disclaimer
[GitLab Security]: https://gitlab.com/gitlab-org/security/
[GitLab Canonical repository]: https://gitlab.com/gitlab-org/gitlab
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[GitLab Security repo]: https://gitlab.com/gitlab-org/security/gitlab
[Gitaly Security repo]: https://gitlab.com/gitlab-org/security/gitaly
[GitLab Pages Security Repo]: https://gitlab.com/gitlab-org/security/gitlab-pages
[GitLab Security issue template]: https://gitlab.com/gitlab-org/security/gitlab/issues/new?issuable_template=Security+developer+workflow
[GitLab Security merge request template]: https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/merge_request_templates/Security%20Fix.md
[group relevant to the vulnerability being fixed]: https://handbook.gitlab.com/handbook/product/categories/#devops-stages
[have the feature label]: (https://about.gitlab.com/handbook/engineering/security/#feature)
[Mermaid release]: https://github.com/mermaid-js/mermaid/releases/tag/8.13.9
[monthly release date]: https://about.gitlab.com/releases/
[Omnibus Security repo]: https://gitlab.com/gitlab-org/security/omnibus-gitlab
[process]: #process
[Release Dashboard]: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1
[remediation SLAs]: https://handbook.gitlab.com/handbook/security/product-security/vulnerability-management/#remediation-slas
[security implementation issue]: #security-implementation-issue
[Security Incident Response Team]: https://about.gitlab.com/handbook/security/security-operations/sirt/sec-incident-response.html#engaging-sirt
[security repository]: https://gitlab.com/gitlab-org/security/
[security terminology]: terminology.md
[Security Tracking Issue]: https://gitlab.com/gitlab-org/gitlab/-/issues/?label_name%5B%5D=upcoming%20security%20release
[Summary section]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md#summary
[upstream security patches process]: /runbooks/security/upstream_security_patches.md
