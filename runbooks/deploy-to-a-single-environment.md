---
tags:
  - auto-deploy
---
# How to deploy to a single environment?

This runbook aims to cover the use cases where our coordinated pipelines
are not functioning correctly but an urgent deployment needs to bypass the
failure. In this case, you would manually deploy a specific package to an environment.

## Pre-deploy considerations

* Packages on `gstg` and `gprd` should be kept in sync, if you need to manually deploy
to one of these environments, it'd be required to also deploy to other one as well.
* When deploying to a canary environment, make sure [canary is undrained] so
that QA tests are accurately using the canary stage.

## Deploying to a single environment

1. Identify the auto-deploy package version you wish to deploy. You can grab the package
  from the `#announcements` channel.
2. Use chatops to deploy to a single environment:

| Environment | ChatOps command |
| --- | --- |
| staging           | `/chatops run deploy 14.9.202203101220-571badda7c4.4e8301d17e3 gstg` |
| staging-canary    | `/chatops run deploy 14.9.202203101220-571badda7c4.4e8301d17e3 gstg-cny` |
| production        | `/chatops run deploy 14.9.202203101220-571badda7c4.4e8301d17e3 gprd` |
| canary-production | `/chatops run deploy 14.9.202203101220-571badda7c4.4e8301d17e3 gprd-cny` |
| pre               | `/chatops run deploy 14.9.202203101220-571badda7c4.4e8301d17e3 pre`|
| release           | `/chatops run deploy 14.8.3-ee.0 release`

[canary is undrained]: https://about.gitlab.com/handbook/engineering/infrastructure/environments/canary-stage/#re-enabling-canary-stage-in-an-environment
