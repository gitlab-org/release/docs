---
tags:
  - auto-deploy
---
# How to speed up the auto-deploy process for urgent merge requests?

Specific situations such as mitigating incidents or unblocking the security release,
could require for a specific merge request (most likely a revert) to be deployed as
quickly as possible. Instead of waiting for a new auto-deploy branch to be created,
which could be time-consuming, there are some steps the release manager can follow to
speed up the auto-deploy process:

## For canonical and security merge requests

1. Cherry-pick the merge request into the latest auto-deploy branch by
   adding `~Pick into auto-deploy` and the `severity::2` or higher labels.
1. Trigger the `MANUAL auto-deploy pick&tag - ⚠️ KEEP INACTIVE!!` [pipeline schedule].

Even if the manual schedule is not activated, the `auto_deploy: Create new package` schedule will take
care of picking the change and tagging during its regular run (every 15 minutes).

This will start building a package containing the MR. Once the package is ready, a deployment pipeline
should start automatically for it.

To start a deployment pipeline immediately, see [How do I start a deployment pipeline?].

Refer to the [handbook on the pick label] for up-to-date information.

## Once the deployment pipeline is running

In cases of high severity incidents (S1 or S2) where a fix must be deployed to production to mitigate customer impact,
it is possible to skip the [baking_time](https://gitlab.com/gitlab-org/release-tools/-/blob/cce10a5490e703f04f439a5eaefdeb284ed536a0/.gitlab/ci/coordinated-pipeline.gitlab-ci.yml#L933)
30 minute delay that runs after the `gprd-cny` deployment.

This process should only be considered in the following scenarios:

- The merge request is a revert merge request - No new application code is being added.
- The auto-deploy package only contains the revert merge request - No new application code is being deployed.
- Rollbacks, disabling canary, and hot-patching are not viable options.

For validation purposes, checking the `gprd-cny` error rate is a good place to start,
and you should only proceed if the benefits of the change outweigh the risk.

1. To stop the active timer of the delayed `baking_time` job, select `Unschedule` on the job.
1. Select `Run` to manually start the job without waiting for the delay to finish.

[pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[handbook on the pick label]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-pick-label
[GitLab security project]: https://gitlab.com/gitlab-org/security/gitlab/-/branches?state=all&sort=updated_desc&search=auto-deploy
[cherry-pick UI on the security merge request]: https://docs.gitlab.com/ee/user/project/merge_requests/cherry_pick_changes.html#cherry-pick-all-changes-from-a-merge-request
[How do I start a deployment pipeline?]: ../general/deploy/overview-of-the-auto-deploy-pipelines.md#how-do-i-start-a-deployment-pipeline-for-a-particular-package
