---
tags:
  - auto-deploy
---

# Code structure for triggering deployment pipelines

The entry point is the `AutoDeploy::DeploymentPipeline::Service` class. When a deployment pipeline
needs to be triggered, this class should be called.

Diagram showing the interaction of classes in the `AutoDeploy::DeploymentPipeline` module:

```mermaid
sequenceDiagram
  Service->>Sequencer: Check if a new deployment pipeline can be started. Helps avoid pipelines overlapping each other.
  Sequencer->>Service: Returns boolean indicating if a pipeline can be started
  box PackageSelection module
    participant VersionSelector
    participant Latest
  end
  Service->>VersionSelector: Select a package
  VersionSelector->>Service: Returns ProductVersion to be deployed
  Service->>Latest: Select a package
  Latest->>Service: Returns ProductVersion to be deployed
  Service->>Trigger: Pass the ProductVersion obtained in the previous step to the Trigger class for triggering a deployment pipeline
```

## Determining if a new deployment pipeline can be started

The `Service` class first calls `Sequencer` to check if it is an appropriate time to start
a new deployment pipeline. `Sequencer` checks if the previous deployment pipeline has
completed deploying to `gprd-cny`. If the previous pipeline has completed deploying to `gprd-cny`,
a new pipeline can be started.

If the previous deployment pipeline failed or was canceled before deploying to gprd-cny, `Sequencer`
will prevent a new pipeline from starting unless 90 minutes have passed since the start of the previous
pipeline. This is similar behavior to the current process where coordinator pipelines are started on
a schedule, approximately every 2 hours.

## Choosing a package to deploy

Depending on the arguments passed to the `Service` class, it will then call one of the classes
in the `PackageSelection` module.
- `PackageSelection::VersionSelector` - Takes a `Version` argument, checks if it is suitable and returns a `ProductVersion`.
- `PackageSelection::Latest` - Finds the latest version that is suitable for deployment.

Classes in the `PackageSelection` module call the `DeploymentPipeline::Version` class to check if a
version is suitable for deployment.

A version is considered suitable if it satisfies the following conditions:
- It is a valid auto deploy version
- The Omnibus and CNG packager pipelines for the version have completed
- The version is newer than the version of the latest deployment pipeline

The classes in the `PackageSelection` module return a `ProductVersion` (or `nil`). The `Service`
class then calls the `Trigger` class to actually trigger a new deployment pipeline.

## Triggering a new deployment pipeline for the selected package

The Trigger class takes a `ProductVersion` argument and uses the [create pipeline API](https://docs.gitlab.com/ee/api/pipelines.html#create-a-new-pipeline)
to trigger a new deployment pipeline for the given version.
