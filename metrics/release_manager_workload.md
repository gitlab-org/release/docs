---
tags:
  - auto-deploy
  - release-management
---
## Release Manager workload metrics

The Release Manager workload metrics aim to track and measure the amount of work involved with different release manager activities. Tracking these numbers over time will allow us to plan improvements to our tools and processes. [Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/744) related to the creation of these measures.

This guide aims to document how the different metrics are tracked.

Release Manager workload metrics spreadsheet - all data is collected in https://docs.google.com/spreadsheets/d/1xENgrQwAQkA3ImtxsnqgQYEGxxevbUeNhXLFRUUKayk/edit#gid=1820673269

### Release Manager workload - Deployments
https://docs.google.com/spreadsheets/d/1xENgrQwAQkA3ImtxsnqgQYEGxxevbUeNhXLFRUUKayk/edit#gid=953768711 tracks the amount of work involved with deployments each week. A weekly tracking was selected to line up with our existing tracking of blockers on the [Deployment blockers epic].

Deployments are tracked in two sections:
1. A view of the current process. These numbers should help us to plan improvements to the deployment process. Measures in this section:

    1. End to end process duration (hours): currently an estimate based on the number of stages in the deployment pipeline
    2. Number of manual steps (on the process): number of expected actions a Release Manager will need to take to complete a deployment
    3. Active time required to complete the typical end-to-end process (in hours): an estimate of the amount of time needed to complete the manual steps from the above count

2. A view of events that affected deployments. These numbers should help us to find patterns in the blockers that interrupt the auto-deploy process

    1. Number of times the process kicked off: a count of the number of packages promoted to production. If this metric is tracked at the start of the week we can use the [Thanos query](https://thanos.gitlab.net/graph?g0.expr=sum%20by(target_env)(increase(delivery_deployment_completed_total%7Btarget_env%3D%22gprd%22%7D%5B1w%5D))&g0.tab=1&g0.stacked=0&g0.range_input=2w&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) to count, otherwise Slack is the easiest way to count the number of promotions (Search `#announcements` for gprd started messages between the required dates)
    2. Active time spent on manual steps (hours per week): Number from 1.2 ("Number of manual steps (on the process)" x 2.1 (Number of times the process kicked off) to give the amount of time we spend on expected manual steps
    3. Number of deployment failures - Count from https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/873 to measure the number of failures in the deployment pipelines
    4. Total time deployments were blocked for (hours per week): Number of hours deployments were blocked for. The number is taken from https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/873
    5. Number of failed jobs: Number taken from https://dashboards.gitlab.net/d/delivery-release_management_toil/delivery-release-management-toil?orgId=1. This number allows us to track jobs that failed but succeeded on a retry
    6. Total time spent on failures (hours per week): Numbers are Production Deployment blocker numbers from https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/873 plus 1hr per failure to track admin tasks including issue updates, adding tracking labels, Grafana annotations

### Release Manager workload - Post-deploy migrations
https://docs.google.com/spreadsheets/d/1xENgrQwAQkA3ImtxsnqgQYEGxxevbUeNhXLFRUUKayk/edit#gid=42408884 tracks the amount of work involved with running post-deploy migrations each week.

Post-deploy migrations are tracked in two sections:
1. A view of the current process. These numbers should help us to plan improvements to the deployment process. Measures in this section:

    1. Number of manual steps (on the process): Number of expected actions a Release Manager will need to take to complete a post-deploy migration pipeline
    2. Active time required to complete the typical end to end process (in hours): An estimate of the amount of time needed to complete the manual steps from the above count

2. A view of active amount of time involved with running the pipeline and handling failures. These numbers should help us to find patterns in the blockers that interrupt the deployment process

   1. Number of times process kicked off (per week): A count of the number of time the PDM pipeline was triggered in the specific week. Counted via Slack using the "post-deploy-migrations started" messages in the [#announcements] slack channel
   2. Active time spent on manual steps (hours per week): Calculated as Active time required to complete the typical end to end process (in hours) (Number from 1.2) * Number of times process kicked off (per week) (Number from 2.1). Removing manual steps from the process would reduce effort and affect this value
   3. Number of post-deploy migration failures (per week): Count from [Deployment blockers epic] to measure the number of failures in the pipelines
   4. Total time spent on post-deploy migration failures (hours per week): Calculations is total time blocked (according to the Deployment blockers) + 1hr per failure to track incident admin including issue updates, adding tracking labels, Grafana annotations

### Release Manager workload - Monthly release
https://docs.google.com/spreadsheets/d/1xENgrQwAQkA3ImtxsnqgQYEGxxevbUeNhXLFRUUKayk/edit#gid=163949445 tracks the amount of work involved with preparing the Monthly release.

Effort is tracked using the following data:
1. Start date (Candidate commit announced): Indicating the beginning of the monthly release preperation steps. Measured using the "Candidate commit" announcement in Slack.
2. Date preparation complete: The date that we completed the work to tag and test a package. This is usually the working day before the release day but the specfic date is being tracked in case we see changes over time
3. [Release date](https://about.gitlab.com/releases/): The date the monthly release package is published.
4. Number of working days spent on the release: Counts the number of working days between the start date and the date preperation complete to measure the number of days spent prepering the release.
5. Number of manual steps (on the process): The completed checkbox count shown on the monthly release issue. This number will be used to track the manual complexity of the process
6. Total hours spent on release preperation: Used as the overal measure of monthly release effort. Counted as the total hours between the Start time (1) and Date preparation complete (2)

In this iteration we aren't tracking the effort required to publish the monthly release. Although effort, this is a more predictable task that can be added to a future iteration of the Release Manager workload metrics.

### Release Manager workload - Patch release
https://docs.google.com/spreadsheets/d/1xENgrQwAQkA3ImtxsnqgQYEGxxevbUeNhXLFRUUKayk/edit#gid=93735038 tracks the amount of work involved with preparing a patch release.

Effort is tracked in three sections:
1. Process overview: tracking measures that indicate how efficient the overall process is. This section includes:

    1. Process start date: Date that the patch release preparation began. The patch release preparation creation date is used where possible, or if the issue is created but not immediately used, we use the date that the First steps began
    2. Publish date: The date that the release was published. Taken from the patch release blog post
    3. Number of working days spent on the release: The number of working days between the dates in measures 1 and 2. This number shows how much release manager time was needed for the patch release
    4. Number of manual steps (on the process): The completed checkbox count that is shown on the patch release preparation issue. This number will be used to track the manual complexity of the process
    5. Is the patch release within the current maintenance policy?:  To be used to help us connect effort to backport requests
    6. Wall time of process (hours per release): Number of working hours between the process start time and the patch release completion time. Both times are taken from the patch release issue activity log to track the overall number of hours needed to prepare a patch release

2. Size of release: This section may help us connect the amount of effort to the number of fixes we're working with

    1. Number of changes included in release: Tracked from the Patch release blog post to give a number fo fixes included in the release

3. Active time tracking: Tracking the amount of time Release Managers are actively working on the release

    1. Active time spent on release preparation steps (hours): Estimated number of hours based on activity log on the release preparation issue
    2. Active time spent on release packaging (hours): Estimated using Slack to track the first step (checking mirror status) and the activity log to track the last step - Estimated because on activity log on the release preparation issue
    3. Active time spent on release publishing (hours): Using Slack to track the publish task and the activity log on the release preparation issue to track the final step
    4. Total active time spent on manual steps (hours per release): Sum of the hours tracked in 1, 2, 3.

### Release Manager workload - Security release
https://docs.google.com/spreadsheets/d/1xENgrQwAQkA3ImtxsnqgQYEGxxevbUeNhXLFRUUKayk/edit#gid=1709173347 tracks the amount of work involved with preparing a security release.

Effort is tracked across three sections:
1. Process overview: tracking measures that indicate how efficient the overall process is. This section includes:

    1. Process start date: Date that the release preparation began. The release preparation creation date is used where possible, or if the issue is created but not immediately used, we use the date that the First steps began
    2. Publish date: The date that the release was published. Taken from the release blog post
    3. Number of working days spent on the release: The number of working days between the dates in measures 1 and 2. This number shows how much release manager time was needed for the release
    4. Number of manual steps (on the process): The completed checkbox count shown on the security release preperation issue. This number will be used to track the manual complexity of the process
    5. Number of special cases included in release (e.g. Git fixes or similar): Likely to include things like Git fixes and fix reverts, plus any other special condition that might increase the effort involved in preparing the release. A note will usually be included on the spreadsheet to give details
    6. Critical security release process used? Yes or No. Used to track the difference in effort between the two processes
    7. Wall time of process (hours per release): Number of working hours between the process start time and the release completion time. Both times are taken from the Security release issue activity log to track the overall number of hours needed to prepare a security release

2. Size of release: tracking the number of fixes involved with this release. The number and pattern of these fixes may match other patterns of effort and could help us plan process improvements

   2. Number of security fixes released: Number taken from the release blog post
   3. Number of non-security fixes released: Number taken from the release blog post. This number might help to track additional complexity coming from including non-security fixes in a security release

3. Active time tracking: tracking the time taken to complete each stage of the release. Due to the manual tracking of these numbers they cannot be fully accurate but they are good enough to show the most time-consuming parts of the process so we can improve.

    1. Active time spent on First Steps phase of release (hours): The time taken to complete all steps in the release issue First Steps section. The number is tracked using the Security release issue Activity Log
    2. Active time spent on the Early merge phase of release (hours): An early merge is assumed to take 2 hours per day to cover the merge plus any ad-hoc manual checking of associated issues. This measure counts 2hrs for each day that we're running the early merges. In the current process, the early merge phase runs from the first day of the release process through to the day before the publish date
    3. Active time spent on Merging backport phase of release (hours): The time taken to complete all tasks in the One Day before Due section of the security release preparation issue. The number is tracked using the Activity log on the Security release issue
    4. Active time spent on release tagging (hours): Calculated using a combination of Slack for the start time and the Activity Log time on the security release issue as the end time
    5. Active time spent on release publishing (hours): Calculated using a combination of Slack for the start time for publishing and the Activity Log time on the security release issue as the end time (currently the time the last version is created)
    6. Active time spent on post-publishing steps phase of release (hours): The time taken to complete all tasks in the Final Steps section of the security preparation issue. Tracked using the Activity Log on the security release issue
    7. Active time spent on failures (hours per release): An additional measure that can be used to track failures that take place outside of the other measures. Examples include long-running broken master problems that delay preparation without being directly related to a specific release task or unrelated incidents
    8. Total active time spent on manual steps (hours per release): Total time from numbers tracked in 1-6
    9. Total Active time spent working on the release (total active time spent on manual steps + Active time spent on failures): Total time for numbers tracked in 1-7 (all effort plus failures)


[Deployment blockers epic]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/873
[#announcements]: https://gitlab.slack.com/archives/C8PKBH3M5
