---
tags:
- auto-deploy
---
# Overview

Sometimes, we have risky changes being merged, which we would like to deploy in an
isolated manner so that the cause of any failure can be quickly identified.

Examples of such changes:

- <https://gitlab.com/gitlab-org/gitlab/-/merge_requests/65201>
- <https://gitlab.com/gitlab-org/gitlab/-/merge_requests/69655>
- <https://gitlab.com/gitlab-org/gitlab/-/issues/437948>

## Process to follow for deploying risky MRs in isolation

1. Determine the DRI (and team) for the change and coordinate the time for merging.
Merging can be performed by a release manager.

1. [Deactivate auto-deploy tasks](stopping-auto-deploy.md#temporarily-stopping-automated-deployments) with
`/chatops run auto_deploy pause`.

1. Decide on appropriate time to do the deploy and how long we should monitor.  Make sure to account for peak/higher traffic times.  

1. Merge the MR and set labels `pick into auto-deploy` and `severity::2`.

1. Follow the [speed up the auto-deploy runbook](../../runbooks/how_to_speed_up_auto_deploy_process_for_urgent_merge_requests.md).

1. Inform the EOC and the DRI about the coordinated pipeline deploying the change.

1. Once the gstg-cny rollout begins, enable auto-deploy tasks with `/chatops run auto_deploy unpause`.

1. Make sure that the last coordinated pipeline before the one containing the MR is making it into gprd, so that we don't have any other undeployed changes.

1. Watch the coordinated pipeline and ping the DRI and/or the team responsible in case of any problems.

1. Pause PDM Automation / Make sure to not run PDM until the agreed upon baking/observation time has passed.