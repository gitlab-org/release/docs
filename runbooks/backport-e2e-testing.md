---
tags:
  - release-management
---
# E2E Testing Backports

## Reasoning

E2E pipelines test the functionality of a backport Merge Request (MR).

### Failing tests

When the pipeline of a backport MR is ran, it runs E2E pipelines such as `e2e-test-on-gdk`
and `e2e-package-and-test`. These are then reported to the section "E2E Test Result Summary"
with generated Allure test reports.

If there are failures, reach out to [Developer Experience](https://gitlab.enterprise.slack.com/archives/C07TWBRER7H)
and request the [DRI engineer](https://gitlab.com/gitlab-org/quality/pipeline-triage#dri-weekly-rotation-schedule)
to look into the failures.

They might be failing because of a bug/issue in the codebase, in which case the fix is usually
cherry-picked into the MR.

Otherwise, if the failure is due to a stale test that was later fixed, we make a decision whether
that is a risk or if we can proceed.