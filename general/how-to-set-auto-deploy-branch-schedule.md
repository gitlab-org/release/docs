---
tags:
  - auto-deploy
---
## How to set the auto-deploy branch schedule?

When starting a Release Manager shift, one of the first things to do is configure
the auto-deploy branch schedule. Well-timed branch creation reduces release manager waiting
and helps us keep [MTTP] low through frequent deployments. The following are a set of
considerations and recommendations to guide upcoming Release Managers when doing so.

### Considerations:

* Auto-deploy schedule should be adjusted based on the Release Manager's work hours.
* This is a shared task between the EMEA, AMER and APAC (if one exists) Release Managers.
  When setting the schedule working hours for all time zones should be considered.
* Release Managers should aim to do as many deployments to production as possible in
  a day. In general, Release Managers should aim for at least 3 deployments to production
  per day: One in EMEA, another in AMER and another in APAC. Note that the one in APAC
  depends if the Delivery team has availability in that timezone.
* Smaller packages with fewer commits are easier to deploy and manage than large packages
  with hundreds of commits. With this in mind, gaps of 5 or more hours between auto-deploy
  branches should be avoided.

### Recommendations

* First branch should be created roughly 3 hours before the Release Manager starts
  their day. For example, if a Release Manager starts their day at 9UTC, the first branch should
  be created around 6UTC. That way, there will be a package ready to promote to production when
  the Release Manager starts their shift.
* The second branch should be set when the Release Manager starts their day. This way, the second package
  should be on its way to staging/canary when the first package is finishing the deployment
  to production.
* Upcoming branches should be created two or three hours apart to mirror the same pace as the
  first and second package.

### How to set the auto-deploy branch schedule?

1. Modify the `AUTO_DEPLOY_SCHEDULE` [variable for `release-tools` in OPS] using a comma separed list of hours in UTC timezone.
1. Update the [Releases handbook documentation] with the new schedule ([example])

[variable for `release-tools` in OPS]: https://ops.gitlab.net/gitlab-org/release/tools/-/settings/ci_cd
[example]: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92305
[MTTP]: https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#mean-time-to-production-mttp
[Releases handbook documentation]: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/engineering/deployments-and-releases/deployments/index.html.md#gitlabcom-deployments-process
