---
tags:
  - release-management
---
# Monthly release

The monthly release goal is to ship a release package for self-managed users. GitLab releases a new version (`XX.YY.0`) every month
on the [release date]. This release is a semver versioned package containing changes from many successful deployments on GitLab.com.

The history and reasoning behind the monthly schedule can be found on the [GitLab blog].
## Overview

The monthly self-managed release timelines are concentrated around the [release date], this section provides an overview of
the end-to-end process for monthly releases

![monthly release diagram](./images/monthly_release_diagram.png)

[Source] - Internal link only

1. **First steps** - Initial steps to setup the monthly release, including setting up the release schedule and the deployment cadence.
2. **GitLab.com deployments** - From the start of the milestone up to one week before the [release date], GitLab.com receives multiple
 deployments per day. For application changes to be considered in a self-managed release they need to be successfully deployed to GitLab.com.
3. **Candidate commit** - One week before the release date a candidate commit is selected and broadcasted by [release managers], the commit is usually
  the current commit running on GitLab.com. Pending GitLab.com availability, a different commit might be choosen.
4. **Release candidate** - A test release candidate (RC) is created. The release candidate package is built, tested and deployed to the pre environment.
    A successful outcome indicates this package can be used as the final version.
5. **Tag** - Release managers tag the final version of the release based on the release candidate.
6. **Release** - On the release day, the release packages are published.

For the current information and status of the active monthly release, have a look at the [Grafana dashboard "delivery: Release Information"](https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1).

For further details about this process have a look at the [releases] and the [deployment and releases] pages.

## Links

- [Maintenance Policy](https://docs.gitlab.com/ee/policy/maintenance.html)
- [Release Information Grafana dashboard](https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1)
- [Auto deploy process](../deploy/auto-deploy.md)
- [Release Candidates](../release-candidates.md)
- [Prepare Merge Requests](../picking-into-merge-requests.md)
- [Cherry Picking](../picking-into-merge-requests.md)
- [Tagging and Releasing](../omnibus-packages/publishing-packages.md)
- [Deployment Documentation](../deploy/gitlab-com-deployer.md#CI_Pipeline_for_Deployment)

[release date]: https://about.gitlab.com/releases/
[release managers]: https://about.gitlab.com/community/release-managers/
[releases]: https://about.gitlab.com/handbook/engineering/releases/
[deployment and releases]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/
[GitLab blog]: https://about.gitlab.com/blog/2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab/
[Source]: https://docs.google.com/presentation/d/1YRjA1dYCXNXp06VltDYlik1MdFyzUvaeXKk69mMPcA4/edit#slide=id.g2951f7d5d31_1_0