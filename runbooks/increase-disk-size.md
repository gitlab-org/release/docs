---
tags:
  - auto-deploy
---
# Increase disk size

This runbook shows how to increase disk size of a virtual machine (VM) running
on GCP managed by GitLab Infrastructure Team.

Some examples:

- Deploy nodes
- Gitaly nodes

## Prerequisites

- The VM runs stand-alone, not a part of an auto-scaling group or managed by another GCP service.

## Increase boot disk size and resize the root partition

Follow the official [GCP
documentation](https://cloud.google.com/compute/docs/disks/resize-persistent-disk#console).
Make sure to execute the two steps:

- [Increase the size of a
  disk](https://cloud.google.com/compute/docs/disks/resize-persistent-disk#increase_the_size_of_a_disk)
- [Resize the file system and
  partitions](https://cloud.google.com/compute/docs/disks/resize-persistent-disk#resize_partitions)

## Update Terraform code

The infrastructure configuration is managed by Terraform in [Config
Management](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt) repository.
For example, `pre` environment configuration is managed in
[`env/pre`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/tree/main/environments/pre?ref_type=heads).

The disk size parameter is ignored, to avoid node recreation ([module
link](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group/-/blob/2b33ce6860ff6f345909556b7d9fbdfed853d72d/instance.tf#L162-168)).
That why it was increased manually in the previous step [Increase boot disk size
and resize the root
partition](#increase-boot-disk-size-and-resize-the-root-partition). The
configuration still needs to be updated to keep the consistency between code and
the actual infrastructure.

Update or add the
[`os_disk_size`](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group/-/blob/c2adbd4d4507d705c7135b38129907a0165fe5f8/README.md#L107)
parameter to the instance's module. Follow this [example merge
request](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/10280).
