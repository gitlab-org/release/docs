---
tags:
  - auto-deploy
---
# Resolving QA failures

## Overview

QA smoke and reliable specs are run as part of the auto-deploy pipeline - this means they are run regularly and can be assumed to be stable.
A breakdown of specs executed per environment can be seen on the [QA test pipelines] documentation.

This document describes the process to follow if QA tests are failing on the deployment or release pipelines.

## Process Overview

Resolving failing QA tests involves three parts:

1. A release issue to track & collaborate on the failure: Failing QA tests are always tracked using an issue in the [release tracker].
    1. Release issues give us a record of the failure so we can spot patterns and escalate re-occurring problems.
    2. We don't initially begin with an incident because the Quality DRIs can resolve many problems without us needing to page the EOC. We can upgrade to an incident if we later decide we need a wider group to investigate or resolve the failure.
2. Reviewing the appropriate QA Slack channel to see if someone is already investigating the failure: For context, there's a quality Slack channel per environment:
    * [`#e2e-run-staging`](https://gitlab.enterprise.slack.com/archives/CBS3YKMGD): List pipelines for `gstg-cny` and `gstg`.
    * [`#e2e-run-production`](https://gitlab.enterprise.slack.com/archives/CCNNKFP8B): List QA pipelines for `gprd-cny` and `gprd`.
    * [`#e2e-run-release`](https://gitlab.enterprise.slack.com/archives/C0154HCFLRE): List QA pipelines associated with the release environment.
    * [`#e2e-run-preprod`](https://gitlab.enterprise.slack.com/archives/CR7QH0RV1): List QA pipelines associated with the pre environment.
3. Pinging the Quality on call DRI using the [Quality on call schedule] to ask them to investigate the failure: Quality are here to help us investigate and solve any failures. You can use the release tracker issue to share error messages and collaborate on a solution.

Some test failures will pass with a retry. Take care of simply retrying all failing jobs without also reporting the problem in a release issue, otherwise, the cause of the flakiness probably won't be fixed.

## Process steps to follow

When a QA job fails in any of our environments (`gstg-cny`, `gstg`, `gprd-cny`, `gprd`, `pre` or `release`)

1. An issue will be automatically created in the [release tracker] and assigned to release managers.
1. Check the `#e2e-run-<env>` Slack channel, the failure may already be known and being worked on.
1. If not known, ping the on call Quality DRI on a failing test Slack message and ask
   for assistance on the issue. To know who is the engineer on-call use:
    * Execute `/chatops run quality dri schedule` on Slack, or,
    * Navigate to the [Quality on call schedule].
1. The Quality engineer on-call will analyze the failure and suggest next steps. Release managers can also help, see the
   "Fixing QA failures from the Release manager side" section below.
1. If the failure appears to be code or environment-related, [declare an incident] with the correct [availability severity] and [Delivery impact] label.
    * Link to the release issue created in step 1 and close the release issue to reduce duplication.
    * Work with the EoC and the Quality DRI to resolve the problem.
1. Once tests are passing again:
    * Update the release, or incident issue with a summary of the failure.
    * Apply the `RootCause::*` label to track the root cause of the failure.
    * Apply the `deploys-blocked-gprd::*` and `deploys-blocked-gstg::*` labels to track the amount of time deployments were blocked for.
    * Close the issue.
    * Add an annotation to the [Auto deploy packages dashboard] to track the QA failure.

## Troubleshooting QA failures from the Release manager side

The following are a list of steps that a release manager can use to
help fixing a broken QA and unblock the auto-deploy process.

**Note**: Quality owns the process of analyzing, determining the root cause and fixing a broken QA spec. If inclined,
Release managers can help to speed up this process, but this is not expected of them.

Broken QA jobs can be caused by:

1. [A feature flag](#feature-flag).
1. [Code that is no longer compatible with the QA spec](#code-is-no-longer-compatible-with-the-qa) due to a bug or a change in the feature.
1. [QA users being blocked](#a-users-being-blocked).
1. A environment/infrastructure failure. In this case, an an [incident will need to be reported](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident).
1. [Database timeouts on staging](#database-timeouts-in-staging)
1. [Faulty quailty specs](#faulty-quality-specs)

### Feature flag

This is the quickest way to fix a broken QA job. To determine if a QA spec is failing because of
a feature flag:

1. Search for feature flags enabled or disabled around the time of the failures. To search for feature flags
   recently toggled:
    * Take a look at the [feature flag log tracker], or,
    * [Feature flags logs], or
    * Check `#qa-staging` or `#qa-production` Slack channels. End-to-end pipelines
    are triggered when feature flags are toggled. Example

  ![Qa failure example](./images/qa_failure_feature_flag.png)

2. If a feature flag is found, restore the feature flag to the previous state. Optionally, notify the FF author about this,
   but do note their authorization is not required for flipping the feature flag state.
   Switching the feature flag can be done via Slack comand:
   * For staging: `/chatops run feature set <feature_flag_name> <previous_state> --staging`, e.g. `/chatops run feature set foo false --staging`
   * For production: `/chatops run feature set <feature_flag_name> <previous_state>`, e.g. `/chatops run feature set foo false`.
3. Pending the environment, fhe feature flag should be toggled in the `#staging` or the `#production` Slack channel.
4. Retry the QA job.
5. If the QA job succeeds, notify feature flag author and let them know about the problem.
6. If the QA job did not succeed, restore the feature flag to the original state.

### Code is no longer compatible with the QA

In this case, the code should be reverted and re-deployed to the environment to unblock deployments.
Once a merge request has been identified as the culprit:

1. Proceed to revert it. On the merge request widget, click on the `Revert` link and select the default branch.
   This will create a revert merge request

![Revert link](./images/revert_link.png)

2. On the revert merge request, add the `pipeline::revert` label, this will create a shorter pipeline.
3. Go to `#backend_maintainers` or `#frontend_maintainers` Slack channel and ask for an approval
4. Once merged, the merge request will be included in the next auto-deploy branch.
   * Alternatively, you can use `Pick into auto-deploy` to [speed up the deployment process].

### A users being blocked

From time to time, QA users are blocked causing the QA specs to fail. To validate if a QA user has been blocked,
open up the QA job that failed and search for the `"You're account has been blocked"` error. Users are automatically unblocked
after some minutes, so in this case, wait for at least 15 minutes and retry the job. If the error persists,
this should be escalated to Quality on-call or the SRE on-call.

Investigation about this failure can be found on the [anti-abuse issue].

### Database timeouts in staging

Database timeouts on staging can be caused by the existence of stale records in the Quality test group. An alternative to mitigate this problem
is to perform a [manual clean-up of records] on the test group. If a database statement is suspected to be the root cause of the failures:

1. Determine the [Quality test group](#how-to-determine-the-quality-test-group) being used.
1. Notify the Quality engineer on-call about this and request them to run a clean-up on the dedicated group.
   * The clean-up usually takes around 1h.
1. After the clean-up finishes retry the job.

Note the clean-up solution is limited to the staging environment, database timeouts on production will likely require
a [production incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident)

### How to determine the Quality test group?

There are two ways for determine which Quality group is being used on the quality specs:

1. Have a look at the quality logs:

```
[Aug 23 2023 16:00:07 UTC (QA Tests)] INFO  -- ==> Retrieved a QA::Resource::Sandbox with full_path 'gitlab-qa-sandbox-group-4' via api in 0.19 seconds
[Aug 23 2023 16:00:07 UTC (QA Tests)] INFO  -- ==> Retrieved a QA::Resource::Group with full_path 'gitlab-qa-sandbox-group-4/qa-test-2023-08-23-15-55-33-247495c79dee5e65' via api in 0.57 seconds
```

From the above, we can infer the `gitlab-qa-sanbox-group-4` is used.

2. Quality specs are configured to the group based on the day, with Sunday being 1, so if today is Thursday, the group will be `gitlab-qa-sanbox-group-5`

### Faulty quality specs

Quality specs can be deemed to be faulty by a Quality engineer. These defective specs
can be [fast-quarantined] to unblock deployments, for that:

1. Ask the Quality engineer on-call to open up a merge request to fast quarantine
   the spec ([example](https://gitlab.com/gitlab-org/quality/engineering-productivity/fast-quarantine/-/merge_requests/90)).
1. After the merge request is merged in the Fast Quarantined repository, retry the QA job.
1. The QA job should succeed.

## Additional troubleshooting

QA tests are run against the live environments. So if a job is retried from a pipeline, it will be run against whichever version
was most recently deployed to that environment. This means that by retrying a job on an older pipeline, the test code being run will be
from that pipeline, but the application code deployed to the environment may be from a newer pipeline. A test's success or failure should
only be trusted if it is run against the application code from it's same version.

[QA test pipelines]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/#qa-test-pipelines
[release tracker]: https://gitlab.com/gitlab-org/release/tasks/-/issues
[Handling Deploy Failures runbook]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/failures.md#handling-deployment-failures
[availability severity]: https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability
[Delivery impact]: https://about.gitlab.com/handbook/engineering/releases/#delivery-impact-labels
[declare an incident]: https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident
[Quality on call schedule]: https://gitlab.com/gitlab-org/quality/pipeline-triage/#dri-weekly-rotation-schedule
[feature flag log tracker]: https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/-/issues/?sort=updated_desc&state=closed&first_page_size=20
[Feature flags logs]: https://nonprod-log.gitlab.net/goto/7a9df4f0-6b3e-11ed-9af2-6131f0ee4ce6
[speed up the deployment process]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-pick-label
[anti-abuse issue]: https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/119
[Auto deploy packages dashboard]: https://dashboards.gitlab.net/d/delivery-auto_deploy_packages/delivery-auto-deploy-packages-information?orgId=1&refresh=5m
[manual clean-up of records]: https://gitlab.com/gitlab-org/quality/runbooks/-/tree/main/cleanup_test_resources#cleanup-projects-and-groups-in-live-environments
[fast quarantined]: https://gitlab.com/gitlab-org/quality/engineering-productivity/fast-quarantine
