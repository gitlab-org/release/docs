---
tags:
  - auto-deploy
---
### Working on a high severity issue

When working on a high severity ~S1 issue, the initial work on a [post-deployment patch](utilities/post_deployment_patch.md)
may be done before any other tasks in the normal workflow at the request of the security engineer,
with the review of the delivery team and infrastructure team as outlined in the post-deployment patch process.
