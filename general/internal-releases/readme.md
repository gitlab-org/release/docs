---
tags:
  - release-management
---
# Internal releases

Internal releases represent a private GitLab strategy for delivering high-severity fixes to single tenant SaaS
instances like GitLab Dedicated before public disclosure, without disrupting the regular release process.

For complete details, please refer to the comprehensive documentation: ["Internal releases for GitLab SaaS single
tenant instances"](https://gitlab.com/handbook/engineering/architecture/design-documents/internal-releases/).

## Key resources

For complete information on the internal releases strategy for GitLab Dedicated and other SaaS single tenant
instances, please refer to the following:

* **Hanbook**: ["Internal releases for GitLab SaaS single tenant instances"](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/internal_releases/)
* **YouTube explanations**: [Part 1](https://youtu.be/YstSC_SBpOQ?t=1131), [Part 2](https://youtu.be/KIePvjv81O4)

## Related epics

* Define prerequisites to GitLab internal releases: [Epic #1292](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1292)
* Decouple auto-deploy: [Epic #1050](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1050)
* Ability to create internal packages to remediate SaaS Single Tenant: [Epic #1373](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1373)
* Introduce internal release process: [Epic #1201](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1201)

## Characteristics

### Package notation

An Internal release notation has the format `17.2.7-internal9` with:

* `17.2.7` is the last patch release available of 17.2
* `internal` is the notation indicating this is an internal release version
* `9` is the internal version, start from `0`. Between two patch releases or a patch release and a monthly
 release, there can be one or more internal release versions, e.g. between 17.3.2 and 17.3.3, there can be
 17.3.2-internal0 and 17.3.2-internal1

#### Git Diagram

```mermaid
%%{
    init:
    {
        'theme': 'base',
        'gitGraph':
        {
            "mainBranchName": 'security/17-3-stable'
        }
    }
}%%
gitGraph
    commit id: "Version v17.3.2" tag: "v17.3.2"
    commit id: "bug fix 1"
    commit id: "bug fix 2"
    branch critical-fix-1
    checkout critical-fix-1
    commit id: "critical fix 1"
    checkout security/17-3-stable
    merge critical-fix-1 tag: "v17.3.2-internal.0"
    commit id: "bug fix 3"
    branch critical-fix-2
    checkout critical-fix-2
    commit id: "critical fix 2"
    checkout security/17-3-stable
    merge critical-fix-2 tag: "v17.3.2-internal.1"
    commit id: "bug fix 4"
    commit id: "bug fix 5"
    commit id: "Version v17.3.3" tag: "v17.3.3"
```

Related issue: [Proposal: Define the internal release package notation](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20501)

### Tagless

Internal release packages are built using the tagless approach, which mean unlike patch release and monthly
release, there is no Git tag used to indicate the version.

Related issues:

* [Understand the tagless process for internal release](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20565)
* [Comparative between tag and tagless approach for internal releases](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20580#note_2160006599)

### Available backport versions for an internal release

Considering N, N-1 and N-2 are the three minor versions within our [maintenance
policy](https://docs.gitlab.com/ee/policy/maintenance.html), Internal release is done for N-1 and N-2.

*Example*: On time date 2024-10-28, 17.6 is the active version, the three minor versions within our
maintenance policy are 17.5, 17.4 and 17.3. Thus, in case internal releases is needed today, they would be done for
17.4 (N-1) and 17.3 (N-2).

> Note: Why N is not supported at the moment: N is required in case _an internal release happens after a patch
> release and before the next monthly release_. However, if the internal release happens between a patch release
> and a monthly release, [it falls into the case of Unplanned critical patch
> release](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/security/mitigate-gitlab-environments-out-of-cycle.md#what-if-the-monthly-release-is-scheduled-before-the-next-patch-release).
> The first iteration of internal release (i.e. [Ability to create internal packages to remediate SaaS Single
> Tenant](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1373)) still cannot solve _the blocking nature of
> GitLab releases_. It will be solved in another epic following the [Proposed plan of
> action](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/internal_releases/#proposed-plan-of-action) in
> the blueprint. In summary, the only use case of N is not supported by Internal releases at the moment, so N is
> not needed.

Related issue: [What are the backports required for an internal release?](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20621)

### Package Storage

An Internal release builds a Omnibus package and many CNG component images for each [available backport
version](#available-backport-versions-for-an-internal-release) on the `dev` mirrors of
[Omnibus](https://dev.gitlab.org/gitlab/omnibus-gitlab) and
[CNG](https://dev.gitlab.org/gitlab/charts/components/images/) projects. They are stored:

* CNG images: `dev` instance at `dev.gitlab.org:5005/gitlab/charts/components/images`
* Omnibus packages: `pre-release` channel on the [GitLab package repository](https://packages.gitlab.com)

Related issue: [Decide storage for CNG images and Omnibus packages](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20638)

### Supported cloud environments

Internal Release is currently supported on Dedicated instances running on AWS.

> Note: Dedicated on GCP was not tested. Please contact Delivery team if you want to extend the support.
