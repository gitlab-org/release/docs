---
tags:
  - auto-deploy
  - release-management
---
# How to sync Security repository with Canonical repository?

This How-to explains the different Security to Canonical syncing tasks involved with security releases and gives steps on how to achieve them.

There are two points in security releases where conflicts can occur:
1. During the early merge stage of security release preparation. These conflicts are caused by conflicting changes being merged into the Canonical master branch and causing the merge-train to fail. [More details on how to resolve](general/security/how_to_sync_security_with_canonical.md#dealing-with-merge-conflicts-caused-by-conflicting-changes-being-merged-into-canonical).
2. At the end of the security release when we re-sync the security and canonical repos. [More details on how to do this](general/security/how_to_sync_security_with_canonical.md#re-syncing-the-security-and-canonical-repos-at-the-end-of-the-security-release).

## Why do we have conflicts?

During normal operation, changes are merged into the Canonical master branch are automatically mirrored to the Security master branch.

Auto-deploy branches are created from the Security master branch to allow deployments to continue during security release preperation. This means that in normal operations a change is merged to Canonical master, mirrored to Security master and then incuded in a future auto-deploy branc to be deployed.

This changes during security release preperation.

To allow security fixes to be applied to GitLab.com and included in security releases without the vulnerability being revealed, we merge the security fixes directly into the Security master branch. At the same time, regular changes are being merged into the Canonical master branch. This causes Canonical and Security repositories to diverge.

After the master branches diverge mirroring is automatically replaced by the merge-train. This automated task is handled by the [`security:merge_train` pipeline schedule],
which in turn executes the [`security:merge_train` task in release-tools]. The merge train can also be run manually from the Ops instance.

At the end of the security release we re-sync the Security and Canonical repositories to resolve the divergance and re-enable mirroring.

## Dealing with merge conflicts caused by conflicting changes being merged into Canonical

During the 'early-merge' phase of a security release, security fixes are being merged to the Security master branch at the same time as regular changes are being merged to the Canonical master branch. The merge train handles the divergance to bring the Canonical changes into the Security master to allow changes to be included in upcoming deployments.

If a change being merged into Canonical master conflicts with a change being merged into the Security master we see the merge train failure error in the #g_delivery Slack channel.

If the merge train fails, no new changes will be recived from Canonical master until the error is resolved.

See the steps below for steps on how to resolve the conflict to unblock deploys.

### Manually syncing the repositories

If the merge train fails due to a conflict, it will attempt to open a merge request with those conflicts.

The automatically created merge request will ping the release managers and contains steps to follow in the description to resolve the conflicts.

#### If the automatic merge request fails

Check if the branch `sync-master-with-gitlab-org/gitlab` exists on the security repository: https://gitlab.com/gitlab-org/security/gitlab/-/branches.

If it exists: create a merge request to merge that branch into `master` and work to resolve the conflicts. Go to Step 8 (gather the required approvals).

If it does not exist, you will need to manually create the MR.

1. Clone the GitLab repository:

```ssh
git clone git@gitlab.com:gitlab-org/gitlab.git # This may take some time
```

2. Add the security remote:

```ssh
git add remote security git@gitlab.com:gitlab-org/security/gitlab.git
```

3. Fetch the content from both remotes:

```ssh
git fetch origin
git fetch security
```

4. Create a branch from `security/master`

```ssh
git checkout master --track security/master
git checkout  -b sync-security-with-canonical
```

5. Merge `origin/master` into your branch, at this point conflicts will need to be resolved

```ssh
git merge origin/master`
```

6. Push the changes into `security` (**be careful of not pushing the changes to Canonical**)


```ssh
git push security sync-security-with-canonical
```

7. Create a merge request
8. Wait until the pipeline finishes.
9. [Disable the "Squash commits" setting](#disable-the-squash-commits-setting).
10. If the pipeline is red and the failure is unrelated to the merge request, [disable the Pipelines must succeed setting](#disable-the-pipelines-must-succeed-setting).
11. Assign the merge request to your release manager counterpart for approval and merging.
12. After merging, **restore the "Squash commits" and the "Pipelines must succeed" settings**. See below.

### Disable the "Squash commits" setting.

As stated on [gitfaq](https://github.com/git/git/blob/083e01275b81eb6105247ec522adf280d99f6b56/Documentation/gitfaq.txt#L248-L274),
squash merges can cause the "possibility of needing to re-resolve conflicts again and again".

To disable the "Squash commits" setting:
1. Go to [merge requests settings on GitLab security repository]
1. Under the `Squash commits when merging` section, select `Encourage - Checkbox is visible and selected by default.` and save changes.
1. Open the merge request edit page, disable the `Squash commits when merge request is accepted` option, and save changes.
1. Merge the merge request.
1. After merging, restore the "Squash commits" check on the [merge requests settings on GitLab security repository]:
   * Select  `Require - Squashing is always performed. Checkbox is visible and selected, and users cannot change it.` under `Squash commits when merging` and save changes.

### Disable the "Pipelines must succeed" setting.

The merge request can fail due to the danger-review job failing, which [outputs a comment with error `Feature flags are discouraged from security merge requests.`]

1. Wait until the merged result pipeline is passing on everything except the `danger-review` job.
2. Go to the [merge requests settings on GitLab security repository] and under `Merge checks`, disable the "Pipelines must succeed" check box
3. Save changes
4. Refresh the page of the merge request
5. Merge the merge request
6. Write on the merge request why it was force-merged
7. Re-enable the "Pipelines must succeed" check box, and make sure to save your changes

These steps should unblock the merge train jobs.

[`security:merge_train` task in release-tools]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/security.rake#L31
[`security:merge_train` pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/121/edit
[outputs a comment with error `Feature flags are discouraged from security merge requests.`]: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2942#note_1188542112
[merge requests settings on GitLab security repository]: https://gitlab.com/gitlab-org/security/gitlab/-/settings/merge_requests

## Manually run the merge train from Ops

1. Go to https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules
1. Click on `Play` on the `gitlab-org/gitlab@master -> gitlab-org/security/gitlab@master` pipeline schedule.

## Re-syncing the Security and Canonical repos at the end of the security release

Once the security release has been published we can re-sync the Security and Canonical repos. This will bring all Security fixes into the Canonical master branch to resolve the divergance.

Our current process uses the merge-train to achieve this. With https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/19460 we hope to move to re-syncing via MR.

**Release Managers should follow the current steps in the Security Release issue.**

For knowledge-sharing, the process involved two steps:
1. Disable the Canonical -> Security merge train
2. Enable the Security -> Canoncial merge train

[Details about how to so this can be seen in the security release issue template](https://gitlab.com/gitlab-org/release-tools/-/blob/a986ce2668ea0b4bd31acf859587a7bb4352b13a/templates/security_patch.md.erb#L196-203)

### Requesting direct push permissions

Due to compliance requirements, release managers should use the tools described in the above sections to re-sync at the end of the security release.

However, if this is not possible for any reason, we also have a fallback option to allow release managers to request the elevated permissions needed to complete the repo sync.

This can done by opening an access request to Request the elevated permissions. Make sure you use the "[Release Manager Elevated Permissons](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/merge_requests/1413)" template and assign to one of the Delivery EMs. Use the [Release Manager escalation](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/#release-management-escalation) if needed.



