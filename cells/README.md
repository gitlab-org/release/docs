# Cells

> This is a short overview of the Cells project for GitLab Delivery team members. This document
> will not duplicate information that is already present in other places. Wherever possible, it will
> link to other documents. It aims to provide the reader with a gentle introduction to Cells.

Cells is a new architecture for the GitLab SaaS platform.[^1]

This section of `release/docs` will focus on the work that is being done to deploy new versions of
the GitLab application to each Cell. It will also document any associated tooling that we will build
to manage these deployments.

If you are new to Cells, then, please read through the following documents before delving into other
documents in this section:

| Document                                               | Questions answered                                                             |
|--------------------------------------------------------|--------------------------------------------------------------------------------|
| [GitLab Cells (formerly Pods) - Internal presentation] | Genesis of the project, why are we working on Cells                            |
| [Cells: Goals]                                         | Goals that the design of Cells aims to achieve                                 |
| [Cells: Infrastructure]                                | Infrastructure design in the Cells world                                       |
| [Ubiquitous language related to Cells]                 | Terms that are commonly encountered in the Cells project-related documentation |

## Architecture

Cells will use the [GitLab Dedicated architecture].[^2] In practice, this means that:

1. Each Cell can be thought of as a GitLab Dedicated tenant
1. Consequently, each Cell will have a tenant model. A [Tenant model] is a JSON file which describes
   a tenant using information such as software version, AWS account, GCP project ID, etc. (See a
   sample
   [here](https://gitlab.com/gitlab-com/gl-infra/cells/tissue/-/blob/a4df6291ff5759f4f2814e689a663567b3e90f84/rings/cellsdev/0/c01j2t2v563b55mswz.json))
1. [`ringctl`], a fork of `switchboard_uat`, will be used to manage and deploy new software versions
   to Cells
1. [Instrumentor] will be used to provision and update Cells

## Deployment

``` mermaid
---
title: Cells Deployment Architecture
---
flowchart LR
    ringctl --> |Makes commits| Tissue
    Tissue --> |Triggers pipeline| GitLabCI
    GitLabCI --> |Submits deployment job| AMP
    AMP --> |Deploys new version| C1["Cell 1"]
    AMP --> |Deploys new version| C2["Cell 2"]
```

### Deployment-related Design Documents

These are the current design documents related to the deployment of Cells:

1. [Application Deployment with a Cellular Architecture](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/infrastructure/deployments/)
1. [Coordinating changes in Cells](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/infrastructure/managing_changes/)
1. [Cells: Feature Flags](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/feature_flags/)
1. [Setting feature flags in Cells](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/infrastructure/feature_flags/)

> **NOTE:** All design documents related to Cells must be in the handbook. `release/docs` should
> contain only information related to the implementation of the above design documents.

[GitLab Cells (formerly Pods) - Internal presentation]: https://docs.google.com/presentation/d/1x1uIiN8FR9fhL7pzFh9juHOVcSxEY7d2_q4uiKKGD44/edit#slide=id.ge7acbdc97a_0_147
[Cells: Goals]: https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/goals/
[Cells: Infrastructure]: https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/infrastructure/
[GitLab Dedicated architecture]: https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/architecture/Architecture.html#managing-gitlab-dedicated
[Tenant model]: https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/architecture/tenant-model.html
[Instrumentor]: https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor
[`ringctl`]: https://gitlab.com/gitlab-com/gl-infra/ringctl
[Ubiquitous Language related to Cells]: https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/infrastructure/#glossaryubiquitous-language

[^1]: This project's original name was GitLab Pods. The project was later renamed to Cells.
[^2]: GitLab Dedicated is GitLab's single-tenant SaaS offering
