---
tags:
  - auto-deploy
---
# Release metadata

The <https://ops.gitlab.net/gitlab-org/release/metadata> repository contains useful information about GitLab releases.

It is located on the ops instance in order to maintain availability of this repository during any gitlab.com downtime.

## SHA associated with each GitLab release

The SHA for each component of a GitLab release is stored in the <https://ops.gitlab.net/gitlab-org/release/metadata>
repository. Some examples:

- `14.10.202204220620` auto-deploy package: [releases/14/14.10.202204220620.json](https://ops.gitlab.net/gitlab-org/release/metadata/-/blob/master/releases/14/14.10.202204220620.json).
- `14.0.0` major release: [releases/14/14.0.0.json](https://ops.gitlab.net/gitlab-org/release/metadata/-/blob/master/releases/14/14.0.0.json).
- `14.7.7` security release: [releases/14/14.7.7.json](https://ops.gitlab.net/gitlab-org/release/metadata/-/blob/master/releases/14/14.7.7.json).

**Note**: Due to the large number of files in each directory of the repository, browsing all files can be slow. Please use the [Find file](https://ops.gitlab.net/gitlab-org/release/metadata/-/find_file/master) feature if you are looking for a specific release version.

## Deployments

Deployments to each environment are recorded in <https://ops.gitlab.net/gitlab-org/release/metadata/-/environments>. However, this is not currently open to all GitLab team members since it requires a minimum role of `Reporter` on the project.

For example:

- [gstg-cny](https://ops.gitlab.net/gitlab-org/release/metadata/-/environments/664)
- [gstg](https://ops.gitlab.net/gitlab-org/release/metadata/-/environments/665)
- [gprd-cny](https://ops.gitlab.net/gitlab-org/release/metadata/-/environments/666)
- [gprd](https://ops.gitlab.net/gitlab-org/release/metadata/-/environments/667)
