---
tags:
  - auto-deploy
---
### Deployment Glossary

#### Deployments

**Canary:** identifies a stage in [our auto-deploy process](https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-deployments-process) where a new package is rolled out to the `gprd-cny` and `gstg-cny` [infrastructure environments](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#environments) and used as an initial "canary" test to a limited percentage of users. No traffic shifting is currently involved in this deployment strategy at GitLab. The configuration of the traffic received by the Canary stage is static.

**Package rollout:** rollout of a package into an environment, e.g., `gprd`.

**Blue/Green deployment:** deploying another package version alongside the current one and shifting the traffic from the current version to the other one. Traffic shifting happens after the newly deployed package version passes all required checks. The traffic is fully shifted in a single step.

**Gradual traffic shifting:** traffic is shifted between different deployed packages. The shifting of the traffic is gradual and adopts a stepped approach.

**Progressive delivery:** refers to the collection of deployment capabilities used for package rollout and selective traffic control.


#### Packages

**Package:** defines an auto-deploy package. An auto-deploy package is the atomic unit used for deployments in GitLab environments. It contains different components that work together.

**Current:** is the last successful deployed package. It may not represent the entire fleet, as it may exist an ongoing deployment

**Previous:** is the last successful deployed package prior to the current package.

**New:** is a package version that is being deployed at the time of speaking.
