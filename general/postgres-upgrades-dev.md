---
tags:
  - auto-deploy
---
# PostgreSQL Upgrades (Dev)

PostgreSQL upgrades are usually performed by the Software Delivery group.  We require approvals from the engineer-on-call (EOC) and an engineering manager (EM).

The upgrade work generally starts after the work to add support for a PG version to GitLab product has been completed. ([example epic](https://gitlab.com/groups/gitlab-org/-/epics/9065))

We need to ensure dev.gitlab.org is upgraded as early as possible, so that when we drop support for previous PostgreSQL version from the package, dev.gitlab.org won't be a blocker in the release process.

Usually before the change request for the upgrade is created, the Build team will notify that the new version of PostgreSQL is in the package. ([Example issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18799))

This runbook will cover the steps needed to perform the PostgreSQL upgrades in the `dev` instance.

## Change Request

We need to create a [change request](https://handbook.gitlab.com/handbook/engineering/infrastructure/change-management/#change-request-workflows) in `gl-infra/production` repo. [Example CR](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18825). Use the guidance below to fill in the change request.

Services Impacted - PostgreSQL service on dev.gitlab.org

Impacts:

- No downtime is estimated for gitlab.com SaaS.
- Auto-deployments will be paused.
- Delivery group and Test Platform department will be negatively impacted.

Estimated time to complete: 180 minutes

### Preparation Steps - steps to take in advance of the change

These steps need to be done successfully _in advance_ to make sure that the change can be performed. Make sure to keep track of these on the change request, but it's recommended to complete them a few days in advance from the change request due date.

1. Coordinate with the `@release-managers` to plan for the auto-deployments to be paused during the change window.
1. Test local gcloud access: `gcloud config get project`
1. Test SSH access to the dev instance: `ssh dev.gitlab.org`
1. Perform the [preparation steps for upgrading packaged PostgreSQL server](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server:~:text=To%20upgrade%20the%20PostgreSQL%20version%2C%20be%20sure%20that%3A)
    - In the CR, copy the steps from the official docs to perform the [preparation steps for upgrading packaged PostgreSQL server](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server:~:text=To%20upgrade%20the%20PostgreSQL%20version%2C%20be%20sure%20that%3A).
    - You can find details of PostgreSQL versions shipped with various GitLab versions in [PostgreSQL versions shipped with the Linux package](https://docs.gitlab.com/ee/administration/package_information/postgresql_versions.html).
    - No need to manually upgrade and run `sudo gitlab-ctl reconfigure` during these steps as it should be covered by deploying the nightly builds.
1. Prepare a MR to move dev.gitlab.org wal-g backup location to new version ([example](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/4660)). Get the MR approved, and set the MR to `draft` mode until the actual change day.

### Change steps - Upgrading PostgreSQL

1. Set the CR label `~change::in-progress`
1. Notify EOC and current RM's of the start of this maintenance in slack, leverage handles `@sreoncall` and `@release-managers`
1. Pause Auto-Deploy (in `#f_upcoming_release`): `/chatops run auto_deploy pause`
1. Configure your local `gcloud` CLI to leverage the `gitlab-dev` project

      ``` bash
      gcloud config set project gitlab-dev-1
      # Check again after setting
      gcloud config get project
      ```

1. Open an ssh session to the `dev` instance: `ssh dev.gitlab.org`
    1. Stop chef: `sudo systemctl stop chef-client`
    1. Remove the `wal-g backup` cron: `sudo -u gitlab-psql crontab -e` (it's the only item in there)
    1. Enable maintenance mode.
        - Open a rails console on the dev instance: `sudo gitlab-rails c`

        ``` bash
        ::Gitlab::CurrentSettings.update!(maintenance_mode: true)
        ::Gitlab::CurrentSettings.update!(maintenance_mode_message: "Postgres Upgrade: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/<issue_number>") # replace <issue_number>
        ```

1. From a terminal on your local machine: Take Snapshot of Data Disk for the dev instance: `gcloud compute machine-images create cr<issue_number> --source-instance=dev-1-01-sv-dev-1 --source-instance-zone=us-east1-c`

    If this [command](https://cloud.google.com/sdk/gcloud/reference/compute/machine-images/create) doesn't work as intended, you can always create the machine image directly using the gcloud console.

1. Delete the auto-upgrade skip file: `sudo rm /etc/gitlab/disable-postgresql-upgrade`
1. Upgrade the database. Replace `<version>` with the PostgreSQL version, e.g. for PG 16 upgrade, it'll be `16`.

    This command can take a longer time than the previous ones, so we add a timeout of 3 hours.

    ```bash
    sudo gitlab-ctl pg-upgrade -V <version> --timeout=3h
    ```

    If there was an error in the output while running the `ANALYZE` step, follow [these steps on the official document](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server:~:text=If%20there%20was%20an%20error%20in%20the%20output%20while%20running%20the) to determine whether `ANALYZE` should be run manually.

1. Verify that the psql binary and the data versions are updated to the new version

    ```bash
    /opt/gitlab/embedded/bin/psql --version
    cat /var/opt/gitlab/postgresql/data/PG_VERSION
    ```

1. Verify that the dev instance is online, and shows under the [admin page](https://dev.gitlab.org/admin) that the postgres version is updated.
1. After you have verified that your GitLab instance is running correctly, you can clean up the old database files

    ```bash
    sudo rm -rf /var/opt/gitlab/postgresql/data.<old_version>
    sudo rm -f /var/opt/gitlab/postgresql-version.old
    ```

1. Add the file back to disable pg upgrades: `sudo touch /etc/gitlab/disable-postgresql-upgrade`
1. Remove maintenance mode
    - Open a rails console on the dev instance: `sudo gitlab-rails c`

      ``` bash
      ::Gitlab::CurrentSettings.update!(maintenance_mode: false)
      ```

1. Merge the MR to move dev.gitlab.org wal-g backup location to new version, which was created in the [preparation steps](#preparation-steps---steps-to-take-in-advance-of-the-change). After merging, ensure the merge commit was mirrored to `ops`. There will be a bot comment with a pipeline [like this](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/5318#note_2232643361).
1. Start and Run Chef: `sudo systemctl start chef-client; sudo chef-client`
1. Validate our cron change from earlier has been reverted: `sudo -u gitlab-psql crontab -l`
1. Unpause Auto-Deploy (in `#f_upcoming_release`): `/chatops run auto_deploy unpause`
1. Notify EOC and current RM's of the completion of this maintenance in slack, leverage handles `@sreoncall` and `@release-managers`
1. Set the CR label `~change::complete`

### Rollback steps

In case we need to rollback, these steps will take about 20 minutes. Follow all three sections in order: rollback, verify, and cleanup.

#### Rollback

1. If the `pg-upgrade` script encounters a fatal error, it will attempt to auto-rollback
    - If the auto-rollback worked, perform the [verify steps](#verify-the-rollback)
1. If the auto-rollback did not work, we need to rollback manually ([official docs for revert](https://docs.gitlab.com/omnibus/settings/database.html#revert-packaged-postgresql-server-to-the-previous-version)). Replace `<old version>` with the initial version, e.g. if the upgrade was from PostgreSQL 14 to 16, `<old version>` would be `14`. This version is also in `/var/opt/gitlab/postgresql-version.old` (used by default if version not specified).

    ```bash
    gitlab-ctl revert-pg-upgrade -V <old version>
    ```

1. If both above options fail, we need to restore from the data disk.
    - Follow the [official docs for detaching and reattaching boot disks](https://cloud.google.com/compute/docs/disks/detach-reattach-boot-disk).
    - Make appropriate changes to [`config-mgmt`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/) after this is completed

#### Verify the rollback

1. Verify that the psql binary and the data versions still show the old version

    ```bash
    /opt/gitlab/embedded/bin/psql --version
    cat /var/opt/gitlab/postgresql/data/PG_VERSION
    ```

1. Verify that the dev instance is back online, and shows under the [admin page](https://dev.gitlab.org/admin) that the postgres version is back to the old version.
1. Gather disagnostic information as needed, and add the information to the change request issue comments for future reference.

#### Cleanup after rollback

1. Add the file back to disable pg upgrades: `sudo touch /etc/gitlab/disable-postgresql-upgrade`
1. Remove maintenance mode
    - Open a rails console on the dev instance: `sudo gitlab-rails c`

      ``` bash
      ::Gitlab::CurrentSettings.update!(maintenance_mode: false)
      ```

1. Start and Run Chef: `sudo systemctl start chef-client; sudo chef-client`
1. Validate our cron change from earlier has been reverted: `sudo -u gitlab-psql crontab -l`
1. Unpause Auto-Deploy (in `#f_upcoming_release`): `/chatops run auto_deploy unpause`
1. Notify EOC and current RM's of the completion of this maintenance in slack, leverage handles `@sreoncall` and `@release-managers`
1. Set the CR label `~change::aborted`

### Monitoring

Observe the following to determine if anything should prompt a rollback.

- We should be able to access dev.gitlab.org.
- For metrics, we don't currently have decent dashboards for our dev instance, but
- For logs, we should monitor for any `HTTP500` errors that are related to database connectivity: [log query](https://nonprod-log.gitlab.net/app/r/s/bisVD) (Note that we do receive occasional `HTTP500` errors, so a keen eye will be required).
