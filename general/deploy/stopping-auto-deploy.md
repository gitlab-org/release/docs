---
tags:
  - auto-deploy
---
# Overview

This page is dedicated to the process of [auto-deploy](auto-deploy.md).

## Temporarily stopping automated deployments

In case of an issue that could cause a severe disruption of GitLab.com and the related environments, it might be necessary to stop automatic deployments from
running.

In case of such an event:

* Issue describing P1 and S1 needs to be created.
* In the `#f_upcoming_release` channel, run:
  ```
  /chatops run auto_deploy pause
  ```

**Note that this will not pause automated package creation.** See [below](#temporarily-stopping-automated-package-creation)
if you would like to stop package creation.

The ChatOps command will:
- Deactivate the `auto_deploy: Deploy latest available package` [scheduled pipeline][release/tools scheduled pipelines]
- Turn on the `do_not_auto_create_new_deployment_pipelines` [feature flag][release/tools feature flags]

Deactivating the scheduled pipeline and turning on the feature flag will effectively prevent the tooling from triggering automated deploys to the staging environment.
Ensuring that release managers are informed will ensure that manual part of deployment (progression to production environments) is not executed.

Once the issue is resolved, release managers need to make a decision on case per case basis on what is the best way to progress further towards enabling the automated deployments again.

### Reactivating automated deployments

In the `#f_upcoming_release` channel, run:
```
/chatops run auto_deploy unpause
```

## Temporarily stopping automated package creation

Automated package creation should not be stopped when deployments need to be paused, so that when deployments
restart, we have a fresh package to choose from.

However, if there is a situation where automated package creation needs to be paused, do the following:
- Take ownership of the [`auto_build: Create new package` pipeline schedule][release/tools scheduled pipelines]
- Deactivate the [`auto_build: Create new package` pipeline schedule][release/tools scheduled pipelines]

### Reactivating automated package creation

- Take ownership of the [`auto_build: Create new package` pipeline schedule][release/tools scheduled pipelines]
- Activate the [`auto_build: Create new package` pipeline schedule][release/tools scheduled pipelines]

[release/tools scheduled pipelines]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[release/tools feature flags]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags
