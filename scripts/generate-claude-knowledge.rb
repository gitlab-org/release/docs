#!/usr/bin/env ruby

require 'find'
require 'json'

CLAUDE_LIMITS = {
  max_files: 50,
  max_file_size: 500_000,
  max_total_size: 1_000_000
}

def get_git_status
  sha = `git rev-parse HEAD`.strip
  is_dirty = !`git status --porcelain`.empty?
  "#{sha}#{is_dirty ? '-dirty' : ''}"
rescue
  'unknown'
end

def get_base_url
  sha = `git rev-parse HEAD`.strip
  "https://gitlab.com/gitlab-org/release/docs/-/tree/#{sha}/"
end

SKIP_PATTERNS = [
  %r{^\./\.},           # Files starting with .
  %r{^\./scripts/},     # Files in scripts directory
  %r{^\./[^/]+$},       # Root files
  %r{^.*(?<!\.md)$},    # Files not ending in .md
  %r{^\./\.git/}        # Git directory
]

def should_skip?(file)
  SKIP_PATTERNS.each do |pattern|
    return true if file.match?(pattern)
  end
  false
end

def extract_tags(content)
  # First try HTML comment format
  if match = content.match(/<!--\s*tags:\s*\[(.*?)\]\s*-->/)
    return match[1].scan(/"([^"]+)"/).flatten
  end

  # Then try YAML frontmatter format
  if content.start_with?("---\n")
    # Find the end of the frontmatter block
    second_marker = content.index("\n---\n", 4)
    if second_marker
      frontmatter = content[3...second_marker]
      if frontmatter =~ /^tags:\s*$/
        # Multi-line tag format
        tag_lines = frontmatter.lines.drop_while { |line| !line.start_with?("tags:") }.drop(1)
        tag_lines.take_while { |line| line.start_with?("  - ") }
               .map { |line| line.sub(/^\s*-\s*"?([^"]*)"?\s*$/, '\1').strip }
      elsif match = frontmatter.match(/^tags:\s*\[(.*?)\]\s*$/)
        # Single-line array format
        match[1].scan(/"([^"]+)"/).flatten
      elsif match = frontmatter.match(/^tags:\s*(.+)$/)
        # Single tag format
        [match[1].strip.delete('"')]
      end
    end
  end || []
end

def parse_files
  files_by_tag = Hash.new { |h, k| h[k] = {} }
  tag_stats = Hash.new { |h, k| h[k] = { total_size: 0, file_count: 0, skipped: [] } }
  stats = []
  created_files = []

  Find.find('.') do |file|
    if should_skip?(file)
      puts "Skipping #{file} (matched skip pattern)"
      next
    end

    unless File.file?(file)
      puts "Skipping #{file} (not a file)"
      next
    end

    begin
      puts "\nProcessing: #{file}"
      content = File.read(file)
      tags = extract_tags(content)

      if tags.empty?
        puts "  No tags found, skipping"
        next
      end

      puts "  Found tags: #{tags.join(', ')}"
      file_size = content.bytesize
      puts "  File size: #{file_size} bytes"
      relative_path = file.sub(/^.\//, '')

      tags.each do |tag|
        puts "  Processing tag: #{tag}"
        if file_size > CLAUDE_LIMITS[:max_file_size]
          puts "    Skipping (exceeds file size limit of #{CLAUDE_LIMITS[:max_file_size]} bytes)"
          tag_stats[tag][:skipped] << {path: relative_path, size: file_size}
          next
        end

        if tag_stats[tag][:total_size] + file_size > CLAUDE_LIMITS[:max_total_size]
          puts "    Skipping (would exceed total size limit of #{CLAUDE_LIMITS[:max_total_size]} bytes)"
          stats << "Warning: Total size limit reached for tag #{tag}, skipping remaining files"
          next
        end

        puts "    Added to #{tag} collection"
        tag_stats[tag][:total_size] += file_size
        tag_stats[tag][:file_count] += 1

        files_by_tag[tag][relative_path] = {
          path: relative_path,
          content: content,
          size: file_size
        }
      end
    rescue => e
      puts "Error processing #{file}: #{e.message}"
      stats << "Error reading #{file}: #{e.message}"
    end
  end

  return files_by_tag, tag_stats, stats, created_files
end

files_by_tag, tag_stats, stats, created_files = parse_files
git_status = get_git_status

files_by_tag.each do |tag, files|
  output = {
    version: "1.0",
    auto_deploy_files: files,
    total_files: files.length,
    total_size: tag_stats[tag][:total_size],
    git_version: git_status,
    base_url: get_base_url,
    skipped_files: tag_stats[tag][:skipped]
  }

  output_file = "claude-#{tag}-#{git_status}.json"
  File.write(output_file, JSON.pretty_generate(output))
  created_files << output_file
end

puts stats
puts "\nCreated files:"
puts "-------------"
created_files.sort.each do |file|
  puts "- #{file} (#{File.size(file)} bytes)"
end