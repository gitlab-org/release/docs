---
tags:
  - auto-deploy
---
# Building Packages

Building artifacts for public consumption is handled on our dev instances via
[`omnibus-gitlab`](https://dev.gitlab.org/gitlab/omnibus-gitlab).  All builds
are initiated via some sort of trigger:

* Nightly pipelines are kicked off [via a Cron Job][nightlies]
* Builds of tagged packages are automatically started when that tag is pushed
  into the repo

[nightlies]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipeline_schedules

## Nightly Builds

### Nightly Builds Pipeline

```mermaid
graph LR
  A(Fetch Assets) --> B(Build Ubuntu 16.04 Package)
  B --> C(Upload Ubuntu 16.04 Package)
  C --> D(Build CentOS 6)
  C --> E(Build CentOS 7)
  C --> F(Build Debian 8)
  C --> G(Build Debian 9)
  C --> H(Build OpenSUSE)
  C --> I(Build Scientific Linux)
  C --> J(Build Ubuntu 14.04)
  C --> K(Build Ubuntu 18.04)
```

## Tagged Builds

### Tagged Builds Pipeline

```mermaid
graph LR
  A(Fetch Assets) --> B(Build Ubuntu 16.04 Package)
  B --> C(Upload Ubuntu 16.04 Package)
  C --> D(Build CentOS 6)
  C --> E(Build CentOS 7)
  C --> F(Build Debian 8)
  C --> G(Build Debian 9)
  C --> H(Build OpenSUSE)
  C --> I(Build Scientific Linux)
  C --> J(Build Ubuntu 14.04)
  C --> K(Build Ubuntu 18.04)

  D --> L(Dependency Scanner)
  E --> L
  F --> L
  G --> L
  H --> L
  I --> L
  J --> L
  K --> L

  L --> M(Upload CentOS 6 Package)
  L --> N(Upload CentOS 7 Package)
  L --> O(Upload Debian 8 Package)
  L --> P(Upload Debian 9 Package)
  L --> Q(Upload OpenSUSE Package)
  L --> R(Upload Scientific Linux Package)
  L --> S(Upload Ubuntu 14.04 Package)
  L --> T(Upload Ubuntu 18.04 Package)

  M --> U>Release CentOS 6 Package]
  N --> V>Release CentOS 7 Package]
  O --> W>Release Debian 8 Package]
  P --> X>Release Debian 9 Package]
  Q --> Y>Release OpenSUSE Package]
  R --> Z>Release Scientific Linux Package]
  S --> AA>Release Ubuntu 14.04 Package]
  S --> BB>Release Ubuntu 16.04 Package]
  T --> CC>Release Ubuntu 18.04 Package]
  S --> DD>Release Docker Images]
  S --> EE>Release AMI Images]

  EE --> FF(Build and Upload Raspberry Pi Package)

  FF --> GG>Release Raspberry Pi Package]
```

Legend:

```mermaid
graph LR
A(Automatically run job)
B>Manually triggered job]
A -.- B
```

* The Tagged pipeline will start whenever a new tag is pushed to
  `omnibus-gitlab`, however, some steps are manually kicked off as to prevent
  unnecessary artifacts.  When a release manager runs the `release` process,
  these manually triggered jobs will be started.

## How Assets are Built and Deployed

Copied from [Incident review 18127](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18127#note_1941456117)

### Stage 1: Build

```mermaid
flowchart TD
    release-tools[release-tools] -->|Scheduled pipeline| auto-deploy-tag(Create auto-deploy tags)
    auto-deploy-tag --> security-gitlab-rails[gitlab-org/security/gitlab]
    auto-deploy-tag --> security-CNG[gitlab-org/security/charts/components/images]
    auto-deploy-tag --> release-tools-b(release-tools)
    security-gitlab-rails --> |Push mirror| dev-gitlab-ee[dev.gitlab.org/gitlab-org/gitlab-ee]
    security-CNG --> |Push mirror| dev-CNG[dev.gitlab.org/gitlab/charts/components/images]
    security-CNG --> |gitlab-rails-ee| security-gitlab-rails-ee(Builds container with assets)
    security-gitlab-rails-ee --> |gitlab-webservice-ee| security-webservice-ee(Push to registry.gitlab.com/security/charts/components/images/gitlab-rails-ee)

    dev-gitlab-ee --> |compile-production-assets| compile-assets(Calculate hash of sources in GITLAB_ASSETS_HASH)
    compile-assets --> hash-matches{Cached package available?}
    hash-matches --> |Yes| hash-matches-yes(Download and extract from package registry)
    hash-matches-yes --> do-compile-assets
    hash-matches --> |No| do-compile-assets(Compile assets,
    store GITLAB_ASSETS_HASH in cached-assets-hash.txt artifact)
    do-compile-assets --> |build-assets-image| check-if-assets-image-exists{Does image w/ hash exist?}
    check-if-assets-image-exists --> |Yes| do-nothing(Do nothing)
    check-if-assets-image-exists --> |No| build-assets-image(Build image,
    Push to dev.gitlab.org:5005/gitlab/gitlab-ee/gitlab-assets-ee)
```

In more detail:

1. <https://ops.gitlab.net/gitlab-org/release/tools> has a scheduled pipeline that runs every hour at :10 (e.g. 5:10, 6:10, etc.). Example pipeline: <https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/14227886>
1. This runs a script that creates an auto-deploy branch (e.g. `17-1-auto-deploy-2024060720`) and tag in a number of key projects:
   1. `GitLab Rails` :arrow_right: `gitlab-org/security/gitlab/security`
   1. `CNG` :arrow_right: `gitlab-org/security/charts/components/images`
   1. `release-tools` :arrow_right: This tags the project itself, which in turns launches a `coordinator` pipeline.
   1. `Omnibus Dev` :arrow_right: dev.gitlab.org/gitlab/omnibus-gitlab
1. The first two tags get mirrored on their corresponding projects on `dev.gitlab.org` and run identical pipelines. Note there is a bit of redundancy here because Docker images get created on both `dev.gitlab.org` and `registry.gitlab.com`.
1. After tagging on release-tools, an Omnibus pipeline is triggered to build the package - ([example](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines/330059))
1. This pipeline has a `fetch-assets` job that waits for the `compile-production-assets` job on the GitLab dev project
1. `GitLab Rails` runs a CI job called `compile-production-assets` that builds the assets. First, it computes `GITLAB_ASSETS_HASH` from the JavaScript checksums of the source files. If a cached package is available, it downloads and extracts it from the package registry. Either way, it runs `bin/rake gitlab:assets:compile`. Example jobs: <https://dev.gitlab.org/gitlab/gitlab-ee/-/jobs/20942621>, <https://gitlab.com/gitlab-org/security/gitlab/-/jobs/7048548039>.
1. Before `compile-production-assets` finishes, it stores `GITLAB_ASSETS_HASH` into `cached-assets-hash.txt`.
1. `GitLab Rails` runs a CI job called `build-assets-image`. Its job is to publish the `gitlab/gitlab-assets-ee` registry image if needed. It uses `cached-assets-hash.txt` to see if the image is present, and builds it if it is not. Example jobs: <https://dev.gitlab.org/gitlab/gitlab-ee/-/jobs/20942632>, <https://gitlab.com/gitlab-org/security/gitlab/-/jobs/7048548139>
1. Meanwhile, in the `CNG` project on gitlab.com, the `gitlab-rails-ee` image generates its own assets in a totally different build process. Example job: <https://gitlab.com/gitlab-org/security/charts/components/images/-/jobs/7048548365>
1. This runs a script that creates an auto-deploy branch (e.g. 17-1-auto-deploy-2024060720) and tag in a number of key projects:
  a. GitLab Rails ➡ gitlab-org/security/gitlab/security
  b. CNG ➡ gitlab-org/security/charts/components/images
  c. release-tools ➡ This tags the project itself, which in turns launches a coordinator pipeline.
  d. Omnibus Dev ➡ dev.gitlab.org/gitlab/omnibus-gitlab
1. The first two tags get mirrored on their corresponding projects on dev.gitlab.org and run identical pipelines. Note there is a bit of redundancy here because Docker images get created on both dev.gitlab.org and registry.gitlab.com.
1. After tagging on release-tools, an Omnibus pipeline is triggered to build the package - (example)
1. This pipeline has a fetch-assets job that waits for the compile-production-assets job on the GitLab dev project
1. GitLab Rails runs a CI job called compile-production-assets that builds the assets.

Assets are built in two places:

1. gitlab-assets-ee pipeline in GitLab Rails - Used by Omnibus images and the deployer step
1. gitlab-rails-ee pipeline in CNG - Used in the CNG images (gitlab-webservice-ee).
If they are not exactly the same we run into an incident that takes a while to resolve because we have to understand the whole asset compilation flow above to understand what might have gone wrong.

### Stage 2: Deploy

We saw in the first stage that release-tools tagged itself. When that happens, a coordinator pipeline is launched. Example: <https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/3348871>

This pipeline triggers a deploy:gstg-cny pipeline: Example: <https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/3348871>

One of the jobs in this pipeline is gstg-cny-assets. This job is responsible for copying the assets from dev.gitlab.org:5005/gitlab/gitlab-ee/gitlab-assets-ee:<auto deploy tag> to an object storage bucket. Example job: <https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/jobs/14182968>

The coordinator pipeline runs another pipeline that ultimately deploys the webservice-ee images to Kubernetes. Example job: <https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/jobs/14183044>

#### Comments

As you can see, the whole process is pretty complex and spans multiple projects. It even spans across GitLab instances, including a dependency on dev.gitlab.org!
The main issue we had was that the assets in the CNG webservice-ee images were different from the assets produced by the gitlab-assets-ee images.
Normally, the assets should be the same. However, due to a subtle config bootstrapping problem, the diff for the Tailwind CSS was subtly different by a few lines.
We got sidetracked by the caching issues because we noticed that the Tailwind CSS file hadn't been updated in a day for the gitlab-assets-ee image. That might not have ultimately the problem, but it was a problem nonetheless.

#### Questions for next steps

* Could we simplify the deploy step to fetch the assets from the CNG gitlab-rails-ee image? That would at least ensure that we are consistent with whatever we deploy in production.
* Could we stop using dev.gitlab.org to deploy those assets? Why not use the same source as the webservice-ee images, which I believe is Google Artifact Registry?
* Do we really need to build everything on dev.gitlab.org and on gitlab.com? This seems like a waste of compute and storage.
* Right now we build assets in three places: Omnibus, CNG, and GitLab Rails pipelines. We don't have any guarantees that they match up, so should we centralize this or introduce some verification step?

### References

[1] <https://docs.gitlab.com/ee/development/pipelines/performance.html#compile--assets>
[2] <https://handbook.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-deployments-process>
