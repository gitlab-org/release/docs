---
tags:
  - auto-deploy
---
# No Slack notification when a deployment happens

Each merge to a stable branch triggers a deployment to the corresponding release environment, sequentially triggering two notifications via [release-tools]:

- Start notification: sent before the deployment happens.
- Finish notification: sent after the deployment happens. One in these 3 cases happens:
  - Notify success deploy
  - Notify failed deploy
  - Notify failed QA

## Workflow

```mermaid
flowchart LR
    A[Notify start deploy] --> B[Deploy]
    B --> C{success?}
    C -->|True| D[Run QA]
    C -->|False| E[Notify failed deploy]
    D --> F{success?}
    F -->|True| G[Notify success deploy]
    F -->|False| H[Notify failed QA]
```

## Failure in notification delivery

If you followed all the steps in [Debug Release Environments Pipeline](./debug-release-environments-pipeline.md) but all the jobs and pipelines are green, then it can be something wrong with the notification delivery.

   1. Open the pipeline triggered by the job `start-release-environments-pipeline` in `gitlab` repository.
   1. There should be two stages `start` and `finish`, each with one job runs successfully.
   1. Check the log of the missing notification job (start/finish). The job triggers a pipeline in [release-tools] by [using the API](https://docs.gitlab.com/ee/ci/triggers/), the job log contains the response of the API call.
      1. A successful pipeline triggering returns a response like [this](https://gitlab.com/gitlab-org/gitlab/-/jobs/6753835889):

          ```text
          Response body: {"id":3193632,"iid":670436,"project_id":130,"sha":"a1dea47a707bb463022de649d9f4a22beebac27b","ref":"master","status":"created","source":"trigger","created_at":"2024-05-01T00:08:36.419Z","updated_at":"2024-05-01T00:08:36.419Z","web_url":"https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/3193632","before_sha":"0000000000000000000000000000000000000000","tag":false,"yaml_errors":null,"user":{"id":1460,"username":"dat.tang.gitlab","name":"Dat Tang","state":"active","locked":false,"avatar_url":"https://secure.gravatar.com/avatar/1c9d3ea240e53e3afe2cc6dd655bdcd7a1128a266a07eaae44258b49b2a6c527?s=80\u0026d=identicon","web_url":"https://ops.gitlab.net/dat.tang.gitlab"},"started_at":null,"finished_at":null,"committed_at":null,"duration":null,"queued_duration":null,"coverage":null,"detailed_status":{"icon":"status_created","text":"Created","label":"created","group":"created","tooltip":"created","has_details":false,"details_path":"/gitlab-org/release/tools/-/pipelines/3193632","illustration":null,"favicon":"/assets/ci_favicons/favicon_status_created-4b975aa976d24e5a3ea7cd9a5713e6ce2cd9afd08b910415e96675de35f64955.png"}}
          ```

          From this output, you get the pipeline's `web_url`. Following the link, you can check the details of the notification pipeline in [release-tools].

          > **NOTE**: The notification jobs in GitLab repository like `start-release-environments-pipeline` triggers and forgets the pipeline in [release-tools], so the statuses of these two don't reflex each other.
      1. If the triggering returns 404 with the body `{"message":"404 Not Found"}`, it can be the pipeline running without a valid `OPS_RELEASE_TOOLS_PIPELINE_TOKEN` environment variable.

         - This variable is a pipeline trigger token of [release-tools]. Follow [GitLab document](https://docs.gitlab.com/ee/ci/triggers/#create-a-pipeline-trigger-token) to create it.
         - It should be set in the `CI/CD Settings` of the [gitlab-org/gitlab] repository.

[gitlab-org/gitlab]: https://gitlab.com/gitlab-org/gitlab
[release-tools]: https://ops.gitlab.net/gitlab-org/release/tools