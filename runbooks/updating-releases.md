---
tags:
  - auto-deploy
---
# Overview of the releases.yml file

## What is releases.yml?

The [`releases.yml` file] is the single source of truth (SSoT) for absolute GitLab release dates and milestones. The information
is stored in a format that allows other teams to build on top of it to meet their needs. The Delivery group is responsible for maintaining this SSOT.

Each entry on the `releases.yml` is conformed by the following keys:

- `version`: Represents a valid GitLab version
- `date`: Represents the release date for the given version.
- `manager_americas`: (Optional) A list of GitLab team members to act as release managers in AMER for the given version.
- `manager_apac_emea`: (Optional) A list of GitLab team members to act as release managers in EMEA/APAC for the given version.
- `appsec`: (Optional) A list of AppSec engineers to act as a security release managers for the given version.

## Where is this file located?

The [`releases.yml` file] can be located on the [www-gitlab-com repository] under the [`data` directory].

## How is this file updated?

The release information of this file is provided by the [`releases` gem]. This library removes the mental burden of
manually calculating what is the third Thursday of each month and prevents human errors.

To update the release information (`version` and `date`) of this file:

1. [Install the `releases`] gem
1. [Fetch the information of the releases] from the gem.
1. Open up a merge request on the handbook repo to update the information in `releases.yml`.
1. Assign the merge request to a Delivery manager.

Upcoming iterations aim to [automatically update the release information] of this file.
The release manager information can be updated independently from the release information.

## What about release_managers.yml file?

The [`release_managers.yml` file] includes release manager information for each GitLab release. As part of establishing a dynamic release date,
this file will eventually be deprecated in favor of `releases.yml`.

[`releases.yml` file]: https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/releases.yml
[`data` directory]: https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data?ref_type=heads
[www-gitlab-com repository]: https://gitlab.com/gitlab-com/www-gitlab-com
[`releases` gem]: https://gitlab.com/gitlab-org/delivery/releases
[Install the `releases`]: https://gitlab.com/gitlab-org/delivery/releases#installation
[Fetch the information of the releases]: https://gitlab.com/gitlab-org/delivery/release_dates#usage
[automatically update the release information]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/19561
[`release_managers.yml` file]: https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/release_managers.yml?ref_type=heads
