---
tags:
  - auto-deploy
---
# Release and pre environments

The pre and release environments are automatically updated when a Release Manager
tags the monthly release candidate and the final release respectively.

## pre

When release candidates are tagged, e.g.:

```
/chatops run release tag 14.0.0-rc44
```

An Omnibus pipeline on dev is triggered as a result to build the package,
`Ubuntu-18.04-staging` is the job that triggers the deploy to the [pre environment]:

* Pipeline example: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines/248981
* Job example: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/13475069

```bash
Ready to send trigger for environment(s): pre
Triggering pipeline https://ops.gitlab.net/api/v4/projects/151/trigger/pipeline for ref master, status code: 201 Created
Deployer build triggered at https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/1370080 on master for the pre environment
```

### Manual deployments

Sometimes we want to deploy a newer version to pre outside of a monthly release
for testing purposes. It is safe to do so via the usual chatops command, and
will not cause issues with the next RC deployment, e.g.:

```
/chatops run deploy 14.2.202108020321-b94d2478afd.c278429ee94 pre
```

### Pre environment usage

Information about what the `pre` environment is used for can be found in the handbook: https://about.gitlab.com/handbook/engineering/infrastructure/environments/#pre.

## Release

When tagging the final version of the release, e.g.:

```
/chatops run release tag 14.0.0
```

An Omnibus pipeline on dev is triggered as a result to build the package,
`Ubuntu-20.04-staging` is the job that triggers the deploy to the [release environment]:

* Pipeline example: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines/249232
* Job example: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/13490958

```bash
Ready to send trigger for environment(s): release
15.3.0+ee.0
Triggering pipeline https://ops.gitlab.net/api/v4/projects/151/trigger/pipeline for ref master, status code: 201 Created
Deployer build triggered at https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/1372333 on master for the release environment
```

### Release environment usage

Information about what the `release` environment is used for can be found in the handbook: https://about.gitlab.com/handbook/engineering/infrastructure/environments/#release.

[pre environment]: https://pre.gitlab.com/help
[release environment]: https://release.gitlab.net/help
