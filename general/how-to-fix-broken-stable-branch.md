---
tags:
  - auto-deploy
---
## Dealing with broken stable branches

GitLab releases depend on stable branches, as such, it's important to ensure these branches have green pipelines. The process
to fix a stable branch depends on the project where the failure is.

### GitLab project

If a failure in a stable branch is found:

1. Search for an existent issue on the [release tracker]. [GitLab tooling] was built to automatically
   create incident issues when a stable branch fails so chances are one issue is already created.
1. If no issue is found, create a new incident using the [Broken stable branch] template and follow the steps from the incident issue.
   * To fix the failure, it is likely a merge request targeting the stable branch will be required on the canonical repository.
   * Code changes merged into the canonical repository will be automatically propagated to the security and dev repositories via mirroring.
1. The [dev-on-call process] can be used at any time to fix the stable branch failure.

### Any other project under [Managed Versioning]

If a failure in a stable branch is found, contact the respective [project maintainers] and ask about the failure.
Optionally, the [dev-on-call process] can be used at any time to fix the stable branch failure.

[release tracker]: https://gitlab.com/gitlab-org/release/tasks/-/issues/?label_name%5B%5D=release-blocker
[GitLab tooling]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/create-pipeline-failure-incident.rb
[Broken stable branch]: https://gitlab.com/gitlab-org/release/tasks/-/blob/master/.gitlab/issue_templates/Broken-stable-branch.md
[dev-on-call process]: https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#process-outline
[Managed Versioning]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/index.md
[project maintainers]: https://about.gitlab.com/handbook/engineering/projects/
