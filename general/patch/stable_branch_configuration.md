---
tags:
  - release-management
---
# Stable branches configuration

Stable branches are configured to respect the [GitLab Maintenance Policy] for bug fixes:

* Engineers can backport bug fixes into the current stable branch following the [runbook for GitLab engineers].
* Stable branches outside the maintenance policy for bug fixes are restricted to release managers. GitLab
  engineers require a [backport request] to be approved by release managers to backport a high-priority bug
  fix into an older stable branch.

Stable branches configuration[^1] consists of:

1. A protected branch rule for the current stable branch allowing `merge` access to maintainers.
1. A wildcard that covers stable branches limiting the `push` and `merge` access to release managers.
1. A merge request approval rule that enforces maintainer approval on merge requests targeting stable branches[^2].

| [Protected branch rules] | [Merge request approval rules] |
| ----- | ----- |
| ![protected branch](images/protected_branch_rules.png) | ![merge request approval rules](images/merge_request_approval_rules.png) |


Stable branches permissions are adjusted by release managers at the end of each release.

[^1]: Rules 1 and 2 are configured on GitLab and Omnibus projects, the remaining projects under [Managed Versioning] allow maintainers to merge into any stable branch.
[^2]: This rule is only implemented on the [GitLab project]

[backport request]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[GitLab project]: https://gitlab.com/gitlab-org/gitlab/
[Managed Versioning]: components/managed-versioning/index.md
[runbook for GitLab Engineers]: ./engineers.md
