---
tags:
  - auto-deploy
---
# No environment is created when a new release tagged

During release tagging, the [rake task `release_environment:create`] is called to create a new [release-environments]. This runbooks aims to debug if a new environment is not created.

## Identify if the environment is created

Check [Workflow of Release Environment](https://gitlab.com/gitlab-com/gl-infra/release-environments#workflow-of-release-environment) for more information.

### Check environment file

If an environment is created, a new file is created in [release-environments] repository at the path `environments/<branch-name>/environment.yaml`.

### Check Helm release on `release` cluster

> **NOTE**: The new environment is only deployed to the cluster after the first pipeline ran on the stable branch in [gitlab-org/gitlab].

- Connect to the `release` cluster:

  ```bash
  glsh kube use-cluster release
  ```
- Check if the Helm releases are available on the namespace.

  ```bash
  helm ls -n gitlab-<stable-branch>
  ```

  Example:

  ```bash
  helm ls -n gitlab-16-10-stable
  NAME                                            NAMESPACE               REVISION        UPDATED                                      STATUS          CHART                           APP VERSION
  gitlab-16-10-stable                             gitlab-16-10-stable     1               2024-04-09 21:01:48.664198897 +0000 UTC      deployed        gitlab-7.6.2-1113082919         master
  gitlab-16-10-stable-prafect-db-automation       gitlab-16-10-stable     1               2024-04-09 21:01:48.257764793 +0000 UTC      deployed        praefect-db-creation-0.1.0      1.0.0
  gitlab-16-10-stable-secrets                     gitlab-16-10-stable     1               2024-04-09 21:01:47.82651866 +0000 UTC       deployed        vault-secrets-1.2.13
  ```

## `release_environment` Feature Flag is not enabled

If the `release_environment` Feature Flag is not enabled, when the release tagging happens, the [rake task `release_environment:create`] is not called.

- Find `release_environment` in the [list of feature flags](https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags) in the release-tools repository on the `ops` instance. Check if its Status is enabled.

## `release_environment:create` fails

> **NOTE:** This runbook assumes that the release pipeline succeeds (i.e. rake task `release:tag` succeeds). If the release pipeline fails, the release environment is also not created. This debugging part is not covered in this runbook.

The [rake task `release_environment:create`] runs as a part of the rake task `release:tag`. Thus, you should be able to find its log in the `release:tag` job

[gitlab-org/gitlab]: https://gitlab.com/gitlab-org/gitlab
[release-environments]: https://gitlab.com/release-environments/
[rake task `release_environment:create`]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/release.rake?ref_type=heads#L92