---
tags:
  - auto-deploy
---
# Build KAS versions older than 17.0

The KAS project has switched to hosted arm64 runners in 17.0. Before that, custom runners were being used. The custom runners were removed on Aug 21st 2024, after the release of 17.3.

Any patch releases to versions older than 17.0 will require the custom arm64 runners to be added back, otherwise the gitlab-agent pipelines will be stuck without a runner to execute them.

Follow the procedure in <https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/infra/arm64-runners#resurrecting-the-runners> to add back the runners
when creating a patch for versions older than 17.0.
