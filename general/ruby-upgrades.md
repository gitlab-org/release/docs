---
tags:
  - auto-deploy
---
# Ruby upgrades

Ruby upgrades are usually performed by a group comprising a manager, engineers from the Development department, Distribution team and Delivery team.

The Ruby upgrade work starts with an epic in the `gitlab-org` group created by the manager in the Ruby upgrade group.

Before Delivery needs to get involved, the following usually occurs:
- Gems in GitLab Rails are audited to see if they are all compatible with the new Ruby version.
- Scheduled pipelines in `gitlab-org/gitlab` run against master with the new Ruby version.
- Engineering productivity team updates MR pipelines in `gitlab-org/gitlab` to use the new Ruby version.
- Test-platforms team runs the [GitLab Performance Tool](https://gitlab.com/gitlab-org/quality/performance) on an environment running the new Ruby version.

At the same time, Distribution starts doing the following. Example Distribution epic: https://gitlab.com/groups/gitlab-org/-/epics/11659
- Support building packages containing new Ruby version.
- Switch feature branch builds to new Ruby version.
- Switch nightly builds to use the new Ruby version. This is usually done after the gem audit is complete.

After this, Delivery can start our work.

Create an epic in the `gitlab-com/gl-infra` group for Delivery work, and link it to the main epic. The Delivery epic should be a child epic
of the "Release Velocity" epic.

## Test rollouts

Depending on the expected risk from the Ruby upgrade, Delivery can do a test rollout of the new Ruby version to gstg-cny and gstg-ref a few weeks before the production rollout.
If all goes well, Delivery can do a test rollout to gprd-cny as well, where it can get real production traffic.
Production will not be affected by a test rollout.

Date & time of test rollouts should be coordinated with the Release Managers, and should be scheduled such that they do not clash with release dates.

Test rollouts usually take between 4 and 6 hours, assuming the package sits on the desired environment for about 2 hours.
During this time, auto-deploys will be blocked. Auto-deploys will be unblocked at the end of the test rollout procedure.

To start a deployment pipeline containing the new Ruby version:

- Run `git commit --allow-empty -m "Empty commit to trigger a new auto-deploy pkg"` to add an extra commit to the Omnibus or CNG auto deploy branch.
  This new commit will be used to trigger a new deployment pipeline in the next step.

- Trigger a deployment pipeline by running the `MANUAL auto-deploy pick&tag` inactive manual scheduled pipeline: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/.
  Make a note of the tag of the created pipeline.

- Cancel the Omnibus and CNG packager pipelines created for the tag noted in the previous step
  - https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines?scope=tags&page=1
  - https://dev.gitlab.org/gitlab/charts/components/images/-/pipelines?scope=tags&page=1

- Manually start new packager pipelines on the same tags.
  Set the following variables when starting the pipelines:
  - `USE_NEXT_RUBY_VERSION_IN_AUTODEPLOY` to true

  Check with Distribution team to confirm the variables and their values that need to be set on the packager pipelines.

  Check that the default value of `next_ruby_version` (in [software/ruby.rb](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/blob/master/config/software/ruby.rb)) is the expected version.

  Use the following links to start the new packager pipelines:
  - https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines/new?var%5BUSE_NEXT_RUBY_VERSION_IN_AUTODEPLOY%5D=true
  - https://dev.gitlab.org/gitlab/charts/components/images/-/pipelines/new?var[USE_NEXT_RUBY_VERSION_IN_AUTODEPLOY]=true

- Keep an eye on the deployment pipeline and do not allow it to be deployed to production.
  If the deployment is meant to be deployed only to gstg-cny, you can cancel the `validate_ownership:gstg-cny` job to prevent
  a deployment to gprd-cny from starting.

- The next regular auto-deploy pipeline will deploy a package containing the old version of Ruby and automatically remove
  the package containing the new version of Ruby.

## PCL

A PCL places restrictions on execution of CRs and Feature flag toggles.
This can make it easier to identify the cause of a production incident, simply because there are fewer possible causes.
A detailed description of a PCL can be seen at <https://handbook.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl>.

A hard PCL is suggested for major version upgrades.
A soft PCL is suggested for minor version upgrades.
These are not compulsory requirements, but are left to the DRI to take a call if a PCL is required.

If there is going to be a PCL during the production deployment, make sure to:
- Add the PCL to https://gitlab.com/gitlab-com/www-gitlab-com (example MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/118681)

- Broadcast the PCL date and time to:
  - The Engineering week in review document
  - And the following Slack channels:
    - #whats-happening-at-gitlab
    - #backend
    - #development
    - #test-platform
    - #releases
    - #infrastructure-lounge
    - #reliability-lounge
    - #eng-managers

## Deploying to production

Create a Change Request to deploy the new Ruby version to `gprd`.

Example CR: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18519

- Set a date for the production deployment in coordination with Release Managers.
  Ideally, try to deploy early in the monthly release cycle so that there is time to fix any bugs before the monthly release date.
- Make sure to define a rollback strategy in the CR.
- Ask the Ruby upgrade group for engineers from the Development department and SRE from the Infrastructure department to help keep an eye on the environments to make
  sure there are no adverse effects from the Ruby upgrade.
- Keep the SRE-on-call and Release Managers informed about the progress of the CR.

### Deploy to zonal clusters before regional cluster

We can leverage the variable `MANUAL_GPRD_DEPLOY` in https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/settings/ci_cd
to make the kubernetes deploy jobs manual and independent of each other.

Setting `MANUAL_GPRD_DEPLOY` to `true` allows us to:

- Manually deploy/start gprd-us-east1-b:auto-deploy
- Manually deploy/start gprd-us-east1-c:auto-deploy
- Manually deploy/start gprd-us-east1-d:auto-deploy
- Then manually deploy/start gprd:auto-deploy

This is to minimize risk of `sidekiq` changes in the regional cluster deploy.
It also allows for us to easily add baking/monitoring time inbetween the deployments.

Remember to set the variable to `false` after the `gprd-kubernetes` pipeline gets created.

## Rollback

Rollback strategy must be defined on the Change Request used for the `gprd` deployment. See example CR on the section above.

Steps:

1. Before starting the CR note the previous Ruby version package that was successfully deployed to production
2. To Rollback production-canary only
  - If you have not promoted to production and need to rollback production-canary, follow the following steps:
  - Notify the EOC, Release Managers in #production that Production Canary will be drained.
    - `/chatops run canary --disable --production`
  - Follow the steps in [Make sure that the next auto deploy package will be built with previous Ruby version](#make-sure-that-the-next-auto-deploy-package-will-be-built-with-previous-ruby-version) and deploy the new package to gprd-cny.
3. To Rollback production and staging
  - If we need to rollback production and staging, follow the steps in https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/rollback-a-deployment.md to rollback to the previous Ruby version package.

### Make sure that the next auto deploy package will be built with previous Ruby version

1. Set `USE_NEXT_RUBY_VERSION_IN_AUTODEPLOY` to false in https://dev.gitlab.org/gitlab/omnibus-gitlab/-/settings/ci_cd.
2. Set `USE_NEXT_RUBY_VERSION_IN_AUTODEPLOY` to false in https://dev.gitlab.org/gitlab/charts/components/images/-/settings/ci_cd.
  - If you had already unpaused auto-deploys, cancel any auto-deploy pipelines whose packages were built before you changed the `USE_NEXT_RUBY_VERSION_IN_AUTODEPLOY` variable to false.
3. Revert MR to update README. (MR tbd)
4. Set label change aborted `/label ~change::aborted`

## After deployment to production

### Update gitlab CI's default ruby version

* After we ensure that we are not rolling back after the deployment, update the `RUBY_VERSION_DEFAULT` in the [gitlab CI version file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/version.yml) to the new Ruby version.

* Also update `OMNIBUS_GITLAB_CACHE_EDITION` in [gitlab CI config file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab-ci.yml) to use the new Ruby version as suffix.

The variables `RUBY_VERSION_DEFAULT` and `RUBY_VERSION_NEXT` will be the same for a brief period of time after switching to a new version.
Similarly, value of `OMNIBUS_GITLAB_CACHE_EDITION` will be same in different scenarios for a brief period of time after switching to a new version.

## Related links

Ruby 3.1 (minor version) upgrade:
- Delivery epic: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1128

Ruby 3 (major version) upgrade:
- Delivery epic: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/865
- Rollout retrospective: https://gitlab.com/gl-retrospectives/enablement-section/application-performance-team/-/issues/59
