---
tags:
  - auto-deploy
---
## `secpick` script

This is a small script that helps cherry-picking across multiple releases. It will stop
if there is a conflict while cherry-picking, otherwise will push the change to GitLab Security.

### Quickstart

An easy way to use this script is to:

- start on the original `security-` branch for the fix
- ensure the commits are squashed
- pass the backport release number
- pass the option --mr to have the merge request created automatically

```sh
$ git checkout security-example-worker

$ bin/secpick -d -v 13.5 --mr
```

### Options

To see the list of options available run:

```sh
$ bin/secpick --help

Usage: bin/secpick [options]
    -v, --version 10.0               Version
    -b, --branch security-fix-branch Original branch name (optional, defaults to current)
    -s, --sha abcd                   SHA or SHA range to cherry pick (optional, defaults to current)
    -r, --remote dev                 Git remote name of security repo (optional, defaults to `security`)
        --mr, --merge-request        Create relevant security Merge Request targeting the stable branch
    -d, --dry-run                    Only show Git commands, without calling them
    -h, --help                       Displays Help
```

### Example

```sh
bin/secpick -v 10.6 -b security-fix-mr-issue -s SHA
```

You can also pick a range of commits. To pick a range from `aaaa` (inclusive)
to `bbbb` you can:

```sh
bin/secpick -v 10.6 -b security-fix-mr-issue -s aaaa^..bbbb
```

It will change local branches to push to a new security branch for each specified release,
meaning that local changes should be saved prior to running the script.


#### Dry run

We can see what `git` commands will be executed by performing a dry run. For example when creating a backport for `15-5`:

```sh
$ git checkout security-example-worker

$ bin/secpick -d -v 15.5 --mr

Git commands:
git fetch security 15-5-stable-ee
git checkout -B security-example-worker-15-5 security/15-5-stable-ee --no-track
git cherry-pick f6407f7120c57286af7b200b86a1afd4a8f3a990
git push security security-example-worker-15-5 --no-verify -o mr.create -o mr.target='15-5-stable-ee' -o mr.description='Please apply Security Release template. /milestone %15.5'
git checkout security-example-worker

Merge request params:
{:issuable_template=>"Security Release",
 :merge_request=>
  {:source_branch=>"security-example-worker-15-5",
   :target_branch=>"15-5-stable-ee"}}
```

#### Actual run

```sh
$ bin/secpick -v 15.5 --mr
Auto-merging db/structure.sql
Auto-merging config/sidekiq_queues.yml
Auto-merging config/initializers/1_settings.rb
Removing app/workers/security-example-worker.rb
Auto-merging app/workers/all_queues.yml
[security-example-worker-15-5 35683040af4] Prevent abuse of ExampleWorker
 Date: Fri Oct 30 09:12:42 2020 +0100
 20 files changed, 252 insertions(+), 81 deletions(-)
 create mode 100644 app/finders/examples/example_finder.rb
 delete mode 100644 app/workers/security-example-worker.rb
 create mode 100644 app/workers/examples/security-example-worker.rb
 create mode 100644 app/workers/examples/manage_example_worker.rb
 create mode 100644 changelogs/unreleased/security-example-worker.yml
 create mode 100644 db/migrate/20201029144524_add_index_to_examples.rb
 create mode 100644 db/schema_migrations/20201029144524
 create mode 100644 spec/finders/examples/example_finder_spec.rb
 rename spec/workers/{ => examples}/security-example-worker_spec.rb (95%)
 create mode 100644 spec/workers/examples/manage_example_worker_spec.rb

From gitlab.com:gitlab-org/security/gitlab
 * branch                    15-5-stable-ee -> FETCH_HEAD
 * [new branch]              15-5-stable-ee -> security/15-5-stable-ee
Switched to a new branch 'security-example-worker-15-5'
remote:
remote: View merge request for 'security-example-worker-15-5'
remote:   https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1234
remote:
To gitlab.com:gitlab-org/security/gitlab.git
 * [new branch]              security-example-worker-15-5 -> security-example-worker-15-5
Switched to branch 'security-example-worker'

https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/new?issuable_template=Security+Release&merge_request%5Bsource_branch%5D=security-example-worker-13-5&merge_request%5Btarget_branch%5D=13-5-stable-ee
```

The git push output contains a link to the newly created backport MR.

Click on that to complete the process and repeat for each additional backport.
