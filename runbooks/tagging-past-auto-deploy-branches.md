---
tags:
  - auto-deploy
---
# Tagging Past Auto-deploy Branches

## Overview

This document describes the procedure to lock Auto-Deploy operations
on an older branch.

## Locking auto-deploy to a specific branch

In a highly unstable production environment, a release manager may
decide to stick with an old auto-deploy branch. This option is
extremely valuable closer to a release day or during a security
release.

### Instructions

Locking and unlocking auto-deployments is done using chatops. To lock the
deploys to the latest auto-deploy branch deployed to production, run:

```text
/chatops run auto_deploy lock
```

This deactivates the [`auto_deploy:prepare` schedule][auto_deploy:prepare], and
changes the `AUTO_DEPLOY_BRANCH` [release-tools variable][release-tools
variables] to store the latest auto-deploy branch that is deployed to
production.

If you want to instead lock it to a specific branch (`foo` here), run:

```text
/chatops run auto_deploy lock foo
```

From now on, all the auto-deploy commands, like `auto_deploy:tag` and
`auto_deploy:pick`, will operate on the locked branch.

Unlocking deploys is done using the `unlock` command:

```text
/chatops run auto_deploy unlock
```

After this, auto-deploy branches are created again as usual.

### Optional steps

Depending on the gravity of the situation, it may be wise to also
disable auto-deploy cherry-picks and/or change the default deployment
environments.

#### Disabling cherry-picks

If the instability is such that the release managers want to keep
maximum control over the content of the builds, consider disabling
[`auto_deploy:pick` schedule][auto_deploy:pick].

### Cleanup

When the cause of the instability has been fixed, release managers
should resume normal operations.

1. Activate [`auto_deploy:prepare` schedule][auto_deploy:prepare].
1. Run [`auto_deploy:prepare` schedule][auto_deploy:prepare].
1. **Optional** activate [`auto_deploy:pick` schedule][auto_deploy:pick].

[auto_deploy:prepare]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/73/edit
[auto_deploy:pick]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/72/edit
[release-tools variables]: https://ops.gitlab.net/gitlab-org/release/tools/-/settings/ci_cd
