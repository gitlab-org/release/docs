---
tags:
- auto-deploy
---
# Patch Releases

Patch releases are performed according to the [GitLab Maintenance Policy], they include bug fixes for the current
stable released version of GitLab and security fixes for the current stable and previous two GitLab versions.

* [Engineer runbook for backporting bug fixes]
* [Engineer runbook for preparing security fixes]
* [Release Managers runbook]
* [Release Information Grafana dashboard]

## Patch release types

At GitLab, there are two types of patch releases:

1. **Planned (default)**: An SLO-driven patch to publish all available bug and vulnerability fixes per
   the [GitLab maintenance policy]. Scheduled twice a month on the Wednesday before and after the [monthly release], planned patches comply
   with the [bug SLO] and the [security remediation SLAs]. Patches that include [`critical` vulnerabilities] will be considered critical patches.
1. **Unplanned**: An immediate patch required outside of the planned patch release cadence to mitigate a high-severity (critical) vulnerability.
   These ad-hoc patches are the result of an incident, they require an RCA done by the team that introduced the high-severity vulnerability and
   forced an unplanned release. The AppSec team is responsible for assessing the vulnerability, working with engineering to decide on the best approach
   to resolve it and coordinating with release managers on a timeline for the release.

There is also an [internal-use patch release] which is created in limited cases to mitigate GitLab
Dedicated tenants in light of high-severity issues before publicly releasing bug fixes.

Patches outside the maintenance policy for bug and security fixes must be requested and agreed upon by the
Release Managers and the requester (see [backporting to versions outside the Maintenance Policy] for details).

Once the release managers begin preparing a patch release they may make the decision to remove any lower severity MRs.
This is to avoid an S1 fix, or large number of other fixes from being delayed by a lower severity fix.

## Planned patch releases

Default GitLab patches scheduled twice a month on the Wednesdays of the weeks before and after the monthly release,
to meet the [bug SLO] and [security remediation SLAs]. The schedule is best-effort and dates might be subject to change.

* **Note** For the current information and status of the active patch release, have a look at the [Release Information Grafana dashboard](https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1).

These patches include all bug and security fixes ready at the time of the patch release preparation. Bug fixes are
worked on in the canonical GitLab repositories, while security fixes are worked on in the mirrored GitLab security repositories
to avoid revealing vulnerabilities before the release.

## Unplanned patch releases

Unplanned patch releases are ad-hoc processes used to immediately patch and mitigate a high-severity (`critical`) vulnerability
in order to meet the [security remediation SLAs]. These releases will likely only include the security fix for the high-severity vulnerability

## Process

Regardless of the type, the process is the same for all patches:

![patch release diagram](./images/patch_release_diagram.jpg)

[Source] - Internal link only

At any given time, GitLab Engineers prepare bug fixes for the current version and vulnerability fixes
to the current and previous two GitLab versions:

- **1a. Bug fixes are prepared** - A merge request backporting a bug fix to the current version is prepared by GitLab Engineers.
- **1b. Vulnerability fix prepared** - GitLab engineers fix vulnerabilities in the relevant [Security repositories].

Patch Releases are published on the Wednesday on the weeks before and after the Monthly Release.

Two days before the planned due date (usually on Monday), release managers start the patch release process, they make sure that
all prepared bug and security fixes are safely released. Deployments to GitLab.com run in parallel.

A patch release has the following phases:

- 2. **Initial steps**, Monday: Release preparation begins when release managers run the prepare chatops command to create the new
    release task issue to guide the patch release. From here they follow the checklist to complete the initial set up and
    communication issues needed to prepare the release.
- 3. **Early Merge Phase**, Monday: Release Managers deploy security fixes to GitLab.com. Fixes with the ~"security-target" label
    that are linked to the security tracking issue will have the MR targeting the default branch merged. This allows fixes
    to be deployed to GitLab.com before they are released to self-managed users.
- 4. **Merge backports**, Tuesday: The day before the release due date, backports with security fixes targeting the supported versions are merged.
     At this point, everything included in the patch must be deployed to GitLab.com, and backports must apply to all stable branches.
- 5. **Release preparation**, Tuesday: When all fixes are deployed and merged, Release managers prepare, test and publish the packages.
- 6. **Final steps**, Wednesday: At this point patch release packages are available to all users. Release managers wrap up the final steps of the patch release
    by publishing the blog post and syncing to Canonical to return to our default state of working in the open.

Above timeline is best effort and is subject to change pending on GitLab.com availability.

Details of step 1a can be seen on [Engineer runbook for backporting bug fixes], details of step 1b can be found on
[Engineer runbook for preparing security fixes], and details for steps 4 and 7 can be found on [Release Managers runbook]

---

[Return to Guides](../README.md#guides)

[`critical` vulnerabilities]: https://handbook.gitlab.com/handbook/security/product-security/vulnerability-management/sla/
[backporting to versions outside the Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
[bug SLO]: https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/issue-triage/#severity-slos
[Engineer runbook for backporting bug fixes]: engineers.md
[Engineer runbook for preparing security fixes]: ../security/engineer.md
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[monthly release]: https://about.gitlab.com/releases/
[overview]: #overview
[planned]: #planned-patch-release
[Release Managers runbook]: release_managers.md
[Release Information Grafana dashboard]: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1
[Source]: https://docs.google.com/presentation/d/12JXlLnZ8lQp7ATdaSoL4x_oCUv04rmqzYp6dQb8AXHE/edit#slide=id.g2d0bc50ab08_0_5
[security repositories]: https://gitlab.com/gitlab-org/security/
[security remediation SLAs]: https://handbook.gitlab.com/handbook/security/product-security/vulnerability-management/sla/
[unplanned critical]: #unplanned-critical-patch-release
[unplanned critical release]: unplanned_critical_release.md
[internal-use patch release]: general/patch/patches_for_internal_use.md
