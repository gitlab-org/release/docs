#!/bin/bash

script_dir="$(dirname "$0")"
# Define working dir
WORKING_DIR="$(cd "$1" || exit; pwd)"
# Define header/title of the README file
HEADER=$2

if ! diff -u "$WORKING_DIR"/README.md <(bash "$script_dir"/generate-toc-file.sh "$WORKING_DIR" "$HEADER");
then
  printf 'Table of Content is not updated. Please run '\''bash scripts/generate-toc-file.sh "%s" "%s" > %s/README.md'\'' to update.\n' "$WORKING_DIR" "$HEADER" "$WORKING_DIR"
  exit 1;
else
  printf "Table of Content is up to date.\n"
fi