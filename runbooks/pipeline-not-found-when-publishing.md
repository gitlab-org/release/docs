---
tags:
  - release-management
---
# `PipelineNotFoundError` when publishing a package

During a self-managed release process, there are two distinct operations
regarding the production of the final packages: tagging and publishing.

Those two steps are connected by the same pipeline that is generated when we
tag a package. Those pipelines, one for each tag on each packager (CNG and
Omnibus), lives on our DEV instance and have a manual job at the end that
release-tools plays when we publish a package.

Due to the load on the DEV instance, it is possible that a tag will not produce
its pipeline. When this happen, the publishing step will fail with an error
similar to the one below.

```
❌ The section "Publishing CNG" produced an error:
2023-06-29 08:39:42.703069 F Rake::Task -- Task failed -- Exception: ReleaseTools::Services::BasePublishService::PipelineNotFoundError: Pipeline not found for v16.1.1-fips
```

## Recovery steps

1. Create the missing pipeline from the GitLab UI, selecting the missing tag
   that we can see on the error above from the "Run pipeline" page of
   [Omnibus](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines/new) or
   [CNG](https://dev.gitlab.org/gitlab/charts/components/images/-/pipelines/new).
1. Wait for the pipeline to complete. It will reach the `blocked` state.
1. Re-try the failed publishing job.


