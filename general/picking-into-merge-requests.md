---
tags:
  - release-management
---
## Merge CE stable changes to EE

[Merge Train](https://gitlab.com/gitlab-org/merge-train) attempts to do this on
a [regular cadence](https://ops.gitlab.net/gitlab-org/merge-train/pipeline_schedules). Make
note that if you merge the default branch into stable at the right time, you might create a
slight divergence that you need to take care of.

There are a few approaches to doing the CE->EE merge: CE `X-Y-stable-patch-Z`
can be merged into the existing EE preparation MR, can be merged into a new MR
to fix conflicts, or could even be merged into stable.

Keeping the stable branches clean can help if you unexpectedly need to do a
security release on those branches. Beyond that the trade-offs are complexity,
checking pipelines are green on the branch vs MR, and creating a workflow which
allows you to do things quickly and in parallel.

### To merge CE into the EE MR

1. Check out an up to date  `X-Y-stable-ee-patch-Z` branch in your local EE repo, or pull changes.
    ```shell
    git fetch origin
    git checkout -b X-Y-stable-ee-patch-Z origin/X-Y-stable-ee-patch-Z
    ```

1. Fetch `X-Y-stable-patch-Z` branch from CE ready for merging
    ```shell
    git fetch git@gitlab.com:gitlab-org/gitlab-ce.git X-Y-stable-patch-Z
    ```

1. Merge that branch into the current EE preparation branch
    ```shell
    git merge FETCH_HEAD
    ```

1. Optional: repeat previous two steps with `X-Y-stable` to double check that no new changes have been introduced outside of the CE preparation MR.

1. Optional: Create a new branch if you'd like to fix conflicts in a separate MR

1. Fix simple conflicts and push
    ```shell
    git push origin X-Y-stable-ee-patch-Z
    ```

1. Optional: Ask others to help fix conflicts in the MR.  Engineers may need to
   create a new MR targeting the specific branch.
