---
tags:
  - auto-deploy
---
# Implementing a recurring rollback practice

In Q2 FY22 we implemented tooling and process to support rollbacks on Staging and Production. Throughout Q3 we're continuing to improve reliability of [Assisted Rollbacks](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/282) as a path towards [Hands-off Deployments](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/280).

Running regular practice rollbacks on Staging will help to build confidence in the tools and process. Suggested Staging Rollback sessions have been added to the release issue template. Release Managers should attempt to schedule rollbacks spread across the Release Managers over the course of a release issue.

## Document

When performing the procedure below, make every attempt to document the before
and after snapshot of what staging looked like, and link to the various
pipelines and results of Chatops commands.  Doing so enables us to ensure that
the procedure works as desired and provides a snapshot of time in the case we
see a failure.

1. Document/Screenshot the version running noted at
   <https://staging.gitlab.com/help>
1. Screenshot the results of all Chatops commands run in the procedure
1. Link to the pipeline created for the rollback
1. Document/Screenshot the result of the version after the rollback is complete
1. Because this is a practice session, there are multiple options for bringing
   staging back into alignment or continuing forward, ensure to document the
   chosen method and the reason why.

## Process

1. On the suggested date Release Managers should plan a Staging rollback, or schedule for a suitable date if needed.
1. A suitable time to run should be considered:

   * Choose a time that is in between deployment pipelines if able
   * Validate there is enough time between starting the session and when
     `gstg-cny` would receive a new package to prevent QA interference

1. A suitable package should be identified using the [runbook].
1. Rollback timing is decided by the Release Manager. Rolling back a suitable
   package once it has started to deploy to Production can help minimise the
   impact a test rollback will have on deployments and MTTP.
1. Follow the [runbook] for steps to complete the rollback.
   * Check the accuracy of the rollback pipeline. It **should** include:
      * Preparation and tracking
      * Kubernetes Deployment job
      * QA tests
      * Manual Gitaly job
   * It **should not** include:
      * Post-deploy migration jobs
1. Please open issues in the [Delivery issue tracker][issues] for problems or improvements following the test.
1. Roll forward to the package deployed prior to the rollback (essentially
   "undoing" the rollback) to [keep consistency with Production][consistency]. See the <https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/deploy-to-a-single-environment.md> for steps
1. Enable Staging canary again if you disabled it during the practice:

```plaintext
/chatops run canary --enable --staging
```

[runbook]: runbooks/rollback-a-deployment.md
[consistency]: runbooks/rollback-a-deployment.md#6-ensure-consistency-between-staging-and-production
[issues]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues
