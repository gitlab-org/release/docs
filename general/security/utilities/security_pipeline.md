---
tags:
  - release-management
---
# The security pipeline

When a patch release is started, a pipeline is created to complete many tasks on behalf of
the release manager. The  Release Task Issue will contain a link to this pipeline in
any steps where the release manager needs to go and manually start a job.

Usually, when the release manager starts one job, it causes several jobs to run in the next
stage or stages. The success of failure of those jobs are posted in slack in `#f_upcoming_release`.

See the [release pipeline documentation] for
information about the development of these pipelines and jobs.

![Security pipeline](general/images/security-pipeline.png)

## What to do if a job fails

If a job fails, the job log likely includes further instructions, including instructions on
how to perform the task manually if needed. You may have to scroll up a ways beyond the stack trace
to the top of the error message to see these instructions.

It is ok for jobs to be left in a failed state. If something ends up being completed
manually, or the release manager decides a certain failure can be ignored, they
have the power to do so and continue forward with the release at their discretion.

[release pipeline documentation]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/doc/release-pipelines.md
