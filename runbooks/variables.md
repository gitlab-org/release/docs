---
tags:
  - auto-deploy
---
# Deployer CI variables

Our tooling makes heavy use of environment variables for functionality. This
document aims to provide a reference for the most important ones, but is not
necessarily comprehensive.

Please note that items that are specific to configurations or are set by CI jobs
may not be documented below.  For these, refer to the `gitlab-ci.yaml` or the
CI/CD Variables for that project.

## Change-lock

https://gitlab.com/gitlab-com/gl-infra/change-lock

| Variable Name                  | Purpose         |
| ------------                   | ------------    |
| `CHANGE_LOCK_FORCE_FAILURE`    | Forces change-lock to halt immediately, this will exit the job and report a job failure to CI |
| `CHANGE_LOCK_OVERRIDE`         | Overwrites the configuration when pipelines attempt to run during times which a change lock would normally fail a pipeline |

## Deployer

https://ops.gitlab.net/gitlab-com/gl-infra/deployer

**Note**: `Boolean` types **must** be set to `true` to enable, empty values as well as any other string but `true` will evaluate to `false`.

| Variable Name             | Value | Purpose         |
| ------------              | ---   | ------------    |
| `ALLOW_TRACK_FAILURE`     | `Boolean`  | When set to `true`, allows deployment tracking jobs to fail. Be sure to create an issue in [gl-infra/delivery](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues) when there is a tracking failure, and in that issue mention this override is set so we don't forget to unset it. |
| `CHECKMODE`               | `Boolean`  | Forces ansible to run in dry-run |
| `CMD`                     | `String`   | Instructs ansible to run specific commands, see [one-off command](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/tree/master/cmds) documentation |
| `CNY_MANUAL_PROMOTE`      | `Boolean` | Disables automatic Canary deployments, making it a manual job instead |
| `SKIP_WAIT_FOR_PID_CHANGE`| `Boolean`  | Instructs Ansible to not wait for PID changes during rolling deployments/restarts of services |
| `DEPLOY_ENVIRONMENT`      | `String`   | The Environment(s) to which Ansible will attempt to perform a deployment or run a custom action |
| `DEPLOY_ROLLBACK`         | `Boolean`  | Utilized during times when one must rollback a production deployment.  This essentially reverses a few jobs in the pipeline to minimize the potential for user surfacing errors in the live environment. |
| `DEPLOY_VERSION`          | `String`   | Instructs Ansible which version of GitLab is to be installed |
| `GITLAB_INSTALL_OVERRIDE` | `Boolean`  | Always install the omnibus package, even if the version hasn't changed. |
| `IGNORE_PRODUCTION_CHECKS`| `String`   | Allows a production deploy to continue despite any warnings detected by the release-tools auto_deploy:check_production failing any checks. Production checks are ignored when this variable is set to any non-zero length value |
| `PRECHECK_IGNORE_ERRORS`  | `Boolean`  | Instructs Ansible to ignore failures in our `prechecks_pipeline` play |
| `ANSIBLE_SKIP_TAGS`       | `String`   | Skip certain tasks during deploy, setting this to `haproxy` will skip all haproxy operations (removing, adding nodes, etc.) and is the same as passing `--skip-haproxy` to the deploy chatops command. |
| `SKIP_OMNIBUS_ROLE_CHECK` | `Boolean`  | Bypass the check that will not disable omnibus version updates, if they are already disabled |
| `PIPELINE_TRIGGER_OVERRIDE` | `Boolean`  | Bypass `pipeline-trigger` which is used for triggering pipelines |
| `TRIGGER_MAX_DURATION` | `String`  | Amount of time to wait in seconds for a trigger to be successful, defaults to 1800 (30 minutes in seconds) |
| `TRIGGER_INTERMEDIATE_PRODUCTION_CHECK` | `Boolean` | When set to  `true`, triggers production checks after the current hosts' batch is completed |
| `ES_EVENT_OVERRIDE` | `Boolean` | When set to  `true`, will skip event notifications to ElasticSearch, see https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/events/README.md |

### K8S-Workloads

https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com

| Variable Name                  | Type | Purpose         |
| ------------                   | ---- | ------------    |
| `CLUSTER_SKIP`                 | `String`  | Instructs `k-ctl` to skip the cluster specified in this variable.  Refer to: https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/TROUBLESHOOTING.md#skipping-cluster-deployments |
| `SKIP_QA`                      | `Boolean` | Instructs `.gitlab-ci.yml` to skip running a QA job after the deploy has completed |
| `IMAGE_CHECK_OVERRIDE`         | `Boolean` | Skips the image check which will allow deploys when there are additional configuration changes |
| `POST_DEPLOYMENT_PATCH`        | `Boolean` | Indicates that we are performing a post-deployment patch, which will skip image checks since we do not support patching components like gitlab-shell |
| `ES_EVENT_OVERRIDE`            | `Boolean` | When set to  `true`, will skip event notifications to ElasticSearch, see https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/events/README.md |

:warning: **The above lists are not exhaustive.  Please refer to the
.gitlab-ci.yml file for any items that are set by our own CI jobs**

# Release Tools CI Variables

Many variables are associated with our release-tools.  This list can be found
here: https://gitlab.com/gitlab-org/release-tools/-/blob/master/doc/variables.md

# Best practices

## Situational variables

Several variables (such as `SKIP_OMNIBUS_ROLE_CHECK`) are used situationally and
usually get deleted once they're no longer needed.

As an alternative, consider prefixing these variables with double underscores
(`__`) so that they're no longer active, but still readily available when the
need arises.

---

[Return to Documentation](../README.md#documentation)
