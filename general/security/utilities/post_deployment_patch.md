---
tags:
  - auto-deploy
---
### Creating a post-deployment patch

For high severity ~S1 issues, a post-deployment patch may be required.

If a post-deployment patch was not already created as part of the initial response
to an ~S1 issue, verify with the security engineer if one is required. If an
existing patch exists, work with the security engineer to determine if the
temporary patch should be replaced with one based on the completed solution.

Follow the [post-deployment patch process](../../deploy/post-deployment-patches.md)
