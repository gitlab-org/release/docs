---
tags:
  - auto-deploy
---
# Overview of the auto-deploy pipelines

The auto-deploy process relies on three [pipelines schedules]:

1. `auto_deploy: Create new package`
2. `auto_deploy: Deploy latest available package`
3. `auto-deploy:cleanup`

The code definition of these rake tasks can be found on release-tools, particularly
on the [`auto-deploy.rake`] file.

## `auto-deploy: Create new package`

Please see [auto-build](../auto-build/readme.md) for details about the process of building
packages.

## `auto_deploy: Deploy latest available package`

This runs the `auto_deploy:deploy_latest` rake task. It checks if the previous deployment pipeline has
completed deploying to `gprd-cny` and only then it looks for a suitable package.

If the time is right to start a new deployment pipeline, it searches for the latest package that is
ready to be deployed. If a package is found, a deployment pipeline for that package is started.

In the deployment pipeline, before starting the rollout to the first environment,
the pipeline for the GitLab project is verified to ensure that it is successful now.

For more details about how deployment pipelines are triggered, see
[Code structure for triggering deployment pipelines](code_structure_for_creating_deployment_pipelines.md).

## `auto-deploy:cleanup`

Auto-deploy branches are kept for seven days, branches older than that are deleted
by this task that is executed every day at 12UTC.

### Cleanup of packages

release-tools does not handle cleanup of packages.

- Omnibus packages are not automatically deleted from the packages server.

- CNG images are automatically removed after the threshold of 3000 is crossed.

## FAQ

### How do I start a deployment pipeline for a particular package?

You can find the version numbers for the latest packages at:
- https://ops.gitlab.net/gitlab-org/release/metadata/-/commits/
- https://auto-deploy-view-e121e0.gitlab.io/

Once you know the version that you would like to start a deployment pipeline for, you can use the
`/chatops run auto_deploy pipeline <version>` ChatOps command
to start a deployment pipeline for a particular version. It runs the `auto_deploy:deploy_version`
rake task.

Note that the `version` needs to be a normalized version like `17.9.202501280806`.

The ChatOps command takes an optional `--force` flag to skip some automated checks
that might prevent the deployment pipeline from being created. With the flag set, the command
will not check if the packager pipelines (Omnibus and CNG) have completed. It will also not
check that the selected version is newer than the version of the previous deployment pipeline.
This can be used during exceptional circumstances (for example incident remediation) when Release
Managers want to force the deployment a particular package.

### How do I view a list of deployment pipelines?

To view decoupled deployment pipelines, use the following link:
<https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines?page=1&scope=all&source=api&username=gitlab-release-tools-bot>

The above list will also contain patch release pipelines. Deployment pipelines can be identified by
their name.

Decoupled deployment pipelines are named as `Deployment pipeline - <version>`, for example
`Deployment pipeline - 17.7.202411221205`. Having the version number in the pipeline name makes
it easy for Release Managers to identify which version is being deployed by the pipeline.

### How are new deployment pipelines automatically created?

New deployment pipelines are started by:

- [`auto_deploy: Deploy latest available package` scheduled pipeline](#auto_deploy-deploy-latest-available-package)

- A `deploy:start-new-pipeline` job in each deployment pipeline also starts a new deployment pipeline
  if [conditions are right](code_structure_for_creating_deployment_pipelines.md#determining-if-a-new-deployment-pipeline-can-be-started).

### How do I cancel a deployment pipeline so that release-tools creates a new one for the next package?

If a particular deployment needs to be stopped, you can cancel the main coordinator pipeline by doing the following:
- Cancel a pending job instead of a running job.
- Once the running job completes, cancel the coordinator pipeline by clicking on the "Cancel" button on the
  top right of the pipeline view.

Some points to keep in mind:
- Do not cancel a running job since that can leave the system in an unknown state.
- Canceling a deployer pipeline while it is deploying to k8s can leave Helm in a bad state,
  which will require manual intervention to fix.

![Cancel a pipeline](../images/cancel-pipeline.png)

### How to stop automatic deployments?

[Pausing auto deploy](stopping-auto-deploy.md)

### What alerts can Release Managers receive during release management?

[Release management alerts](../../release_manager/alerts.md)

### How do I create a new package?

Execute the [`auto_build: Create new package` scheduled pipeline][pipelines schedules]. This will
create a new auto deploy branch if the [`AUTO_DEPLOY_SCHEDULE` project variable][project variables] contains the
current UTC hour. If a branch already exists for the current hour, a new branch will not be created.

[`auto-deploy.rake`]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/auto_deploy.rake
[pipelines schedules]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[project variables]: https://ops.gitlab.net/gitlab-org/release/tools/-/settings/ci_cd
