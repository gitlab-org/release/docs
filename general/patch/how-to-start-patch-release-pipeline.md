# How to start Patch Release pipeline?

## Complete pipeline

To start a complete Patch Release pipeline, pass the variable `SECURITY_RELEASE_PIPELINE=true` when creating a new pipeline ([quick link](https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/new?var%5BSECURITY_RELEASE_PIPELINE%5D=true)).

## Run a specific stage

To run a specific stage in the release pipeline, pass the name of the stage via `SECURITY_RELEASE_PIPELINE`. For example, to start the `prepare` stage, pass `SECURITY_RELEASE_PIPELINE=prepare` ([quick link](https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/new?var%5BSECURITY_RELEASE_PIPELINE%5D=prepare)). For the detailed implementation, check [`.gitlab/ci/security`](https://gitlab.com/gitlab-org/release-tools/-/tree/master/.gitlab/ci/security) in `release-tools`.
