# GitLab Release Process

This repository contains instructions for releasing new versions of
GitLab Community Edition (CE), Enterprise Edition (EE) and GitLab.com release
related processes.

The goal is to provide clear instructions and procedures for our entire release
process. This repository includes documentation which should help perform the
role of [Release Manager](#release-manager) as well as
documentation that should help other stake-holders in the release process.

The topics are divided per each type of release. Each type of release has a
general process overview and specific documentation for different stakeholders.

## Release Manager

- [Getting started](release_manager/index.md#getting-started)
- [Big Picture](release_manager/big_picture.md)
- [Offboarding](release_manager/index.md#offboarding)
- [Release manager availability](release_manager/index.md#release-manager-availability)
- [How to temporarily join release management](release_manager/index.md#temporary-permissions)
- [Guide to Incidents](release_manager/release-manager-incident-guide.md)
- [Guide to Production Change Locks (PCLs)](release_manager/pcl-guide.md)
- [Required permissions to tag and deploy a release](general/permissions.md)
- [Pro tips](general/pro-tips.md)
- [How to manually create a new release tools pipeline](general/manually-create-release-tools-pipelines.md)
- [Alerts](release_manager/alerts.md)

## Releasing monthly stable version

- [General process overview](general/monthly/process.md)
- [Release manager in monthly release](general/monthly/release-manager.md)
- [Guidelines for a new major release of GitLab](general/major.md)
- [Deprecation Evaluation](runbooks/deprecation-evaluation.md)

## Releasing patch versions

- [General process overview](general/patch/readme.md)
- [Process for GitLab engineers](general/patch/engineers.md)
- [Process for Release Managers](general/patch/release_managers.md)
- [How to create a blog post for a patch release](general/patch/blog-post.md)
- [How to handle backport requests](general/patch/how-to-handle-backport-requests.md)
- [Running E2E for Backports](runbooks/backport-e2e-testing.md)
- [Patch release pressure](general/patch/patch_release_pressure.md)
- [Patch for a single version](general/patch/patches_for_single_version.md)

## Security release

- [General process overview](general/security/readme.md)
- [Release manager in security release](general/security/release-manager.md)
- [Security engineer in security release](general/security/security-engineer.md)
- [Developer in security release](general/security/engineer.md)
- [Quality engineer in security release](general/security/quality.md)
- [Merge train overview](general/security/utilities/merge_train.md)
- [How to sync Security with Canonical?](general/security/how_to_sync_security_with_canonical.md)
- [How to handle security merge requests from Gitaly and KAS](general/security/how_to_handle_gitaly_kas_security_merge_requests.md)
- [Guide to configuring security mirrors](general/security/mirrors.md)
- [Handling bugs introduced by security merge requests](general/security/bugs_introduced_by_security_merge_request.md)
- [Runbooks](./runbooks/security/README.md)

## Internal Releases

- [Readme](general/internal-releases/readme.md)
- [How to determine if a security fix includes assets modifications?](general/internal/determine-if-security-fix-includes-assets-modifications.md)

## Deployments

- [Overview of auto-build process](general/auto-build/readme.md)
- [Overview of the auto-deploy pipelines](general/deploy/overview-of-the-auto-deploy-pipelines.md)
- [Traffic Generation](general/deploy/traffic-generation.md)
- [Post Deployment Patches](general/deploy/post-deployment-patches.md)
- [Auto-deploy process](general/deploy/auto-deploy.md)
- [Deploy Failures](general/deploy/failures.md)
- [Temporarily stopping auto-deploy process](general/deploy/stopping-auto-deploy.md)
- [Components deployment and release](components/index.md)
- [How to set the auto-deploy branch schedule](general/how-to-set-auto-deploy-branch-schedule.md)

## Delivery metrics

- [Release Manager workload metrics](metrics/release_manager_workload.md)
- [Overview of the release management dashboard](release_manager/dashboard.md)
- [Overview of the Deployment SLO dashboard](general/overview-of-deployment-slo-dashboard.md)
- [Pipeline Traces Query Interface](https://observe.gitlab.com/v1/jaeger/1112072)
- [Troubleshooting Delivery-metrics server](metrics/troubleshoot-delivery-metrics.md)
- [Troubleshooting release status metrics](metrics/release-status-metrics.md)

## Guides

- [Overview of our Tooling](general/tooling.md)
- [PDM - Post deploy migrations](https://gitlab.com/gitlab-org/release/docs/-/tree/master/general/post_deploy_migration)
- [Overview of Building Packages](general/omnibus-packages/building-packages.md)
- [Guidelines for a new major release of GitLab](general/major.md)
- [Release template files](https://gitlab.com/gitlab-org/release-tools/tree/master/templates)
- [How the GitLab FOSS mirror is kept up-to-date](general/gitlab-foss-mirror.md)
- [QA in a local environment](general/qa-in-local-environment.md)
- [Overview of pre and release environments](general/pre-and-release-environments.md)
- [Guide to configuring security mirrors](general/security/mirrors.md)
- [Release metadata](general/deploy/release_metadata.md)
- [How to prepare for a Major milestone](general/how-to-prepare-for-a-major-milestone-release.md)
- [Overview of the releases.yml file](runbooks/updating-releases.md)
- [Delivery guide to the Dedicated group](general/gitlab-dedicated/index.md)
- [Overview of Ruby upgrade procedure](general/ruby-upgrades.md)
- [Clickhouse](general/clickhouse/index.md)

## Cells

- [Cells - Overview](cells/README.md)

## Delivery Runbooks

### Tokens and bots

- [How to request a new token](runbooks/token-management.md)

### GitLab.com

- [What to do in case of an Incident](runbooks/incident.md)
- [Auto-Deploy - How to block Auto-Deploy on specific branch](runbooks/tagging-past-auto-deploy-branches.md)
- [Background Migrations](runbooks/background-migrations.md)
- [Variables!](runbooks/variables.md)
- [Auto-Deploy - How to rollback a deployment](runbooks/rollback-a-deployment.md)
- [Auto-deploy - Merging with a red pipeline](runbooks/merging-with-a-red-pipeline.md)
- [Auto-deploy - How to resolve failing QA tests](runbooks/resolving-qa-failures.md)
- [Auto-deploy - How to drain Canary](general/deploy/canary.md#how-to-stop-all-production-traffic-to-canary)
- [Auto-deploy - How to deploy risky MRs in isolation](general/deploy/deploying-risky-mrs-in-isolation.md)
- [Auto-deploy - How to speed up the auto-deploy process for urgent merge requests?](runbooks/how_to_speed_up_auto_deploy_process_for_urgent_merge_requests.md)
- [Auto-deploy - Evaluate Deployment health](runbooks/evaluate-deployment-health-metrics.md)
- [Auto-deploy - How to deploy to a single environment](runbooks/deploy-to-a-single-environment.md)
- [Auto-deploy - How to fix an auto deploy branch that is too far behind the default branch](general/deploy/how-to-fix-auto-deploy-branch-behind-master.md)
- [Auto-deploy - How to force start a new deployment pipeline for a package](runbooks/re-deploy-auto-deploy-package.md)
- [Auto-deploy - How to fix an auto deploy branch that is too far behind the default branch](general/deploy/how-to-fix-auto-deploy-branch-behind-master.md)
- [Auto-deploy - How to deploy an old package (check-package failure)](runbooks/deploy-an-old-package.md)

### Self-Managed

- [Self-Managed - How to release new minor versions of GitLab each month](general/monthly/process.md)
- [Self-Managed - How to release patch versions of GitLab](general/patch/process.md)
- [Self-Managed - Running E2E for Backports](runbooks/backport-e2e-testing.md)
- [Self-Managed - How to create release candidates for new monthly versions of GitLab](general/release-candidates.md)
- [Self-Managed - How to remove packages from packages.gitlab.com](general/omnibus-packages/removing-packages.md)
- [Self-Managed - How to create a blog post for a patch release](general/patch/blog-post.md)
- [Self-Managed - How to fix a broken stable branch](general/how-to-fix-broken-stable-branch.md)
- [Self-Managed - How to manually sync a release tag](runbooks/manually-sync-release-tag.md)
- [Self-Managed - `PipelineNotFoundError` when publishing a package](runbooks/pipeline-not-found-when-publishing.md)
- [Self-managed - Building KAS package for versions older than 17.0](runbooks/build-kas-versions-older-than-17-0.md)

### Release Environments

- [Connect to a release environment instance](runbooks/release-environment/connect-to-instance.md)
- [Debug Release Environments Pipeline](runbooks/release-environment/debug-release-environments-pipeline.md)
- [Deploy an old stable branch](runbooks/release-environment/deploy-old-stable-branch.md)
- [No environment is created when a new release tagged](runbooks/release-environment/no-environment-creation.md)
- [No Slack notification when a deployment happens](runbooks/release-environment/no-slack-notification.md)
- [Resolving QA failures](runbooks/release-environment/resolving-qa-failures.md)

## Glossary

- [Deployment related terminology](general/deploy/glossary.md)

## Further Reading

- ["Release Manager - The invisible hero"](https://about.gitlab.com/2015/06/25/release-manager-the-invisible-hero/) (2015-06-25)
- ["How we managed 49 monthly releases"](https://about.gitlab.com/2015/12/17/gitlab-release-process/) (2015-12-17)

## Claude

> [!WARNING]
> This is very rudimentary

In CI for every run on this repository, the job `generate_claude_knowledge` outputs artifacts that can be uploaded into Claude "knowledge."
This is an effective substitute until we have a proper RAG database to leverage in our future.
We leverage two Claude projects, described below, because of the knowledge limitations per project in Claude.
There is no API for Claude projects, thus the artifacts that are created need to be uploaded manually.

In order for this to work, all Markdown files that _should_ be included as knowledge need a Frontmatter style header, such as this:

```md
---
tags:
  - something
---
```

And output file is generated for each tag we create.

### Claude Project Instructions

If you don't have access to these projects in Claude, just ask.

#### Auto-Deploy Chat

- [Claude Auto-Deploy Chat](https://claude.ai/project/8e06f635-a044-4c3b-a330-0917fb8201bd)

> Provide answers citing the source, this is located in the "path" key.  Use the "base_url" as a prefix to "path" and generate valid URLs in your citations. Be brief in your response.  For any question about release topics, even if contained in provided resources, redirect the end user to Release Chat.  Assume the end user speaking to you already knows what this is.

> All responses must contain a finishing line that indicates what version of our documentation was used.  This is the "git_version" key of the knowledge provided.

Knowledge:

- The [scripts/generate-claude-knowledge.rb] - outputs a file for upload manually - the old one will need to be deleted
- This file is named `claude-auto-deploy-<sha>.json`

To add a file to this chat, use the following tag:

```md
---
tags:
  - auto-deploy
---
```

#### Release Chat

- [Claude Release Chat](https://claude.ai/project/6559ecb9-6058-4d4f-ab4f-b8e9ea5f8e84)

Prompt Instructions:

> Provide answers citing the source, this is located in the "path" key.  Use the "base_url" as a prefix to "path" and generate valid URLs in your citations. Be brief in your response.  For any question about auto-deployment topics, even if contained in provided resources, redirect the end user to Auto-Deploy Chat.  Assume the end user speaking to you already knows what this is.

> All responses must contain a finishing line that indicates what version of our documentation was used.  This is the "git_version" key of the knowledge provided.

Knowledge:

- The [scripts/generate-claude-knowledge.rb] - outputs a file for upload manually - the old one will need to be deleted
- This file is named `claude-release-management-<sha>.json`
- A snapshot of the following URL's were also added:
  - <https://handbook.gitlab.com/handbook/engineering/releases/>
  - <https://about.gitlab.com/community/release-managers/>

To add a file to this chat, use the following tag:

```md
---
tags:
  - release-management
---
```

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).

[scripts/generate-claude-knowledge.rb]: ./scripts/generate-claude-knowledge.rb
