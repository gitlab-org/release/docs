---
tags:
  - auto-deploy
---
## How to fix an auto deploy branch that is too far behind the default branch?

An auto-deploy branch can be considered to be far behind the default branch if the auto-deploy branch
doesn't contain commits that were committed within the last 7 business days (not counting changelock
periods like weekends).

If an auto-deploy branch is far behind the default branch due to red pipelines
on the default branch, ask the team that handles that project for help.

Auto deploy branches are created in the following security repositories:

### gitlab-org/security/gitlab

If pipelines on the default branch are broken, check with the Engineering
Productivity team in the `#master-broken` Slack channel.

### gitlab-org/security/omnibus-gitlab

If pipelines are broken on the default branch, check with the `#g_distribution` Slack channel.

### gitlab-org/security/charts/components/images

If pipelines are broken on the default branch, check with the `#g_distribution` Slack channel.

### gitlab-org/security/charts/gitlab

If pipelines are broken on the default branch, check with the `#g_distribution` Slack channel.
