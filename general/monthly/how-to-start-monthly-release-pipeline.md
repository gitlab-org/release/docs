---
tags:
  - release-management
---
# How to start Monthly Release pipeline?

## Complete pipeline

To start a complete Monthly Release pipeline, pass the variable `MONTHLY_RELEASE_PIPELINE=true` when creating a new pipeline ([quick link](https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/new?var%5BMONTHLY_RELEASE_PIPELINE%5D=true)).

## Run a specific stage

To run a specific stage in the release pipeline, pass the name of the stage via `MONTHLY_RELEASE_PIPELINE`. For example, to start the `verify` stage, pass `MONTHLY_RELEASE_PIPELINE=verify` ([quick link](https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/new?var%5BMONTHLY_RELEASE_PIPELINE%5D=prepare)). For the detailed implementation, check [`.gitlab/ci/monthly`](https://gitlab.com/gitlab-org/release-tools/-/tree/master/.gitlab/ci/monthly) in `release-tools`.
