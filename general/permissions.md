---
tags:
  - auto-deploy
---
# Permissions required

In order to be able to tag and deploy a release you need to be a member of the
Release Managers groups on [gitlab.com][rm-prod] and [dev.gitlab.org][rm-dev].

These permissions are automatically given to release managers based on the
[release manager schedule](https://gitlab.com/gitlab-org/release-tools/blob/master/lib/release_tools/release_managers/schedule.rb).

If you need these permissions when you are not a release manager,
they can be [temporarily granted](../release_manager/index.md#temporary-permissions).

[rm-prod]: https://gitlab.com/groups/gitlab-org/release/managers
[rm-dev]: https://dev.gitlab.org/groups/gitlab/release/managers

## Group memberships

Access to the release manager group is controlled through a [file in
release-tools]. As part of your [onboarding issue](./onboarding.md), you should
have edited that file to add your information, and then opened a merge request.

[file in release-tools]: https://gitlab.com/gitlab-org/release-tools/blob/master/config/release_managers.yml

---

[Return to Guides](../README.md#guides)
