---
tags:
  - auto-deploy
---
# Post-Deploy Migrations

## What is this

Release Tool's Post Deployment Migrations is a pipeline that start execution of [Post-deployment Migrations] that are kicked off via our [Post-Deploy Migration pipeline (PDM)](./deployment.md)

The execution of these happen outside the realm of out Auto-Deploy Pipeline to provide us an opportunity to rollback should an incident occur.

## What do we do for Auto-Deploy

Broadly, the PDM pipeline:

1. Executes Post-Deploy Migrations on Staging
2. Triggers a QA pipeline against Staging to make sure tests continue to pass after the start of executing of post-deploy migrations in Staging
3. Executes Post-Deploy Migrations on Production.

![Post-deploy migration pipeline](./images/post-deploy-migration-pipeline.png)

As with any other deploy pipeline, it also:

* Notifies the start, end, and status of the execution on Slack.
* Keeps a record of the post-deploy migrations executed by adding a comment in the release monthly issue.
* Performs production checks before executing the post-deploy migrations on any environment to check for active incidents and ongoing changes (CRs).
* Tracks the auto-deploy package running on GitLab.com on the GitLab canonical and security projects at the moment of the PDM execution.

It should be noted that [Batched background migrations] typically take a long time to run as these usually make changes to the data inside of the database.
Our PDM will only trigger the start of these, the pipeline does not wait for execution to complete.
[See the runbook](./deployment.md) for further information.

### Special Consideration for Production Change Locks

Historically, when PCLs are scheduled, the execution of PDMs is normally paused.
This is due to the low availability of team members to assist in case a migration is the root cause of an incident.
Any time a large event or holiday is approaching, we should consider denoting not running the PDM on these days.
Evaluating this is part of our release task issue.

## What do we need to be aware of for Releases

As batched background migrations can take a long time to execute, on the order of days or weeks, this should be noted as a low priority item.
Various [safety mechanisms](https://docs.gitlab.com/ee/development/sidekiq/idempotent_jobs.html) are built in to both the code as well as our processes to make this a safe operation for our development teams.

Only in times of incident shall we be concerned with a target migration and be aware of what needs to happen and how it may impact an upcoming release.
Follow our [runbook as necessary](./deployment.md#handling_post_deploy_migrations_failures)

## Utilities

The definition of the PDM pipeline can be found on:

* [CI configuration](https://gitlab.com/gitlab-org/release-tools/-/blob/master/.gitlab/ci/post-deploy-migrations-pipeline.gitlab-ci.yml)
* [Code on release-tools](https://gitlab.com/gitlab-org/release-tools/-/tree/master/lib/release_tools/auto_deploy/post_deploy_migrations)

[Post-deployment Migrations]: https://docs.gitlab.com/ee/development/database/post_deployment_migrations.html
[Batched background migrations]: https://docs.gitlab.com/ee/development/database/batched_background_migrations.html
