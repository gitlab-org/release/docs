# Alerts

The following are the alerts that Release Managers need to handle. For each alert, there is a runbook linked which you
can follow to resolve the alert.

## Deploy pressure alert

- Runbook: https://gitlab.com/gitlab-com/runbooks/blob/master/docs/release-management/deploy_pressure.md?plain=0
- Alert definition: https://gitlab.com/gitlab-com/runbooks/blob/master/mimir-rules/gitlab-ops/release-management/deploy-pressure.yml
- Dashboard: https://dashboards.gitlab.net/d/delivery-release_management/delivery3a-release-management

## Build pressure alert

- Runbook: https://gitlab.com/gitlab-com/runbooks/blob/master/docs/release-management/build_pressure.md?plain=0
- Alert definition: https://gitlab.com/gitlab-com/runbooks/blob/master/mimir-rules/gitlab-ops/release-management/build-pressure.yml
- Dashboard: https://dashboards.gitlab.net/d/delivery-release_management/delivery3a-release-management
