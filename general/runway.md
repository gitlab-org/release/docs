---
tags:
  - auto-deploy
---
# Runway

[Runway] is GitLab's internal Platform as a Service, which aims to enable teams to deploy and run their services quickly and safely.

## List of GitLab services running on Runway

You can find the list of services running on Runway in this [yaml file].

## Runway services and auto-deploy

Runway services have their own repositories (not the GitLab Rails repository) and are deployed separately. Because of that, their deployments are independent from `auto-deploy`.

## Runway services handling

When there is an incident related to a Runway service, depends on where the issue is, from the perspective of release manager, we need to handle the incident differently:

- If the fix is in the GitLab Rails code that takes care of communicating with the Runway service, then the fix is still under the coverage of `auto-deploy` and we can follow our normal procedure to [mitigate an incident].
- If the fix is in the Runway service repository, we need to contact the team responsible for the service to merge and deploy the fix.

[Runway]: https://gitlab.com/gitlab-com/gl-infra/platform/runway
[yaml file]: https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.yml
[mitigate an incident]: ../runbooks/incident.md