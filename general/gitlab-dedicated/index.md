---
tags:
  - release-management
---
# GitLab Dedicated guides for Delivery team members

This section of the documentation contains pages related to understanding GitLab
Dedicated.

1. [General overview](./gitlab-dedicated.md)
2. [GET overview for Cloud Native Hybrid in GitLab Dedicated](./get-usage-with-dedicated.md)
3. [Long-lived Sandbox for Delivery](./long-lived-sandbox.md)
4. [Instrumentor Sandbox setup](./instrumentor-sandbox-setup.md)
