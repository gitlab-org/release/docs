---
tags:
  - release-management
---
## Unplanned critical release

> **:warning: NOTE:** Starting with the 16.7 milestone, patch releases are scheduled twice a month as an
> effort to halve the cycle time of security fixes and help deliver security improvements whilst meeting [Security Remediation SLAs].
> **If a high-severity security fix is discovered, please proceed to include them in the [next planned patch release]**. The scheduled
> releases guarantee bug and security fixes are released at least every 23 days to meet bug and security SLAs.

Unplanned critical patch releases are ad-hoc processes used to immediately
patch and mitigate a high-severity vulnerability in order to meet the [Security Remediation SLAs].

Following the [GitLab Maintenance Policy], the vulnerability will be fixed in all supported versions
following [Security Remediation SLAs]. The AppSec team is responsible for assessing the vulnerability
and working with development to decide on the best approach to resolve it.

Depending on the severity and impact of the vulnerability, an
immediate patch release consisting of just the security fix may be warranted.

If an unplanned critical patch release is needed the AppSec engineer will work with
Release Managers to agree on a timeline for the release:

1. The vulnerability has been reported and discussed between a [Security
   Engineer] and an [GitLab Engineer]. When the timeline of a possible fix is
   established, the Security Engineer informs a [Release Manager] of a need
   for the critical patch release.
1. A release manager proposes a timeline for the critical patch release based
   on the other release tasks, and informs a security engineer and a [Quality
   Engineer] in the designated release issue.
1. Depending on the nature of the issue, a security engineer may decide that
   a temporary mitigation strategy is required. If a patch is required,
   the security engineer works with the developer and infrastructure engineer
   following the [post-deployment patch process].
   In other cases, the security engineer will work with infrastructure
   engineers on remediation.
1. The security engineer creates a [security operations issue]
   using the template `user_impact_investigation` for the
   security operations team to track further investigations and other post-remediation
   activities, such as determining and notifying affected users.
1. A security engineer works on a blog post to pre-announce the critical
   patch release. They also work with the marketing team to inform the
   customers of an upcoming release. They also follow the [JiHu Support Security Process].
   The security engineer also prepares the second blog post which will be published
   at release time.
1. The developer follows the [Engineer process] to create the
   artifacts necessary for a release.
1. A release manager prepares the release with all backports. If there are any non security items pending, such
   as a release candidate, release managers may decide to release those first.
1. If any post-deployment patches were applied, an on-call Security engineer
   should verify that the applied patches are still working; unless the newly
   deployed version removes the need for these patches.
1. A quality engineer prepares environments and executes QA tasks together with
   the security engineer to verify that all released versions have successfully
   resolved the vulnerability.
1. When all environments contain the fix, any temporary mitigation strategy is
   reverted.
1. Following deployment, a release manager coordinates with a security engineer
   on the exact timing of a release.
1. A release manager will promote the packages at the designated time, and merge
   the release blog post.
1. A security engineer works with the marketing team to send notification emails
   to any affected users.
1. A release manager closes all release issues.
1. A security engineer keeps the issues where the vulnerabilities were reported
   open for the next 30 days.
1. A security engineer prepares a blog post that explains the vulnerability in
   detail and releases it approximately 30 days after the original release.
1. Once the final blog post is released, a security engineer removes the
   confidentiality from the issues and closes them.

Each involved role should follow their own guide, and create separate issues
linking to the main release issue.

[Engineer process]: ../security/engineer.md
[GitLab Engineer]: ../security/engineer.md
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[JiHu Support Security Process]: https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/#security-release-process
[next planned patch release]: https://gitlab.com/gitlab-org/gitlab/-/issues/?label_name%5B%5D=upcoming%20security%20release
[post-deployment patch process]: ../deploy/post-deployment-patches.md
[Release manager]: https://about.gitlab.com/community/release-managers/
[Security Engineer]: ../security/security-engineer.md
[security operations issue]: https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/new?issuable_template=User+Impact+Investigation
[Security Remediation SLAs]: https://handbook.gitlab.com/handbook/security/product-security/vulnerability-management/#remediation-slas
