---
tags:
  - release-management
---
# Security general information

Security vulnerabilities in GitLab and its dependencies are addressed with
the highest priority based the [remediation SLAs].

Security fixes are released on [patch releases] and prepared according to the [GitLab Maintenance Policy]

## Security guides by role

- [Release Manager]
- [GitLab Engineer]
- [Security Engineer]
- [Quality Engineer]

## Links

- [Security Terminology]

---

[Return to Guides](../README.md)

[GitLab Engineer]: engineer.md
[Quality Engineer]: quality.md
[patch releases]: ../patch/raedme.md
[Release Manager]: release-manager.md
[Security Engineer]: security-engineer.md
[Security Terminology]: terminology.md
[remediation SLAs]: https://handbook.gitlab.com/handbook/security/threat-management/vulnerability-management/#remediation-slas
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
