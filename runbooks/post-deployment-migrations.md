---
tags:
  - auto-deploy
---
# Run Post Deployment Migrations (PDM)

For runbooks related to PDM, please check the [docs on post deployment migrations](../general/post_deploy_migration/readme.md).
