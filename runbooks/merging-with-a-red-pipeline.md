---
tags:
  - auto-deploy
---
# Merging with a red pipeline.

When preparing release work, Release Managers depend on protected branches to have green pipelines.
Due to unrelated situations, these branches may have red pipelines preventing Release Managers from completing their work.

When it's imperative for a merge request to be merged, whether because it's blocking a deployment or
because it's blocking release managers workflow, it's suggested to:

* Ensure there's an issue for the failure (if the failure is Danger-related this step can be skipped).
* Disable `Pipelines must succeed` option for the GitLab project. The option can be found on
  **Settings > General > MergeRequests**.
* Refresh the page of the merge request, at this point the `Merge` button will be enabled.
* Merge the merge request.
* Re-enable the `Pipelines must succeed` option.
