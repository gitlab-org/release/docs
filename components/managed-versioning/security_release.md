---
tags:
  - auto-deploy
---
# Managed Versioning - Security release process

The security release process for components in managed versioning
takes place under the general [security release
process](../../general/security/readme.md) it's important to
familiarize with that process and its terminology.

## Development and AppSec process

To release the vulnerability fix:

1. We need **a first security implementation issue** for the
   component with all the necessary merge requests: one targeting the
   default branch and up to three backports.
1. The fix must be reviewed like any other GitLab security merge
   request with AppSec approval on the MR targeting the default branch.
1. Once the fix is ready, merge requests should be assigned to the bot.
1. We need **a second security implementation issue** for
   `gitlab-org/security/gitlab` labeled as `~reduced backport` that only
   touches the relevant version file.
1. The fix must be reviewed like any other GitLab security merge
   request with AppSec approval on the default branch.
1. Both security implementation issues must be linked to the Security release issue.
1. Ping release-managers (`@gitlab-org/release/managers`) on the security release
   issue to inform them that a component fix is included in the release.
   that a component fix is included in the relese.

### Updating the `COMPONENT_NAME_VERSION` file on `gitlab-org/security/gitlab`

There is a little complication in this process to ensure we do
not rollback the component and break mirroring ahead of time.

Let's consider the following example: this is the repository of the
component. The squared commit represents the SHA that is stored in
`gitlab-org/gitlab` repository as `COMPONENT_NAME_VERSION` file.

```mermaid
gitGraph
    commit
    commit type: HIGHLIGHT
    branch feature-a
    branch security-mr
    checkout feature-a
    commit
    checkout main
    merge feature-a tag: "gprd + feat a"
    branch feature-b
    checkout security-mr
    commit tag: "gprd + sec fix"
    checkout main
    merge security-mr tag: "gprd + feat a + sec fix"
    checkout feature-b
    commit
    checkout main
    merge feature-b
```

In a situation like this, we cannot rely on merge commits as they are
not ready ahead of time. We don't even want to merge any of the merge
requests targeting master or this will break mirroring.

We can use the `gprd + sec fix` SHA in our merge request for
`gitlab-org/security/gitlab` so that the release tooling will take care of
deploying that commit regardless of when it will be merged.

Using this approach has the advantage to identify future component
deployments as a merge conflict, protecting from an accidental
component rollback. If the content of the version file changes in
`gitlab-org/gitlab` before the security release completes, this merge
request will no longer be mergeable and the author will receive a TODO
item.

#### Merge conflict resolution process

To resolve this conflict, avoiding any rollback, it is necessary that
we follow this process:

1. Take the `COMPONENT_NAME_VERSION` from `gitlab-org/gitlab` master
   (`MASTER_COMPONENT_SHA`).
2. Rebase the component merge request so that its branch (`security-mr`
   in the above example) starts exactly from `MASTER_COMPONENT_SHA`.
3. Rebase the `gitlab-org/security/gitlab` merge request onto master
   **updating the version with the new HEAD of merge request on step
   2**.

## Release manager process

Release managers are pinged when a component fix is included in a
security release.

To add the merge request to the security release:

1. Manually merge the component merge request targeting the default
   branch before running the early merge phase.
1. Continue with the regular process until the tagging steps.
1. Verify that the component backports have been merged by
   release-tools.
1. Continue with the regular process.

### Context

Release-tools currently lacks the ability to make sure the component
merge request and the version bump are merged atomically. Each
security implementation issue will be validated independently and it
is not possible to have more than one merge request targeting the
default branch connected to the same security implementation issue.

Because of the above, if no manual intervention takes place,
the default release process will automatically:

1. In the early merge phase, the reduced backport for gitlab will be
   merged ensuring we deploy the fix to gitlab.com.
1. In the backports merging phase, all the components merge requests
   will be merged ensuring that the fix will be included in the next
   tagged release.

Those two steps are usually split across multiple days, giving the
chance for the component merge requests to change. The change itself
will not affect our ability to deploy the previously approved SHA, but
it will create inconsistency and potentially break our ability to
later create a stable branch from that commit.

## Known limitations

### Releasing multiple fixes

The process described in this page can only release a single fix on
a given security release. In order to correctly deploy multiple fixes
for the same component in a single security release, an accumulation
branch will be needed in order to accumulate all the necessary fixes.

Please inform the release managers as soon as we are aware of the need
to deliver multiple security fixes in the same release.

### Component deployments are suspended during the release preparation window

As soon as the merge request updates the `COMPONENT_VERSION` file on
the security mirror, it will not be possible to deploy new versions of
the component until the security release completes (usually 4-5 days).

Due to the broken mirroring between security and canonical, during
this period, an update to the `COMPONENT_VERSION` file on
`gitlab-org/gitlab` will result in a conflict that could lead to a
rollback.

Theoretically, it will be possible to deploy a new version, but it
must be done on the security repository with all the necessary
approval. Because Release Managers and AppSec focus on completing the
security release, we cannot prioritize component deployments unless it
fixes a problem with the security release.

The security release date varies, so if you want to understand when
deployments might be blocked during a given release cycle, check the
[upcoming security release tracking
issue](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=upcoming%20security%20release)
or ask the release managers in the `#releases` slack channel.

### Gitaly special process

Gitaly implements Automated Version Updates and this requires special
considerations described in [our documentation for
gitaly](../../general/security/how_to_handle_gitaly_kas_security_merge_requests.md).

<!--
Marked metadata
XHTML Header: <script src="https://unpkg.com/mermaid@9.3.0/dist/mermaid.min.js"></script>
-->
