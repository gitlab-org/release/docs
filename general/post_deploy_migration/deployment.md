---
tags:
  - auto-deploy
  - release-management
---
# Overview of the post-deploy migration pipeline

The post-deploy migration (PDM) pipeline will execute pending post-deploy migrations on
the staging and production environments. Introduced as part of <https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/585>,
it unties the post-deploy migration execution from the auto-deploy packages making them
suitable for rollback if needed.

The PDM enables us to follow [our own developer documentation for zero downtime deployments.](https://docs.gitlab.com/ee/development/database/post_deployment_migrations.html)

## How to execute post-deploy migrations?

To execute the PDM pipeline:

1. Review the [release manager dashboard] for any pending post migration available:

   ![release manager example](./images/release-manager-dashboard-example.png)

1. Make sure there are no other PDMs are happening at the same time by checking the [running pipelines](https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines?page=1&scope=all&status=running) wtih the name `Execute post deploy migrations`.
1. If there are pending post migrations, trigger the PDM pipeline:

   ```text
   # On Slack
   /chatops run post_deploy_migrations execute
   ```

Note that PDM complies with the production checks, post-deploy migrations won't be executed
if there are active incidents or deployments, or if there are no pending post-deploy migrations.

## Handling post-deploy migrations failures

A failing post-deploy migration should not be retried without investigation, because
data could be in an unstable state. Instead:

1. Follow these steps to [request support].
2. Then work with the SRE on call to determine the next steps:

### Report an incident

When creating incident issues, don't worry about not having all the details yet;
a link to a failing pipeline with some basic information is enough to begin. Once
the issue is created, make sure to add any additional information to the incident,
such as links to the merge request that introduced the migration.

Note that as a release manager **your primary task at this point is to coordinate
the effort to resolve the incident**, rather than trying to resolve it yourself.
This helps balance the workload, instead of one person having to do all the work.

If the incident happens at the end of your shift and there is no immediate need
to resolve it, make this clear in both the issue and the appropriate Slack
channels, and inform the next release manager about the state of things. This
ensures the next release manager can take over the work when they begin their
shift.

Take a look at the [Release manager requesting support guide] for more details on
getting EOC and dev on-call involved.

#### Finding information about the post migration

In order to provide the EOC, developers and DBREs the information the need to
help debug the issue, we need to identify the post migration that failed and
the merge request that introduced it.

**1. Determine the post deploy migration that failed.**
To do this you will need to go to the pipeline that contains the failure. This can
be done by clicking on the link in the announcements channel indicating the
migrations for the environment failed, and then clicking on the
`$env-postdeploy-migrations` job. Scroll to the bottom of the output, and take
note of the error from the failure, and the migration itself. The migration
should have a name like `20220525201022 AddTemporaryIndexForVulnerabilityReadsClusterAgentIdMigration`.

**2. Determine the MR that introduced the post deploy migration.**
Once you have the name of the migration that failed, search for the name (excluding
the date part) in the
[gitlab-org/gitlab](https://gitlab.com/search?project_id=278964&group_id=9970&search_code=true&repository_ref=master)
project, and you should get a result pointing to an `.rb` file in the `db/post_migrate`
folder. Click on the file in the search results, and then click on the commit
message at the top. Finally, you should see a link to the relevant merge request
that last touched that file (just below the line with `parent`). This is the MR
that introduced the migration, and should be noted on the incident issue

**3. Determine the engineers associated with the merge request.**
Once you have located the MR of the failure, you can identify the following
engineers involved

* The engineer who created the change (the requester of the MR)
* The database maintainer who reviewed the MR (this will involve looking at the
comments until you find an approval from a member of the Database maintainer group,
the approval is done by approving the merge request and assigning the ~"database::approved" label

### Next Steps

1. Check if there is an autovacuum activity happening

   At the moment, in the post deployment migration (PDM) pipeline, there is a job called
   `post_deploy_migrations:prepare` that checks various things to make sure it is safe to perform a PDM.
   One check is
   ["No wraparound autovacuum detected"](https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/release_tools/auto_deploy/post_deploy_migrations/prepare.rb).
   However, between the check and the actual PDM on prod, there are PDM on staging and QA, so it can take
   around 30min (example pipeline). Thus, there is no guarantee that when prod PDM happens, no wraparound
   autovacuum is running.

   If you see a timeout error in the PDM job's log, there can be an autovacuum activity happening at the same time.
   Check the metric
   [`pg_stat_activity_autovacuum_age_in_seconds{type="patroni-ci", relname=~".*wraparound.*"}`](https://dashboards.gitlab.net/goto/9qBsfXOHR?orgId=1)
   (make sure to pick the right Data Source corresponding to the environment, e.g. `Mimir - Gitlab Gprd`). If it
   has recent data points, it means autovacuum activities are happening. Work with the engineers associated with
   the merge request to determine if it causes the timeout.

1. Determine if the post deploy migration is safe to retry

   Now that you have identified the key people involved in the MR, you should reach
   out to them on slack and let them know about the incident issue, the error
   from the failed migration, and ask if it's safe to retry the migration. If it
   is, retry the migration job in the pipeline.

1. Determine if the failure should block deployments

   If the post deploy migration is not safe to retry, analyze it with the EOC and engineers
   involved in the MR to determine if the failure should block deployments, the impact of the failure will depend on
   the nature and the operations performed by the post-deploy migration. If it's deemed to block deployments it
   should be treated as a [deployment blocker].

1. Determine a plan for a mitigation strategy

   Work with the EOC and the engineers responsible for the migration on a plan that
   allows deploys to continue and/or unblock the post migration execution. Make sure this plan is documented in the incident
   when agreed upon.

   The mitigation depends on the nature of the post migration, whether they perform
   [DDL](https://en.wikipedia.org/wiki/Data_definition_language) or [DML](https://en.wikipedia.org/wiki/Data_manipulation_language)
   operations.

#### DDL Operations

Typical DDL operations include adding or dropping tables, columns, foreign keys
or indices.

Often the best mitigation for failed DDL operations is to

1. Mark the post-migration as successful. See the section below on ['Unblocking the PDM via ChatOps command'](#unblocking-the-pdm-via-chatops-command)

1. Have the engineers create an MR to `gitlab-org/gitlab` that makes the migration in question a no-op.

1. Make sure that a deployment with that MR makes it to all environments

1. Re-run the post deployment pipeline through chatops as described [here](https://gitlab.com/gitlab-org/release/docs/-/tree/master/general/post_deploy_migration#how-to-execute-post-deploy-migrations)

#### DML Operations

Typical DML operations include inserting, updating, or deleting new data, as part
of a background migration or data cleanup exercise.

Depending on the migration in question, a common process for mitigating these
migrations is to

1. Investigate and work with the engineers and EOC to do a change request to mark the migration as complete
in the database.

1. Work with the engineers and EOC to create a change request to perform any work needed to
cleanup data from the failed migration [example](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6646)

1. Confirm with the engineers to follow up on why the failure happened and determine
if the migration is safe for self-managed customers (should be documented on the
incident).

Another option is to follow the process used for [DDL Operations] instead.

### Unblocking the PDM via ChatOps command

There's a ChatOps command release managers can use to mark the post-migration as successful.

This is an optional tool to be used to unblock the PDM after the incident's root cause has been found and a mitigation
process has been established.

The usage depends on the release manager's best discretion, a good rule of thumb for using this tooling would be:

* The post-migration executes [DML operations](#dml-operations) and it is **not a background or data migration**
* There are pressing release activities that depend on the PDM being unblocked

If release managers are uncertain about executing this tooling, they should discuss it with Delivery folks or escalate
this to the dev-on-call process.

The usage of this command will cause the post-deploy migration to be skipped, so there are steps to properly revert the
MR that introduced the post-deploy migration, and the engineers/team associated with that MR needs to make another MR to
create another fixed migration.

Follow the following steps:

1. Note the migration number. For example, on `db/post_migrate/20231004080224_swap_columns_for_ci_stages_pipeline_id_bigint.rb`,
   the migration number is `20231004080224`.
1. Run the following `chatops` command in `#f_upcoming_release`, depending on the database the migration is going to be run in:

   ```text
      /chatops run migrations mark 20231004080224                    # if the migration is in the main database
      or
      /chatops run migrations mark 20231004080224 --database ci      # if the migration is in the `gitlab_ci` database
   ```

   If not sure which database the migration is going to run in, it's safe to run both (one of them will safely fail in that case).

1. Re-run the failed post-deploy migration job. If other post-deploy migrations also fail, confirm with the EOC or the
   engineers involved if it is okay to mark it as successful as well.
1. If the migrations the post-deploy migration job succeeded after a retry, the incident can be marked as mitigated and it is safe
   to continue with promoting new packages.
1. To resolve the incident and to make sure the post-deploy migration can be included in a release, the [post-deploy migration should be disabled](#disabling-a-post-deploy-migration)
   and deployed to GitLab.com

#### Disabling a post-deploy migration.

This process details how to revert a post-deploy migration that failed to be executed on staging or production:

- The original merge requests should be reverted following the [How to disable a data migration?] ([Example of revert MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134446)).
- The revert should be done by a backend engineer. If none is available, release managers should trigger the [dev-on-call process].
- The merge request will require a (`#database_maintainers`) approval
- Once the revert merge request has been merged, notify the author(s) of the MR(s) about the mitigation and revert. Another MR needs to be created by the stage teams for the migration to
   properly be executed on the database.

**IMPORTANT** - If a monthly release is about to be tagged, the revert merge request must be included in the release candidate: deployed to production
   prior to the tagging step of the release).

### Final step

Make sure once the incident is resolved, to ping the release managers on the
incident to make them aware of it, and to make sure they follow up on any fixes
that might get done to ensure they are included in the monthly release.

## How to determine if a post-deploy migration has been executed on GitLab.com?

There are three ways to determine if a post migration has been executed the GitLab staging and production environments:

1. **Through the merge request widget**: In the merge request, if the environment widget indicates `db/gstg` and `db/gprd`,
   the post migration has been executed in staging and in production.
1. **Through the merge request labels**: In the merge request, if the ~workflow::post-deploy-db-staging and ~workflow::post-deploy-db-production
   labels have been added, the post migration has been executed in environment that matches the label name.
1. **Through ChatOps**: Using `/chatops run auto_deploy status <sha>` outputs the environments the commit has been deployed to, if `db/gstg` and
   `db/gprd` are included, the post migration has been executed in staging and production.

| Merge request widget | Merge request labels | ChatOps |
| ---- | ---- | ---- |
| ![MR widget](./images/mr_widget.png) | ![MR label](./images/mr_labels.png) | ![ChatOps](./images/chatops.png) |

[request support]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/release-manager-incident-guide.md#release-manager-requesting-support
[deployment blocker]: https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers
[Release manager requesting support guide]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/release-manager-incident-guide.md#release-manager-requesting-support
[release manager dashboard]: https://dashboards.gitlab.net/d/delivery-release_management/delivery-release-management?orgId=1&refresh=5m
[How to disable a data migration?]: https://docs.gitlab.com/ee/development/database/deleting_migrations.html#how-to-disable-a-data-migration
[dev-on-call process]: https://handbook.gitlab.com/handbook/engineering/development/processes/infra-dev-escalation/process/#escalation-process
