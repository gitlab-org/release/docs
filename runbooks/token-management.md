---
tags:
  - auto-deploy
---
# Token management

Token management should follow the [company guidelines for automation and access
tokens](https://about.gitlab.com/handbook/engineering/automation/).

If you need a token from a project or a bot owned by the delivery group,
please [file an access token
request](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/new?issuable_template=access_token_request)
in our issue tracker.

The issue template will guide you on collecting the needed information
and will serve as [tracker for all the tokens we issued](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=access-token).

## Adding Tokens to Vault

It is recommended to store tokens in [Vault][what-is-vault]. Project tokens should be generated in
[infra-mgmt](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt), with `readonly_secret_paths` specified as illustrated [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/usage.md#:~:text=If%20you%20created%20a%20project).

All other tokens should be stored and read from [Vault][what-is-vault] as opposed to storing the values as CI variables.

You need [access to Vault](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/usage.md#accessing-vault) to store tokens and access existing values. You can use either the web UI or CLI to store the token. More information about secrets management in Vault can be found [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/usage.md#secrets-management)

**Web UI:**

| Instance | Type |Secret's Path                                     | Steps to Add a Secret |
| -------- | ----- | ------------------------------------------------- | --------------------- |
|  `ops`   | shared | `ci/ops-gitlab-net/gitlab-org/release/tools/shared` | 1. Go to https://vault.gitlab.net<br>2. Navigate to the secret path `ci/ops-gitlab-net/gitlab-org/release/tools/shared`<br>3. Click on the `Create secret button`<br>4. Provide a suitable name for the token in the path for this secret field<br>5. Enter the token type (e.g., api_token,auth_token) in the secret data field       |
|  `ops`   | protected | `ci/ops-gitlab-net/gitlab-org/release/tools/protected/shared` | 1. Go to https://vault.gitlab.net<br>2. Navigate to the secret path `ci/ops-gitlab-net/gitlab-org/release/tools/protected/shared`<br>3. Click on the `Create secret button`<br>4. Provide a suitable name for the token in the path for this secret field<br>5. Enter the token type (e.g., api_token,auth_token) in the secret data field       |
| `.com`     | shared | `ci/gitlab-com/gitlab-org/release-tools/shared`     | 1. Go to https://vault.gitlab.net<br>2. Navigate to the secret path `ci/gitlab-com/gitlab-org/release-tools/shared`<br>3. Click on the `Create secret button`<br>4. Provide a suitable name for the token in the path for this secret field<br>5. Enter the token type (e.g., api_token,auth_token) in the secret data field |
| `.com`     | protected | `ci/gitlab-com/gitlab-org/release-tools/protected/shared`     | 1. Go to https://vault.gitlab.net<br>2. Navigate to the secret path `ci/gitlab-com/gitlab-org/release-tools/protected/shared`<br>3. Click on the `Create secret button`<br>4. Provide a suitable name for the token in the path for this secret field<br>5. Enter the token type (e.g., api_token,auth_token) in the secret data field |

> **NOTE:** A secret which can be accessed only by CI jobs running on protected branches is called a
> protected secret. Protected secrets should be stored in one of the protected subdirectories, such
> as `ci/ops-gitlab-net/gitlab-org/release/tools/protected/shared`. If the secret needs to be scoped
> to an environment, then `shared` is replaced with `environment`
> e.g. `ci/ops-gitlab-net/gitlab-org/release/tools/protected/environment`

**CLI:**

- If you are using CLI you need to install the [Vault client](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/usage.md#cli).
- Then use the command `vault kv put instance_path/secret secret_token=XXXX` `instance_path` is the location where the token should be stored as defined above e.g. `vault kv put ci/ops-gitlab-net/gitlab-org/release/tools/protected/shared/delivery-metrics api_token=TOKEN_VALUE`

Once the token is saved in Vault we need to update the CI/CD YAML files to read the token from Vault using the [secrets](https://docs.gitlab.com/ee/ci/yaml/#secrets) keyword.

Head to the release tools repo and open an MR to update the following files:

- In the [secrets.gitlab-ci.yml](https://gitlab.com/gitlab-org/release-tools/-/blob/master/.gitlab/ci/secrets.gitlab-ci.yml), add a job which will fetch the token from Vault.
- Next the job added in the previous step needs to be extended in the `.common-ci-tokens` job. This
  job is defined in multiple files depending on where the token will be used. This table lists the
  appropriate file for each GitLab instance and token type

| Instance | Type | File |
| -------- | ---- | ---- |
| `ops`    | protected | [protected-ops-vault-tokens.gitlab-ci.yml](https://gitlab.com/gitlab-org/release-tools/-/blob/master/.gitlab/ci/protected-ops-vault-tokens.gitlab-ci.yml) |
| `ops`    | shared | [shared-ops-vault-tokens.gitlab-ci.yml](https://gitlab.com/gitlab-org/release-tools/-/blob/master/.gitlab/ci/shared-ops-vault-tokens.gitlab-ci.yml) |
| `.com`   | protected | [protected-vault-tokens.gitlab-ci.yml](https://gitlab.com/gitlab-org/release-tools/-/blob/master/.gitlab/ci/protected-vault-tokens.gitlab-ci.yml) |
| `.com`   | shared | [shared-vault-tokens.gitlab-ci.yml](https://gitlab.com/gitlab-org/release-tools/-/blob/master/.gitlab/ci/shared-vault-tokens.gitlab-ci.yml) |

Here's an [example MR](https://gitlab.com/gitlab-org/release-tools/-/merge_requests/3591) that can be used for guidance.

> **NOTE:** When adding shared tokens to either the `shared-ops-vault-tokens.gitlab-ci.yml` or the `shared-vault-tokens.gitlab-ci.yml` you have two options:
>
> - Add the token under `.tokens-for-all-branches` or,
> - Add the token under `.common-ci-tokens`
>
> The option you choose depends on where you want the token to be available:
>
> - If you want the shared token to be accessible in both **protected** and **unprotected** branches, add it under `.tokens-for-all-branches`.
> - If you add the token under `.common-ci-tokens`, it will only be available in **unprotected**
>   branches. **Protected** branches will not have access to the token.

If you want to access tokens stored in Vault in a dynamically generated pipeline, then in addition
to the above change, you should also include the file
`.gitlab/ci/include/secrets-self-contained.yml` as a local include for the `trigger` job, such as in
the [`security_release_tag:generate_tag_jobs` job][sample-dynamic-pipeline-with-secrets].

The following snippet shows a sample case where a dynamic pipeline is generated and run with access
to tokens from Vault:

``` yaml
generate_dynamic_pipeline:
  script: bundle exec rake generate_dynamic_pipeline > dynamic-pipeline.yml
  artifacts:
    paths:
      - dynamic-pipeline.yml

trigger_dynamic_pipeline:
  trigger:
    include:
      - local: .gitlab/ci/include/secrets-self-contained.yml
      - artifact: dynamic-pipeline.yml
        job: generate_dynamic_pipeline
```

[what-is-vault]: https://developer.hashicorp.com/vault/docs/what-is-vault
[sample-dynamic-pipeline-with-secrets]: https://gitlab.com/gitlab-org/release-tools/-/blob/c0a9e964f89b7d319228e895e1c80722ff37ef6a/.gitlab/ci/security/tag-ci.yml#L63
