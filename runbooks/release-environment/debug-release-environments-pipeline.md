---
tags:
  - auto-deploy
---
# Debug Release Environments Pipeline

A deployment to release environment is a series of many triggered pipelines from [gitlab-org/gitlab] to [release-environments], so the most systematic way to debug is to track from [gitlab-org/gitlab]'s pipeline, and check if any CI job fails along the way.

1. Go to the list of commits of the stable branch (e.g. [16-10-stable](https://gitlab.com/gitlab-org/gitlab/-/commits/16-10-stable-ee?ref_type=heads)).
1. Open the pipeline next to the commit that you expect a deployment from.
1. The pipeline should have the stage `release-environments`. Open the trigger job `start-release-environments-pipeline`.
1. In this pipeline, check if there are any failures in the jobs in `prepare` or `deploy` stages.
1. If `release-environments-deploy` job fails:
   1. Download the `release-environments-deploy-env` job's artifact, to see if the environment variables are set correctly. The artifact's content should be similar to this example:

      ```bash
      ENVIRONMENT=16-10-stable
      VERSIONS={"gitaly":"16-10-stable-13369789","registry":"16-10-stable-13369789","kas":"16-10-stable-13369789","mailroom":"16-10-stable-13369789","pages":"16-10-stable-13369789","gitlab":"16-10-stable-13369789","shell":"16-10-stable-13369789","praefect":"16-10-stable-13369789"}
      ```

   1. Open the trigger job `release-environments-deploy`. It opens a pipeline in [release-environments].
   1. Check if any jobs here fail.
   1. Open the trigger job `trigger-deploy` and check if any job fails.
   1. There should be a job named `<environment>:deploy`, e.g. `16-10-stable:deploy`. If there is no such job, there may be something wrong with the dynamic pipeline generation. Go back to the parent pipeline, and check the artifact of `create-dynamic-pipeline` job. This job generate the pipeline for the trigger job.

[gitlab-org/gitlab]: https://gitlab.com/gitlab-org/gitlab
[release-environments]: https://gitlab.com/release-environments/