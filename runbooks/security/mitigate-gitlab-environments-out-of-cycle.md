---
tags:
  - auto-deploy
---
# Remediation of GitLab environments in case of a critical vulnerability

In case of a high-severity vulnerability, GitLab operated environments, including [GitLab production environments],
[GitLab Dedicated], and [GitLab self-managed] must be mitigated and remediated based on the [GitLab remediation SLAs]. This runbook describes
the process of mitigating a critical vulnerability for the different environments managed by GitLab, Inc.

**Note** More discussions around GitLab Dedicated Specific SLAs are in progress in https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20220

![incidents workflow](./images/critical-incidents-workflow.png)

[Source](https://docs.google.com/presentation/d/1lM_MM3C3T4zDJ3eNsU6wYePnwLc08wYCq0tWerIT7n8/edit#slide=id.g2ecc0024a03_0_0) - Internal only

1. **Coordination** - A SIRT incident is created where SIRT team members, AppSec, ProdSec, Release managers, GitLab Dedicated engineers
     and the associated Stage team are assembled.
1. **Analysis** - The impact on the GitLab instances is evaluated. Pending on the impact, a mitigation / remediation plan will need to be prepared for
    - GitLab production environments
    - GitLab Dedicated
    - GitLab self-managed
1. **Development and validation** - A security fix is prepared by the stage team and validated by AppSec.
1. **Deployment** - Once the security fix is ready, release managers will merge the security fix and deploy it to GitLab.com. This will
    break the mirroring between the canonical and security repositories. Monthly releases can't be performed during patch releases, see
    [What if the monthly release is scheduled before the next patch release](#what-if-the-monthly-release-is-scheduled-before-the-next-patch-releae)
1. **Internal package** - If GitLab Dedicated is deemed vulnerable, an internal package must be prepared to remediate the in-house instances.
    After the security fix has been deployed to GitLab.com, and validated by AppSec, release managers will tag a private package to be
    delivered to Dedicated, this package will be available on the [pre-release package server] and won't be released to the public. The package
    will be used by GitLab Dedicated to perform an [unscheduled emergency maintenance]
1. **Patch release** - The security fix, along with any other security and bug fix available, will be delivered to self-managed in the
    next planned patch release. Packages are public and available on the [public package server].
1. **GitLab Dedicated maintenance window** - GitLab tenants will be upgraded with the next patch release via regular [Dedicated maintenance window].

## GitLab.com

Participants:
- Release managers
- Stage team
- Security teams: AppSec, ProdSec, and SIRT.

Mitigating GitLab production environments can be done by:

- Disabling a functionality via Admin/Feature Flag
- Applying traffic blocking rules at the Infrastructure level (e.g., Cloudflare)
- Developing a security fix to address the vulnerability.

In the event of a high-severity incident, it is likely a security fix will be required. Release managers are expected to provide:

1. Timeline for the next patch release, including the deadline to include the security fix. This information is available on the [release dashboard]
1. Any other release schedule, including if the monthly release might interfere with the deployment of the fix
    (see [What if the monthly release is scheduled before the next patch release](#what-if-the-monthly-release-is-scheduled-before-the-next-patch-release)).

To mitigate GitLab.com with a security fix:

1. Stage team will prepare a security fix to remediate the vulnerability. Pending on the complexity, this step might from hours to days.
1. The security fix will follow the same process as any other security fix: AppSec will review and validate the security fix remediates the vulnerability.
1. Once the security fix has been completed (approved by required maintainers and validated by AppSec), it is ready to be deployed to GitLab.com
1. Release managers will proceed to merge the merge request into master
   - At this point the mirroring between canonical and security repositories will break, the merge train will automatically keep the security repositories updated
   - The stage team can prepare backports for the next [planned patch release], see [GitLab self-managed](#gitlab-self-managed).
1. Release managers will deploy the security fix all the way to production.
1. Release managers will notify the SIRT incident channel when the security fix lands on production:
    - Security teams (AppSec, SIRT and ProdSec) will confirm if GitLab.com has been remediated. The EOC assistance might be required.
1. If GitLab.com has been remediated, the next steps are:
    - Remediate [GitLab in-house instances](#gitlab-dedicated): If applicable, Release managers to prepare an internal package for GitLab Dedicated.
    - Remediate [self-managed instances](#gitlab-self-managed): Wait for the next [planned patch release], information about the next patch can be found on
    the [release dashboard]:

## GitLab Dedicated

Participants:
- Release managers
- GitLab Dedicated
- Security teams: AppSec, ProdSec, and SIRT.

If GitLab Dedicated is deemed impacted, an internal package will be required to remediate the GitLab in-house instances. The following steps
assume a security fix has been prepared, successfully deployed to GitLab.com and that the vulnerability has been remediated on GitLab.com:

1. Release managers will confirm the stable branches associated with the current version have green pipelines
1. Release managers will tag a package for the current version.
    - The package is expected to be private, meaning it will not be released to self-managed customers.
    - The package will use the next patch version available. For example, if the current version is `17.0`, and the latest patch was `17.0.1`,
    release managers will tag `17.0.2`.
    - The package will only contain the security fix for the critical vulnerability and any other bug fix that has been merged into the stable branch.
1. Release managers will follow the process as with any other patch release package.
   - Verify the EE, CNG and Helm packages are built.
   - Ensure the package has been successfully deployed to the release instance.
1. Once the verification steps have been completed, release managers will notify the Dedicated engineers the package is available
1. Dedicated engineers will start the [emergency maintenance update] on their tenants.
1. Dedicated engineers will notify on the SIRT incident channel GitLab Dedicated has been remediated.

## GitLab self-managed

Participants:
1. Release managers
1. Stage team
1. AppSec

With the 30-day SLA for critical vulnerabilities, GitLab self-managed instances will be remediated by the next planned patch release.

1. The stage team will prepare the backports to the patch release targeted versions.
1. Release managers will make sure the security fix is included in the next patch release.
1. Release managers will follow the steps to perform a patch release.
1. Release managers will tag packages for the patch release. For the current version, if `17.0.2` was used as an internal package, release managers will tag
   `17.0.3`.
   - AppSec will add a note in the blog post explaining a previous version (`17.0.2`) was skipped for technical reasons.
1. Release managers will publish the release, disclosing the vulnerability to the public
1. After the packages have been published, release managers will notify on the SIRT incident the patch release has been published.
1. Release managers will sync the repositories, disclosing the vulnerability.

Any special communciation regarding the criticaility of the issue will be handled by the Security team.

## FAQ

### What if the monthly release is scheduled before the next patch release?

[Patch releases] are scheduled twice a month on the Wednesdays before and after the monthly release week, because of it, it could be the
next patch release available is after the monthly release. If this happens, release managers can't merge the security fix because it will be leaked
during the monthly release preparation. Pending on the impact and the plan there will be some options:

**Option 1. Tag the release candidate early**

Release managers can either:
- Tag the release candidate early, before the security fix is merged, or
- Tag the release candidate using a specific commit that doesn't include the security fix.

This option will allow the monthly release to be performed without interfering with the mitigation plan of GitLab.com, GitLab Dedicated and GitLab self-managed.
The only downside is that some features or bug fixes will miss the deadline for being included in the monthly release. Release managers should announce this broadly to the engineering channels.

Most of the time this option will be the ideal one.

**Option 2. Wait until the release candidate is tagged and then merge the security fix**

Release managers can wait until the release candidate is tagged and then proceed to merge the security fix. This will prevent
for the security vulnerability to be disclosed before it is published, and allow us to include the security fix in the next patch release.

The major downside is that GitLab environments will be vulnerable for a longer time inflicting the remediation SLA.

Release managers should only use this option only if there is a mitigation
in place (feature flag, admin setting) for GitLab.com and GitLab Dedicated.

**Option 3. Merge the security fix and include it on the monthly release**

This option is only available if the Security team provides explicit approval to follow this path. This will result in disclosing
the vulnerability before it is remediated to self-managed.

Release managers can merge and deploy the security fix, for this one to be included in the upcoming monthly release. This
option allow us to mitigate GitLab.com within the SLA but it has the major downside of disclosing the vulnerability
before self-managed is remediated. Performing this option will require an [unplanned critical patch release]

[GitLab remediation SLAs]: https://handbook.gitlab.com/handbook/security/product-security/vulnerability-management/#remediation-slas
[GitLab production environments]: https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/
[GitLab Dedicated]: https://about.gitlab.com/dedicated/
[unscheduled emergency maintenance]: https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#unscheduled-maintenance
[Dedicated maintenance window]: https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#maintenance
[emergency maintenance update]: https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#unscheduled-maintenance
[Patch releases]: https://handbook.gitlab.com/handbook/engineering/releases/patch-releases/#planned-patch-release-process
[unplanned critical patch release]: https://handbook.gitlab.com/handbook/engineering/releases/patch-releases/#unplanned-critical-patch-release-process
[release dashboard]: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1
[planned patch release]: https://handbook.gitlab.com/handbook/engineering/releases/patch-releases/#planned-patch-release-process
[GitLab self-managed]: https://docs.gitlab.com/ee/subscriptions/self_managed/
[pre-release package server]: https://packages.gitlab.com/gitlab/pre-release
[public package server]: https://packages.gitlab.com/gitlab/gitlab-ee
