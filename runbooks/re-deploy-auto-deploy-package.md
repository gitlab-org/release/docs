# Auto-deploy - How to force start a new deployment pipeline for a package

There are times when a new deployment pipeline needs to be started for a package. For example, if
a bug in release-tools has been fixed.

Follow the instructions in
[How do I start a deployment pipeline for a particular package?](../general/deploy/overview-of-the-auto-deploy-pipelines.md#how-do-i-start-a-deployment-pipeline-for-a-particular-package)
to start a new deployment pipeline for a package.
