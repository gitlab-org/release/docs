---
tags:
  - auto-deploy
---
# Delivery Guide to Dedicated

This documentation aims to serve as the Delivery team's point of reference and onboarding assistance on [Gitlab Dedicated](https://about.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/). More discussions and context can be found on this [initial issue](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/19552).

To maintain single source of truth on the Dedicated repositories and runbooks, this documentation consists mainly of links and information that the Delivery team would be interested in, mainly for the epic to [Reduce Downtime of Dedicated Deployments](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1092). For a more comprehensive understanding of Dedicated, outside of the scope of Delivery, please refer to the [Dedicated documentation](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/) and `#g_dedicated-team` slack channel.

## Overview of Dedicated

The references and details in this section should serve as a good starting point to understanding how Dedicated tenant environments are set up.

* [Dedicated documentation starting point](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/)
  * Delivery-specific sections are compiled below.
* [Narrated video overview of Dedicated](https://youtu.be/Fs429uq1nYQ)

### Architecture & Components

* [Dedicated High Level Overview Diagram](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/architecture/Diagrams.html#high-level-introductory-overview)
  * It helps to reference this diagram when navigating through the components below.
* [Architecture documentations](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/tree/main/architecture)
  * [`architecture/Architecture.md`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/architecture/Architecture.md) consists of explanation and diagrams. Note the ["Managing GitLab Dedicated" section](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/architecture/Architecture.md#managing-gitlab-dedicated) for interaction between Switchboard, Amp, and AWS cluster setup.
* [Switchboard](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/switchboard)
  * Hosts Tenant configuration data and initiate operations against Dedicated instances; responsible for managing global configuration shared between all cloud providers (initially, AWS only), accessible by Tenants
  * [Switchboard User Acceptance Testing repo](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat)
        1. Creating MR in this repository to update `tenant_models` or `switchboard` versions triggers a pipeline for a new deploy and a testing suite.
        2. Note that the [tenant_models](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/tree/main/tenant_models?ref_type=heads) folder has working examples of tenants setup
        3. MRs to update versions such as [this MR in `switchboard_uat`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/merge_requests/582) trigger pipelines ([pipeline for the MR](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/pipelines/957551488)) upon merge, which creates a child pipeline to deploy new changes.
  * Note that this is not used for the sandbox setup.
* [Amp](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/amp)
  * Responsible for the interaction with the customer tenant accounts; configuring expected roles and policies, enabling the required services, provisioning environments, etc.
  * Service endpoint used by Switchboard to interact with customer tenant environments. Amp is implemented as a Kubernetes API endpoint which is capable of applying configuration, forwarding ports, enabling remote access, and setting up tooling for debugging purposes. Amp executes requests on demand, thus eliminating the need for persistent exposed endpoints increasing the security of the system.
* [Instrumentor](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor)
  * Wrapper around GitLab GET; makes `tenctl` calls
  * Delivers images used by Instrumentor
* [GitLab Environment Toolkit (GET)](https://gitlab.com/gitlab-org/gitlab-environment-toolkit)
  * Provisioning and configuration toolkit for deploying GitLab's Reference Architectures with Terraform and Ansible
  * Also used to deploy `gstg-ref`
* [`tenctl`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/tenctl)
  * Applies tenant data and templates to create authorization, authentication and configurations for the GitLab Dedicated Tenants
  * Besides the [README.md](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/tenctl/-/blob/main/README.md?ref_type=heads) and [DEVELOPMENT.md](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/tenctl/-/blob/main/DEVELOPMENT.md?ref_type=heads) in the repo, there is additional documentation on `tenctl` [in the dedicated engineering runbook](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/engineering/tenctl.html)

## Deployment Process

* [Dedicated documentation on the Application workflow](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/engineering/Application-Workflow.html#control-loop-workflow)
  * Switchboard checks for maintenance window for downtime only if the tenant's **gitlab** version is updated.
  * Note the "rolling deployment" section for current deployment workflow

### Change Deployment Workflow (Ansible)

To simplify the tenant deployment process, in the Amp cluster, the tenant model is used to generate configuration files using templates in the Instrumentor image. These configuration files are (mostly) for Terraform and Ansible, which we use to configure tenant infrastructure.

This section breaks down that process into more detail, focusing mainly on the Ansible configuration of the deployment.

1. Tenant models are implemented in multiple ways, as of now. If a tenant is switchboard enabled, it will not be in any git repository. The switchboard and the gitops models are mutually exclusive.
   * [`gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat) is a gitops based on pipeline, without switchboard, for User Acceptance Test.
   * [`gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_la`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_la) is a gitops based on pipeline, without switchboard, for real tenants (generation 1)
   * [Switchboard](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/switchboard) is a rails application that stores the tenant models in DB and interacts with AMP by scheduling k8s jobs using the instrumentor images and providing the tenant model as parameter (generation 2)
      * Tenants and dedicated operators can use the Switchboard UI to edit the models on the switchboard database.
      * Dedicated operators can also manually edit the models on the switchboard database for tenants.
2. Switchboard or an operator creates kubernetes job to run `tenctl` command, in this example, `tenctl configure`. This is done by invoking [`bin/configure` script](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/bin/configure). This job, based on an `Instrumentor` image, lives in the `Amp` cluster. (In a Sandbox environment, `Amp` cluster is not available, so the job runs in the CI Pipeline of the `Instrumentor` repository)
   * The main code block we're concerned about is:

      ```bash
      tenctl configure -m "${TENANT_MODEL}" ansible-playbook \
      --skip-generate \
      -- \
      -i "${inventory}" \
      playbooks/all.yml \
      ../../configure/ansible/all.yml \
      --diff
      ```

      This command templates and deploys the Ansible playbook configurations during the configure stage. The following steps breaks down and gives more details on components used in this command
3. The Instrumentor image contains jsonnet templates that `tenctl` tool uses to generate files needed for various stages of tenant lifecycle. The template [`jsonnet/configure-ansible.jsonnet`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/jsonnet/configure-ansible.jsonnet?ref_type=heads) is used for writing down `tenant.aws_ec2.yml` and `vars.yml`. These files are required by `GET` for the configuration (Ansible) part to be successful. Here are some examples of what they look like (in the container): [`*.aws_ec2.yml`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/get/examples/10k_hybrid_aws_services/ansible/10k.aws_ec2.yml) and [`vars.yml`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/get/examples/10k_hybrid_aws_services/ansible/vars.yml)
   * This is where the tenant model gets used.
   * Note that this template is not introducing anything new from Dedicated side. It is following the specification set by GET, while filling in particular tenant details. Result is correctly rendered Ansible configuration file for each tenant.
4. `GET` is vendored in the Instrumentor repository under path [`get/`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/get). It's basically a bunch of Ansible playbook templates. [`playbooks/all.yml`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/get/ansible/playbooks/all.yml) is the central place where all playbooks of `GET` is called to deploy components of GitLab.
5. [`../../configure/ansible/all.yml`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/configure/ansible/all.yml) contains ansible playbook definitions created by the Dedicated group, specifically for Instrumentor. It gets passed in alongside vendored GET's `all.yml` into the `tenctl` as shown above.

If possible, we should keep customizations to the tenant model, the [`configure` folder](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/tree/main/configure), and the [`jsonnet` folder](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/tree/main/jsonnet). Changes in those areas should be prioritized before making changes to `tenctl` or `GET`.

### Interaction between Instrumentor and GET during GitLab version upgrades

From the [dedicated document on stages](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/engineering/Stages.html), `GET` is called during `provision` and `configure` stages.

* During the `Provision` stage, `GET` provides Terraform configuration.
  * During this stage, `GET` provides `terraform` configuration for deploying cloud-provisioned resources such as k8s clusters, VMs, Redis, storage, networking, etc.
  * [GET documentation on provision stage](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md)
* During the `Configure` stage, `GET` provides Ansible playbooks.
  * `GET` configures the environment given the environment's inventory and variables in this stage.
  * The configuration for the `GitLab` chart gets passed onto `GET` from `Instrumentor`.
  * [GET documentation on configure stage](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_configure.md)

For the sandbox setup and GitLab version upgrade scenario, the `Instrumentor` MR label affects the [tenant model's `gitlab_version`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/test/integration/review-app-tenant-model.libsonnet#L137) that gets used to generate the `vars.yml` file. Note that the GitLab Omnibus package is used to deploy `Gitaly` nodes and `Consul` nodes, but not `NFS`.

For non-sandbox environments, `GET` is vendored in the Instrumentor repository under path [`instrumentor.git/get`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/get). Since its image is used in Amp cluster's kubernetes jobs, its playbooks can be accessed in the container's mounted path `playbooks/`.

For more information, please check the document about [GET usage with Dedicated](./get-usage-with-dedicated.md).

### Interaction between Instrumentor and Amp

`Amp` project sets up a Kubernetes cluster (EKS) `Amp` for running Instrumentor tenant jobs in. It also prepares the AWS account hosting that `Amp` cluster. It does not deploy the kubernetes cluster for sandbox environments (GitLab-CI does this instead).

`Switchboard` creates kubernetes jobs in the `Amp` cluster, providing the tenant model (stored in switchboard DB) as a parameter. Those kubernetes jobs use `Instrumentor` image that contain code to deploy using configuration files (from Instrumentor and `GET`). Amp jobs communicate to the tenant environments via AWS API, using AWS account ID and roles that the cluster gets deployed with.

Refer to the [architecture diagram for GitLab Dedicated](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/architecture/Architecture.md#managing-gitlab-dedicated) for a visual representation of Switchboard, Amp, and AWS/Tenant network.

The sandbox environment, by default, does not have an Amp cluster. This is why in [step 8 of the sandbox setup](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/engineering/sandbox-setup.md#step-8-setup-gitlab-ci-to-bootstrap-your-sandbox), it generates an AMP-like configuration for the sandbox tenant, allowing instrumentor CI to deploy.

To assume a non-sandbox environment, refer to the [steps in `amp.git/README.md`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/amp#assuming-a-non-sandbox-environment).

### Maintenance Window

Since GitLab Dedicated does not have zero downtime upgrade, upgrades must be done in maintenance windows, with one window for each customer. The upgrade is done via the Switchboard repo (learn more from this [clip](https://www.youtube.com/watch?v=Fs429uq1nYQ)). Because of legacy reasons, the repositories are in the `sandbox` subgroup, like [this one](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_la).

The [Switchboard app's tenant controller](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/switchboard/-/tree/main/app/controllers?ref_type=heads) reacts to changes to a tenant data. It validates and logs the change before creating a new kubernetes (`tenctl`) job according to the changes.

The controller validation fails in the following scenarios:

* The state change is to the GitLab version, but the change is not taking place during a maintenance period
* There are high severity alerts for the tenant
* There is a PCL, or a tenant-specific PCL

Upon failure, the tenant model gets updated with that information, so the operator or Switchboard UI can receive it.

Setting a preferred weekly maintenance window is a [requirement in the tenant onboarding template](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/e9f090261d43c43bedb211e8992e54725e4da945/.gitlab/issue_templates/tenant_onboarding_request.md#weekly-maintenance-window-preference-required). Every maintenance requires an [issue with label `~Requirement:Maintenance`](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/issues/?label_name%5B%5D=Requirement%3AMaintenance). When the tenant gets onboarded, the maintenance window for that tenant gets stored in the tenant's switchboard database.

An example upgrade pipeline for Dedicated: [link](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_la/-/pipelines/994606818).

### Quality/testing

There are various testing tools used throughout the projects. Here is [a table that contains the test coverage](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/engineering/quality-engineering-in-dedicated.md?ref_type=heads#test-coverage). Majority of the testing is done during merge requests, or upon merging.

In Instrumentor, QA stage only exists for test tenants, after the `configure` stage in CI. There are [component tests](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/tree/main/test), and [integration tests](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/tree/main/test/integration). For integration tests, the trusted entity ARN is supplied directly to the onboarding terraform in a CI variable (typically the sandbox account) along with its AWS credentials.

Before deploying to the production environment, the release strategy includes deploying to lower environments (`test`, `beta`, `preprod`) and performing some basic QA/metrics validation. More details in the [release strategy section for switchboard](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/switchboard/-/tree/main#release-strategy).

### References

* [Dedicated automated versions update repo](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/automated-versions-update) enables gitops for generation 1 repositories by performing version bumps.
* [Example deployment pipeline using Switchboard version upgrade flow on `switchboard_uat`](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_uat/-/pipelines/957551488)
* [Example Issue of Upgrading Tenant Components](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2596)
  * When they make MRs for tenants (like [this](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_la/-/commit/ebd46af9d712c90d9cc9d007992500428e99a795)), if it contains a `GitLab` version update, it triggers a deployment pipeline (like [this](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_la/-/pipelines/960779806))
* [DNS overview during `onboard` phase for sandbox and tenants](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/engineering/DNS.html#architecture)
  * The zone _must_ currently be Hosted in AWS Route53
