---
tags:
  - release-management
---
# Stop Gitaly and KAS Version Update

Gitaly and KAS versions are updated in the [GitLab repository](https://gitlab.com/gitlab-org/gitlab) via a [pipeline schedule] in `release-tools` in `ops.gitlab.net`, listed as `components:update_managed_versioning_projects`. If for some reason ([merging a security fix to Gitaly or KAS], bug, maintenance, etc.), these components should not be updated, please disable this [pipeline schedule].

> Note that, pausing the [pipeline schedule] disables the version update for both components. We have an open [issue](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20773) to split them.

[pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[merging a security fix to Gitaly or KAS]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/how_to_handle_gitaly_kas_security_merge_requests.md?ref_type=heads