---
tags:
- auto-deploy
---
# Independent Deploy

This document covers the necessary bits for an on-going project to enable
service teams to independently deploy their service onto Production, or .com, or
GitLab SaaS.

**:warning: This is an on-going bit of work, thus this document is not complete.
:warning:**

## Reference Material

* [Blueprint](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/self-serve-deployments/blueprint.md)
* [In-Progress Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/774)

## Engineering Requirements

* The service must implement all requirements from [the component
  page](../index.md)
* The service must implement [managed
  versioning](../managed-versioning/index.md)
* The service must have an entry inside of our
  [metrics-catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog)
  with a configuration that provides the following:
  * SLI's for Errors and Apdex should be created
  * The Deployment Health metric must return a value for the given service
    * Testable via Prometheus Query:
      `gitlab_deployment_health:service{type='$type'}` - where `$type` is equal
      to the `type` assigned to it via the `metrics-catalog`.
