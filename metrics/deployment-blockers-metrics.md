---
tags:
  - auto-deploy
---
# Deployment Blockers Metrics

We currently have deployment failures automatically captured under [release tasks issues]. Release managers are responsible for labeling these failures with appropriate `RootCause::*` labels.

By the start of the following week (Monday), the [`deployments:blockers_report`] scheduled pipeline in the [ops release/tools repository] reviews the labeled issues and generates a weekly deployment blockers issue, like this [one](https://gitlab.com/gitlab-org/release/tasks/-/issues/11125).

This scheduled pipeline runs the [`record-deployment-blockers`] job which executes three Rake tasks: generation of deployment blockers report, blockers Grafana annotation, and generation of the blockers metrics. These metrics are `deployment_blocker_count` and `deployment_hours_blocked` which capture the total number of blockers encountered and the hours they blocked both `gstg` and `gprd` environments for all root causes in the previous deployment week.

These metrics are currently used in the ["delivery: Deployment Blockers"] dashboard to visualize the trend of recurring root causes for deployment blockers.

This runbook aims to explain the different graphs in the dashboard, provide troubleshooting guidance, and how to manually create those metrics.


## Dashboard Guide

The Deployment Blockers dashboard is composed of different visualizations to help understand the blockers encountered over time.
1. **Overview**
    * This row consists of three bar charts that show the total blocker count and total hours blocked on both `gstg` and `gprd` for each root cause within the selected time frame.
    * At the bottom of each panel is the total value of these metrics for all root causes.

2. **RootCause::<root_cause label>**
    * [Repeated rows] are used to visualize trends for each root cause using the template variable `$root_cause`.
    * Each row has three trend panels and one table panel. These panels plot different metric values weekly and summarize all the metric values respectively for the selected `$root_cause` value.

3. **Template variable `$root_cause`**
    * This variable captures the root causes with blocker counts greater than 0 for the last sixty days to prevent overcrowding the dashboard with rarely encountered.

      * Query: `query_result(max by (root_cause) (last_over_time(delivery_deployment_blocker_count{root_cause!="RootCause::FlakyTest"}[60d]) > 0))`
      * Regex: `/root_cause="(?<text>[^"]+)/g`


## Troubleshooting

### The panel(s) on the dashboard is presenting incorrect information

This could be due to several reasons, so we should ask the following questions to investigate:

1. Does the metrics with the correct labels and values show up on the Grafana page?

    * Check these queries: [delivery_deployment_blocker_count], [delivery_deployment_hours_blocked]
    * If the latest metric appears correct, then the dashboard panel is not working as intended. Further investigation into the panel setup is necessary.
    * Temporary edits can be made to the Grafana panel, but these changes will not persist as the dashboard refreshes from the [runbooks repository].

1. Was the metric created correctly?

    * Metrics are generated during the [deployments:blockers_report] scheduled pipeline execution. Investigate the logs produced by that job.
    * Look for a log line similar to:

      ```log
      ReleaseTools::Deployments::BlockersMetrics -- Setting deployment blockers metric for ~"RootCause::Flaky-Test" -- {:blockers_per_category=>9, :hours_gprd_blocked=>4.5, :hours_gstg_blocked=>4.5, :labels=>{:root_cause=>"RootCause::Flaky-Test", :week=>"2024-07-01"}}
      ```

      See [example](https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/14572333#L116).
    * If there is an error, incorrect metric value, or if that log line does not exist, investigate further into the source
    code in `release-tools`.
    * If there seems to be an error with the metrics server, refer to [Troubleshooting the Delivery-metrics server].
    * Once the problem has been addressed via an MR, it may be necessary to manually create the correct metric if the job that emitted the incorrect metric or encountered an error occurred before the fix MR commit.
    * There are instances where the Release Managers have to review the "Additional incidents" list ([example])(https://gitlab.com/gitlab-org/release/tasks/-/issues/11345#additional-incidents) and add the `Deploys-blocked` label if required. In such cases, the blockers report is manually updated and the captured metrics `delivery_deployment_blocker_count` and `delivery_deployment_hours_blocked` will not include those additional blockers. Manual creation of the blockers metrics for that week is necessary after labeling the blocker issues.


## Manual creation

Sometimes, we need to (re-)create the metrics due to incorrect values and issues discussed above.

Note that this is more of a temporary solution if there is actually a bug within the dashboard or `release-tools` code.

1. Retrigger the scheduled pipeline [`deployments:blockers_report`].

2. Since the job [`record-deployment-blockers`] also regenerates the blocker report issue, the newly created issue needs to have its assignees removed and have `TEST - ` prefixed to its title. It also needs to be closed as a duplicate.

    * See example [job](https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/14221140) and [blockers report issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/11107).

3. Verify the metrics values in the job logs and the Grafana dashboard.


[release tasks issues]: https://gitlab.com/gitlab-org/release/tasks/-/issues
[`deployments:blockers_report`]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules#:~:text=deployments%3Ablockers_report
[`record-deployment-blockers`]: https://gitlab.com/gitlab-org/release-tools/-/blob/madelacruz/update-deployment-blockers-metrics/.gitlab/ci/automation.gitlab-ci.yml#L180
["delivery: Deployment Blockers"]: https://dashboards.gitlab.net/d/delivery-deployment_blockers/delivery3a-deployment-blockers?orgId=1
[Repeated rows]: https://grafana.com/blog/2020/06/09/learn-grafana-how-to-automatically-repeat-rows-and-panels-in-dynamic-dashboards
[delivery_deployment_blocker_count]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%2280w%22:%7B%22datasource%22:%22PA258B30F88C30650%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22delivery_deployment_blocker_count%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22PA258B30F88C30650%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-90d%22,%22to%22:%22now%22%7D%7D%7D&orgId=1
[delivery_deployment_hours_blocked]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%2280w%22:%7B%22datasource%22:%22PA258B30F88C30650%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22delivery_deployment_hours_blocked%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22PA258B30F88C30650%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-90d%22,%22to%22:%22now%22%7D%7D%7D&orgId=1
[runbooks repository]: https://gitlab.com/gitlab-com/runbooks/-/blob/master/dashboards/delivery/deployment_blockers.dashboard.jsonnet
[ops release/tools repository]: https://ops.gitlab.net/gitlab-org/release/tools
[Troubleshooting the Delivery-metrics server]: troubleshoot-delivery-metrics.md
