---
tags:
  - auto-deploy
---
# Evaluate Upcoming Deprecations

## Why

Sometimes configuration deprecations may hinder the installation of new GitLab
versions. As our software evolves, it is typical for structures to change or be
modified, especially around major releases. To avoid potential blocks to
Auto-Deploy caused by these changes, it is crucial to proactively identify and
address these deprecations as soon as possible. Not only does this prevent
potential roadblocks, but it also allows us to thoroughly test and evaluate the
desired configuration directive.

## How

There are a few options for doing such, feel free to engage in your favorite
method:

* Check Omnibus for any newly added
  [deprecations](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/files/gitlab-cookbooks/package/libraries/deprecations.rb)
  which are marked with `deprecation: '<current milestone version>'`
* On the deploy node, execute the following command:
  > `sudo gitlab-ctl  check-config --version <NEXT_MAJOR_VERSION>`

### When Deprecations are Detected

Create a new issue in the Reliability Tracker and indicate the deprecation and
set a due date targeting the appropriate milestone for which if left unaddressed
begins to block Auto-Deployments.

Normally if configuration changes are required, modifications will need to be
made to one or both of the repos that store said configurations:

* For our Ominbus installations, that's [`chef-repo`](https://gitlab.com/gitlab-com/gl-infra/chef-repo)
* For our Kubernetes installations, that's [`k8s-workloads/gitlab-com`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com)
