---
tags:
  - release-management
---
# Major release

GitLab releases a new major version (for example, 12.0) [once per year](#frequency).

A major release allows us to make breaking changes, for instance removing or
changing (deprecating) existing APIs and functionalities.

## Handling deprecations

[Deprecated items](https://about.gitlab.com/handbook/product/#deprecated-no-longer-maintained)
should be announced and, if necessary, replaced one or more minor releases
in advance of the major release.

For instance, when going from API v4 to v5, v5 will have to be active for at
least a single minor release before deprecating v4. In practice, it's unfriendly
to deprecate an API over a single month, and an even longer time period might be chosen
(such as one major release for a new API version, or a span of a number of minor
releases).

## New functionalities and APIs

New functionalities and APIs can freely be added at any time and should not be
ported back.

For instance, GitLab 8.16 introduced an API for time tracking. This adds to the
API and will not break any integrations, so could be introduced in a minor
update.

## Frequency

Major releases will be scheduled for May 22 each year, by default.

For more information, see our
[maintenance policy](https://docs.gitlab.com/ee/policy/maintenance.html).

## Creating awareness

People need to be aware of changes and deprecations. For each major or breaking
change, consider:

- Updating all documentation.
- Making customers aware in blog and release posts.
- Training the service engineers in the change, but also in how to help
  customers transition.
- Optionally reaching out to customers and/or external applications.

---

[Return to Guides](../README.md#guides)
