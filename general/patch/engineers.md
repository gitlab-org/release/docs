---
tags:
  - release-management
---
# Patch release runbook for GitLab engineers: Backporting bug fixes to the current version

## General guidelines

As defined on the [Maintenance Policy], **patch releases can include
bug fixes for the current stable released version of GitLab and security
fixes to the previous two monthly releases in addition to the current stable release.**.

From the GitLab engineer's perspective, this runbook explains how to backport bug
fixes to the current version. If you're a GitLab engineer looking to prepare a security fix,
please have a look at the [GitLab engineer runbook for preparing security fixes]

Bug fixes can be backported to the current version if:

* They have been previously fixed and merged into the GitLab default branch.
* They have been deployed to GitLab.com.
* They are bug fixes. Per the maintenance policy [patch release guidelines], features can't be backported to previous versions.

Backporting bug fixes is limited to the current stable released version, however, exception cases may be considered. See the
[backporting to versions outside of the Maintenance Policy] documentation for more information.

## Backporting a bug fix in the [GitLab project]

To backport a bug fix in the current version, GitLab engineers should:

1. Ensure the bug fixes comply with the [general guidelines]: The merge request that fixes the bug on the default branch has been merged and deployed to GitLab.com
1. Open up a merge request in the [GitLab canonical project], the merge request should target the stable branch associated with the current version.
   * **Note**: A merge request can be created via UI using the [cherry-pick feature] or [manually via UI or terminal](#process-to-create-a-backport-merge-request)
1. Use the [stable branch template] and follow the check list.
1. Ensure the merge request is approved by a maintainer. Merge requests targeting stable branches only require one approval.
1. Add a [severity label] to the merge request (if applicable). Severity labels helps release managers assess the urgency of
   a patch release.
1. Set the milestone of the merge request to match the target backport branch version.
1. Ensure the [`e2e:test-on-omnibus-ee` pipeline] is executed. This pipeline executes end-to-end testing for the GitLab platform
   guaranteeing the code changes are compliant from the Quality side. This pipeline is automatically executed on merge requests targeting
   stable branches.
1. Verify the `e2e:test-on-omnibus-ee` is successfully executed. If failures arise, contact a Software in Test (SET) engineer to
   review if the failure is related to the merge request.

### Process for maintainers

When a merge request targeting a stable branch is assigned to maintainers, they should:

1. Confirm the merge request being backported has been deployed to GitLab.com
1. Verify only bug fixes are being backported. Per the [Maintenance Policy], features can't be backported.
1. Ensure the [`e2e:test-on-omnibus-ee` pipeline] has been executed.
1. Verify if there are failures on the `e2e:test-on-omnibus-ee` pipeline. Due to [test flakiness], this
   pipeline is allowed to fail, and as a consequence, it won't cause a failure on the upstream GitLab pipeline.
   Failures on this pipeline must be reviewed by a Software Engineer in Test (SET).
   **Don't merge the backport until a SET has confirmed the failures are not related.**
1. Once the above conditions are met, approve and set the merge request to MWPS.

## Backporting a bug fix on [Managed Versioning] projects

Once it has been deemed a bug fix should be backported, engineers should:

1. Ensure the bug fixes comply with the [general guidelines]: The original merge request is a bug fix that has been deployed to GitLab.com.
1. Open up a merge request in the respective canonical project. The merge request should target the stable branch associated with the current version.
1. Use the stable branch template and follow the checklist
   * [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md)
   * [CNG](https://gitlab.com/gitlab-org/build/CNG/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md)
1. Ensure the merge request is approved by a maintainer. Merge requests targeting stable branches only require one approval.
1. Add a [severity label] to the merge request (if applicable).

### Process for maintainers

When a merge request targeting a stable branch is assigned to maintainers, they should:

1. Ensure the merge request being backported has been deployed to GitLab.com
1. Verify only bug fixes are being backported. Per the [Maintenance Policy], features can't be backported.
1. Once the above conditions are met, proceed to merge the merge request.

## Additional considerations

1. Patch releases are SLO-driven scheduled twice a month on the Wednesdays of the weeks before and after the monthly release. These are best effort dates and might be subject to change.
1. If a patch release for an older version is required, take a look at the [Backporting to versions outside of the Maintenance Policy]
   section. Backporting outside the [Maintenance Policy] will require release managers' approval.
1. Keep an eye to the [#releases] and [#development] Slack channels. When a patch release is published, a notification is sent to these channels
   to broadcast the patch release.
1. Delivery is working on generating [long lived environments] to be continuously deployed from stable branches,
   as part of this effort, on the a downstream pipeline called `start-release-environments-pipeline` is automatically
   triggered on commits on stable branches on the [GitLab canonical project], this one is allowed to fail, and such,
   failures can be temporarily ignored.

## Backporting to versions outside of the Maintenance Policy

Non-security patches that are outside of our [Maintenance Policy] must be requested
and agreed upon by the Release Managers and the requester. Read through the [backporting to older releases]
documentation and open up a [backport request] issue if the bug fix meets the criteria.

## Feedback and questions

Reach out the Delivery group on the [#releases] Slack channel for feedback and/or questions.

## Utilities

### Process to create a backport merge request

> For efficiency, merge requests can be created via using the [cherry-pick feature].

**Creating backports via UI**

1. Create a new [branch](https://gitlab.com/gitlab-org/gitlab/-/branches/new),  in the "Create from" field, select the stable branch that needs to be patched.
1. Create a [merge request](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/new), in the "Source branch" select the branch created above, and for the "Target branch" select the stable branch that needs to be patched.
1. Once the MR is created, cherry-pick the commit that you wish to change or open the web ide and add your changes.
1. Follow the steps to backport the bug fix described above.

**Creating backports via terminal**

1. Fetch the stable branch associated with the current version:

   ```
   # In this case, 16.3 is the current version
   git checkout -b 16-3-stable-ee --track origin/16-3-stable-ee
   ```

2. Create a new branch from the stable branch:

   ```
   git checkout -b <my_branch>
   ```

3. Cherry-pick the merge commit from the original merge request into the branch:

   ```
   git cherry-pick -m1 <sha>
   ```

4. Push the branch into GitLab.com and follow the [steps to backport a bug fix](#backporting-a-bug-fix-in-the-gitlab-project) described above.

[#releases]: https://gitlab.slack.com/archives/C0XM5UU6B
[#development]: https://gitlab.slack.com/archives/C02PF508L
[`e2e:test-on-omnibus-ee` pipeline]: https://docs.gitlab.com/ee/development/testing_guide/end_to_end/test_pipelines.html#e2etest-on-omnibus-child-pipeline
[backport request]: https://gitlab.com/gitlab-org/release/tasks/-/issues/new?issuable_template=Backporting-request
[backporting to older releases]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
[Backporting to versions outside of the Maintenance Policy]: #backporting-to-versions-outside-of-the-maintenance-policy
[cherry-pick feature]: https://docs.gitlab.com/ee/user/project/merge_requests/cherry_pick_changes.html#cherry-pick-all-changes-from-a-merge-request
[general guidelines]: #general-guidelines
[GitLab canonical project]: https://gitlab.com/gitlab-org/gitlab
[GitLab engineer runbook for preparing security fixes]: ../security/engineer.md
[GitLab project]: https://gitlab.com/gitlab-org/gitlab
[long lived environments]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/837
[Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[Managed Versioning]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/index.md
[patch release guidelines]: https://docs.gitlab.com/ee/policy/maintenance.html#patch-releases
[severity label]: https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity
[stable branch template]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md
[test flakiness]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/test-metrics-dashboards/#package-and-test

---

[Return to README](readme.md)
