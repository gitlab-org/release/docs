---
tags:
  - release-management
---
# Release Manager runbook for patch releases

Release managers can prepare two types of patch releases:

- **Planned patches**: SLO-driven patch based on the [GitLab Maintenance Policy] to accomodate the GitLab SLO/SLAs:
  [release SLO], [bug remediation SLO] and [vulnerability remediation SLA]. They are scheduled twice a month on
  the Wednesdays of the weeks before and after the monthly releaseIncludes bug fixes for the current GitLab version
  and security fixes for the current and the two previous GitLab versions. It is expected of release managers to perform two
  planned patch releases during their shift.
- **Unplanned patches**: Unplanned patches are ad-hoc processes coordinated by AppSec used to quickly mitigate a high-severity
  vulnerability in order to meet the [vulnerability remediation SLA]. It is important to highlight that "quickly" does not mean
  that the release needs to happen the very moment it is requested, because planned patch releases are SLO-driven, it is likely
  the next scheduled patch release can be used to release the high-severity vulnerability fix**.

The [release dashboard] provides an overview of the next patch release information.

## Considerations

1. The patch release schedule is best effort and, as long as the schedule meets the GitLab SLO/SLAs,
it can be updated at the discreation of release managers.
1. Any patch with an S1 vulnerability will be considered by 'critical' by the release tooling. Effects of
   this notation will be present during the blog post and the communications sent over to customers.
1. When dealing with security fixes, it is important that all the work happens **only** on Security repositories
   so nothing is disclosed publicly before we intend it.
1. Any delay needs to be escalated to Sr. Manager of Infrastructure.
1. Avoid releasing security packages to the community on Friday and during worldwide low availability periods
in order to avoid situations where a customer may be exploited during the weekend.
1. If required, release managers can prepare [Patches for a single version].

## Process

While the end-to-end process for planned and unplanned patches is the same, the content of patches is different:
Planned patch releases will include bug fixes for the current GitLab version and security fixes for the current
and the two previous GitLab versions, and unplanned patch releases will likely only include the fix for the
high-severity vulnerability.

Details of the end-to-end patch release process can be found on the [handbook] and on the [patch/readme]

From a release manager standpoint, the following steps are required to perform a patch release:

1. Two days before the patch release date, usually on Monday, release managers trigger the ChatOps command
on the `#f_upcoming_release` Slack channel

   ```text
   /chatops run release prepare --security
   ```

1. A release task issue will be created on the [release task tracker] with the required steps to tag, deploy and
publish the patch release, the steps are described below. Release Managers are the DRIs of this issue and it is
their responsibility to see it through and update it as they progress.
1. Two days before the due date, Release Managers will:
   1. Disable the automated security implementation issue processor to prevent new security issues from being
    automatically linked to the Security Tracking issue. This processor can be disabled at any time by deactivating the `security-target issue processor` [pipeline schedule].
   1. Merge security merge requests targeting the default branch with:

   ```text
   # On Slack
   /chatops run release merge --security --default-branch
   ```

   Security merge requests targeting the default branch will only be merged if:

   - They belong to GitLab Security
   - They are associated to security issues linked to the current Security Tracking Issue.
   - They are associated to security issues that are ready to be processed.

   Other tasks including dealing with other components which may have security
   picks for the target release as well, such as
   [Gitaly merge requests] and GitLab-Pages. Each of these are handled manually.

   Once the security merge requests have been merged, Canonical and Security default branches will diverge,
   [security merge-train pipeline schedule] will deal with this divergence by updating Security default branch
   based on the Canonical default branch content, see [merge train] for more info.

   Security issues that are linked to the Security Tracking issue less than 24 hours
   before the due date will be automatically unlinked by the Release Tools Bot. For
   example, if the due date of the Security Tracking issue is 28th August, security
   issues added after 27th August 00:00 UTC will be automatically unlinked.

   However, security issues that are linked as blockers ('is blocked by' vs
   'relates to') to the Security Tracking issue will not be unlinked.
   This allows high priority security issues to be linked to the tracking issue
   close to the due date.

   The automatic unlinking behavior is behind a feature flag, which can be disabled
   if required: [unlink_late_security_issues]

   Release-tools will generate and post a comment with a table to the security tracking issue. The
   table shows the status of every security issue linked to the security tracking issue.
   Example table: <https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1589#note_1055523334>.

   Do not edit the table, except to add comments under the "Release manager comments" column. Changes to
   any other column will be overwritten any time a new issue is automatically linked or unlinked or
   the next time the `/chatops run release merge --security` command is executed
1. One day before the due date, Release Managers will merge GitLab security backports and
security merge requests associated with other satellite GitLab projects.

   ```text
   # On Slack
   /chatops run release merge --security
   ```

1. A blog post will be automatically created on the [www-gitlab-com security] repository by the
security pipeline referenced on the release task issue. The blog post will include the bug fixes
and the security fixes that were processed. AppSec is the DRI of the blog post content.
1. On the due date of the Patch release, Release Managers will tag, publish the packages and
complete the Patch Release.

## Utilities

- [List of bug fixes waiting to be released]
- [Stable branch configuration]
- [Unplanned patch release considerations]
- [Patches for a single version]

[bug remediation SLO]: https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity-slos
[handbook]: https://handbook.gitlab.com/handbook/engineering/releases/patch-releases/
[merge train]: general/security/utilities/merge_train.md
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[Gitaly merge requests]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/how_to_handle_gitaly_kas_security_merge_requests.md
[List of bug fixes waiting to be released]: ./bug_fixes_waiting_to_be_released.md
[patch/readme]: readme.md#process
[Patches for a single version]: patches_for_single_version.md
[pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[release SLO]: https://internal.gitlab.com/handbook/company/performance-indicators/product/saas-platforms-section/#deliveryreleases---mean-time-between-security-releases
[release dashboard]: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1
[release task tracker]: https://gitlab.com/gitlab-org/release/tasks/-/issues
[security merge-train pipeline schedule]: https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules?scope=inactive
[Stable branch configuration]: ./stable_branch_configuration.md
[unlink_late_security_issues]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags/203/edit
[vulnerability remediation SLA]: https://handbook.gitlab.com/handbook/security/product-security/vulnerability-management/#remediation-slas
[www-gitlab-com security]: https://gitlab.com/gitlab-org/security/www-gitlab-com
[unplanned patch release considerations]: unplanned_patch_release_considerations.md
