---
tags:
  - auto-deploy
  - release-management
---
# How to Mitigate Bugs Introduced by Security Merge Request

Reverting a security merge request is [not encouraged]. Rollback of a security fix compromises the integrity of
GitLab.com and self-managed instances. However, there have been cases where security merge requests introduced
bugs, which required mitigation strategies.

The mitigation options will depend on the severity of the security issue, the impact of the bug introduced,
and the timeline of the security release.

Following are several options for mitigation of a bug introduced by a security merge request.

## Option 1 (Ideal): The bug introduced is fixed in the same security release

This is the ideal option, but it depends on the proximity of the security release due date. If the due date is in less
than 48hrs, this option is not available.

The stage team prepares a bug fix on the security repository, coordinates with release manager for merging, and ensures
it is deployed to GitLab.com at least 48hrs before the security release due date. This option guarantees the security
issue and the bug fix are included in the security release.

Note the security release due date can't be delayed for compliance reasons and to avoid impacting the monthly release
schedule.

## Option 2: Fully-in - Not reverting the security merge request

This is suggested for higher-severity security issues with `Severity::1` or `Severity::2`.

Depending on the impact of the bug, the security merge request and the backports should proceed to be published
with the security release, guaranteeing the vulnerability is fixed on GitLab.com and on self-managed installations.
After the vulnerability is mitigated, the bug should be addressed in the following patch.

Once the security release is out:

* Stage team fixes the bug in the [GitLab canonical repository].
* Stage team backports the bug fix to the current stable version following the [patch release process].

## Option 3: Fully-out - Reverting the security merge request

This is suggested for lower-severity security issues with `Severity::3` or below.
**This option requires explicit approval from AppSec.**

The security merge request is reverted and the security issue is removed from the security release. This implies the
security vulnerability will not be fixed on GitLab.com and self-managed installations, and the details about the
vulnerability will be leaked once the security release is out.

Refer to the [runbook on reverting a security merge request] on steps to revert the security merge request.

## Alternative option for GitLab components

If the bug is in a component repository, not the main GitLab Rails repository, and none of the Options above fits
the situation, we have a [runbook on removing security commits]. **This option should be a last resort option and
it should only be considered if the patch release can't be postponed.**

Refer to the [runbook on removing security commits] on steps to remove security commits.

[not encouraged]: https://handbook.gitlab.com/handbook/engineering/releases/patch-releases/#how-can-i-revert-a-security-merge-request
[GitLab canonical repository]: https://gitlab.com/gitlab-org/gitlab
[patch release process]: ../patch/engineers.md
[runbook on reverting a security merge request]: ../../runbooks/security/revert-security-merge-request.md
[runbook on removing security commits]: ../../runbooks/security/remove-security-commit.md
