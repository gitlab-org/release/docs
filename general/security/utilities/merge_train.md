---
tags:
  - auto-deploy
---
# Merge-train

This doc refers to the <https://gitlab.com/gitlab-org/merge-train> project, not the merge train feature
in the GitLab product.

## Why is `merge-train` used?

Every project that can be part of a patch release has a security mirror (usually under
`gitlab.com/gitlab-org/security`) that is not public. The canonical project and its security mirror are kept in sync
by using the mirroring feature of the GitLab product. The mirroring feature pushes commits from the
canonical repository to the security mirror.

During a patch or internal release, security fixes are merged into the default branch and the stable branches of the
security mirror to keep security vulnerability information private until the patch release is published. Once security
merge requests have been merged, Canonical and Security will diverge, which breaks the automatic mirroring.

With mirroring broken, and since we deploy from the security mirror, we need some way to push canonical commits to the
security mirror in order to allow everyone's work to continue being deployed to GitLab.com. This is where the
merge-train project becomes useful.

## How we use `merge-train`

> **Note**: For a general tutorial on how to use `merge-train` script, please check the project's
> [README](https://gitlab.com/gitlab-org/merge-train/-/blob/master/README.md?ref_type=heads).

`merge-train` runs from the [project's mirror](https://ops.gitlab.net/gitlab-org/merge-train) in the `ops` instance.

A [scheduled task](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/121/edit)
in `release-tools` will check for this divergence periodically and automatically
enable the [security merge-train pipeline schedules] to regularly update the
Security default branch and stable branches until the divergence is resolved.

The [security merge-train pipeline schedules] will deal with this divergence
by updating Security default branch and stable branches with the Canonical content.

If you need to force an execution of the merge-train outside of this schedule,
you can:

* Go to the [security merge-train pipeline schedules].
* Click play.

Full execution of merge-train takes approximately 6 minutes.

For the default branch, there are [security merge-train pipeline schedules] for the Omnibus, GitLab and Gitaly projects.
For the stable branches, there are [security merge-train pipeline schedules] for the GitLab project.

The SSH key used by the merge-train project to clone the security repository is stored in 1password under
the Release vault with the name `merge-train: GitLab EE security/master deploy key`.

The SSH key is present in each project as a [Deploy Key](https://docs.gitlab.com/ee/user/project/deploy_keys/)
with the name `GitLab EE security/master sync`.

## Troubleshoot guide

### What to do if merge-train fails to update a Security branch?

There's a chance of a file being modified both on Canonical and on Security,
if this happens merge-train will fail when performing the merge between the two repositories.

The failure will generate a notification on the `#g_delivery` Slack channel.
Solving this will require coordination by the Release Managers:

* Analyze how difficult it is to recover from breakage.
  * If the conflict is easy to solve, fix it and push the changes to a branch with a merge request, ask another team member to review and merge once the pipeline is green.
  * If the conflict is more involved, ping the developers related to the changes, if they're not available, use the dev-on-call process.
* If the conflict blocks deployments, follow the [deployment blockers process] and raise an incident.

[security merge-train pipeline schedules]: https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules
[deployment blockers process]: https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers
