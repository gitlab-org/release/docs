---
tags:
  - auto-deploy
---
## Cherry pick security fixes into the auto-deploy branch

Security merge requests targeting the default branch branch are automatically cherry-picked
into the current auto-deploy branch after they're merged.

Release Managers verify the auto-deploy branch contains all the security fixes,
if for some reason one or more security fixes are missing, they can use the following script
to cherry-pick them:

```sh
# fetch both remotes
git fetch origin
git fetch security

# sync current auto-deploy branch on security
git checkout -b CURRENT_AUTO_DEPLOY_BRANCH --track security/CURRENT_AUTO_DEPLOY_BRANCH
git pull origin CURRENT_AUTO_DEPLOY_BRANCH
git push security

# get a list of all the merge commits on security/master not available on origin/master
git log origin/master..security/master --merges | grep "^commit"

# cherry-pick each commit on that list in the current auto-deploy branch
git cherry-pick -m 1 -x COMMIT_ID

# push to security
git push security
```
