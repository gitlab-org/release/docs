---
tags:
  - release-management
---
# List of bug fixes waiting to be released

Release managers can make usage of a ChatOps command to fetch the list of backports waiting to be released. For this:

1. Release managers can execute the `/chatops run release pending_backports` command on Slack.
1. Release-tools will fetch the unreleased merge requests merged into the last three active stable branches.
1. A message will be posted on Slack listing the merge requests that will be included in the next patch release

| Example |
| ---- |
| ![pending backports](images/pending_backports.png) |
