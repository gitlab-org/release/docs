---
tags:
  - auto-deploy
---
### Feature flags are not allowed on security merge requests

Dangerbot automatically rejects changes targeting the security repository that include feature flags.

#### Alternate Options

1. Continue with the security release change without the feature flag. Doing so
   requires evaluation to determine the risk of not having the feature flag.
1. Move the security fix to the GitLab canonical repository. Doing so requires
   approval from AppSec to ensure we are not exposing any vulnerability and
   weighing the risk associated with the change.

#### Reasoning

Feature flags are used in regular development to reduce risk and
to do controlled testing. Engineers might be tempted to use them
on security merge requests, but this approach has several inconveniences:

* A self-hosted instance might disable the flag and be vulnerable.
* GitLab.com will be vulnerable if the fix doesn't work as expected.
  * If this happens, GitLab.com will likely need to be hotpatched,
    putting additional workload on the Infrastructure and Development teams.
  * Alternatively, Release Managers and AppSec can decide to publish the security
    release with the vulnerability exposed and follow up with a critical security release.
    This option is no better, it also adds workload to the Release Managers and engineers involved.
* There's no process for handling feature flags in backports. If the feature flag
  is backported, it's less likely to be cleaned up once it's no longer necessary.
* Feature flags should be short-lived: once they've been tested
  they should be removed from the codebase. Security releases are time-sensitive,
  and security merge requests are deployed within a limited timeframe which doesn't
  give enough time to introduce a feature flag, test it, and remove it.

Reference: https://gitlab.com/gitlab-com/feature-change-locks/-/issues/55
