---
tags:
  - auto-deploy
---
# GET Usage with Dedicated

[GitLab Environment Toolkit (GET)](https://gitlab.com/gitlab-org/gitlab-environment-toolkit) is a set of opinionated Terraform and Ansible scripts to assist with deploying scaled self-managed GitLab environments following the Reference Architectures.

GitLab Dedicated is a fully isolated, single-tenant SaaS service. It uses [Instrumentor](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor) to call GET scripts to set up GitLab for different tenants.

This document explains how the GET scripts are used within Instrumentor [pipeline](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/.gitlab-ci.yml?ref_type=heads). There are four main stages in the pipeline: `prepare`, `onboard`, `provision` and `configure`. Since GET is used in `provision` and `configure` only, we will focus on these two stages. If you want to learn more about all stages, please check the [docs](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/engineering/Stages.md?ref_type=heads).

## Tools

- [tenctl](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/tenctl): `tenctl` is a shortening of the phrase 'Tenant Control'. It's a program designed to wrap calls to Terraform, Ansible and other infrastructure tooling, ensuring that the wrapped tools are correctly configured for a specific tenant. tenctl will take a tenant object model, and ensure that Terraform and Ansible are configured for that specific tenant. tenctl is the entrypoint in Instrumentor container images.
- [terra-transformer](https://gitlab.com/gitlab-com/gl-infra/terra-transformer): A small program to help import resources into Terraform in a consistent manner.

## Stages

All the steps in `provision` and `configure` steps are listed. More details and explanation will be provided if needed.

### provision

Create cloud infrastructure WITHOUT deploying GitLab. The list of all cloud infrastructure resources can be found in the stage's [README](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/provision/README.md).

- tenctl migrate-model
- tenctl provision -m \<TENANT-MODEL>  env
  - Generate environment exports (suitable for bash or sh)
- tenctl provision -m \<TENANT-MODEL>  terraform -- init -backend-config=s3backend.tfvars
- terra-transformer --config config/terra-transform-protected.yaml protected-apply --chdir provision --plan-file-json provision.tfplan.json --backend-config s3backend.tfvars --exemptions-dir terratransformer_exemptions20230830-97-wfa0lu
  - This step runs TF plan and apply
- tenctl provision -m \<TENANT-MODEL>  --generate-kubeconfig env
- tenctl list-clusters -m \<TENANT-MODEL>

### configure

This is the step we need to work on, since it is where the (version) upgrade happens.

- tenctl migrate-model
- tenctl configure -m \<TENANT-MODEL>  env
- tenctl list-clusters -m \<TENANT-MODEL>
- terra-transformer --config config/terra-transform-protected.yaml protected-apply --chdir configure/pre_ansible --plan-file-json pre_ansible.tfplan.json --backend-config=s3backend.tfvars
- tenctl configure -m \<TENANT-MODEL>  ansible-playbook --skip-generate -- -i environment-r0/inventory playbooks/all.yml ../../configure/ansible/all.yml --diff
  - Run Ansible to deploy GitLab. More details will be provided below.
- tenctl list-clusters -m \<TENANT-MODEL>

At this stage, the Ansible playbook [playbooks/all.yml](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/dattang-reviewapp/get/ansible/playbooks/all.yml) is called ([an example](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/jobs/4980064941)). This playbook imports many playbooks:

| playbook                | in use             | notes                                                                                     |
|-------------------------|--------------------|-------------------------------------------------------------------------------------------|
| `common.yml`            | :white_check_mark: | Setup common configuration like package repository, etc.                                  |
| `haproxy.yml`           | :x:                | Using AWS load balancers                                                                  |
| `consul.yml`            | :x:                | Currently the usage of Consul is not following the best practices, so it can be reviewed. |
| `postgres.yml`          | :x:                | Use RDS                                                                                   |
| `pgbouncer.yml`         | :x:                | Use RDS                                                                                   |
| `redis.yml`             | :x:                | Use ElastiCache                                                                           |
| `gitaly.yml`            | :white_check_mark: |                                                                                           |
| `praefect_postgres.yml` | :x:                | Use RDS                                                                                   |
| `praefect.yml`          | :x:                | Use RDS                                                                                   |
| `gitlab_rails.yml`      | :x:                | GitLab Rails run on K8S Pods, not on VMs.                                                 |
| `sidekiq.yml`           | :x:                | GitLab Sidekiq run on K8S Pods, not on VMs.                                               |
| `gitlab_charts.yml`     | :white_check_mark: | Install Consul (provides an in-cluster connection with the Consul cluster running on Consul VMs and Gitaly VMs) and GitLab charts. GitLab chart is a wrapper of many sub-charts. In `Instrumentor`, Postgres, Prometheus, Redis, Gitaly and gitlab-runner charts are disabled since they run on VMs|
| `monitor.yml`| :white_check_mark: | |
| `elastic.yml`| :white_check_mark: | |
| `opensearch.yml`| :white_check_mark: | |
| `post_configure.yml`| :white_check_mark: | Wait for GitLab to be available and then do some tasks |

After that, there are some tasks outside of GET. They are called via [configure/ansible/all.yml](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/configure/ansible/all.yml?ref_type=heads)

- Set "gitlab_dedicated_instance" flag to true via GitLab Toolbox pod: used by Service Ping to collect analysis data.

## Discussion

### Zero Downtime Upgrade

- Currently only work with VM setup
  - GET has zero downtime upgrade feature, but only with VM
- From the [GET docs about zero downtime upgrade](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/2e75ded827cd9b9d0aadf88ed43b3041dfad87db/get/docs/environment_upgrades.md#zero-downtime-updates), looks like there are two points of downtime:
  - Supporting services update (Praefect Postgres, HAProxy, OpenSearch)
  - GitLab:
    - From this [`zero_downtime_upgrade` Ansible task](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/dattang-reviewapp/get/ansible/roles/zero_downtime_upgrade/tasks/main.yml), it looks like the upgrade process includes a sequence of service nodes restart (Consul -> Postgres -> PgBouncer -> Redis -> Gitaly -> GitLab -> Sidekiq -> Restart geo-logcursor on secondary site if present -> Run post-deployment migrations)
    - So right now every time Instrumentor runs, all services in GitLab get restarted, regardless of having any change related to them. We should work on having handlers to control that (e.g. if Redis configuration file changes, restart Redis. If not, do nothing)
    - Not sure why we need to run the upgrade command (via Ansible) for each supporting services, while they are included in the `zero_downtime_upgrade` playbook already
- [An interesting comment about zero downtime upgrade](https://gitlab.com/groups/gitlab-org/cloud-native/-/epics/52#note_1469967190)
  - Looks like it is related to this recommendation in the [GitLab Helm chart upgrade](https://docs.gitlab.com/charts/installation/upgrade.html): “During a major database upgrade, we ask you to set `gitlab.migrations.enabled` set to `false`. Ensure that you explicitly set it back to `true` for future updates.”

### Post-Deployment Migration

- This is an important and dangerous step because:
  - No going back
  - One of the reasons why zero downtime upgrade is not possible on K8S Setup (aka. Cloud Native Hybrid Installation).
    - From the [requirements of zero downtime upgrade](https://docs.gitlab.com/ee/update/zero_downtime.html): “You have to use post-deployment migrations”.
- [List of post_upgrade migrations](https://gitlab.com/gitlab-org/gitlab/-/tree/master/db/post_migrate?ref_type=heads)
- [Post Deployment Migrations](https://docs.gitlab.com/ee/development/database/post_deployment_migrations.html)
- How GitLab post_upgrade migration is done:
  - Gitlab.com: run separately after the upgrade (check [Overview of the post-deploy migration pipeline](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/post_deploy_migration/readme.md))
  - Omnibus:
    - By default, it runs when an upgrade happens ([Background migrations and upgrades](https://docs.gitlab.com/ee/update/background_migrations.html)).
    - If we want to do zero downtime upgrade, [post_upgrade migration needs to run separately after the upgrade](https://docs.gitlab.com/ee/update/zero_downtime.html#multi-node--ha-deployment) (same as gitlab.com).
    - K8S Setup (aka. Cloud Native Hybrid Installation).
      - This is the setup that Dedicated uses.
      - [Gitlab-migrations chart](https://docs.gitlab.com/charts/charts/gitlab/migrations/) is used.
      - Always runs whenever **helm upgrade** runs, so it runs together with the upgrade, so it does NOT satisfy the requirement of zero downtime upgrade.
