---
tags:
  - auto-deploy
---
# Manually sync a release tag

At the end of the Security release tags should be automatically synced back to canonical. If this doesn't happen for any reason you can manually sync by following the steps below:

1. Fetch changes from security:  `git fetch security`
2. Checkout the missing tag: `git checkout v15.4.1-ee`
3. Temporarily allow yourself to create tags on the gitlab-org/gitlab repository by:
   * Going to [Protected tags] settings
   * Add your username in the `Allow to create` dropdown for the `v*-ee` tags.
4. Push the tag to canonical: `git push origin v15.4.1-ee`
5. Remove yourself from the Protected tags setting.

[Protected tags]: https://gitlab.com/gitlab-org/gitlab/-/settings/repository
