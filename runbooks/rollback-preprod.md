# Downgrade Preprod

## Why?

Preprod is leveraged to test RC's ahead of tagging a release.
If a specific style of code change is deployed, we may have an issue with the deployment to Preprod.
This is good in that we've caught a potential issue that must immediately be addressed by Development.
To properly test that fix though, it would be wise to rollback Preprod to prior that latest deploy as this will be most similar to deployments that mimic self managed user installations.

## Overview

The rollback procedure gathered below is specific to that of preprod.
Do not use this procedure for any other type of installation.
It takes approximately 6 minutes for GCP to perform the Postgres restore operation.

### Warnings :warning:

* This process does intentionally delete data.
* This procedure will only work if this process is performed within 6 days of the failed deploy.
  * Our Cloud hosted Postgres only holds 7 days of backups, and it is not guaranteed the day of backup is viable until an investigation is performed.
* We do not perform standard procedures and backup this instance, so you will have an inconsistent database and remaining project data on this instance after this work is complete.

## Gather Ahead of Time

* Deploy version to Rollback to
* Current deployed version
* Timestamp of the latest Postgres Backup
* Timestamp of current deployed version
* You must have working `ssh` access to nodes in Preprod
* You must have working `kubectl` client access to the Preprod Infrastructure

## Plan the Restore

* The below is a template, fill the variables in as necessary and create a Change Request.
* Determine the chosen restoration of the database based on when the latest deployment completed.
  * Example, if we deployed on 2025-01-07 at 3AM and later deployed at 6AM, then we'd want to leverage a backup from 2025-01-06.

### Storage Layer

- `cd` into your `chef-repo` on your machine
- [ ] Stop Gitlab services on all nodes

```bash
knife ssh 'roles:pre-base-be-gitaly' 'sudo gitlab-ctl stop gitaly'
knife ssh 'roles:pre-base-be-praefect' 'sudo gitlab-ctl stop praefect'
```
- [ ] Remove current GitLab packages on storage nodes

```bash
knife ssh 'roles:pre-base-be-gitaly' 'sudo dpkg -r gitlab-ee'
knife ssh 'roles:pre-base-be-praefect' 'sudo dpkg -r gitlab-ee'
```

### Kubernetes Layer

- Using our [documentation from runbooks](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md#kubernetes-api-access), gain access to the preprod cluster
- [ ] Delete the helm installation

```bash
helm uninstall gitlab -n gitlab
```

### Database Layer

- [ ] Perform PostgreSQL restore to target version state - https://cloud.google.com/sql/docs/postgres/backup-recovery/restoring
    - Restore Backup Date - <restore version>

### Rollback

- [ ] Initiate a deployment using chatops

```bash
/chatops run deploy <version> pre
```

### Verification

- `cd` into your `chef-repo` on your machine
- [ ] Check Storage Backend node status

```bash
knife ssh 'roles:pre-base-be-gitaly' 'sudo gitlab-ctl status'
knife ssh 'roles:pre-base-be-praefect' 'sudo gitlab-ctl status'
```

- [ ] Confirm database migrations are at correct version
      - SSH into the deploy node

```bash
sudo gitlab-rake db:migrate:status
```

- [ ] Monitor QA on Preprod
- [ ] Monitor logs and our observability stack

## Troubleshooting

### Storage
During one test, we had issues where QA was failing because Gitaly claimed that the repository data already existed.
This procedure intentionally deletes repository data.
The projects remain on the database.
In this instance, we need to purge Gitaly data.
Follow these steps:

1. SSH into any of the Gitaly nodes (they all share an NFS mount)
1. Become root - `sudo su -`
1. Delete data - `find /mnt/storage/git-data/repositories/ -mindepth 1 -maxdepth 1 -exec rm -rf {} \;`

### Reference Information

* Prior Run - https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20818
* Documented Rollback Procedure - https://docs.gitlab.com/ee/update/package/downgrade.html
* Purging Gitaly - https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20818
