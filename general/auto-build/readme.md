---
tags:
- auto-deploy
---
# Overview of process for building packages for auto-deploy

The auto-build process relies on the `auto-build: Create new package` scheduled pipeline.

The `auto-deploy: Create new package` runs the `auto_deploy:timer` rake task. This scheduled pipeline
runs every 15 mins and is the central piece for building auto-deploy packages. The code definition of
the rake task can be found on release-tools, particularly on the [`auto-deploy.rake`] file.

It comprises three operations executed on every run:

1. prepare: auto_deploy branch generation
1. pick: processing urgent fixes
1. tag: package tagging

For an overview of how packages are deployed, have a look at
[overview of auto deploy pipelines](../deploy/overview-of-the-auto-deploy-pipelines.md).

## Prepare

If the current hour is part of [the auto_deploy schedule], and an auto_deploy
branch matching the current time does not yet exists, this task creates
auto-deploy branches on [GitLab], [Omnibus], and [CNG] security repositories.
The branches are created on security repositories to account for security releases.

To ensure the commits don't overlap, the branches use the previous auto-deploy tag as
the minimum commit. Several auto-deploy branches are created throughout a day. The
schedule is set at the discretion of release managers at the beginning of their shift.
The current schedule can be seen on the [releases documentation].

As an aside, this task also creates the monthly release issue if one can't be found.

### How are SHAs selected?

- The latest SHA with a successful pipeline is selected from the security mirrors of the [Omnibus](https://gitlab.com/gitlab-org/security/omnibus-gitlab/),
  [CNG](https://gitlab.com/gitlab-org/security/charts/components/images) and [GitLab](https://gitlab.com/gitlab-org/security/gitlab/)
  projects.
- For other projects under [managed versioning](../../components/managed-versioning/index.md), the SHA present in the
  corresponding version file is selected. For example, for Gitaly, the SHA present in the `GITALY_SERVER_VERSION` file
  in `gitlab-org/security/gitlab` is selected.

## Pick

This task searches for GitLab, Omnibus, and CNG merge requests with the
`Pick into auto-deploy` label, if found, it'll validate they have a severity label
added and then it'll pick them into the respective auto-deploy branch.
For more details, [see our Releases handbook page].

## Tag

The final task will tag the most recent, non failed commit on the current
auto-deploy branches to build an auto-deploy package. Once a package is tagged, pipelines
are created in Omnibus and CNG to build packages from the tagged commits.

## Consideration

When the feature flag [`auto_deploy_tag_latest`] is enabled this forces
the auto-deploy branch to be generated using the latest commit on the
default branch, regardless of the CI status.
It also forces tagging from the latest commit on the auto_deploy branch,
regardless of the CI status.

This behavior is discouraged as a less risky option exists to [speed up auto-deploy].
This feature flag is disabled by default.

## Manual operations

There are two additional inactive [pipelines schedules] intended for manual
operations. The name always begins with _MANUAL auto-deploy_ and then list the
operations included in that specific job.

The one including the _prepare_ operation forces the generation of an auto-deploy
branch unless one for the current hour already exists.

Those jobs are available for release managers in case they need to create a package,
force the generation of a new auto_deploy branch and/or operate when the regular timer
is paused.

[the auto_deploy schedule]: ../general/how-to-set-auto-deploy-branch-schedule.md
[`auto-deploy.rake`]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/auto_deploy.rake
[pipelines schedules]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[releases documentation]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-deployments-process
[GitLab]: https://gitlab.com/gitlab-org/security/gitlab
[Omnibus]: https://gitlab.com/gitlab-org/security/omnibus-gitlab
[CNG]: https://gitlab.com/gitlab-org/security/charts/components/images
[`auto_deploy_tag_latest`]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags/191/edit
[see our Releases handbook page]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-pick-label
[speed up auto-deploy]: ../runbooks/how_to_speed_up_auto_deploy_process_for_urgent_merge_requests.md
