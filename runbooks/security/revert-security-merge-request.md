---
tags:
  - auto-deploy
  - release-management
---
# Revert a Security Merge Request

This runbook aims to guide a stage group in reverting a security merge request as a means to mitigate
a bug introduced by a security fix.

**Reverting a security merge request is [not encouraged].
Consult the [documentation on various options to mitigate bugs introduced by security merge request] first before
continuing with the following steps.**

## Steps to revert a security merge request

These steps are generally similar as reverting any other deployed merge request. The key difference is that
**this process requires explicit approval from AppSec.** To facilitate the process, we have created a template on the
[GitLab security repository] that stage teams must use when going through this process.

1. On the original merged security merge request, create the revert merge request by pressing `Revert` button
and following the flow, pointing the new merge request to security `master` branch.

1. Use the [Revert Security Merge Request template] to make sure the checklist items are complete before approval + merge.

1. Figure out the [security release manager] for the released monthly release. (It's usually the row _below_ the
currently highlighted row.)

1. Ping them on the revert merge request and on `#sec-appsec` Slack channel for their approval, highlighting the
implications of the revert merge request being published as part of the upcoming the security release.

1. Once AppSec has approved the revert merge request, assign it to the release managers for merging. This will ensure
that the revert will be published with the upcoming security release.

[not encouraged]: https://handbook.gitlab.com/handbook/engineering/releases/patch-releases/#how-can-i-revert-a-security-merge-request
[documentation on various options to mitigate bugs introduced by security merge request]: ../../general/security/bugs_introduced_by_security_merge_request.md
[GitLab security repository]: https://gitlab.com/gitlab-org/security/gitlab
[Revert Security Merge Request template]: https://gitlab.com/gitlab-org/security/gitlab/-/blob/master/.gitlab/merge_request_templates/Revert%20Security%20Merge%20Request.md?ref_type=heads
[security release manager]: https://about.gitlab.com/community/release-managers/
