---
tags:
  - auto-deploy
---
# Background Migrations

The purpose of this document is to help determine if there are background migrations in a
select deployment and to determine whether or not migrations are actively
running.

For more information on Background Migrations, consult [the development documentation](https://docs.gitlab.com/ee/development/database/batched_background_migrations.html).

## Determine if latest deploy contains Background Migrations

All Background Migrations are located in
[lib/gitlab/background_migration](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib%2Fgitlab%2Fbackground_migration).  When looking at the diff between
two versions of GitLab, this is easiest via the command line using `git diff
--name-only <commit>..<commit> -- lib/gitlab/background_migration`

Example:

```
% git diff --name-only v12.0.0-ee..v12.1.0-ee -- lib/gitlab/background_migration
lib/gitlab/background_migration/add_merge_request_diff_commits_count.rb
lib/gitlab/background_migration/archive_legacy_traces.rb
lib/gitlab/background_migration/calculate_wiki_sizes.rb
lib/gitlab/background_migration/create_fork_network_memberships_range.rb
lib/gitlab/background_migration/delete_conflicting_redirect_routes_range.rb
lib/gitlab/background_migration/fill_valid_time_for_pages_domain_certificate.rb
lib/gitlab/background_migration/fix_cross_project_label_links.rb
lib/gitlab/background_migration/fix_pages_access_level.rb
lib/gitlab/background_migration/fix_user_namespace_names.rb
lib/gitlab/background_migration/fix_user_project_route_names.rb
lib/gitlab/background_migration/migrate_events_to_push_event_payloads.rb
lib/gitlab/background_migration/migrate_null_private_profile_to_false.rb
lib/gitlab/background_migration/migrate_system_uploads_to_new_folder.rb
lib/gitlab/background_migration/move_personal_snippet_files.rb
lib/gitlab/background_migration/normalize_ldap_extern_uids_range.rb
lib/gitlab/background_migration/populate_fork_networks_range.rb
lib/gitlab/background_migration/populate_merge_request_assignees_table.rb
lib/gitlab/background_migration/populate_merge_requests_latest_merge_request_diff_id.rb
lib/gitlab/background_migration/populate_untracked_uploads.rb
lib/gitlab/background_migration/prepare_untracked_uploads.rb
```

## Determine if a Background Migration is actively running

There are multiple ways to discover this information.

### Chatops

To list all background migrations for production:

```
/chatops run batched_background_migrations list
```

This will show a list of background migartions including their current status, overall progress, when they started, and what table they are working from.

An ID is also provided so if you are concerned with a specific background migration, it can be re-queried with:

```
/chatops run batched_background_migrations status <MIGRATION_ID>
```

See the [batched background migration docs](https://docs.gitlab.com/ee/development/database/batched_background_migrations.html#chatops-integration)
for a full list of possible chatops commands including the abilities to **pause** and **resume** batched background migrations.

### Logs

We can view logs from sidekiq.  [Utilize this saved query on
Kibana](https://log.gprd.gitlab.net/goto/0fb8ed50-b47e-11ed-9f43-e3784d7fe3ca)

### Rails Console

:warning:

This is not desired as not all members that manage the responsibility of said
information can actually get this data.  Or in the case of an incident, the
ability to get this information may be hindered.

:warning:

1. SSH into the console node of the target environment, example:

```
ssh -l <username>-rails console-01-sv-gstg.c.gitlab-staging-1.internal
```

1. Execute the `GitLab::Database::BackgroundMigration::BatchedMigration.with_status(:active)` method:

```
[ gstg ] production> GitLab::Database::BackgroundMigration::BatchedMigration.with_status(:active)
=> 0
```

### Admin Panel of GitLab

:warning:

This is not desired as not all members that manage the responsibility of said
information can actually get this data.  Or in the case of an incident, the
ability to get to this page may be hindered.

:warning:

1. Log into GitLab using administrative credentials
1. Navigate to the Admin Area
    * Monitoring
    * Background Migrations

Active migrations are listed with the ability to pause them.

This is described in the [background migrations upgrade docs](https://docs.gitlab.com/ee/update/background_migrations.html#check-the-status-of-batched-background-migrations).

## Background Migration is taking a very long time

Consult [What do I do if my background migrations are stuck](https://docs.gitlab.com/ee/update/background_migrations.html#background-migrations-stuck-in-pending-state) section in GitLab's Upgrade Documentation.