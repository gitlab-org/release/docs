---
tags:
  - release-management
---
# How to handle upstream security patches

## Third parties

Sometimes the root cause of a vulnerability is in a third-party dependency and even the latest version of that dependency is vulnerable.
This page describes how we process in those situations.

The first step is to disclose the vulnerability to the third party. See in the project's
`README.md`, `SECURITY.md` or on their website if they have official means of disclosing
vulnerabilities. If they don't, simply email the maintainer(s) of the project. This email should
cc the Engineering Manager, the person from Application Security who was involved with the
triage of the security issue, and any other relevant team members. For transparency, we
should copy this communication in the security issue.

The rest of the process depends on the severity and if the third-party is able to provide us with
a patch before the [expected time to remediation (TTR)](https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues)
for the issue or not.

### A patch is not available before TTR or the vulnerability is ~"severity::1"

1. Monkey patch the vulnerability on GitLab in a security release following the security release process with backports as we would for any other fix
    - For ~"severity::1" issues it should be a critical security release
    - If a monkeypatch is not practical, third-party dependency can be forked under `gitlab-org` e.g. For gems https://gitlab.com/gitlab-org/ruby/gems
1. Create a follow-up issue with a due date based on the expected release date of the official fixed version of the third-party dependency to use it instead of the GitLab monkey patch/forked version.
    - This issue should remain confidential until both the security release and the fixed version of the third-party dependency are out
1. When the new version of the third-party dependency is available, update it in the canonical repository and remove the monkey patch

### A patch is available before TTR and the vulnerability is not ~"severity::1"

1. Update the dependency in the canonical repository
    - The new release will potentially disclose the vulnerability to those paying close attention,
    we want to protect GitLab.com as soon as possible
1. Backport the update in the security release

### When to work with GitLab's Security Risk Team

The process above does not apply to issues in third party services or vendors that GitLab interacts with - it is only for code dependencies. Disclosing security issues to third parties should be done with or through GitLab's Security Risk team. See [how to communicate risks to the Security Risk Team](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-risk/storm-program/#communication-of-risks-to-the-security-risk-team).

## GitLab is the upstream

In some cases the dependency with the vulnerability will be a gem maintained by GitLab.

### The vulnerability is ~"severity::2" or ~"severity::1"

1. Monkey patch the vulnerability on GitLab in a security release following the [security release process](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/developer.md#process) with backports as we would for any other fix
    - For ~"severity::1" issues it should be a critical security release
1. After the release, fix the vulnerability in the upstream project
1. Update the dependency in the public GitLab project

### The vulnerability is ~"severity::4" or ~"severity::3"

1. Fix the vulnerability public in the upstream project
1. Update the dependency in the public GitLab project
1. Backport the update in the security release