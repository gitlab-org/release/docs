# mkdocs doesn't like all files in the parent directory, so we'll have to copy the content down
mkdir docs
find ./*  \! -name 'docs' -type d -maxdepth 0 -exec mv {} ./docs \;
mv README.md docs/README.md
