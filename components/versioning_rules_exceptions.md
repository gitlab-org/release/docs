---
tags:
  - release-management
---
## Versioning rules exceptions

Components under Managed Versioning should follow the expected
[versioning rules](./managed-versioning/index.md#versioning-rules),
however we have some exceptions:

**Branches**

| Component | Format | Example |
| ------  | ------- | ------- |
| GitLab  | `<major>-<minor>-stable-ee` | `15-4-stable-ee` |

**Tags**

| Component | Format | Example |
| ------  | ------- | ------- |
| GitLab         | `v<major>.<minor>.<patch>-ee` | `v15.5.4-ee` |
| Omnibus GitLab | `<major>.<minor>.<patch>+ee.0` / `<major>.<minor>.<patch>+ce.0`  | `15.5.4+ee.0` / `15.5.4+ce.0`  |
| CNG            | `v<major>.<minor>.<patch>` / `v<major>.<minor>.<patch>-ee` / `v<major>.<minor>.<patch>-ubi8` / `v<major>.<minor>.<patch>-fips` | `v15.5.4` / `v15.5.4-ee` / `v15.5.4-ubi8` / `v15.5.4-fips` |
