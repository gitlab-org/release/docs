---
tags:
  - auto-deploy
---
# Terminology

1. **Security vulnerability issue** - A confidential issue in a GitLab Canonical project (`gitlab-org/some-project`), detailing a security vulnerability and steps to reproduce.

   Usually created via HackerOne.

   Example: [gitlab-org/gitlab#193100](https://gitlab.com/gitlab-org/gitlab/issues/193100).

1. **Security tracking issue** - A confidential issue in `gitlab-org/gitlab` which is the high-level overview of the security fixes in a patch release.

   Created via [issue template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20Release.md).

   Example: [gitlab-org/gitlab#209019](https://gitlab.com/gitlab-org/gitlab/-/issues/209019).

1. **Security implementation issue** - An issue in a GitLab Security project (`gitlab-org/security/some-project`), outlining the versions affected, detailing the steps a developer needs to take to remediate the vulnerability, and associating backports.

   Created via [issue template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md).

   Example: [gitlab-org/security/gitlab#80](https://gitlab.com/gitlab-org/security/gitlab/-/issues/80).

1. **Security merge request (MR)** - A merge request in a GitLab Security project (`gitlab-org/security/some-project`), resolving a vulnerability in a specific branch.

   Created via [merge request template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Security%20Release.md).

   Example: [gitlab-org/security/gitlab!314](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/314).

1. **Release task issue** - Used by [release managers] as a checklist of steps to complete the release.

   Created via ChatOps (`/chatops run release prepare --security`).

   Example: [gitlab-org/release/tasks#1272](https://gitlab.com/gitlab-org/release/tasks/-/issues/1272).

## Issue association

```mermaid
graph RL
  classDef canonical fill:#74bd3d
  classDef security fill:#ff7272

  secVuln(Security vulnerability Issue)
  secRelease(Patch release Issue)
  secFix(Security implementation issue)
  task{{Release task issue}}

  class secVuln canonical
  class secRelease canonical
  class secFix security

  secFix -->|references| secVuln
  secFix ---|related| secRelease

  task -.->|references| secRelease
```

## Merge request association

```mermaid
graph RL
  classDef canonical fill:#74bd3d
  classDef security fill:#ff7272

  secFix(Security implementation issue)
  secMR1(["Security MR 1 (default-branch)"])
  secMR2(["Security MR 2 (backport)"])
  secMR3(["Security MR 3 (backport)"])
  secMR4(["Security MR 4 (backport)"])

  class secFix security
  class secMR1,secMR2,secMR3,secMR4 security

  secMR1 & secMR2 & secMR3 & secMR4 -.->|references| secFix
```
## Merging

```mermaid
graph TD
  classDef security fill:#ff7272

  coMerge{{"/chatops run release merge --security"}}
  coMergeDefaultBranch{{"/chatops run release merge --security --default-branch"}}

  secMR1(["Security MR 1 (default-branch)"])
  secMR2(["Security MR 2 (backport)"])
  secMR3(["Security MR 3 (backport)"])
  secMR4(["Security MR 4 (backport)"])

  autodeploy(auto-deploy)
  defaultbranch(default-branch)

  stable129(12-9-stable-ee)
  stable128(12-8-stable-ee)
  stable127(12-7-stable-ee)

  coMerge --> secMR2
  coMerge --> secMR3
  coMerge --> secMR4

  secMR2 --> |merge| stable129
  secMR3 --> |merge| stable128
  secMR4 --> |merge| stable127

  coMergeDefaultBranch --> secMR1
  secMR1 --> |merge| defaultbranch
  defaultbranch -.-> |cherry-pick| autodeploy

  class secMR1,secMR2,secMR3,secMR4 security
  class stable129,stable128,stable127 security
  class defaultbranch,autodeploy security
```
