---
tags:
  - auto-deploy
---
# How to handle Git security patches?

This runbook describes how to:
- Deploy a Git vulnerability fix that is under embargo (cannot be made public) to GitLab.com.
- Include the fix in a self-managed release once the embargo is lifted.

The embargo dates are usually decided by the upstream Git team. A Gitaly team member usually interacts with the Git team
and keeps us informed.

Since the Git source has to be included with a self-managed release (due to the license), we cannot release a self-managed
package with the fix until the embargo is lifted.

## Deploy the Git fix to GitLab.com

Before the embargo is lifted, we usually need to deploy the fix to GitLab.com. Since the embargo has not been lifted yet,
the deploy needs to happen without making the fix public.

1. Gitaly creates MRs in the [Git Security Mirror]. Usually, one MR for
the `main` branch, and one MR per Git version (ex: `gitlab-git-v2.38` branch) that needs to be patched for GitLab.com.

2. Once the branch MR (ex: `gitlab-git-v2.38` branch) has been merged, the Gitaly team will create a tag (ex: `v2.38.gl3`).

3. On [Omnibus dev CI/CD variables], set the `GITALY_GIT_VERSION_x_xx` environment variable to the Git tag (e.g
`v2.38.3.gl3`) that needs to be deployed. If the version being deployed is `v2.38.x`,
the variable should be named `GITALY_GIT_VERSION_2_38`.

   Gitaly uses a variable to override the minor version that is bundled with Omnibus. For example, if v2.38 is the
version that is currently deployed to GitLab.com, it is necessary to override the v2.38 minor version that is deployed,
to deploy the patched version to GitLab.com. To do this, Omnibus needs to set the `GIT_VERSION_2_38` environment variable
to v2.38.3.gl3 when `make`ing Gitaly. Omnibus sets the `GIT_VERSION_2_38` variable to the value of `GITALY_GIT_VERSION_2_38`.
This only overrides the Git version for the auto-deploy package, not for any self-managed packages.

4. Set the `GITALY_GIT_REPO_URL` variable in [Omnibus dev CI/CD variables].
You can copy the value of [GIT_REPO_URL](#variables) in <https://gitlab.com/gitlab-org/security/gitlab/-/settings/ci_cd>.

   The value should be `https://username:token@gitlab.com/gitlab-org/security/git.git`. `username` can be any string.
`token` should be a project token from [Git Security Mirror] with the `read_repository` permission. If you do create a
new token, make sure to follow the [token management process](../../../runbooks/token-management.md) to request and
document a new token ([example](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2685)).

5. Increment the value of `AUTO_DEPLOY_CACHE_REVISION` in [Omnibus dev CI/CD variables]
in order to bust the auto deploy package cache.

   For example, if the value is `1.0`, you can set it to `2.0`.

6. Start a deployment pipeline by executing `auto_deploy:prepare` and then `auto_deploy:tag` in
<https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules>. As long as the above variables are present in
<https://dev.gitlab.org/gitlab/omnibus-gitlab/-/settings/ci_cd>, every auto deploy package will contain the given Git
version. Since we updated the cache revision number, the Omnibus runner cache will start fresh and the first Omnibus
packager pipeline will take longer than usual, by about 60 minutes.

7. Once deployment is complete, ask the SRE-on-call to verify that the deployed git versions are correct. On the Gitaly
VM, execute the following after replacing x.xx with the Git version:
   ```shell
   root@c69ddea90c42:/# /opt/gitlab/embedded/bin/gitaly-git-vx.xx --version
   git version x.xx.x.glx
   ```

### Variables

The following variables are important for overriding where the build will look
for repositories.

| Variable Name             | Value | Purpose         |
| ------------              | ---   | ------------    |
| `GIT_REPO_URL` | `String` | This variable can be set to override the Git repository used when building Gitaly. |
| `GITALY_REPO_URL` | `String` | This variable can be set to override the Gitaly repository used when building Gitaly. |

## Include the fix in a self-managed release

### Git team provides sources to be released

As the release date approaches, the Git team will provide us with the source for the new versions that are planned to
be released.

The Gitaly team will open MRs to apply the patches to the versions of Git that are in use. The current version plus the
last 3 versions used.

The MRs in the Git Security Mirror project will be merged by Gitaly once they are ready. New tags will be created
by the Gitaly team once merged.

The Gitaly team will also open MRs (in Gitaly Security Mirror) to change the default Git version that Gitaly uses to
be the new patched versions. As with any other security fix, there should be 4 MRs, one for master, and 3 for the last
three minor versions of GitLab.

Once the Gitaly master MR is merged, we can deploy the new version to gitlab.com. The deployment should be done a day or
two before the embargo is planned to be lifted, so that we don't leak the presence of a new Git version
(Git clients can see the server version) too early.

To deploy the new Git version, `GITALY_SERVER_VERSION` needs to be updated in the Gitaly Security Mirror. Take a look at
the ["How to handle Gitaly security merge requests"](../how_to_handle_gitaly_kas_security_merge_requests.md)
for the specifics.

### Critical security release

If the Git vulnerability being patched is an S1, [open a critical security release issue](../release-manager.md#Critical-Security-Releases)
about 2 weeks before the embargo is to be lifted.

For a critical Git patch, the Dedicated team needs to update to the patched GitLab version before the embargo
is lifted. Dedicated can pull GitLab packages from the [private pre-release package repository](https://packages.gitlab.com/app/gitlab/pre-release).
Packages are pushed to the pre-release repository when a tag is created. So we can tag the new GitLab versions a few
days before the embargo is lifted to allow Dedicated to install the patched versions before the vulnerability is publicized.
Adjust the dates in the critical security release to account for this.

Note that publishing of packages should only be done on the day embargo is lifted, after checking with the AppSec team.

Add the following steps to the critical security release. If the heading already exists in the critical security release
issue, add these steps as the first steps under the heading:

```markdown
## Git preparation

- [ ] Make sure backports are merged into gitlab-org/security/git.

- [ ] Make sure Git tags are created in gitlab-org/security/git.

- If you need to deploy the Git fix many days before the embargo is lifted, you can do so without needing
  to merge the Gitaly master MR.
  You can deploy the Git fix without merging the Gitaly master MR (but after the new Git tags have been created)
  by following this document:
  [runbooks/git-security-patches.md#deploy-the-git-fix-to-gitlabcom](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/security/git-security-patches.md#deploy-the-git-fix-to-gitlabcom)

## Two days before due date

- [ ] Ensure the `GITALY_REPO_URL` and `GIT_REPO_URL` environments variables are set on the project CI/CD variables of
  [GitLab security repository](https://gitlab.com/gitlab-org/security/gitlab/-/settings/ci_cd).

- [ ] Disable the [Gitaly task updater](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/), it is
  listed as `components:update_managed_versioning_projects`.

- [ ] Merge the MR targeting master on gitlab-org/security/gitaly.

- [ ] Create an MR on the [GitLab Security repo](https://gitlab.com/gitlab-org/security/gitlab/) targeting `master`.
  It should bump the [`GITALY_SERVER_VERSION`](https://gitlab.com/gitlab-org/security/gitlab/-/blob/master/GITALY_SERVER_VERSION)
  to the merge commit SHA of the Gitaly security MR targeting `master`.

- [ ] Merge and deploy the GitLab security MR to GitLab.com.

- [ ] Make sure backports are merged into gitlab-org/security/gitaly.

- [ ] Remove any `GITALY_GIT_VERSION_x_xx` variable from
  [Omnibus dev CI/CD variables](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/settings/ci_cd), if any exists.

- [ ] Increment the value of the `AUTO_DEPLOY_CACHE_REVISION` variable in
  [Omnibus dev CI/CD variables](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/settings/ci_cd). This ensures that
  the Omnibus packager pipeline starts with a clean cache.

The next auto deploy pipeline should deploy the updated Git version.

- [ ] Ask SRE on call to check the version of Git on the Gitaly VM (replace `vx.xx` with the Git version):
      ```shell
      root@c69ddea90c42:/# /opt/gitlab/embedded/bin/gitaly-git-vx.xx --version
      ```

## One day before due date

- [ ] Before tagging the security release, add the following variable to
<https://dev.gitlab.org/gitlab/omnibus-gitlab/-/settings/ci_cd>:

  - `SELF_MANAGED_VERSION_REGEX_OVERRIDE_GIT_REPO_URL`: The value should be a regex of self-managed versions that
     should include the Git fixes. For example if the security release is for 15.7.2, 15.6.4, 15.5.7, the value of the
     variable should be: `^15\.(5|6|7)\.(2|4|7)`.

### Final steps

- [ ] Sync the git tags back to canonical
   - [ ] Create a new pipeline on ops release tools: https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/new and
     include the following variables:
      - [ ] Set the `SYNC_GIT_TAGS` variable to `true`.
      - [ ] Set the `GIT_VERSIONS` variable to the tags to be synced, separated by space. For example `v2.39.3.gl1 v2.40.1.gl2`.
```

[Git Security Mirror]: https://gitlab.com/gitlab-org/security/git
[Gitaly Security Mirror]: https://gitlab.com/gitlab-org/security/gitaly
[Omnibus dev CI/CD variables]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/settings/ci_cd
