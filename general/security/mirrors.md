---
tags:
  - auto-deploy
---
# Repository Mirrors

GitLab's "[public by default]" value means we develop in the open. However, security fixes are an exception since public development would expose vulnerabilities before fixes are available.
To handle security issues privately, public GitLab projects have security mirrors in the [`gitlab-org/security`] group.

This document outlines how to create and manage these security mirrors using Infrastructure as Code (IaC).

[public by default]: https://about.gitlab.com/handbook/values/#public-by-default
[`gitlab-org/security`]: https://gitlab.com/gitlab-org/security/

## Prerequisites

Before creating a security mirror, ensure you have:

1. Owner access to the Security group (limited to Delivery team members)
2. Access to the [infrastructure management](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt) repository
3. If the project already exists, we'll need the ID's associated with them for importing.

If you're outside the Delivery team and need a Security mirror, [file an issue] on the Delivery tracker.

[file an issue]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/new?issuable_template=mirroring_request

## Project Structure

A Project mirror setup consists of three components:

1. **Canonical Project**: The public source repository
2. **Security Project**: Private mirror for security work
3. **Build Project**: Private mirror on dev.gitlab.org for release processes

The mirroring flow is:
```mermaid
graph LR
    A[Canonical] --> B[Security]
    B --> C[Build]
```

## Creating Security Mirrors Using Terraform

### 1. Create the Canonical and Security Project Definition File

Create a new Terraform file (e.g., `infra-mgmt/environments/gitlab-com/project_your-project-name.tf`) with the following structure:

```hcl
locals {
  your-project-branch_protections = {
    main = {
      merge_access_level = "maintainer"
      push_access_level  = "no one"
    }
  }
}

// Canonical Project
module "project_your-project-name" { // adjust accordingly
  source  = "ops.gitlab.net/gitlab-com/project/gitlab"
  version = "5.14.0"

  name = "Your Project Name" // adjust accordingly
  path = "your-project-name" // adjust accordingly

  group_id = data.gitlab_group.gitlab-org-your-group.group_id // adjust accordingly

  default_branch = "main" // adjust accordingly

  // Standard project configurations
  issues = {
    access_level = "enabled"
  }
  lfs_enabled      = true
  packages_enabled = true

  // Merge request settings
  merge_requests = {
    approvals_before_merge  = 1
    merge_method           = "merge"
    merge_pipelines_enabled = false
    commit_template        = file("../../data/templates/merge_commit/production.md")
  }

  topics = [
    "Canonical",
    "infra-mgmt Managed"
  ]
}

// Security Project
module "project_security-your-project-name" { // adjust accordingly
  source  = "ops.gitlab.net/gitlab-com/project/gitlab"
  version = "5.14.0"

  name        = "Your Project Name" // adjust accordingly
  path        = "your-project-name" // adjust accordingly
  description = "🔒 Security mirror of gitlab-org/your-group/your-project-name" // adjust accordingly

  group_id = data.gitlab_group.gitlab-org-security-your-group.group_id // adjust accordingly

  default_branch = "main" // adjust accordingly

  // Mirror specific configurations
  visibility_level = "private"
  lfs_enabled      = true
  packages_enabled = true

  merge_requests = {
    approvals_before_merge  = 1
    merge_method           = "merge"
    merge_pipelines_enabled = false
    commit_template        = file("../../data/templates/merge_commit/production.md")
  }

  topics = [
    "security",
    "infra-mgmt Managed"
  ]
}

// Configure Mirroring: Canonical -> Security
module "project_security-your-project-name-mirror" { // adjust accordingly
  source  = "ops.gitlab.net/gitlab-com/project/gitlab//modules/push-mirror"
  version = "5.14.0"

  project_id                = module.project_your-project-name.id // adjust accordingly
  mirror_project            = { id = module.project_security-your-project-name.id } // adjust accordingly
  mirror_branch_protections = local.your-project-branch_protections // adjust accordingly
  only_protected_branches   = true
  keep_divergent_refs       = true

  providers = {
    gitlab.mirror = gitlab
  }
}

// Configure Mirroring: Security -> Build
module "project_build-your-project-name-mirror" { // adjust accordingly
  source  = "ops.gitlab.net/gitlab-com/project/gitlab//modules/push-mirror"
  version = "5.14.0"

  project_id                = module.project_security-your-project-name.id // adjust accordingly
  mirror_project            = { path = "gitlab/your-group/your-project-name" } // adjust accordingly
  mirror_branch_protections = { main = { push_access_level = "maintainer" } }
  only_protected_branches   = true
  keep_divergent_refs       = true
  ee                        = false // We perform release operations on the Dev instance which is a CE instance type

  providers = {
    gitlab.mirror = gitlab.dev-gitlab-org
  }
}
```

And then finally the Build Project configuration file (e.g. `infra-mgmt/environments/dev-gitlab-org/project_your-project-name.tf`)

```hcl
// Build Project
module "project_build-your-project-name" {
  source  = "ops.gitlab.net/gitlab-com/project/gitlab"
  version = "5.14.0"

  name        = "Your Project Name" // adjust accordingly
  path        = "your-project-name" // adjust accordingly
  description = "Build mirror of gitlab-org/your-group/your-project-name" // adjust accordingly
  ee          = false

  group_id = data.gitlab_group.gitlab-your-group.group_id // adjust accordingly

  default_branch = "main" // adjust accordingly

  lfs_enabled      = true
  packages_enabled = true

  pipelines = {
    access_level                  = "enabled"
    auto_cancel_pending_pipelines = "enabled"
  }

  topics = [
    "build",
    "infra-mgmt Managed"
  ]

  visibility_level = "private"
}
```

Create a Merge Request and carefully review the Terraform Plan outputs and adjust as necessary.

If the project exists in any of these places already, follow standard practice with Terraform to import the resources.

## Security Mirror Requirements

The Terraform configuration ensures that security mirrors:

- Are created as new projects (or imported) with a push mirror from their Canonical projects
- Maintain the same subgroup structure within the `security` subgroup
- Include visual indicators (🔒) in the project name and description
- Have appropriate merge request approval requirements
- Disable unnecessary project features
- Configure proper mirroring between Canonical, Security, and Build projects

## Release Tools Integration

For projects managed by [release-tools](https://gitlab.com/gitlab-org/release-tools):

- The Terraform configuration automatically handles permissions
- `@gitlab-release-tools-bot` gets Maintainer access through group inheritance
- Projects are shared with the Delivery team GitLab group on both GitLab.com and dev.gitlab.org

## Troubleshooting

If you encounter issues:

1. Verify all prerequisites are met
2. Check Terraform plan output for errors
3. Ensure group IDs are correct in your configuration
4. Verify credentials and access rights

For additional help file an issue in the Delivery tracker.