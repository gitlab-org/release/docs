---
tags:
  - auto-deploy
---
## Big Picture

This drawing is aiming to provide an overview of all the main components
involved in auto_deploy and release management to make it easier to understand
the dependencies and workflows.

![Big Picture](./images/big_picture.svg)

[Source](https://docs.google.com/drawings/d/1afVuws4aZfSdfLZ3sE0AbictI8Vm4QRJOc8-NJ9QP9c/edit?usp=sharing)
