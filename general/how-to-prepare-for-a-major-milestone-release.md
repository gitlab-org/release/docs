---
tags:
  - auto-deploy
---
## Preparing for the Major Milestone release

Major milestone releases contain breaking changes and deprecations and as a result require us to
1. Prepare our self-managed instances in preparation (Ops, Pre, Release, Dev, Dedicated). This involves analyzing the upcoming breaking changes and upgrading our instances as necessary. See https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2916 for an example. A good example for the impact could be the Postgres Version supported by the major release. For instance, in GitLab 18.0, the [minimum supported version will be Postgres 16](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/8504).
2. Expect additional questions and requests from stage groups who are trying to guarantee changes land in the correct milestone

This guide walks through what we should expect and tasks we should do ahead of the major milestone release.

### Delivery group responsibilities

Delivery is on the frontline for breaking changes. We're also the domain experts for deployments and releases. As a result, we should prepare to communicate and support breaking changes coming into the release and we should be coordinating the work needed across Infrastructure to prepare environments for breaking changes.

### Major milestone overview

Ahead of the major milestone:
1. In the milestones leading up to the Major milestone, Stage groups will be communicating upcoming breaking changes to users. [Users are normally given three months notice about the upcoming change](https://about.gitlab.com/handbook/product/gitlab-the-product/#process-for-deprecating-and-removing-a-feature)
2. Feature Flags will be enabled during particular breaking change windows (e.g.: https://gitlab.com/gitlab-com/Product/-/issues/13238). These windows are communicated to the customers via the [Release Posts](https://about.gitlab.com/releases/2024/04/18/gitlab-16-11-released/#breaking-change-windows-before-the-major-release), Blog articles and CSM.
3. Release Managers should expect additional monthly release preparation complexity and plan the monthly release timeline accordingly. To allow the timeline to be communicated well in advance, Release Managers should review dates and plan the release preparation timeline well ahead of the start of the milestone.
4. Release Managers should communicate the planned timeline via issues ([example](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2924)), Slack, and Engineering-Week-in-Review
5. Product Managers will create and manage the overall list of changes expected in the release. [Example](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/924#what-needs-to-be-done)
6. Delivery group should create and organize the Major milestone coordination epic to keep track of all tasks. See https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/924 as an example.

During the major milestone:
1. Stage groups will begin merging changes. Where possible they'll use feature flags to control the rollout. Feature flags will need to be enabled and then be updated to default to enabled to complete the deprecation
2. Delivery, Production Engineering, Dedicated teams will plan and complete any preperation work needed on our tools or instances. Delivery will coordinate this work via the coordination epic ([example](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/924)).
3. Release Managers will regularly communicate the planned timeline via [issue](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2924), Slack, and Engineering-Week-in-Review

### What you can expect

Breaking changes can affect deployments and releases at many stages of the milestone. Examples include:
1. Breaking changes included in a GitLab.com deployment might fail tests on any of our pre-production environments
2. Breaking changes could cause problems in Production
3. The release RC might not install correctly on the Pre or Release environment, or tetss might fail because we haven't prepared the instance for the breaking changes
4. The released package might not install correctly on the Ops instance if we haven't prepared the instance for breaking changes

To try and avoid these problems we should work closely with Product Managers to review upcoming breaking changes and prepare our tools and instances ahead of time.

To avoid risking the Major milestone release day we should plan additional time into the Major milestone release preparation timeline to allows up time to debug and fix any problems we might find on the Pre or Release environments.


### References

Example of the work done for the major release is on those epics:

- GitLab 17.0 - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1035