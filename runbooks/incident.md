---
tags:
  - auto-deploy
---
# Incident

## Overview

This document serves as an action reference manual for Release Managers during
incidents affecting availability of GitLab.com.

See the [availability] Handbook page for definitions of severities.

## Delivery Involvement

In high severity incidents, S2 and above, Release Managers should be involved if
our assistance is required for remediation.  Release Managers, nor the entirety
of the Delivery team have an on-call rotation as our services are normally
halted on non workdays.  Thus if an incident is spun up, consider reaching out
to an appropriate Delivery team member using their contact information in Slack.

If we are unable to be reached, continue reading through this document as this
is the same runbook that we would leverage for incidents.

## Decision Guidance

### Decision Tree

This decision tree is built based on the characteristics of remediation options,
that will be described in the next sections. It aims to help release managers to
decide quickly and precisely in the chaotic time of incidents.

```mermaid
graph TD
    A([Incident detected]) --> B{Can Feature Flag<br/>toggle fix?}
    B -->|Yes| C([Feature Flag toggle])
    C -->|Continue to Remediation| K{Can MR revert<br/>mitigate?}
    B -->|No| D{Is it<br/>S1 or S2?}
    D -->|Yes| J{Is rollback<br/>available?}
    D -->|No| K
    J -->|Yes| H([Deployment rollback])
    J -->|No| E{Is it S1}
    E -->|Yes| F{Fix in Rails<br/>source code?}
    E -->|No| K
    F -->|Yes| G([Hot Patching])
    F -->|No| O[Work with EOC and IMOC to prepare a mitigation plan]
    K -->|Yes| L([Revert MR])
    K -->|No| M([Work on fixing MR])
    L --> N[Work with<br/>development teams]
    M --> N

    style C fill:#90EE90
    style G fill:#90EE90
    style H fill:#90EE90
    style L fill:#90EE90
    style M fill:#90EE90
```

### Acceptable Remediation Options per Incident Level

| Available Remediation Option | Desired Incident Level Usage      |
| ---------------------------- | ----------------------------      |
| Feature Flag changes         | All Severities                    |
| Merge fixing MR              | All Severities                    |
| Rollback                     | Evaluated for S2, Accepted for S1 |
| [Rollback Individual Service]| S1 and S2                         |
| [Post-Deployment Patch]      | S1 **only**                       |

### Remediation Timings

* *Feature Flag Toggle* takes less than 5 minutes to complete once the
  appropriate flag has been identified.
* *[Deployment rollback]* and [Rollback Individual Service] takes around 30
  minutes.  If we need to perform the full rollback which includes Gitaly, add
  an additional hour to the estimated time.  As noted in the [Deployment
  rollback] runbook, we do not automatically rollback Gitaly.
* *Hot Patching* takes about 45 minutes to rollout once the appropriate patch
  has been identified and MR created.
* *Reverting MR* or *Work on fixing MR* and then deploying via the regular
  auto-deploy schedule has the least impact on other changes though remediation
  will take the longest time toachieve. After merging a fix will be available to
  deploy in ~5 hours.
  * Available in this context refers to waiting for CI on the MR (~1.5 hours),
    CI on the default branch (~1.5 hours), and a build of the package (~1 hour).
  * Deployments will take around ~1.5 hours in lower environments, plus an
    additional ~1.5 hours to get to production.

### Remediation options

Consider the following options in combination with a [preferred
timelines](#timeline):

1. **The source** of the problem **is unknown**
    * *[Deployment rollback]*: Consult the runbook for Rollbacks as there are
      limitations that may prevent this from being a viable option.
1. **The source** of the problem **is known**
    * *Feature Flag toggle*: Consult [feature flag
      log](https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=host%3A%3Agitlab.com)
      and consider disabling any recently enabled features.
    * *[Deployment rollback]*: Consult the runbook for Rollbacks as there are
      limitations that may prevent this from being a viable option.
    * *Revert MR* and *Work on fixing MR*: revert the change that caused the
      issue or make a change forward to fix the issue.
      * Work with development teams owning the change to create, approve and
        merge the fixing MR.
      * [Leverage our runbook on speeding up the auto_deploy process for urgent
        MRs](./how_to_speed_up_auto_deploy_process_for_urgent_merge_requests.md).
      * [Force triggering a new auto-deploy deployment pipeline for the
        package]((../general/deploy/overview-of-the-auto-deploy-pipelines.md#how-do-i-start-a-deployment-pipeline-for-a-particular-package))
      * This process shall be leveraged for incidents S2 and below.
    * *Hot Patching*: Issue a [Post-Deployment Patch] (also commonly known as
      Hot Patch) to patch the production fleet.
      * Post-deployment patches can only be applied when the fix is in the
        [Rails source code](https://gitlab.com/gitlab-org/gitlab).
      * This should be leveraged for only S1 incidents.

### Timeline

It is important to note that in situations such as this one, focus should be **exclusively** on reverting to the previously known working state.

Ensure that you challenge any decision that would consider creating the fix for the problem (if the fix ends up being broken, time was lost and now two problems exist). There are edge cases where it is okay to consider this, but those should be very rare.

In the first hour of the incident, it is common to consider the following options:

1. Disabling feature flags
1. Deployment rollback
1. Environment hot patch

Past the first hour, the options to consider are:

1. Reverting the offending code
1. Creating a new deployment

## Example S1 incident

As an example of S1 outage, we'll reference a scenario where post application deployment to GitLab.com, one of the more important workloads such as CI pipelines is no longer working for everyone on the platform.

At the start of the incident we would have:

1. Initiated incident in #incident-management Slack channel.
1. SRE on call, communications manager, current release manager, developer on call, and GitLab.com support member in the Incident Zoom room.

If any of the above are missing, ensure that you speak up and invite the missing people or create the missing resources.

As a release manager, your tasks would consist of providing sufficient background on tooling and changes that were introduced into the environment.

This means:

1. Provide the commit diff that was deployed to production to find the latest branch running in production and link that to the developer on call for further investigation. There are many places to find this information:
    * It is posted to all incident issues
    * In the `#announcements` Slack channel as a thread for each deployment notification.
    * Via `/chatops run auto_deploy status`
    * Check the [grafana dashboard](https://dashboards.gitlab.net/d/CRNfDC7mk/gitlab-omnibus-versions?orgId=1&refresh=5m) to find the latest deployed commit.
1. Advise on the possible [remediation options](#remediation-options).
1. Define [a timeline](#timeline) for specific remediation options chosen in the incident discussion.
    * Eg. Think about what can be done immediately, what can be left for a couple of hours later, and what should be excluded from the conversation.

[availability]: https://about.gitlab.com/handbook/engineering/performance/#availability
[Deployment rollback]: ./rollback-a-deployment.md
[Rollback Individual Service]: ./rollback-single-service.md
[Post-Deployment Patch]: ../general/deploy/post-deployment-patches.md
