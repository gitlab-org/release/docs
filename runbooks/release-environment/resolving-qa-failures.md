---
tags:
  - auto-deploy
---
# Resolving QA failures

## Overview

QA smoke specs are always run against a release environment after a deployment to the same environment.

This document describes the process to follow if QA tests are failing on the deployment.

## Process steps to follow

1. Check if QA and the environment run the same version:
   1. Check QA's version:
      - Go to the QA job `release-environments-qa`
      - Search for the log line that says `echo "Running - '$QA_COMMAND'"`. It prints out the full image name with tag, e.g. `registry.gitlab.com/gitlab-org/gitlab/gitlab-ee-qa:06b4ec8a68374d7b87501540a1c77d01023bf4e1`
   2. Check environment's version:
       - [Connect to the K8S cluster](./connect-to-instance.md)
       - Check the image tag of any GitLab pod, e.g.

          ```bash
          $ kubectl get pod  gitlab-16-9-stable-webservice-default-8469647cc8-5mlg7 -o=jsonpath='{@.spec.containers[0].image}'

          registry.gitlab.com/gitlab-org/build/cng-mirror/gitlab-webservice-ee:16-9-stable-06b4ec8a
          ```

1. QA jobs are sometimes flaky, so you can retry it.
1. If retrying doesn't help, you should [create an issue to investigate it](https://gitlab.com/gitlab-org/release/tasks/-/issues/new).
1. Follow the step 3 and 4 [here](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/resolving-qa-failures.md#process-steps-to-follow) to work with the Test Platform DRI to detect and fix the issue.
1. Once the issue if fixed, we need to follow the [backport runbook](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/engineers.md) to create another backport and redo the validation on Release Environments.
1. Once the QA failure is fixed, close the issue and apply the `RootCause::*` label to track the root cause of the failure.
