---
tags:
  - auto-deploy
---
# Rolling Back Individual Services During an Incident

## Overview

This runbook describes the process for manually rolling back individual Kubernetes services during an incident.

## Prerequisites

- This incident mitigation option is reserved for `Severity::1` and `Severity::2` incidents
- Access to Kubernetes clusters
  - Due to access permission, at the moment, it is only possible for SRE
- List of clusters running the service.
  - Go to [`bases` directory in k8s-workload/gitlab-com repository](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/tree/master/bases?ref_type=heads).
  - Check [`__base.yaml`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/bases/__base.yaml?ref_type=heads) file to see if the service is enabled on zonal or regional clusters, or both.
  - Check the environment's specific file if there is a specific configuration for a cluster. E.g. [`kas` is disabled on `gstg-gitlab-36dv2`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/bases/gstg.yaml?ref_type=heads#L36):

    <details>
      <summary>KAS is disabled in <code>gitlab-com/bases/gstg.yaml</code></summary>

      ```yaml
      gstg-gitlab-36dv2:
        values:
          - !!merge <<: *regional_cluster_defaults
          - !!merge <<: *gstg_values
          - cluster: gitlab-36dv2
            region: us-east1
            gitlab_external_secrets:
              enabled: true
            gitlab:
              enabled: false
              sidekiq:
                enabled: false
              mailroom:
                enabled: false
              kas:
                enabled: false
      ```

    </details>
  Note down all clusters where the service is running. Later we will need to roll back on all clusters one by one.

- The following conditions MUST be met:
  - Service MUST be [stateless](rollback-single-service.md#stateless-services)
  - Service MUST have its own CNG (i.e. Docker) image
  - Service MUST be the root cause
  - Service MUST NOT be rolled back due to an issue introduced by another component or service

## Stateless Services

- GitLab Agent for Kubernetes (KAS)
- GitLab Shell
- GitLab Pages
- Mailroom
- GitLab Elasticsearch Indexer

## Important Considerations

Before proceeding with an individual service rollback:

1. First consider these faster/safer options:
   - Feature flag toggles
   - Rolling back the full auto-deploy package
   - Disabling Canary

    Read more in the [Incident Runbook.](./incident.md#remediation-timings)

1. Be aware that:
   - This procedure will result in the service running at a different version than other components
   - This procedure will only target the Main stage, thus Canary must be drained
   - Auto-deploy will remain blocked until an appropriate package is built
   - All clusters running the service must be rolled back for consistency
   - All configuration changes made to our Kubernetes clusters must be blocked

## Procedure

### Gather Information

1. Verify the incident enables a service rollback
   - Confirm with service team that rollback is appropriate
   - Document the current service version for reference

1. Validate deployments to the target Environment are completed, refer to the latest Auto-Deployment Pipeline

1. Get current Revision of the GitLab release

    ```bash
    helm status gitlab --namespace gitlab
    ```

    - The above must be run on each cluster the service is run - the revisions numbers will differ on every cluster

1. Validate the latest change is that of a deploy and not a configuration change

    ```bash
    helm diff revision gitlab <REVISION - 1> --context 3
    ```

    - Example, if in the above step we determined a cluster is running revision 4773, we would run `helm diff revision gitlab 4772 --context 3`
    - This diff should indicate only auto-deploy revision changes
    - If we see changes for the target service that are not invoked by Auto-Deploy (e.g. ConfigMap, Secret), this should be analyzed to determine appropriate next steps, this procedure may not be appropriate

1. Document the service's version information and collaborate with the service team to confirm that rolling back to the indicated version is a sufficient course of action
    - Find the current and the previous deployed auto-deploy tags and compare the `git diff`.
      - Use https://auto-deploy-view-e121e0.gitlab.io/ to help
      - If the service has its own repository (e.g. KAS, Gitaly) and the change in the GitLab Rails repository is only the `*_VERSION`
        file, compare the `git diff` between the SHAs mentioned in the two versions of the `*_VERSION` file.

### Rollback

#### Rollback Staging

Start the following steps with Staging environment to validate the effectiveness.

1. Notify the EOC

    Notify and wait for an approval from `@sre-oncall` in `#production` that a rollback is about to be started.
    Make sure they know that Canary will also be drained. Sample Slack message:

    ```plaintext
    @sre-oncall: We are going to rollback <Service-name> due to <Reason>. Canary will also be drained.
    ```

1. Lock Auto-Deploys to the target environment

    - Prevent deployments to the environment:

    ```plaintext
    /chatops run deploy lock <ENVIRONMENT>
    ```

    Consider running this against the Canary environment as well or as needed

1. Drain Canary

    ```plaintext
    /chatops run canary --disable <ENVIRONMENT>
    ```

1. Dry-run rolling back service on each cluster. Validate that only the rolling back service's image is changed:

    ```bash
    kubectl rollout undo --dry-run=server deployment <service-name> --namespace gitlab
    ```

1. Rollback service on each cluster:

    ```bash
    kubectl rollout undo deployment <service-name> --namespace gitlab
    ```

1. Verify rollback on each cluster:

    ```bash
    kubectl rollout status deployment <service-name> --namespace gitlab
    kubectl get deployment <service-name> --namespace gitlab -o template --template '{{ range .spec.template.spec.containers }}{{ .image }}{{ end }}'
    ```

1. Monitor service health after rollback

#### Rollback Production

Once we confirm the Rollback steps above fixes the issue on Staging, repeat all the [rollback steps](#rollback-staging) with Production.

## After Rollback

1. Notify QA since Canary is drained, any on-going or scheduled testing will fail
1. Follow-up with the development team for a proper fix. Auto-deploys will remain blocked until a fix has been provided by the stage team.
1. When the fix has been merged: follow up the regular auto-deploy process, and unlock environments as needed.
1. Once the fix has reached gprd-cny, undrain canary:

  ```text
  /chatops run canary --enable <ENVIRONMENT>
  ```

1. [Skip production promotion checks](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/gitlab-com-deployer.md#skipping-production-promotion-checks) to deploy the fix to Staging and Production.
1. Update incident documentation with actions taken
