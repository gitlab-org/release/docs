---
tags:
  - auto-deploy
---
# Deployment Failures

Failures happen for a wide array of reasons, this runbook will attempt to capture common deployment items and provide steps for resolution.

## Inventory Failures

Ansible leverages the GCP plugin and various tags to find a list of nodes for which to deploy too.  During scenarios where instances may be destroyed, Deployer may fail and continue to fail until the inventory cache has expired.  This could occur when a host is unreachable.  Ansible will fail the node it cannot reach.  An example of this error:

```text
 fatal: [gitaly-01a-gitlab-gitaly-gprd-f33d]: UNREACHABLE! => changed=false
  msg: |-
    Data could not be sent to remote host "10.221.24.230". Make sure this host can be reached over ssh: ssh: connect to host 10.221.24.230 port 22: Operation timed out
```

[Example Source](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/jobs/12704200)

We leverage the default period of time for this cache which is 1 hour.  To resolve errors related to this type of scenario, the best method is to clear the CI cache and retry the job.

[Clearing CI cache manually](https://docs.gitlab.com/ee/ci/caching/#clear-the-cache-manually)

This task would need to be performed in the project and instance where Deployer executes Ansible: https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines
