---
tags:
  - auto-deploy
---
# Resolving Sync Failures

## Overview

At the end of security patch releases, we run the following chatops command in Slack to sync `canonical` repo with commits on the respective `security` mirror repositories:

```text
/chatops run release sync_remotes --security
```

This chatops sync job can have push failures (and still pass). The error message will look like:

```text
F ReleaseTools::RemoteRepository -- Failed to push -- {:remote=>:security, :ref=>"master", :output=>"To gitlab.com:gitlab-org/security/gitlab.git\n ! [rejected]
```

Then when we run `/chatops run mirror status`, we will see the conflicts reflected on the output of that command.

Follow this runbook to resolve the sync failure.

## Process Overview

* Search the chatops job (`security:sync_remotes`) logs for creation of a sync MR. It will look like:

```text
2023-05-23 16:26:56.054742 I ReleaseTools::Security::SyncRemotesService -- Push failed, creating merge request instead -- {:project=>gitlab-org/security/gitlab, :target_branch=>"master"}

...

2023-05-23 16:34:16.047388 I ReleaseTools::Security::SyncRemotesService -- Created merge request -- {:project=>gitlab-org/security/gitlab, :target_branch=>"master", :merge_request_url=>"https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121570"}
```

* If the MR fails to be created, there will be a log line saying `Could not create merge request`. Look at error for hints. Usually this happens if `sync-canonical-with-security-changes` branch already existed. To fix this, manually delete the branch and retry the job.
* If/when the MR has been created:
    1. Temporarily deactivate the merge train schedule `gitlab-org/gitlab@master -> gitlab-org/security/gitlab@master` schedule job in <https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules> and `security:merge_train` schedule job in <https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules>.
    2. Review + merge the sync MR. If possible, we should merge immediately and not wait for the pipeline to complete.
    3. Reactivate the `security:merge_train` schedule job in <https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules>. (This will automatically reactivate the merge train schedule if required)
* The "Mirroring Repositories" section in the [canonical repo's repository settings](https://gitlab.com/gitlab-org/gitlab/-/settings/repository) should indicate that the last successful update was recent. If mirroring has not run recently, release managers can also hit the refresh button on the mirroring settings to force the mirroring to try again.
* `/chatops run mirror status` should also indicate that the mirroring has been fixed

NOTE: We temporarily deactivate the merge train schedule because if/when it runs after the MR has been created, it would add more commits to the security mirror, further causing mirroring to break.

If the mirror is still broken and conflicts are found, [manual intervention will be needed to sync the repositories](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/manually-sync-release-tag.md).
