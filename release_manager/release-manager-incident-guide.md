---
tags:
  - auto-deploy
---
## Release Manager incident guide

Release Managers will encounter incidents as they go through their deployment and release tasks. Involvement will come either at the request of the [EOC or IM](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities), or because Delivery have raised an incident to request help with deployments or releases.

This guide will explain what to expect and our responsibilities for the different types of incident.

Be sure to read through the full handbook page to understand the full [Incident Management](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management) process.

## MR needs to be deployed to fix incident

```mermaid
graph TB;
   id1{Is the MR for an S1 incident?}

   id2(The EOC can choose to perform a hot-patch <br/>if we need to deploy it extremely fast, in less than 8 hours)
   id1-- Yes -->id2

   id3(Note that a deployment will remove the hot patch,<br/> so we cannot deploy after the hot patch until the MR has been merged)
   id2-->id3

   id3_2{Is the MR associated with a security issue?}
   id3-->id3_2

   id3_3{Does AppSec want a critical security release?}
   id3_2-- Yes -->id3_3

   id3_4(Coordinate with AppSec to <br/>start a critical security release)
   id3_3-- Yes -->id3_4

   id4{Is the MR associated with a security issue?}
   id1-- No -->id4

   id5{Does it need a speedy deployment?}
   id4-- No -->id5

   id6(Follow the docs to speed up the auto-deploy process)
   id5-- Yes -->id6
   id3_2-- No -->id6

   id7(The MR will be automatically included <br/>in the next auto-deploy package once it is merged)
   id5-- No -->id7

   id10(Ask the MR author to follow the <br/>security developer workflow to include the MR in the next security release)
   id4-- Yes -->id10
   id3_3-- No -->id10
```

Notes:

1. These are recommendations to help guide release managers. However, release managers may sometimes have to
use their judgement in an unusual situation. Feel free to ask the team for help in making decisions.
You do not have to make them alone.
2. Do not merge a security MR into security master before the start of a security release.
This will cause canonical and security repositories to diverge, which will cause problems
when the security release starts.
3. Hot patches should only be done for S1 issues, and even then they should be used only when absolutely
necessary.

References

1. [Speed up auto-deploy process](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/how_to_speed_up_auto_deploy_process_for_urgent_merge_requests.md)
2. [Security developer workflow](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/developer.md)

## Release Manager support request from EOC or IM

During ongoing incidents the EOC or IM may request support from release managers using the @release-managers slack handle.

Release managers should treat this as a top priority request and join the Slack channel as well as the Zoom room if required.

Typically we will be asked to give details about

 1. Any ongoing or recently deployed changes, having a compare link between recent packages is helpful
 2. Suitability of the package to rollback. If suitable you may want to recommend this option for fast mitigation of software-change incidents

Remember that every incident issue has a comment on it to provide useful information and links to help answer these questions. Take a look at the [Useful information/tools to debug the incident](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/release-manager-incident-guide.md#useful-informationtools-to-debug-the-incident) for more details.

You may need to take action to prevent further deploys or help to get a mitigation deployed, this would either be a rollback, revert, fix, or hotpatch. Which one will depend on the specific incident. Discuss with the EOC and ask for support from Delivery if you're not sure how to proceed.

During these incidents please add comments to the incident issue and help the EOC complete the incident summary, timeline, labelling, and identification of corrective actions following the incident.

If the incident blocked deployments please add the appropriate ["deploys-blocked..." labels.](#tracking-deployment-blockers)

## Release Manager requesting support

During times when we cannot deploy we're vulnerable because in the event of a high-severity problem we would be unable to quickly deploy a fix. Treat all blockers as high-risk and move fast to unblock.

We have two ways to record and track deployment-failures - lightweight, and incidents. [Follow these steps to decide which is right for this failure](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/failures.md).

If a deployment or release failure needs investigation or action from dev-escalation or the EOC, the Release Manager should [declare an incident with the correct availability severity](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability). For incidents that don't have customer-impact, for example, Staging failures or patch release failures, include the "backstage" label to assist the EOC with prioritization. Including one of the [Delivery Impact](https://about.gitlab.com/handbook/engineering/releases/#delivery-impact-labels) labels will help make the impact of the incident visible.

For example, a staging deployment failure caused by an environment problem would be a Severity 3 incident, it would have the "backstage" label, and a "Delivery Impact::1" label.

Follow the instructions to [raise a new incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident)

Once the incident is created:

1. You are the Owner for this incident. Be sure to keep people informed and be pro-active in working towards a resolution
1. Fill in as much information as you have on the incident issue. Assign to yourself
1. Join the incident Slack and consider joining the Zoom bridge
1. Engage with the [EOC](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#when-to-contact-the-current-eoc), [Dev escalation](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#bot-pagerslack-usage), or [Quality On Call](https://about.gitlab.com/handbook/engineering/quality/guidelines/#schedule) as needed to resolve the issue
1. Keep the incident issue updated with the latest decisions and information uncovered

## Once the incident has been resolved

Note: An incident is resolved when the fix that address the root cause is properly deployed to `gprd`. If the situation is unblocked via other means (e.g.: a rollback, or marking a migration as done manually) to allow deployments to resume, but the fix addressing the root cause of the incident hasn't been deployed to `gprd`, then the incident is only mitigated.

1. Set the incident issue labels to "resolved" once the issue is resolved (when the fix is properly deployed to `gprd`)
1. Add the appropriate labels for [root cause](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#root-cause-labeling) and service
1. Work with engineers to agree on corrective actions following this incident
1. Make sure the summary and timeline sections of the description are fully completed
1. Add tracking for the length of delay we experienced by adding appropriate ["deploys-blocked..." labels.](#tracking-deployment-blockers)

## Tracking deployment blockers

To help track time and patterns in deployment blockers any incident or Change Request(CR) that blocks a deployment should have appropriate "Deploys-blocked-gstg-X" and "Deploys-blocked-gprd-X" labels added. See [this incident](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5712) for an example. Deployment delays are recorded and analyzed on <https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/659>

## Useful information/tools to debug the incident

1. Check the diff of the latest deployment to the environment.

   The `/chatops run auto_deploy status` command is useful for this.

   The Gitlab Release Tools Bot puts a comment on the incident issue listing
   the current and previous package on production, along with the diff. This can
   be used for incidents affecting production.

   Also post the diff to the incident issue and incident Slack channel. Someone else might
   see something that you missed.

1. Check if any feature flags have been turned on or turned off.

   The incident issue description has a link called `Feature Flag Changes` which links to logs
   showing feature flag changes in the last few hours.

   Every feature flag change creates an issue in
   <https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/-/issues?scope=all&sort=created_date&state=all>,
   so that is also a good place to check. You can filter by labels to check for changes in
   a particular environment only.
