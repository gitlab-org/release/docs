---
tags:
  - release-management
---
# Patch release pressure

The patch release pressure is the number of unreleased
merge requests merged into the current stable branch.

## Reading the patch release pressure

To determine the number of fixes waiting to be patched:

1. Open up the [release manager dashboard].
1. Take a look at the "Patch release pressure: S1/S2" panel.
1. Search for the information associated with the current version.

| Release Manager Dashboard |
| ------ |
| ![Patch release pressure](images/release_manager_dashboard_example.png) |

## Release manager dashboard

Patch release pressure information can be found in the [release manager dashboard]
in two panels:

* **S1/S2 pressure**: Represents the number of S1/S2 merge requests merged into stable branches
  that haven't been released.
* **Total pressure**: Represents the number of merge requests merged into stable branches that
  haven't been released regardless of severity. This number is inclusive of the `S1/S2` pressure.

| Release Manager Dashboard |
| ------ |
| ![Patch release pressure](images/patch_release_pressure.png) |

[Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[release manager dashboard]: https://dashboards.gitlab.net/d/delivery-release_management/delivery-release-management?orgId=1&refresh=5m

---

[Return to README](readme.md)
