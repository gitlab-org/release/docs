---
tags:
  - release-management
---
## Far-reaching impact fixes or breaking change fixes

If the fix for the security vulnerability you are working on has a
[far-reaching impact](https://handbook.gitlab.com/handbook/engineering/core-development/#reducing-the-impact-of-far-reaching-work)
or is a [breaking change](https://about.gitlab.com/handbook/product/gitlab-the-product/#breaking-changes-deprecations-and-removing-features), and is not categorized as critical or high severity (S1 / S2), for valid good will use cases,
[contact the AppSec team](developer.md#contact-the-appsec-team) and determine if we can reclassify the security issue as a feature change.
If reclassified, this means the fix can be made in the regular `gitlab-org/gitlab` repository instead of through the security process and allow a
[controlled roll out](https://about.gitlab.com/handbook/engineering/development/processes/rollout-plans/). For high and critical severity security issues, [we must always follow the security release process](https://docs.gitlab.com/ee/policy/maintenance.html#security-releases).

If the fix for security vulnerability you are working can't be reclassified as a feature change, then impact estimation must be performed and a mitigation plan must be developed prior to MR merge.

### Estimating impact

Estimating impact varies case by case. Here are some techniques which should help you to get insights on possible impact of your fix:

1. Number of API\page hits. Kibana logs is a good place to query that data.
2. Number of users\groups\projects affected. #database-lab Slack channel is a good place to query that data.
3. List groups with number of seats can also give insights on possible impact.

In cases where impact can't be estimated with existing metrics additional logging\profiling must be introduced to help with estimation.

If significant good will usage (high usage and/or marquee customers) is found, a mitigation plan must be developed.

### Mitigation plan

If the fix requires a mitigation plan, these next steps should be performed:

1. Create a slack channel #mitigation-plan-GITLAB_ISSUE_ID with AppSec engineer, [Release managers](https://about.gitlab.com/community/release-managers/) and a [support manager](https://gitlab-com.gitlab.io/support/team/oncall.html)
2. Together investigate usage details and possible migrations for affected customers. Invite appropriate Technical Account Managers when needed.
3. If the mitigation plan requires new functionality, the security fix can be delayed until an alternative is implemented.
4. If migration to alternatives is possible, TAMs must confirm that their marquee users have migrated to the suggested alternative before the fix can be merged.
5. Support Manager should post mitigation plan summary to [security implementation issue](#security-implementation-issue) with suggested alternative details and the list of respective DRIs for contact.
6. Slack channel #mitigation-plan-GITLAB_ISSUE_ID can be archived 7 days after feature release.
