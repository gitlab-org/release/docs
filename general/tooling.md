---
tags:
  - auto-deploy
---
## Release Process Tooling
![image](release-tooling.png)

Source: [release-tooling.dot](release-tooling.dot)


This diagram is a view into the tooling required for the Release Manager
to perform releases and deploys.  Each highlighted container represents the tool being utilized.
Each colored box represents the method being called or an output item from our
tooling. The border of the box represents where that functionality is executed
upon, while the infill color represents the type of resultant output.
Each arrow represents the connection that each tool has with another to reach
our end goal of a build artifact or perform a deploy.

**NOTE**

Please follow our existing documentation regarding the steps in which a
Release Manager takes throughout the process of releasing a new version of our
product.  This can be found here: [general/monthly.md](../monthly/process.md)

### Tooling
* [chat-ops](https://gitlab.com/gitlab-com/chatops/tree/master) - All commands
  initiated by a Release Manager are done via chat-ops
  * Commands are executed in the appropriate channel for the purposes of
    auditing and providing visibility to the team
* [release-tools](https://gitlab.com/gitlab-org/release-tools) - All commands
  executed via chat-ops call upon the tooling inside of `release-tools`
  * `chat-ops` is simply a proxy to executing these via CI jobs inside of the
    GitLab product
* [release-tasks](https://gitlab.com/gitlab-org/release/tasks/issues) -
  issue container for administrative issues related releases (Not issues related
  to the tools)
* [gitlab-ee](https://gitlab.com/gitlab-org/gitlab-ee) - Project location for
  release tasks and merge requests specific to `gitlab-ee`
* [gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce) - Project location for
  release tasks and merge requests specific to `gitlab-ce`
* [omnibus-gitlab](https://dev.gitlab.org/gitlab/omnibus-gitlab) - Project
  location for building all of GitLab shippable artifacts
* [deployer](https://gitlab.com/gitlab-com/gl-infra/deployer) - Project
  dedicated to deploying GitLab.com and neighboring environments
* [dashboard](../release_manager/dashboard.md) - Provides an at-a-glance
  overview of important metrics for our release process
