---
tags:
  - auto-deploy
---
# How to grant permissions for repo sync push

This guide shows the steps an EM needs to go through to grant push permissions if needed to complete the security release sync.

**These steps must only be used as part of an approved access request**

## Context

One of the final steps for completing a security release involves syncing all changes back into the canonical repo. We have tools to do this in a compliant way but these can fail, for example if there is a lot of other activity taking place on the Canonical default branch. As a final resort, Delivery Engineering Managers we can grant direct push permissions to allow a release manager to push the changes directly to the default branch to complete the release.

Prior to August 2023 release managers were automatically granted these permissions. To meet compliance requirements these permissions were removed and this process was created to allow permissions to be granted if needed.

## Steps to add and remove permissions

1. Make sure a valid access request has been created with the "[Release Manager Elevated Permissons](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/merge_requests/1413)" template. You will need to approve and help record evidence to support Compliance needs.
Steps to change permissions for the GitLab-org/gitlab canonical repo
1. Log in with your GitLab Admin account.
1. Go to the GitLab-org branch settings: https://gitlab.com/gitlab-org/gitlab/-/settings/repository/branch_rules?branch=master and click the `Manage in protected branches` link on the right.
1. Scroll down the Protect branch view until you reach the table showing current rules.

    ![Protected branch rules](./images/default-branch-rules.png)

1. Select the `Allowed to push and merge` dropdown for the Master branch and add the requester (requester = the person who opened the access request issue).
1. Make sure to complete all evidence requests and remove the user from these permissions before closing the access request.

## Steps to add and remove permissions on Gitaly

If push permissions are required for Gitaly syncing, the Release Manager(s) should open an access request with the "[Release Manager Elevated Permissons](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/merge_requests/1413)" template.

Follow these steps to add and remove the permissions:

1. Log in with your GitLab Admin account.
1. Go to the GitLab-org branch settings: <https://gitlab.com/gitlab-org/gitaly/-/settings/repository/branch_rules?branch=master> and click the `Manage in protected branches` link on the right.
1. Select the `Allowed to push and merge` dropdown for the Master branch and add the requester (requester = the person who opened the access request issue).
1. Make sure to complete all evidence requests and remove the user from these permissions before closing the access request.