
# Rebuild Assets Image

## Overview

Many jobs in the CNG and Omnibus pipelines requires a Docker image called `gitlab-assets-ee`. Missing of this required image leads to errors similar to:

```text
Error response from daemon: manifest for dev.gitlab.org:5005/gitlab/gitlab-ee/gitlab-assets-ee:17-5-stable not found: manifest unknown: manifest unknown
```

Normally, this image is built automatically whenever a frontend change is merged into the Rails code base ([CI pipeline configuration](https://gitlab.com/gitlab-org/gitlab/-/blob/094d058fc5bcb245a7d70cca1b9bfb25c7854b05/.gitlab/ci/rules.gitlab-ci.yml?page=2#L1102-1114)). When it is not found, normally it means an old image is required (e.g. for an old stable branch), and the image was already cleaned up by the retention policy. It can be fixed by rebuilding the assets image.

## Step-by-step

* Locate the repository where the `gitlab-assets-ee` is placed. E.g. The image `dev.gitlab.org:5005/gitlab/gitlab-ee/gitlab-assets-ee:17-5-stable` is in <https://dev.gitlab.org/gitlab/gitlab-ee>.
* Run a new pipeline:
  * On the required branch. E.g. `gitlab-assets-ee:17-5-stable` should be built on the branch `17-5-stable`
  * Set the following variables:
    * ENABLE_BUILD_ASSETS_IMAGE=true
    * ENABLE_COMPILE_PRODUCTION_ASSETS=true
