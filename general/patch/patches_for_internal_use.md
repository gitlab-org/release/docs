# Internal-Use Patch Releases

> [!note]
> This process will not be used once the [Internal release process] is ready.

This is a patch release made outside of the patch release cadence to mitigate a bug specifically for
GitLab Dedicated customers. These ad-hoc patches are usually the result of discovering high-severity bugs or vulnerabilities which
have been fixed in GitLab.com through auto-deploy and will be fixed for self-managed customers
through our planned patch releases. An expedited release may be prepared for GitLab Dedicated
customers in a limited number of cases.

The main difference from the other [types of patch releases] is that the version and tags will not
be published to the canonical codebases of GitLab.

Information associated with these internal patches (such as tags, changelogs, entries on the [version server], etc.)
must not be released publicly during the release process. So, the tagging step **MUST** include the `--security` flag:

``` shell
/chatops run release tag 17.8.5 --security
```

> [!warning]
> A manual update to the patch release GitLab issue will be required to append the `--security`
> flag.

These versions should be skipped (publicly) and the changelog of this version should be included in
the next planned or unplanned patch release which is meant for publication.

[version server]: https://version.gitlab.com
[Internal release process]: general/internal-releases/readme.md
[types of patch releases]: general/patch/readme.md
