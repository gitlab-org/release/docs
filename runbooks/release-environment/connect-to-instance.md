---
tags:
  - auto-deploy
---
# Connect to a release environment instance

When a deployment to a release environment fails, as other deployments to Kubernetes, there can be many reasons. This runbook focus on showing how to connect to its infrastructure.

## Requirements

- An SRE with access will be required to access the release environments.
- [K8S On-call Setup] is done. It should be a part of the SRE's Onboarding.

## Connect to Kubernetes infrastructure

> **NOTE**: The new environment is only deployed to the cluster after the first pipeline ran on the stable branch in [gitlab-org/gitlab].

- Connect to the `release` cluster:

  ```bash
  glsh kube use-cluster release
  ```

- A release environment is deployed in a namespace named `gitlab-<stable-branch>`, e.g. `gitlab-16-10-stable`.
- Check if the Helm releases are available on the namespace.

  ```bash
  helm ls -n gitlab-<stable-branch>
  ```

  Example:

  ```bash
  helm ls -n gitlab-16-10-stable
  NAME                                            NAMESPACE               REVISION        UPDATED                                      STATUS          CHART                           APP VERSION
  gitlab-16-10-stable                             gitlab-16-10-stable     1               2024-04-09 21:01:48.664198897 +0000 UTC      deployed        gitlab-7.6.2-1113082919         master
  gitlab-16-10-stable-prafect-db-automation       gitlab-16-10-stable     1               2024-04-09 21:01:48.257764793 +0000 UTC      deployed        praefect-db-creation-0.1.0      1.0.0
  gitlab-16-10-stable-secrets                     gitlab-16-10-stable     1               2024-04-09 21:01:47.82651866 +0000 UTC       deployed        vault-secrets-1.2.13
  ```

## Connect to the Web UI

The URL of a GitLab installation is at <https://gitlab.(branch-name).release.gke.gitlab.net>. The initial password can be found in the K8S Secret.

[K8S On-call Setup]: <https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md>
