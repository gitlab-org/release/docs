---
tags:
  - auto-deploy
---
# Deploy an old stable branch

As a result after the [evaluation of the requirements to deploy release environment to old stable branches](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20119), it is known that images in registries of [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab/-/settings/packages_and_registries) and [CNG-mirror](https://gitlab.com/gitlab-org/build/CNG-mirror) have retention policies, so they are cleaned up after some time. Thus, Release Environments fails to use the images from these registries, if the deployment happens after the images' retention period.

We also want to [automatically detect and fix this issue](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20215), but until then, this runbook shows the step-by-step to rebuild the deleted images:

- Go to the main GitLab pipeline triggered by new commits to the stable branch ([e.g.](https://gitlab.com/gitlab-org/gitlab/-/pipelines/1203703651)).
  - Rerun the `build-assets-image` job ([e.g.](https://gitlab.com/gitlab-org/gitlab/-/jobs/6825977137)) in the main GitLab pipeline.
- Go to the Release Environment pipeline by choosing the triggered job `start-release-environments-pipeline` in the main GitLab pipeline.
  - Rerun the `release-environments-build-cng` job ([e.g.](https://gitlab.com/gitlab-org/build/CNG-mirror/-/pipelines/1286106970))
- Rerun the `trigger-deploy` job in the `release-environments` repository.

> **Note:** If a Release Environments pipeline has been automatically created by a new commit to an old stable branch, this runbook is not needed since the images will be newly built on that pipeline.
