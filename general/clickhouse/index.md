---
tags:
- auto-deploy
---
# ClickHouse

Clickhouse is an open source column oriented database.
GitLab SaaS uses the managed service provided by [ClickHouse Inc](https://clickhouse.com).

## Migrations

Clickhouse migrations are executed as part of the same [Ansible playbook](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/blob/master/deploy_gitlab.yml) that executes Postgres migrations.

Clickhouse migrations are executed during the canary deployment in staging and production, in the `<env>-cny-migrations` CI job.

### Similarities to Postgres migrations

- Clickhouse migrations should take the same amount of time as [Postgres migrations](https://docs.gitlab.com/ee/development/migration_style_guide.html#how-long-a-migration-should-take).
- Clickhouse migrations will be backwards compatible.

### Differences with Postgres migrations

- There is no mechanism for background migrations as of now.
- There is no testing infrastructure for Clickhouse.
For Postgres, we have anonymized and cloned databases that developers use for testing queries and migrations.

## Post deploy migrations

There are no Clickhouse post-deploy migrations currently.
They are planned to be [added soon](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/merge_requests/484#note_189905).
Once added, the following guidelines will apply.

- Clickhouse post-deployment migrations do not have to be executed immediately after a deployment.
They can be executed at the Release Manager's discretion, along with Postgres post-deploy migrations.
- Clickhouse post-deploy migrations are not guaranteed to be backwards compatible (similar to Postgres post-deploy migrations).

## Troubleshooting

### Migration failure

In case of a failure, follow the guide for [Handling Deployment failures](../deploy/failures.md).

For any help contact:
- `#f_clickhouse` channel in Slack.
- `@gitlab-org/maintainers/clickhouse` group in issues.

## References

- [Infrastructure runbook for Clickhouse](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/clickhouse/README.md)
